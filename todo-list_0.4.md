# List of packages for Hyperbola 0.4

Further explanations: The **unwanted dependencies** are defined throughout the packages below being explicitely not functional without the following:

- ConsoleKit
- PolicyKit
- PAM
- D-Bus
- systemd / elogind
- PulseAudio
- Avahi

If the corresponding package / application has one or more absolutely dependency towards the above named it cannot be used. Furthermore **Hyperbola GNU/Linux-libre** won't use languages like Rust, Java and Mono within the upcoming release 0.4.

Please note also the packaging-rules of the Hyperbola-project: https://wiki.hyperbola.info/doku.php?id=en:main:packaging_guidelines

<br/>

| Name | Description | No unwanted requirements? | Can be included? | Upstream Bug Reports? | Annotations | Responsible | Tracking Issue |
--- | --- | :---: | :---: | :---: | --- | :---: | :---:
| i3 | dynamic wm | Y | Y | NA | Done | rachad |   |
| dwm | dynamic wm | Y | Y | NA | Done | throgh |   |
| spectrwm | dynamic wm | Y | Y | NA | Done | throgh |   |
| stumpwm | dynamic wm | Y | Y | NA | Done | throgh |   |
| ratpoison | dynamic wm | Y | Y | NA | Done | throgh  |   |
| icewm | stacking wm | Y | Y | NA | Done | throgh |   |
| jwm | stacking wm | Y | Y | NA | Done | throgh |   |
| blackbox | stacking wm | Y | Y | NA | Done | throgh |   |
| fluxbox | stacking wm | Y | Y | NA | Done | sagaracharya |   |
| openbox | stacking wm | Y | Y | NA | Done | throgh |   |
| windowmaker | stacking wm | Y | Y | NA | Done | throgh |   |
| cwm | stacking wm | Y | Y | NA | Done | rachad |   |
| fvwm3 | stacking wm | Y | Y | NA | Done | rachad |   |
| ctwm | stacking wm | Y | Y | NA | Done | throgh |   |
| twm | stacking wm | Y | Y | NA | Done |   |   |
| 9wm | stacking wm | Y | Y | NA | Done | rachad |   |
| vtwm | stacking wm | Y | Y | NA | Done | HarvettFox96 |   |
| sawfish | stacking wm | Y | Y | NA | Done | rachad |   |
| enlightenment (e16) | stacking wm | Y | Y | NA | Done | throgh |   |
| notion | tiling wm | Y | Y | NA | Done | throgh |   |
| bspwm | tiling wm | Y | Y | NA | Done | biovoid |   |
| awesomewm | tiling wm | Y | Y | NA | Done | throgh |   |
| xdm | login manager | Y | Y | NA | Done |   |   |
| console-tdm | login manager | Y | Y | NA | Done | throgh |   |
| lumina | desktop environment | Y | Y | NA | Done | Emulatorman / coadde |   |
| picom | compositor | Y | Y | NA | Done | throgh |   |
| conky | compositor | Y | Y | NA | Done | saravia |   |
| xscreensaver | screensaver | Y | Y | NA | Done | sagaracharya |   |
| htop | system monitor (terminal) | Y | Y | NA | Done | throgh |   |
| spacefm | file-manager | Y | Y | NA | Done | throgh  |   |
| gnu mc (midnight commander) | file-manager | Y | Y | NA | Done | throgh |   |
| xfe | file-manager | Y | Y | NA | Done | throgh |   |
| ranger | file-manager | Y | Y | NA | Done | biovoid |   |
| xarchiver | archive manager | Y | Y | NA | Done | rachad |   |
| unarchiver (unar) | archive manager | Y | Y | NA | Done | throgh |   |
| weechat | irc client | Y | Y | NA | Done | rachad |   |
| hexchat | irc client | Y | Y | NA | Done | throgh |   |
| mcabber | xmpp client | Y | Y | NA | Done | throgh |   |
| toxic | tox client | Y | Y | NA | Done | rachad |   |
| utox | tox client | Y | Y | NA | Done | rachad |   |
| qtox | tox client | Y | Y | NA | Done | rachad |   |
| nitrogen | background setter | Y | Y | NA | Done | throgh |   |
| feh | image viewer | Y | Y | NA | Done | rachad |   |
| qiv | image viewer | Y | Y | NA | Done | throgh |   |
| pqiv | image viewer | Y | Y | NA | Done | throgh |   |
| imv | image viewer | Y | Y | NA | Done | throgh |   |
| geeqie | image viewer | Y | Y | NA | Done | throgh |   |
| mirage | image viewer | Y | Y | NA | Done | throgh |   |
| scrot | utilities (screenshot) | Y | Y | NA | Done | throgh |   |
| tint2 | taskbar | Y | Y | NA | Done | HarvettFox96 / throgh |   |
| alltray / trayer | system tray | Y | Y | NA | Done | rachad |   |
| cbatticon | system tray | Y | Y | NA | Done  | throgh |   |
| volumeicon | system tray | Y | Y | NA | Done | throgh |   |
| cmus | audio player (console) | Y | Y | NA | Done | throgh |   |
| mpv | media player | Y | Y | NA | Done | throgh |   |
| simplescreenrecorder | desktop screen recorder | Y | Y | NA | Done | throgh |   |
| icedove-uxp | mail client | Y | Y | NA | Done | g4jc |   |   |
| iceape-uxp | mail and web client | Y | Y | NA | Done | g4jc |   |   |
| iceweasel-uxp | web client | Y | Y | NA | Done | g4jc |   |   |
| midori | web client | Y | Y | NA | Done | rachad |   |
| rawtherape | post-production tool for photography | Y | Y | NA | Done | throgh |   |
| gimp | raster graphics editor | Y | Y | NA | Done | coadde |   |
| mypaint | raster graphics editor | Y | Y | NA | Done | Coadde |   |
| grafx2 | raster graphics editor | Y | Y | NA | Done | throgh |   |
| tuxpaint | graphics editor (for kids) | Y | Y | NA | Done | throgh |   |
| gpick | colour picker | Y | Y | NA | Done | rachad |   |
| opentoonz | raster animation editor | Y | Y | NA | Done | throgh |   |
| pencil2d | raster animation editor | Y | Y | NA | Done | throgh |   |
| inkscape | vector graphics editor | Y | Y | NA | Done | throgh |   |
| xfig | vector graphics editor | Y | Y | NA | Done | throgh |   |
| synfig (and synfigstudio) | vector animation editor | Y | Y | NA | Done | throgh |   |
| shotcut | post-production tools for video | Y | Y | NA | Done | throgh |   |
| avidemux | basic video editor | Y | Y | NA | Done | throgh |   |
| gigamesh | 3d mesh tools | Y | Y | NA | Done | throgh |   |
| yafaray | 3d renderer | Y | Y | NA | Done | throgh |   |
| povray | 3d renderer | Y | Y | NA | Done | throgh |   |
| hydrogen | drum machine (audio) | Y | Y | NA | Done | throgh |   |
| audacity | audio editor | Y | Y | NA | Done | throgh |   |
| lmms | digital audio workstation | Y | Y | NA | Done | HarvettFox96 / rachad |   |
| muse | digital audio workstation | Y | Y | NA | Done | HarvettFox96 |   |
| qtractor | digital audio workstation | Y | Y | NA | Done | throgh |   |
| ardour | digital audio workstation | Y | Y | NA | Done | throgh |   |
| l3afpad | text editor | Y | Y | NA | Done  | throgh |   |
| xedit | text editor | Y | Y | NA | Done |   |   |
| nano | text editor (console) | Y | Y | NA | Done  |   |   |
| vim | text editor (console) | Y| Y | NA | Done | Emulatorman |   |
| libreoffice | office suite | Y | Y | NA | Done | HarvettFox96 / throgh |   |
| abiword | word processor | Y | Y | NA | Done | throgh |   |
| mupdf | viewer | Y | Y | NA | Done |   |   |
| gnu grub | bootloader | Y | Y | NA | Done | Emulatorman |   |
| syslinux | bootloader | Y (for Hyperbola GNU/Linux-libre); N (for HyperbolaBSD) | Y | NA | Done | Emulatorman |   |
| handbrake | video transcoder | Y | Y | NA | Done | throgh |   |
| gnumeric | spreadsheet program | Y | Y | NA | Done | throgh |   |
| supertuxkart | game (racing) | Y | Y | NA | Done | throgh |   |
| supertux | game (platform) | Y | Y | NA | Done | throgh |   |
| megaglest | game (strategy) | Y | Y | NA | Done | throgh |   |
| warzone2100 | game (strategy) | Y | Y | NA | Done | throgh |   |
| mednafen | game (emulator, multi-system) | Y | Y | NA | Done | throgh |   |
| stella | game (emulator, atari) | Y | Y | NA | Done | throgh |   |
| wesnoth | game (strategy) | Y | Y | NA | Done | throgh |   |
| dosbox | game (emulator, dos) | Y | Y | NA | Done | throgh |   |
| scummvm | game (virtual machine, multi) | Y | Y | NA | Done | throgh |   |
| neverball | game (logic) | Y | Y | NA | Done | throgh |   |
| minetest | game (open-world) | Y | Y | NA | Done | HarvettFox96 / throgh |   |
| dolphin-emu | game (emulator, Gamecube / Wii) | Y | Y | NA | Done | throgh |   |
| qemu | virtual-machine, emulator | Y | Y | NA | Done |   |   |
| virt-manager | GUI for qemu | Y | Y | NA | Done | HarvettFox96 / throgh |   |
| vlc | media player | Y | Y | NA | Done | throgh |   |
| smplayer | media player | Y | Y | NA | Done | throgh |   |
| deadbeef | audio player | Y | Y | NA | Done | throgh |   |
| audacious | audio player | Y | Y | NA | Done | throgh |   |
| easytag | audio editor for tags | Y | Y | NA | Done | throgh |   |
| lxrandr, xrandr | display settings | Y | Y | NA | Done | throgh |   |
| light | brightness control | Y | Y | NA | Done | throgh |   |
| keepassxc | password manager | Y | Y | NA | Done | rachad |   |
| wine-stable | compatibility layer for running Windows programs | Y | Y | NA | Done | throgh |   |
| cabextract | extract compressed files (CAB) | Y | Y | NA | Done | throgh |   |
| icoutils | graphics conversion | Y | Y | NA | Done | throgh |   |
| graphicsmagick | graphics library | Y | Y | NA | Done |   |   |
| jq | commandline json parser | Y | Y | NA | Done | throgh |   |
| wxpython | wxWidgets GUI toolkit for Python | Y | Y | NA | Done | throgh |   |
| xterm | terminal emulator | Y | Y | NA | Done |   |   |
| p7zip | extract compressed files (7zip) | Y | Y | NA | Done | throgh |   |
| transmission-gtk | torrent client | Y | Y | NA | Done | throgh |   |
| urxvt-perls | url for rxvt | Y | Y | NA | Done | throgh |   |
| sc | spreadsheet program (console) | Y | Y | NA | Done | throgh |   |
| librecad | 2d CAD drawing | Y | Y | NA | Done | throgh |   |
| moc | ncurses console audio player | Y | Y | NA | Done | throgh |   |
| calcurse | text-based personal organized | Y | Y | NA | Done | throgh |   |
| cadaver | webdav client | Y | Y | NA | Done | throgh |   |
| terminus-font | font | Y | Y | NA | Done | throgh |   |
| radis-font | font | Y | Y | NA | Done | throgh |   |
| cups | printer management | Y | Y | NA | Done |   |   |
| dovecot | IMAP and POP3 server | Y | Y | NA | Done | throgh |   |
| postfix | secure mail server | Y | Y | NA | Done | heckyel |   |
| opensmtpd | secure mail server | Y | Y | NA | Done | throgh |   |
| vsftpd | secure ftp daemon | Y | Y | NA | Done | throgh |   |
| radicale | calendar and contact server | Y | Y | NA | Done | throgh |   |
| getmail6 | pop3 mail retriever | Y | Y | NA | Done | throgh |   |
| fetchmail | remote-mail retrieval utility | Y | Y | NA | Done | throgh |   |
| abook | text-based addressbook | Y | Y | NA | Done | throgh |   |
| xcalc | accessories, calculator | Y | Y | NA | Done |   |   |
| xeyes | accessories | Y | Y | NA | Done |   |   |
| mesa-demos | accessories | Y | Y | NA | Done |   |   |
| meld | system tools | Y | Y | NA | Done | throgh |   |
| grsync | system tools | Y | Y | NA | Done | throgh |   |
| fdupes | system tools (console) | Y | Y | NA | Done | throgh |   |
| rmlint | system tools (console) | Y | Y | NA | Done | throgh |   |
| php | A general-purpose scripting language that is especially suited to web development | Y | Y | NA | Done | throgh |   |
| xtrlock | minimal X display lock program | Y | Y | NA | Done | throgh |   |
| profanity | console based XMPP client | Y | Y | NA | Done | heckyel |   |
| pidgin | GTK based, full featured XMPP client | Y | Y | NA | Done | throgh |   |
| prosody | XMPP server | Y | Y | NA | Done | throgh |   |
| dhcpcd-ui | dhcpcd Monitor in GTK+ | Y | Y | NA | Done | throgh |   |
| boost | free peer-reviewed portable C++ source libraries | Y | Y | NA | Done |   |   |
| widelands | game (strategy) | Y | Y | NA | Done | throgh |   |
| ppp | core utility for dial-up | Y | Y | NA | Done |   |   |
| gnupg-stable | PGP implentation | Y | Y | NA | Done |   |   |
| ledger | accounting software | Y | Y | NA | Done | throgh |   |
| emacs | progamming | Y | Y | NA | Done | heckyel |   |
| emacs-exwm | window-manager with emacs | Y | Y | NA | Done | throgh |   |
| mumble | Chat client (Audio) | Y | Y | NA | Done | rachad |   |
| lighttpd | web server | Y | Y | NA | Done | throgh |   |
| nginx | web server | Y | Y | NA | Done | heckyel |   |
| galculator | acessories, calculator | Y | Y | NA | Done | throgh |   |
| dino | messenger for XMPP | Y | Y | NA | Done | throgh |   |
| haveged | entropy generator | Y | Y | NA | Done | throgh |   |
| rox | file manager which can optionally manage the desktop background and panels | Y | Y | NA | Done | throgh |   |
| sl | Steam Locomotive runs across your terminal when you type "sl" as you meant to type "ls". | Y | Y | NA | Done | throgh |   |
| cmatrix | A curses-based scrolling 'Matrix'-like screen | Y | Y | NA | Done | throgh |   |
| asciiquarium | An aquarium/sea animation in ASCII art | Y | Y | NA | Done | throgh |   |
| libcaca | Color AsCii Art library | Y | Y | NA | Done |   |   |
| simpleburn | CD-burn tool | Y | Y | NA | Done | throgh |   |
| krita | raster graphics editor | Y | Y | NA | Done | Coadde |   |

<br/>

---

**[List of packages for Hyperbola 0.4][SOURCE]**
 © 2022 by **[Tobias Dausend][THROUGH]** is licensed under
 **[Creative Commons&reg; Attribution-ShareAlike 4.0 International][LICENSE]**


[SOURCE]: https://git.hyperbola.info:50100/~team/documentation/todo.git/tree/todo-list_0.4.md
[LICENSE]: https://creativecommons.org/licenses/by-sa/4.0/

[THROUGH]: https://www.hyperbola.info/members/developers/#throgh
