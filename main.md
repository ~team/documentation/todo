* [Todo-list to contact authors for unlicensed files (OpenBSD Kernel)](openbsd_kernel-list-of-unlicensed-files.md)
* [Todo-list to contact authors for files with license issues (OpenBSD Kernel)](openbsd_kernel-file-list-with-license-issues.md)
* [Todo-list for HyperBK (HyperbolaBSD Kernel)](hyperbk-file-list.md)
* [Todo-list for HyperBLibC (HyperbolaBSD C Library)](hyperblibc-file-list.md)
* [Todo-list for HyperBLibC Extra (HyperbolaBSD C Library Extra)](hyperblibc-extra-file-list.md)
* [Todo-list for HyperBLibC Headers (HyperbolaBSD C Library Headers)](hyperblibc-headers-file-list.md)
* [Todo-list for version 0.4](todo-list_0.4.md)
* [Packages not included](packages_not-included.md)

---

## main.md - Main file for Todo list

Written in 2021-2022 by **[M&aacute;rcio Silva][COADDE]** <coadde@hyperbola.info>
<br/>Written in 2023 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.info>
<br/>Written in 2023 by **[rachad][RACHAD]** <rachad@hyperbola.info>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[COADDE]: https://www.hyperbola.info/members/founders/#coadde
[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
[RACHAD]: https://www.hyperbola.info/members/developers/#rachad
