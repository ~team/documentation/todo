File name | File type | License(s) | Description(s) / Status | More
---|---|---|---|---
`libexec/ld.so/aarch64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/aarch64/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/aarch64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 2 commented lines; 12 total lines | .
`libexec/ld.so/aarch64/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/aarch64/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/aarch64/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 69 total lines | .
`libexec/ld.so/aarch64/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/alpha/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/alpha/SYS.h` | C header | Simplified-BSD + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`libexec/ld.so/alpha/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`libexec/ld.so/alpha/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/alpha/ldasm.S` | Assembler code | Simplified-BSD + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`libexec/ld.so/alpha/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/amd64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/amd64/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/amd64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 10 total lines | .
`libexec/ld.so/amd64/rtld_machine.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`libexec/ld.so/amd64/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/amd64/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 69 total lines | .
`libexec/ld.so/amd64/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/arm/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/arm/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/arm/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 2 commented lines; 14 total lines | .
`libexec/ld.so/arm/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/arm/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/arm/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 64 non empty lines; 70 total lines | .
`libexec/ld.so/arm/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/hppa/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/hppa/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/hppa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 2 commented lines; 10 total lines | .
`libexec/ld.so/hppa/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/hppa/ldasm.S` | Assembler code | Simplified-BSD | OK | .
`libexec/ld.so/hppa/boot_md.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/hppa/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/i386/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/i386/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/i386/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 6 total lines | .
`libexec/ld.so/i386/rtld_machine.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`libexec/ld.so/i386/ldasm.S` | Assembler code | Simplified-BSD | OK | .
`libexec/ld.so/i386/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/ldconfig/ldconfig.8` | troff (manual) | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/ldconfig/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 3 commented lines; 18 total lines | .
`libexec/ld.so/ldconfig/etc.c` | C code | Public-Domain (notified in file) | OK | .
**`libexec/ld.so/ldconfig/ld.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 24 total lines | .
`libexec/ld.so/ldconfig/shlib.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/ldconfig/ldconfig.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/ldconfig/ldconfig_path.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/ldconfig/sod.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/ldd/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 13 total lines | .
`libexec/ld.so/ldd/ldd.1` | ? | Simplified-BSD | OK | .
`libexec/ld.so/ldd/ldd.c` | C code | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`libexec/ld.so/m88k/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/m88k/SYS.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`libexec/ld.so/m88k/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 6 total lines | .
`libexec/ld.so/m88k/rtld_machine.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`libexec/ld.so/m88k/ldasm.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`libexec/ld.so/m88k/memcpy.c` | C code | Modified-BSD | OK | .
`libexec/ld.so/m88k/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/mips64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/mips64/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/mips64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 2 commented lines; 11 total lines | .
`libexec/ld.so/mips64/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/mips64/ldasm.S` | Assembler code | Simplified-BSD | OK | .
`libexec/ld.so/mips64/boot_md.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/mips64/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 8 total lines | .
`libexec/ld.so/powerpc/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/powerpc/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/powerpc/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 64 non empty lines; 70 total lines | .
`libexec/ld.so/powerpc/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc64/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/powerpc64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 3 commented lines; 11 total lines | .
`libexec/ld.so/powerpc64/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/powerpc64/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/powerpc64/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 64 non empty lines; 70 total lines | .
`libexec/ld.so/powerpc64/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/riscv64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/riscv64/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/riscv64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 2 commented lines; 13 total lines | .
`libexec/ld.so/riscv64/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/riscv64/ldasm.S` | Assembler code | Simplified-BSD | OK | .
**`libexec/ld.so/riscv64/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 69 total lines | .
`libexec/ld.so/riscv64/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sh/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sh/SYS.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sh/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 2 commented lines; 14 total lines | .
`libexec/ld.so/sh/rtld_machine.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/sh/ldasm.S` | Assembler code | Simplified-BSD | OK | .
`libexec/ld.so/sh/syscall.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sparc64/archdep.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sparc64/SYS.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`libexec/ld.so/sparc64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 2 commented lines; 15 total lines | .
`libexec/ld.so/sparc64/rtld_machine.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`libexec/ld.so/sparc64/ldasm.S` | Assembler code | Simplified-BSD + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
**`libexec/ld.so/sparc64/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 69 total lines | .
`libexec/ld.so/sparc64/syscall.h` | C header | Simplified-BSD | OK | .
**`libexec/ld.so/SPECS.randomdata`** | ? | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 48 total lines | .
**`libexec/ld.so/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 96 non empty lines; 120 total lines | .
`libexec/ld.so/chacha_private.h` | C header | Public-Domain (D. J. Bernstein) | OK | .
`libexec/ld.so/boot.c` | C code | Simplified-BSD | OK | .
**`libexec/ld.so/Symbols.map`** | ? | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 27 total lines | .
`libexec/ld.so/dl_dirname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/dir.c` | C code | Modified-BSD | OK | .
`libexec/ld.so/dir.h` | C header | Modified-BSD | OK | .
`libexec/ld.so/library.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/dlfcn.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/dl_printf.c` | C code | Modified-BSD | OK | .
`libexec/ld.so/dl_uname.c` | C code | Modified-BSD | OK | .
`libexec/ld.so/ld.so.1` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/reallocarray.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/loader.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/library_mquery.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/library_subr.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/malloc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/path.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/path.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/util.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/sod.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/resolve.c` | C code | Simplified-BSD | OK | .
`libexec/ld.so/resolve.h` | C header | Simplified-BSD | OK | .
`libexec/ld.so/sod.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`libexec/ld.so/strtol.c` | C code | Modified-BSD | OK | .
`libexec/ld.so/tib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/trace.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`libexec/ld.so/util.h` | C header | Simplified-BSD | OK | .
`regress/lib/libc/_setjmp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 11 total lines | .
`regress/lib/libc/alloca/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/alloca/alloca.c` | C code | Public-Domain (Michael Shalayeff) | OK | .
**`regress/lib/libc/arc4random-fork/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 25 total lines | .
`regress/lib/libc/arc4random-fork/arc4random-fork.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/arch/alpha/divremtest/divremtest.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`regress/lib/libc/arch/alpha/divremtest/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 30 total lines | .
`regress/lib/libc/arch/alpha/divremtest/mkcases.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`regress/lib/libc/arch/alpha/divremtest/mktestcases.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`regress/lib/libc/arch/alpha/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`regress/lib/libc/asr/bin/getaddrinfo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/gethostnamadr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/getnameinfo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/getnetnamadr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/getrrsetbyname/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/res_mkquery/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/res_query/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/asr/bin/threads/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/asr/bin/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 0 commented lines; 10 total lines | .
`regress/lib/libc/asr/bin/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 0 commented lines; 10 total lines | .
`regress/lib/libc/asr/bin/getaddrinfo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/common.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/common.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/gethostnamadr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/getnameinfo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/getnetnamadr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/getrrsetbyname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/res_mkquery.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/res_query.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/asr/bin/threads.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/asr/regress.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 97 non empty lines; 111 total lines | .
**`regress/lib/libc/asr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 41 non empty lines; 52 total lines | .
**`regress/lib/libc/asr/regress.subr`** | ? | __Unlicensed!!!__ | Non-Trivial; 102 non empty lines; 122 total lines | .
`regress/lib/libc/atexit/atexit_test.c` | C code | Simplified-BSD T2 | OK | .
`regress/lib/libc/atexit/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented lines; 16 total lines | .
`regress/lib/libc/atexit/valid.ok` | ? | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`regress/lib/libc/atexit/invalid.ok` | ? | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 4 total lines | .
`regress/lib/libc/basename/basename_test.c` | C code | Public-Domain (Bret S. Lambert <blambert@gsipt.net>) | OK | .
`regress/lib/libc/basename/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/lib/libc/cephes/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`regress/lib/libc/cephes/drand.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/econst.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/eexp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/ehead.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/elog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/epow.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/etanh.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/etodec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/ieee.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/ieetst.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/mconf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cephes/mtherr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cxa-atexit/libgd1/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/lib/libc/cxa-atexit/libgd1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/lib/libc/cxa-atexit/libgd1/gd1.C` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cxa-atexit/libgd2/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/lib/libc/cxa-atexit/libgd2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/lib/libc/cxa-atexit/libgd2/gd2.C` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/cxa-atexit/test1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 21 non empty lines; 29 total lines | .
`regress/lib/libc/cxa-atexit/test1/test1.C` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/cxa-atexit/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/lib/libc/db/Makefile` | Makefile | "Unlicensed!!!" |  | .
**`regress/lib/libc/db/README`** | Readme | __Unlicensed!!!__ | Non-Trivial; 58 non empty lines; 69 total lines | .
`regress/lib/libc/db/dbtest.c` | C code | Modified-BSD | OK | .
**`regress/lib/libc/db/run.test`** | ? | __Unlicensed!!!__ | Non-Trivial; 684 non empty lines; 707 total lines | .
`regress/lib/libc/dirname/dirname_test.c` | C code | Public-Domain (Bret S. Lambert <blambert@gsipt.net>) | OK | .
`regress/lib/libc/dirname/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/lib/libc/env/envtest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/env/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/explicit_bzero/explicit_bzero.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/explicit_bzero/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/ffs/ffs_test.c` | C code | Public-Domain (Christian Weisgerber <naddy@openbsd.org>) | OK | .
`regress/lib/libc/ffs/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 6 total lines | .
`regress/lib/libc/fmemopen/fmemopentest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/fmemopen/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/fnmatch/fnm_test.c` | C code | Public-Domain (Todd C. Miller <millert@openbsd.org>) | OK | .
`regress/lib/libc/fnmatch/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
**`regress/lib/libc/fnmatch/fnm_test.in`** | Input file | __Unlicensed!!!__ | Non-Trivial; 260 non empty lines; 260 total lines | .
`regress/lib/libc/fpclassify/fpclassify.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/fpclassify/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/fread/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/fread/fread.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/gcvt/gcvt_test.c` | C code | Public-Domain (Todd C. Miller <millert@openbsd.org>) | OK | .
`regress/lib/libc/gcvt/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/getaddrinfo/gaitest.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`regress/lib/libc/getaddrinfo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
**`regress/lib/libc/getaddrinfo/answer`** | ? | __Unlicensed!!!__ | Non-Trivial; 103 non empty lines; 109 total lines | .
`regress/lib/libc/getaddrinfo/testsuite.sh` | Shell script | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`regress/lib/libc/getcap/getcaptest.c` | C code | Public-Domain (Raymond Lai <ray@cyth.net>) | OK | .
`regress/lib/libc/getcap/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/getcap/shortcap.in` | Input file | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`regress/lib/libc/getopt/getopt-test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/getopt/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/getopt/getopt.sh` | Shell script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/getopt_long/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 32 total lines | .
**`regress/lib/libc/getopt_long/test.ok`** | ? | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 83 total lines | .
`regress/lib/libc/getopt_long/getopt_long_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/glob/globtest.c` | C code | Public-Domain (Todd C. Miller <millert@openbsd.org>) | OK | .
`regress/lib/libc/glob/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 16 total lines | .
**`regress/lib/libc/glob/files`** | ? | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 47 total lines | .
**`regress/lib/libc/glob/globtest.in`** | Input file | __Unlicensed!!!__ | Non-Trivial; 109 non empty lines; 115 total lines | .
`regress/lib/libc/hsearch/hsearchtest.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`regress/lib/libc/hsearch/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
**`regress/lib/libc/ieeefp/except/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 16 non empty lines; 24 total lines | .
**`regress/lib/libc/ieeefp/except/except.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 93 non empty lines; 110 total lines | .
`regress/lib/libc/ieeefp/inf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/lib/libc/ieeefp/inf/inf.c` | C code | Public-Domain (Peter Valchev <pvalchev@openbsd.org>) | OK | .
`regress/lib/libc/ieeefp/infinity/infinity.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`regress/lib/libc/ieeefp/infinity/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 13 code lines + 1 commented lines; 22 total lines | .
`regress/lib/libc/ieeefp/round/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/ieeefp/round/round.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`regress/lib/libc/ieeefp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 8 total lines | .
`regress/lib/libc/ifnameindex/ifnitest.c` | C code | Public-Domain (Claudio Jeker) | OK | .
`regress/lib/libc/ifnameindex/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 12 total lines | .
`regress/lib/libc/ldexp/ldexp_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/ldexp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/locale/check_isw/check_isw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/locale/check_isw/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/locale/mbrtowc/test_mbrtowc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/locale/mbrtowc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/locale/setlocale/setlocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/locale/setlocale/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/locale/uselocale/uselocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/locale/uselocale/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/locale/wcrtomb/test_wcrtomb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/locale/wcrtomb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/locale/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/longjmp/longjmp.c` | C code | Public-Domain (Artur Grabowski <art@openbsd.org>) | OK | .
`regress/lib/libc/longjmp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 13 total lines | .
`regress/lib/libc/malloc/malloc0test/malloc0test.c` | C code | Public-Domain (Theo de Raadt) | OK | .
`regress/lib/libc/malloc/malloc0test/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/lib/libc/malloc/malloc_errno/malloc_errno.c` | C code | Public-Domain (Otto Moerbeek) | OK | .
`regress/lib/libc/malloc/malloc_errno/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/malloc/malloc_general/malloc_general.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/malloc/malloc_general/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 39 total lines | .
`regress/lib/libc/malloc/malloc_threaderr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 2 commented lines; 13 total lines | .
`regress/lib/libc/malloc/malloc_threaderr/malloc_threaderr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/malloc/malloc_ulimit1/malloc_ulimit1.c` | C code | Public-Domain (Otto Moerbeek <otto@drijf.net>) | OK | .
`regress/lib/libc/malloc/malloc_ulimit1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/malloc/malloc_ulimit2/malloc_ulimit2.c` | C code | Public-Domain (Otto Moerbeek <otto@drijf.net>) | OK | .
`regress/lib/libc/malloc/malloc_ulimit2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/malloc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/mkstemp/mkstemp_test.c` | C code | Public-Domain (Philip Guenther <guenther@openbsd.org>) | OK | .
`regress/lib/libc/mkstemp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 3 commented lines; 19 total lines | .
`regress/lib/libc/modf/modf_test.c` | C code | Public-Domain (Tobias Ulmer <tobiasu@tmux.org>) | OK | .
`regress/lib/libc/modf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/lib/libc/netdb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/netdb/netdb.c` | C code | Public-Domain (Otto Moerbeek <otto@drijf.net>) | OK | .
`regress/lib/libc/open_memstream/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/open_memstream/open_memstreamtest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/orientation/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/orientation/orientation_test.c` | C code | Simplified-BSD T2 | OK | .
`regress/lib/libc/popen/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/popen/popen.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/printf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 13 total lines | .
`regress/lib/libc/printf/fp.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/printf/int.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/printf/string.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/qsort/antiqsort.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 58 total lines | .
`regress/lib/libc/qsort/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 13 total lines | .
`regress/lib/libc/qsort/qsort_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/regex/t_exhaust.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`regress/lib/libc/regex/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 21 non empty lines; 27 total lines | .
**`regress/lib/libc/regex/debug.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 200 non empty lines; 208 total lines | .
`regress/lib/libc/regex/debug.ih` | ? | "Unlicensed!!!" | Trivial; 9 code lines + 5 commented lines; 17 total lines | .
**`regress/lib/libc/regex/main.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 473 non empty lines; 517 total lines | .
**`regress/lib/libc/regex/main.ih`** | ? | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 22 total lines | .
**`regress/lib/libc/regex/split.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 290 non empty lines; 317 total lines | .
**`regress/lib/libc/regex/tests`** | ? | __Unlicensed!!!__ | Non-Trivial; 589 non empty lines; 612 total lines | .
`regress/lib/libc/setjmp-fpu/_setjmp.c` | C code | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 16 total lines | .
**`regress/lib/libc/setjmp-fpu/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 26 total lines | .
**`regress/lib/libc/setjmp-fpu/setjmp-fpu.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 56 non empty lines; 65 total lines | .
**`regress/lib/libc/setjmp-fpu/fpu.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 45 non empty lines; 53 total lines | .
`regress/lib/libc/setjmp-fpu/sigsetjmp.c` | C code | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 16 total lines | .
`regress/lib/libc/setjmp-fpu/setjmp.c` | C code | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 16 total lines | .
`regress/lib/libc/setjmp-signal/setjmp-signal.c` | C code | Public-Domain (Artur Grabowski <art@openbsd.org>) | OK | .
`regress/lib/libc/setjmp-signal/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/setjmp/jmptest.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`regress/lib/libc/setjmp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`regress/lib/libc/sigsetjmp/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 19 total lines | .
`regress/lib/libc/sigthr/sigthr_test.c` | C code | Public-Domain (Philip Guenther <guenther@openbsd.org>) | OK | .
`regress/lib/libc/sigthr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/sleep/sleep_test.c` | C code | Simplified-BSD T2 | OK | .
`regress/lib/libc/sleep/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 13 total lines | .
`regress/lib/libc/sprintf/sprintf_test.c` | C code | Simplified-BSD T2 | OK | .
`regress/lib/libc/sprintf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`regress/lib/libc/stdio_threading/fgetln/fgetln_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fgetln/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/fgets/fgets_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fgets/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/fopen/fopen_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fopen/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/fputs/fputs_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fputs/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/fread/fread_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fread/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/fwrite/fwrite_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/fwrite/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 6 total lines | .
`regress/lib/libc/stdio_threading/include/local.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/stdio_threading/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/lib/libc/stpncpy/stpncpy_test.c` | C code | Public-Domain (Christian Weisgerber <naddy@openbsd.org>) | OK | .
`regress/lib/libc/stpncpy/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/lib/libc/strchr/strchrtest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strchr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/lib/libc/strerror/strerror_test.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strerror/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 12 total lines | .
**`regress/lib/libc/strerror/valid.ok`** | ? | __Unlicensed!!!__ | Non-Trivial; 65 non empty lines; 65 total lines | .
`regress/lib/libc/strlcat/strlcattest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strlcat/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/strlcpy/strlcpytest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strlcpy/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/strnlen/strnlentest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strnlen/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/strtod/strtodtest.c` | C code | Public-Domain (Otto Moerbeek <otto@drijf.net>) | OK | .
`regress/lib/libc/strtod/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/strtol/strtoltest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strtol/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/strtonum/strtonumtest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/strtonum/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/sys/h_macros.h` | C header | Simplified-BSD | OK | .
`regress/lib/libc/sys/Makefile` | Makefile | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/lib/libc/sys/README`** | Readme | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 73 total lines | .
`regress/lib/libc/sys/atf-c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/sys/atf-c.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/sys/t_access.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/macros.h` | C header | Public-Domain (Moritz Buhl) | OK | .
`regress/lib/libc/sys/t_chroot.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_bind.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_fsync.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_dup.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_fork.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_clock_gettime.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`regress/lib/libc/sys/t_connect.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_listen.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_kill.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getgroups.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getitimer.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getlogin.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getpid.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getrusage.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getsid.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_getsockname.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_gettimeofday.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_kevent.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_link.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_msgctl.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_mmap.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`regress/lib/libc/sys/t_minherit.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_mkdir.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_mkfifo.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_mknod.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_mlock.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_pipe2.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`regress/lib/libc/sys/t_pipe.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_msgget.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_msgrcv.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_msgsnd.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_msync.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_pollts.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_poll.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_syscall.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_stat.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_ppoll.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_ptrace.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_revoke.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_select.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_sendrecv.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_setrlimit.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_setuid.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_sigaction.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_sigaltstack.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_socketpair.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`regress/lib/libc/sys/t_wait_noproc_wnohang.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_truncate.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_umask.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_unlink.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_vfork.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_wait_noproc.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/sys/t_write.c` | C code | Simplified-BSD | OK | .
`regress/lib/libc/telldir/shortseek.c` | C code | Public domain (Otto Moerbeek) + Public domain (Ingo Schwarze) | OK (Combined Licenses) | .
`regress/lib/libc/telldir/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/lib/libc/telldir/main.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`regress/lib/libc/telldir/utils.c` | C code | Public-Domain (Otto Moerbeek) | OK | .
`regress/lib/libc/telldir/telldir.c` | C code | Public-Domain (Otto Moerbeek) | OK | .
`regress/lib/libc/telldir/utils.h` | C header | Public-Domain (Ingo Schwarze) | OK | .
`regress/lib/libc/time/strptime/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 12 total lines | .
**`regress/lib/libc/time/strptime/expected`** | ? | __Unlicensed!!!__ | Non-Trivial; 263 non empty lines; 346 total lines | .
`regress/lib/libc/time/strptime/main.c` | C code | Simplified-BSD | OK | .
**`regress/lib/libc/time/strptime/tests`** | ? | __Unlicensed!!!__ | Non-Trivial; 265 non empty lines; 267 total lines | .
`regress/lib/libc/time/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/timekeep/test_time_skew.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/timekeep/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/timekeep/test_clock_gettime.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/timekeep/test_gettimeofday.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/timingsafe/timingsafe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/timingsafe/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/uuid/uuidtest.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/lib/libc/uuid/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/vis/vis_test.c` | C code | Public-Domain (Otto Moerbeek) | OK | .
`regress/lib/libc/vis/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`regress/lib/libc/vis/valid.ok` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/lib/libc/wprintf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/lib/libc/wprintf/wfp.c` | C code | Simplified-BSD | OK | .
**`regress/lib/libc/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 41 total lines | .
`regress/libexec/ld.so/constructor/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/constructor/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/constructor/libaa/aa.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/constructor/libaa/aa.h` | C header | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 4 total lines | .
`regress/libexec/ld.so/constructor/libab/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/constructor/libab/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 14 total lines | .
`regress/libexec/ld.so/constructor/libab/ab.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/constructor/libab/ab.h` | C header | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/constructor/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 34 total lines | .
`regress/libexec/ld.so/constructor/prog1/prog1.C` | ? | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/constructor/prog2/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 34 total lines | .
`regress/libexec/ld.so/constructor/prog2/prog2.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/constructor/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dependencies/order1/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dependencies/order1/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 9 total lines | .
`regress/libexec/ld.so/dependencies/order1/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order1/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dependencies/order1/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 6 total lines | .
`regress/libexec/ld.so/dependencies/order1/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order1/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 7 total lines | .
`regress/libexec/ld.so/dependencies/order1/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dependencies/order1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dependencies/order2/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dependencies/order2/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 9 total lines | .
`regress/libexec/ld.so/dependencies/order2/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order2/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dependencies/order2/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 6 total lines | .
`regress/libexec/ld.so/dependencies/order2/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order2/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 7 total lines | .
`regress/libexec/ld.so/dependencies/order2/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dependencies/order2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dependencies/order2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dependencies/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 8 total lines | .
**`regress/libexec/ld.so/df_1_noopen/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 43 total lines | .
`regress/libexec/ld.so/df_1_noopen/dlopen.c` | C code | "Unlicensed!!!" | Trivial; 14 code lines + 1 commented lines; 19 total lines | .
**`regress/libexec/ld.so/df_1_noopen/test.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 29 total lines | .
`regress/libexec/ld.so/dlclose/test1/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test1/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlclose/test1/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test1/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlclose/test1/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/libbb/bbb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlclose/test1/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/prog2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/dlclose/test1/prog2/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/prog3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/dlclose/test1/prog3/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlclose/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlclose/test2/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test2/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 13 total lines | .
`regress/libexec/ld.so/dlclose/test2/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test2/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test2/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlclose/test2/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test2/libcc/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test2/libcc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlclose/test2/libcc/cc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test2/libzz/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlclose/test2/libzz/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlclose/test2/libzz/zz.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test2/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlclose/test2/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlclose/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlclose/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlclose/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlopen/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlopen/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlopen/libaa/aa.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/dlopen/libaa/aa.h` | C header | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 7 total lines | .
`regress/libexec/ld.so/dlopen/libab/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlopen/libab/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 7 total lines | .
`regress/libexec/ld.so/dlopen/libab/ab.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/dlopen/libab/ab.h` | C header | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/dlopen/libac/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlopen/libac/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 14 code lines + 0 commented lines; 16 total lines | .
`regress/libexec/ld.so/dlopen/libac/ac.C` | ? | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/dlopen/libac/ac.h` | C header | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/dlopen/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 35 total lines | .
`regress/libexec/ld.so/dlopen/prog1/prog1.C` | ? | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/dlopen/prog2/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 34 total lines | .
`regress/libexec/ld.so/dlopen/prog2/prog2.C` | ? | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/dlopen/prog3/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 34 total lines | .
`regress/libexec/ld.so/dlopen/prog3/main.c` | C code | Public-Domain (Dale Rahn) | OK | .
**`regress/libexec/ld.so/dlopen/prog4/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 35 total lines | .
`regress/libexec/ld.so/dlopen/prog4/main.c` | C code | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/dlopen/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/dlsym/test1/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test1/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test1/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test1/libaa/aa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/dlsym/test1/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 16 non empty lines; 21 total lines | .
`regress/libexec/ld.so/dlsym/test1/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/dlsym/test1/prog2/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 16 non empty lines; 21 total lines | .
`regress/libexec/ld.so/dlsym/test1/prog2/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlsym/test2/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test2/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test2/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test2/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test2/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/libcc/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test2/libcc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test2/libcc/cc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test2/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/prog2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test2/prog2/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/prog3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test2/prog3/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/prog4/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 12 total lines | .
`regress/libexec/ld.so/dlsym/test2/prog4/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/prog5/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 12 total lines | .
`regress/libexec/ld.so/dlsym/test2/prog5/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlsym/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlsym/test3/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test3/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test3/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test3/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test3/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/libcc/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test3/libcc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test3/libcc/cc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/libdd/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test3/libdd/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/dlsym/test3/libdd/dd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/libee/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/dlsym/test3/libee/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/dlsym/test3/libee/ee.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 12 total lines | .
`regress/libexec/ld.so/dlsym/test3/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/prog2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 13 total lines | .
`regress/libexec/ld.so/dlsym/test3/prog2/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/prog3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 13 total lines | .
`regress/libexec/ld.so/dlsym/test3/prog3/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/prog4/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/dlsym/test3/prog4/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/dlsym/test3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlsym/test3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/dlsym/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_b/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_b/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 4 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_b/aa.c` | C code | "Unlicensed!!!" | Trivial; 4 code lines + 6 commented lines; 10 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_g/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_g/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 4 total lines | .
`regress/libexec/ld.so/edgecases/test1/libaa_g/aa.c` | C code | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 6 total lines | .
**`regress/libexec/ld.so/edgecases/test1/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 36 total lines | .
`regress/libexec/ld.so/edgecases/test1/prog1/main.c` | C code | Public-Domain (Dale Rahn) | OK | .
`regress/libexec/ld.so/edgecases/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/edgecases/test2/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/edgecases/test2/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/edgecases/test2/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/edgecases/test2/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 16 non empty lines; 22 total lines | .
`regress/libexec/ld.so/edgecases/test2/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/edgecases/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/edgecases/test3/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/edgecases/test3/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/edgecases/test3/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/edgecases/test3/libbb/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/edgecases/test3/libbb/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/edgecases/test3/libbb/bb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/edgecases/test3/prog1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/edgecases/test3/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/edgecases/test3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/edgecases/test3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/edgecases/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/elf/foo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented lines; 18 total lines | .
`regress/libexec/ld.so/elf/foo/main.c` | C code | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/elf/libbar/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/elf/libbar/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/elf/libbar/bar.c` | C code | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/elf/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/elf/elfbug.h` | C header | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/hidden/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/hidden/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/hidden/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/hidden/libab/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/hidden/libab/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 11 total lines | .
`regress/libexec/ld.so/hidden/libab/ab.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/hidden/test1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 31 total lines | .
`regress/libexec/ld.so/hidden/test1/test1.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/hidden/test2/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 35 total lines | .
`regress/libexec/ld.so/hidden/test2/test2.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/hidden/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/init-env/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/init-env/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 10 total lines | .
`regress/libexec/ld.so/init-env/libaa/aa.C` | ? | Public-Domain (Philip Guenther <guenther@openbsd.org>) | OK | .
**`regress/libexec/ld.so/init-env/prog/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 23 total lines | .
`regress/libexec/ld.so/init-env/prog/prog.c` | C code | Public-Domain (Philip Guenther <guenther@openbsd.org>) | OK | .
`regress/libexec/ld.so/init-env/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 5 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif1/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif1/lib.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif2/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif2/lib.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 3 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif3/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif3/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test1/libif3/lib.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 3 total lines | .
`regress/libexec/ld.so/initfirst/test1/libnormal/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test1/libnormal/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test1/libnormal/lib.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 3 total lines | .
`regress/libexec/ld.so/initfirst/test1/prog1/expected1` | ? | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented lines; 8 total lines | .
**`regress/libexec/ld.so/initfirst/test1/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 30 total lines | .
`regress/libexec/ld.so/initfirst/test1/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/initfirst/test1/prog1/expected2` | ? | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test1/prog1/expected3` | ? | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test1/prog1/expected4` | ? | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/initfirst/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/initfirst/test1/lib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/initfirst/test2/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test2/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test2/libaa/aa.C` | ? | Public-Domain (Kurt Miller) | OK | .
`regress/libexec/ld.so/initfirst/test2/libab/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test2/libab/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test2/libab/ab.C` | ? | Public-Domain (Kurt Miller) | OK | .
`regress/libexec/ld.so/initfirst/test2/libac/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test2/libac/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 8 total lines | .
`regress/libexec/ld.so/initfirst/test2/libac/ac.C` | ? | Public-Domain (Kurt Miller) | OK | .
`regress/libexec/ld.so/initfirst/test2/libad/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test2/libad/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 9 total lines | .
`regress/libexec/ld.so/initfirst/test2/libad/ad.C` | ? | Public-Domain (Kurt Miller) | OK | .
`regress/libexec/ld.so/initfirst/test2/libae/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/initfirst/test2/libae/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 6 total lines | .
`regress/libexec/ld.so/initfirst/test2/libae/ae.C` | ? | Public-Domain (Kurt Miller) | OK | .
**`regress/libexec/ld.so/initfirst/test2/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 58 total lines | .
`regress/libexec/ld.so/initfirst/test2/prog1/prog1.C` | ? | Public-Domain (Kurt Miller) | OK | .
**`regress/libexec/ld.so/initfirst/test2/prog2/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 58 total lines | .
`regress/libexec/ld.so/initfirst/test2/prog2/prog2.C` | ? | Public-Domain (Kurt Miller) | OK | .
`regress/libexec/ld.so/initfirst/test2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`regress/libexec/ld.so/initfirst/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/lazy/libbar/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/lazy/libbar/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/lazy/libbar/bar.c` | C code | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/lazy/libfoo/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/lazy/libfoo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 11 code lines + 1 commented lines; 18 total lines | .
`regress/libexec/ld.so/lazy/libfoo/foo.c` | C code | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/lazy/prog/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 15 total lines | .
`regress/libexec/ld.so/lazy/prog/prog.c` | C code | Public-Domain (Matthieu Herrb) | OK | .
`regress/libexec/ld.so/lazy/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/link-order/lib10/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/link-order/lib10/10.c` | C code | Public-Domain (Niklas Hallqvist) | OK | .
`regress/libexec/ld.so/link-order/lib10/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/link-order/lib11/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/link-order/lib11/11.c` | C code | Public-Domain (Niklas Hallqvist) | OK | .
`regress/libexec/ld.so/link-order/lib11/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/link-order/lib20/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/link-order/lib20/20.c` | C code | Public-Domain (Niklas Hallqvist) | OK | .
`regress/libexec/ld.so/link-order/lib20/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/link-order/libnover/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 16 total lines | .
`regress/libexec/ld.so/link-order/libnover/nover.c` | C code | Public-Domain (Niklas Hallqvist) | OK | .
`regress/libexec/ld.so/link-order/prog/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
**`regress/libexec/ld.so/link-order/prog/prog.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 19 total lines | .
**`regress/libexec/ld.so/link-order/test/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 70 non empty lines; 97 total lines | .
`regress/libexec/ld.so/link-order/test/test.sh` | Shell script | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 10 total lines | .
`regress/libexec/ld.so/link-order/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 10 total lines | .
`regress/libexec/ld.so/nodelete/liba/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/nodelete/liba/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/nodelete/liba/liba.c` | C code | "Unlicensed!!!" | Trivial; 4 code lines + 0 commented lines; 4 total lines | .
`regress/libexec/ld.so/nodelete/test1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 15 total lines | .
**`regress/libexec/ld.so/nodelete/test1/test1.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 48 non empty lines; 59 total lines | .
`regress/libexec/ld.so/nodelete/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
**`regress/libexec/ld.so/randomdata/ld.so-cookie/test.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 42 non empty lines; 53 total lines | .
`regress/libexec/ld.so/randomdata/ld.so-cookie/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/randomdata/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/randomdata/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/randomdata/libaa/aa.c` | C code | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 10 total lines | .
`regress/libexec/ld.so/randomdata/prog-dynamic/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/randomdata/prog-pie/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 10 total lines | .
`regress/libexec/ld.so/randomdata/prog-static/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`regress/libexec/ld.so/randomdata/prog/prog.c` | C code | "Unlicensed!!!" | Trivial; 13 code lines + 1 commented lines; 18 total lines | .
`regress/libexec/ld.so/randomdata/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/randomdata/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`regress/libexec/ld.so/subst/libaa/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/subst/libaa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/subst/libaa/aa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/subst/libaa/aa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`regress/libexec/ld.so/subst/prog1/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 33 total lines | .
`regress/libexec/ld.so/subst/prog1/main.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`regress/libexec/ld.so/subst/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/weak/libstrong/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/weak/libstrong/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/weak/libstrong/strong.c` | C code | Public-Domain (Federico Schwindt <fgsch@openbsd.org>) | OK | .
`regress/libexec/ld.so/weak/libweak/shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`regress/libexec/ld.so/weak/libweak/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`regress/libexec/ld.so/weak/libweak/weak.c` | C code | Public-Domain (Federico Schwindt <fgsch@openbsd.org>) | OK | .
**`regress/libexec/ld.so/weak/prog/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 23 total lines | .
`regress/libexec/ld.so/weak/prog/defs.h` | C header | Public-Domain (Federico Schwindt <fgsch@openbsd.org>) | OK | .
`regress/libexec/ld.so/weak/prog/main.c` | C code | Public-Domain (Federico Schwindt <fgsch@openbsd.org>) | OK | .
`regress/libexec/ld.so/weak/prog2/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 16 total lines | .
`regress/libexec/ld.so/weak/prog2/main.c` | C code | Public-Domain (Federico Schwindt <fgsch@openbsd.org>) | OK | .
`regress/libexec/ld.so/weak/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/weak/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`regress/libexec/ld.so/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 11 total lines | .

---

## hyperblibc-extra-file-list.md - HyperBLibC Extra file list for Todo list

Written in 2023 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.in` | . | . | . | . fo>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
