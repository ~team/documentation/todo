File name | File type | License(s) | Description(s) / Status | More
---|---|---|---|---
`arch/aarch64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/aarch64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/aarch64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/aarch64/gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/flt_rounds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/aarch64/gen/fabs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 2 commented lines; 12 total lines | .
`arch/aarch64/gen/_atomic_lock.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/_setjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/aarch64/gen/byte_swap_2.S` | Assembler code | Simplified-BSD | OK | .
`arch/aarch64/gen/byte_swap_4.S` | Assembler code | Simplified-BSD | OK | .
**`arch/aarch64/gen/clz_tab.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 34 total lines | .
`arch/aarch64/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/fpgetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/fpgetround.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/fpgetsticky.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/fpsetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/fpsetround.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/fpsetsticky.c` | C code | Simplified-BSD | OK | .
`arch/aarch64/gen/infinity.c` | C code | Public-Domain (notified in file) | OK | .
`arch/aarch64/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/aarch64/gen/setjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/aarch64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/gen/sigsetjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/aarch64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 3 commented lines; 5 total lines | .
`arch/aarch64/string/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`arch/aarch64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/aarch64/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/cerror.S` | Assembler code | Simplified-BSD | OK | .
`arch/aarch64/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/aarch64/sys/tfork_thread.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/aarch64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/aarch64/DEFS.h` | C header | Modified-BSD | OK | .
`arch/aarch64/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 11 code lines + 0 commented lines; 11 total lines | .
`arch/aarch64/SYS.h` | C header | Modified-BSD | OK | .
`arch/alpha/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/alpha/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/alpha/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/alpha/gen/flt_rounds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/fabs.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 7 total lines | .
`arch/alpha/gen/_atomic_lock.S` | Assembler code | Public-Domain (David Leonard) | OK | .
`arch/alpha/gen/_setjmp.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/gen/divrem.m4` | ? | CMU (similar to ISC) | OK | .
`arch/alpha/gen/sigsetjmp.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/alpha/gen/fpgetmask.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/fpgetround.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/fpgetsticky.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/alpha/gen/fpsetmask.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/fpsetround.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/fpsetsticky.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/gen/infinity.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/gen/setjmp.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/net/htonl.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/alpha/net/byte_swap_2.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/net/byte_swap_4.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/net/htons.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/net/ntohl.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/net/ntohs.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/string/memmove.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/string/bcopy.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/alpha/string/bzero.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/string/ffs.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/string/memcpy.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/sigpending.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/Ovfork.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/brk.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/sbrk.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/sigprocmask.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/sigsuspend.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/syscall.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/alpha/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 13 code lines + 0 commented lines; 13 total lines | .
`arch/alpha/SYS.h` | C header | CMU (similar to ISC) | OK | .
**`arch/alpha/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 39 non empty lines; 48 total lines | .
`arch/amd64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/amd64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/amd64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/amd64/gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`arch/amd64/gen/flt_rounds.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
**`arch/amd64/gen/fabs.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 20 total lines | .
`arch/amd64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/amd64/gen/_atomic_lock.c` | C code | Public-Domain (David Leonard) | OK | .
`arch/amd64/gen/_setjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/fpgetmask.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/gen/fpgetround.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/gen/fpgetsticky.S` | Assembler code | Public-Domain (Frank van der Linden at Wasabi Systems) | OK | .
`arch/amd64/gen/fpsetmask.S` | Assembler code | Public-Domain (Frank van der Linden at Wasabi Systems) | OK | .
`arch/amd64/gen/fpsetround.S` | Assembler code | Public-Domain (Frank van der Linden at Wasabi Systems) | OK | .
`arch/amd64/gen/fpsetsticky.S` | Assembler code | Public-Domain (Frank van der Linden at Wasabi Systems) | OK | .
`arch/amd64/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 8 total lines | .
`arch/amd64/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/ldexp.c` | C code | Modified-BSD | OK | .
`arch/amd64/gen/modf.S` | Assembler code | Original-BSD | OK | .
`arch/amd64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/amd64/gen/setjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/amd64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/gen/sigsetjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/net/htonl.S` | Assembler code | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/amd64/net/htons.S` | Assembler code | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/net/ntohl.S` | Assembler code | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/net/ntohs.S` | Assembler code | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/string/memmove.S` | Assembler code | Original-BSD | OK | .
**`arch/amd64/string/bcmp.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 23 total lines | .
`arch/amd64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/amd64/string/bzero.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/ffs.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/memchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/strrchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/memset.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/strchr.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/string/strcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/string/strlen.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/sbrk.S` | Assembler code | Original-BSD | OK | .
`arch/amd64/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`arch/amd64/DEFS.h` | C header | Modified-BSD | OK | .
`arch/amd64/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 7 code lines + 0 commented lines; 7 total lines | .
`arch/amd64/SYS.h` | C header | Modified-BSD | OK | .
`arch/arm/dlfcn/exidx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/dlfcn/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`arch/arm/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/arm/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`arch/arm/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/arm/gen/flt_rounds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/arm/gen/divsi3.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 378 non empty lines; 395 total lines | .
`arch/arm/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 6 total lines | .
`arch/arm/gen/_atomic_lock.c` | C code | Simplified-BSD | OK | .
`arch/arm/gen/_setjmp.S` | Assembler code | Original-BSD | OK | .
`arch/arm/gen/byte_swap_2.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/gen/byte_swap_4.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/gen/fabs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/sigsetjmp.S` | Assembler code | Original-BSD | OK | .
`arch/arm/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/arm/gen/fpgetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/fpgetround.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/fpgetsticky.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/fpsetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/fpsetround.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/fpsetsticky.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/gen/infinity.c` | C code | Public-Domain (notified in file) | OK | .
`arch/arm/gen/setjmp.S` | Assembler code | Original-BSD | OK | .
`arch/arm/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 3 commented lines; 5 total lines | .
**`arch/arm/softfloat/milieu.h`** | C header | __Nonfree-License!!!__ | (John R. Hauser; it contains restrictions to use the code for any purpose) | .
`arch/arm/softfloat/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 11 total lines | .
**`arch/arm/softfloat/arm-gcc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 92 non empty lines; 99 total lines | .
`arch/arm/string/memmove.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/string/bcopy.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/arm/string/_memcpy.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/string/bzero.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/string/ffs.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T2) | OK | .
`arch/arm/string/memcmp.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/arm/string/memcpy.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm/string/strncmp.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/arm/string/memset.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/string/strcmp.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/arm/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/cerror.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/arm/sys/tfork_thread.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 3 commented lines; 14 total lines | .
`arch/arm/DEFS.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/arm/Symbols.list`** | ? | __Unlicensed!!!__ | Non-Trivial; 87 non empty lines; 90 total lines | .
`arch/arm/SYS.h` | C header | Modified-BSD | OK | .
`arch/hppa/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/hppa/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 0 commented lines; 3 total lines | .
`arch/hppa/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/hppa/gen/flt_rounds.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fabs.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 7 total lines | .
`arch/hppa/gen/_atomic_lock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/gen/setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/hppa/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/hppa/gen/fpgetmask.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fpgetround.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fpgetsticky.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fpsetmask.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fpsetround.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/fpsetsticky.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/hppa/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/hppa/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/hppa/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/hppa/string/memmove.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/sigpending.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/Ovfork.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/brk.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/sbrk.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/sigprocmask.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/sigsuspend.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/syscall.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`arch/hppa/DEFS.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/hppa/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/hppa/SYS.h` | C header | Simplified-BSD | OK | .
`arch/i386/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/i386/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`arch/i386/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/i386/gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`arch/i386/gen/fixdfsi.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/divsi3.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/i386/gen/_atomic_lock.c` | C code | Public-Domain (David Leonard) | OK | .
`arch/i386/gen/_setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/fabs.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/fixunsdfsi.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/flt_rounds.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/fpgetmask.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/gen/fpgetround.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/gen/fpgetsticky.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/gen/fpsetmask.S` | Assembler code | Public-Domain (Charles M. Hannum) | OK | .
`arch/i386/gen/fpsetround.S` | Assembler code | Public-Domain (Charles M. Hannum) | OK | .
`arch/i386/gen/fpsetsticky.S` | Assembler code | Public-Domain (Charles M. Hannum) | OK | .
`arch/i386/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 3 commented lines; 7 total lines | .
`arch/i386/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/ldexp.c` | C code | Modified-BSD | OK | .
`arch/i386/gen/modf.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/i386/gen/setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/gen/sigsetjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/gen/udivsi3.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/net/htonl.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/i386/net/htons.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/net/ntohl.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/net/ntohs.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/stdlib/abs.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/stdlib/div.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/stdlib/labs.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/stdlib/ldiv.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/memmove.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/string/bcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/i386/string/bzero.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/ffs.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/memchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/memcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strncmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/memset.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strcat.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strcpy.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/string/strrchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`arch/i386/DEFS.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/i386/SYS.h` | C header | Modified-BSD | OK | .
`arch/m88k/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/m88k/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 2 total lines | .
`arch/m88k/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/m88k/gen/flt_rounds.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/divsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 8 total lines | .
`arch/m88k/gen/_atomic_lock.c` | C code | Simplified-BSD | OK | .
`arch/m88k/gen/_setjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/gen/fabs.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/gen/sigsetjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/gen/modsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/gen/fpgetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/fpgetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/fpgetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/fpsetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/fpsetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/fpsetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/m88k/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/m88k/gen/setjmp.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/gen/udivsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/gen/umodsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/net/htonl.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/m88k/net/htons.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/net/ntohl.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/net/ntohs.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/m88k/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/cerror.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/m88k/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/m88k/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 2 total lines | .
**`arch/m88k/DEFS.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 25 total lines | .
`arch/m88k/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented lines; 8 total lines | .
`arch/m88k/SYS.h` | C header | Modified-BSD | OK | .
`arch/mips64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/mips64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 10 code lines + 0 commented lines; 10 total lines | .
`arch/mips64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/mips64/gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`arch/mips64/gen/flt_rounds.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fabs.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/mips64/gen/_atomic_lock.c` | C code | Public-Domain (Miodrag Vallat) | OK | .
`arch/mips64/gen/_setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/gen/cacheflush.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/fpc_csr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/fpgetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fpgetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fpgetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fpsetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fpsetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/fpsetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 7 code lines + 3 commented lines; 12 total lines | .
`arch/mips64/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/ldexp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/gen/modf.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/mips64/gen/setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/gen/sigsetjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/net/htonl.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/mips64/net/htons.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/strrchr.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/bcmp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/mips64/string/bcopy.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/bzero.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/ffs.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/strchr.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/strcmp.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/string/strlen.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/cerror.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/mips64/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 13 code lines + 0 commented lines; 13 total lines | .
`arch/mips64/SYS.h` | C header | Modified-BSD | OK | .
`arch/mips64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 2 total lines | .
`arch/powerpc/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/powerpc/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 0 commented lines; 3 total lines | .
`arch/powerpc/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/powerpc/gen/flt_rounds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/gen/fabs.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`arch/powerpc/gen/_atomic_lock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/gen/sigsetjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/powerpc/gen/fpgetmask.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/fpgetround.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/fpgetsticky.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/fpsetmask.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/fpsetround.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/fpsetsticky.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/powerpc/gen/setjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/powerpc/string/memmove.S` | Assembler code | Modified-BSD | OK | .
`arch/powerpc/string/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`arch/powerpc/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/powerpc/sys/sigpending.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/sys/Ovfork.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/brk.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/sbrk.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/sigprocmask.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/sigsuspend.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/syscall.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/sys/tfork_thread.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/powerpc/SYS.h` | C header | Original-BSD | OK | .
`arch/powerpc/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`arch/powerpc64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/powerpc64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`arch/powerpc64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/powerpc64/gen/flt_rounds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/gen/fabs.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/powerpc64/gen/_atomic_lock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/gen/sigsetjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/powerpc64/gen/fpgetmask.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/fpgetround.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/fpgetsticky.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/fpsetmask.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/fpsetround.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/fpsetsticky.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/powerpc64/gen/setjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/powerpc64/string/memmove.S` | Assembler code | Modified-BSD | OK | .
`arch/powerpc64/string/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`arch/powerpc64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/powerpc64/sys/sigpending.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/sys/Ovfork.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/brk.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/sbrk.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/sigprocmask.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/sigsuspend.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/syscall.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc64/sys/tfork_thread.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 2 total lines | .
`arch/powerpc64/DEFS.h` | C header | Modified-BSD | OK | .
`arch/powerpc64/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/powerpc64/SYS.h` | C header | Original-BSD | OK | .
`arch/riscv64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/riscv64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/riscv64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/riscv64/gen/flt_rounds.c` | C code | Public-Domain (Mark Kettenis based on the hppa version written by Miodrag Vallat) | OK | .
`arch/riscv64/gen/fabs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 10 total lines | .
`arch/riscv64/gen/_atomic_lock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/_setjmp.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpgetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpgetround.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpgetsticky.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpsetmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpsetround.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/fpsetsticky.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/infinity.c` | C code | Public-Domain (notified in file) | OK | .
`arch/riscv64/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/riscv64/gen/setjmp.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/gen/sigsetjmp.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/riscv64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 9 total lines | .
`arch/riscv64/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/sys/brk.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/sys/sbrk.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/sys/tfork_thread.S` | Assembler code | Modified-BSD | OK | .
`arch/riscv64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 2 commented lines; 5 total lines | .
`arch/riscv64/DEFS.h` | C header | Modified-BSD | OK | .
`arch/riscv64/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`arch/riscv64/SYS.h` | C header | Modified-BSD | OK | .
`arch/sh/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/sh/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 0 commented lines; 3 total lines | .
`arch/sh/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/sh/gen/flt_rounds.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fabs.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 6 total lines | .
`arch/sh/gen/_atomic_lock.c` | C code | Simplified-BSD | OK | .
`arch/sh/gen/_setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/gen/sigsetjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/sh/gen/fpgetmask.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fpgetround.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fpgetsticky.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fpsetmask.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fpsetround.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/fpsetsticky.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 8 code lines + 3 commented lines; 14 total lines | .
`arch/sh/gen/setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sh/net/htonl.c` | C code | Modified-BSD | OK | .
`arch/sh/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/sh/net/htons.c` | C code | Modified-BSD | OK | .
`arch/sh/net/ntohl.c` | C code | Modified-BSD | OK | .
`arch/sh/net/ntohs.c` | C code | Modified-BSD | OK | .
**`arch/sh/softfloat/milieu.h`** | C header | __Nonfree-License!!!__ | (John R. Hauser; it contains restrictions to use the code for any purpose) | .
`arch/sh/softfloat/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 2 commented lines; 11 total lines | .
**`arch/sh/softfloat/sh-gcc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 81 non empty lines; 88 total lines | .
`arch/sh/string/memmove.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/string/bcopy.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 8 total lines | .
`arch/sh/string/bzero.S` | Assembler code | "Unlicensed!!!" | Trivial; 2 code lines + 2 commented lines; 5 total lines | .
`arch/sh/string/ffs.S` | Assembler code | Simplified-BSD | OK | .
`arch/sh/string/memcpy.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/string/memset.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/syscall.S` | Assembler code | Modified-BSD | OK | .
`arch/sh/sys/tfork_thread.S` | Assembler code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/Symbols.list` | ? | "Unlicensed!!!" | Trivial; 10 code lines + 0 commented lines; 10 total lines | .
`arch/sh/SYS.h` | C header | Modified-BSD | OK | .
`arch/sh/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`arch/sparc64/fpu/fpu_reg.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_q.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/fpu/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 6 total lines | .
`arch/sparc64/fpu/fpu_add.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_arith.h` | C header | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_compare.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_div.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_emu.h` | C header | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_explode.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_extern.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_implode.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_mul.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_q.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_qp.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_reg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_sqrt.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/fpu/fpu_subr.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK | .
`arch/sparc64/gdtoa/gd_qnan.h` | C header | "Unlicensed!!!" | Trivial; 12 code lines + 0 commented lines; 12 total lines | .
`arch/sparc64/gdtoa/arith.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented lines; 6 total lines | .
`arch/sparc64/gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/sparc64/gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/gen/fixunsdfsi.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/fabs.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 9 total lines | .
`arch/sparc64/gen/_atomic_lock.c` | C code | Public-Domain (David Leonard) | OK | .
`arch/sparc64/gen/_setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/divrem.m4` | ? | Modified-BSD | OK | .
`arch/sparc64/gen/isnormall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/isinfl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/flt_rounds.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpclassifyl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/fpgetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpgetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpgetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpsetmask.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpsetround.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/fpsetsticky.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/gen/infinity.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 4 commented lines; 9 total lines | .
`arch/sparc64/gen/isfinitel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/isnanl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/signbitl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/gen/modf.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/mul.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/nan.c` | C code | Public-Domain (Martynas Venckus) | OK | .
`arch/sparc64/gen/setjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/umul.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/sigsetjmp.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/gen/usertc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/net/htonl.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/net/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/sparc64/net/htons.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/net/ntohl.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/net/ntohs.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/stdlib/abs.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/stdlib/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/sparc64/string/ffs.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/string/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 10 total lines | .
`arch/sparc64/string/strlen.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/sigpending.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/Ovfork.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/brk.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/sbrk.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/sigprocmask.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/sigsuspend.S` | Assembler code | Modified-BSD | OK | .
`arch/sparc64/sys/syscall.S` | Assembler code | Public-Domain (art@openbsd.org) | OK | .
`arch/sparc64/sys/tfork_thread.S` | Assembler code | Simplified-BSD | OK | .
**`arch/sparc64/Symbols.list`** | ? | __Unlicensed!!!__ | Non-Trivial; 54 non empty lines; 54 total lines | .
`arch/sparc64/SYS.h` | C header | Modified-BSD | OK | .
**`arch/sparc64/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 32 total lines | .
`asr/asr_debug.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/asr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 2 commented lines; 11 total lines | .
`asr/getaddrinfo_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/asr_private.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/asr_run.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/asr_utils.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getaddrinfo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/gethostnamadr_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/gethostnamadr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getnameinfo_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getnameinfo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getnetnamadr_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getnetnamadr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/getrrsetbyname_async.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`asr/getrrsetbyname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_search_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_debug.c` | C code | "Unlicensed!!!" | Trivial; 0 code lines + 2 commented lines; 2 total lines | .
`asr/res_init.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_mkquery.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_query.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_send.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/res_send_async.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`asr/sethostent.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`citrus/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 7 total lines | .
`citrus/citrus_ctype.h` | C header | Simplified-BSD | OK | .
`citrus/citrus_none.c` | C code | Simplified-BSD | OK | .
`citrus/citrus_utf8.c` | C code | Simplified-BSD | OK | .
`compat-43/getdtablesize.3` | ? | Modified-BSD | OK | .
`compat-43/creat.3` | ? | Modified-BSD | OK | .
`compat-43/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 10 total lines | .
`compat-43/creat.c` | C code | Modified-BSD | OK | .
`compat-43/sethostid.c` | C code | Modified-BSD | OK | .
`compat-43/getwd.c` | C code | Modified-BSD | OK | .
`compat-43/getdtablesize.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`compat-43/gethostid.3` | ? | Modified-BSD | OK | .
`compat-43/gethostid.c` | C code | Modified-BSD | OK | .
`compat-43/killpg.3` | ? | Modified-BSD | OK | .
`compat-43/killpg.c` | C code | Modified-BSD | OK | .
`compat-43/sigvec.3` | ? | Modified-BSD | OK | .
`compat-43/setpgrp.c` | C code | Modified-BSD | OK | .
`compat-43/sigblock.3` | ? | Modified-BSD | OK | .
`compat-43/sigcompat.c` | C code | Modified-BSD | OK | .
`compat-43/sigpause.3` | ? | Modified-BSD | OK | .
`compat-43/sigsetmask.3` | ? | Modified-BSD | OK | .
`crypt/arc4random_uniform.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypt/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`crypt/arc4random.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypt/arc4random.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypt/arc4random.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypt/blowfish.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypt/bcrypt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypt/cryptutil.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypt/crypt.3` | ? | Modified-BSD | OK | .
`crypt/blowfish.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypt/chacha_private.h` | C header | Public-Domain (D. J. Bernstein) | OK | .
**`crypt/crypt.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 22 total lines | .
`crypt/crypt_checkpass.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`db/btree/bt_open.c` | C code | Modified-BSD | OK | .
`db/btree/bt_get.c` | C code | Modified-BSD | OK | .
`db/btree/bt_close.c` | C code | Modified-BSD | OK | .
`db/btree/bt_conv.c` | C code | Modified-BSD | OK | .
`db/btree/bt_debug.c` | C code | Modified-BSD | OK | .
`db/btree/bt_delete.c` | C code | Modified-BSD | OK | .
`db/btree/bt_search.c` | C code | Modified-BSD | OK | .
`db/btree/bt_put.c` | C code | Modified-BSD | OK | .
`db/btree/bt_overflow.c` | C code | Modified-BSD | OK | .
`db/btree/bt_page.c` | C code | Modified-BSD | OK | .
`db/btree/bt_split.c` | C code | Modified-BSD | OK | .
`db/btree/bt_seq.c` | C code | Modified-BSD | OK | .
`db/btree/btree.h` | C header | Modified-BSD | OK | .
`db/btree/bt_utils.c` | C code | Modified-BSD | OK | .
`db/btree/extern.h` | C header | Modified-BSD | OK | .
`db/db/db.c` | C code | Modified-BSD | OK | .
`db/hash/hash_bigkey.c` | C code | Modified-BSD | OK | .
**`db/hash/README`** | Readme | __Unlicensed!!!__ | Non-Trivial; 59 non empty lines; 68 total lines | .
`db/hash/extern.h` | C header | Modified-BSD | OK | .
`db/hash/hash.c` | C code | Modified-BSD | OK | .
`db/hash/hash.h` | C header | Modified-BSD | OK | .
`db/hash/hash_page.c` | C code | Modified-BSD | OK | .
`db/hash/ndbm.c` | C code | Modified-BSD | OK | .
`db/hash/hash_buf.c` | C code | Modified-BSD | OK | .
`db/hash/hash_func.c` | C code | Modified-BSD | OK | .
`db/hash/hash_log2.c` | C code | Modified-BSD | OK | .
`db/hash/page.h` | C header | Modified-BSD | OK | .
`db/man/btree.3` | ? | Modified-BSD | OK | .
`db/man/dbopen.3` | ? | Modified-BSD | OK | .
`db/man/hash.3` | ? | Modified-BSD | OK | .
`db/man/ndbm.3` | ? | Public-Domain (David Leonard) | OK | .
`db/man/recno.3` | ? | Modified-BSD | OK | .
`db/mpool/README` | Readme | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 8 total lines | .
`db/mpool/mpool.c` | C code | Modified-BSD | OK | .
`db/recno/rec_close.c` | C code | Modified-BSD | OK | .
`db/recno/extern.h` | C header | Modified-BSD | OK | .
`db/recno/rec_open.c` | C code | Modified-BSD | OK | .
`db/recno/recno.h` | C header | Modified-BSD | OK | .
`db/recno/rec_delete.c` | C code | Modified-BSD | OK | .
`db/recno/rec_get.c` | C code | Modified-BSD | OK | .
`db/recno/rec_put.c` | C code | Modified-BSD | OK | .
`db/recno/rec_search.c` | C code | Modified-BSD | OK | .
`db/recno/rec_seq.c` | C code | Modified-BSD | OK | .
`db/recno/rec_utils.c` | C code | Modified-BSD | OK | .
**`db/changelog`** | ? | __Unlicensed!!!__ | Non-Trivial; 85 non empty lines; 105 total lines | .
`db/README` | Readme | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
**`db/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 18 non empty lines; 29 total lines | .
`dlfcn/init.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dlfcn/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 7 total lines | .
`dlfcn/dlfcn_stubs.c` | C code | Simplified-BSD | OK | .
`dlfcn/init.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dlfcn/tib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`gdtoa/gdtoa_fltrnds.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 18 non empty lines; 18 total lines | .
`gdtoa/dmisc.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 11 code lines + 2 commented lines; 18 total lines | .
`gdtoa/dtoa.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/gdtoa.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/gdtoa.h` | C header | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/hd_init.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/gethex.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/gdtoaimp.h` | C header | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/gmisc.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtodg.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/hdtoa.c` | C code | Simplified-BSD | OK | .
`gdtoa/hexnan.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/ldtoa.c` | C code | Simplified-BSD | OK | .
`gdtoa/misc.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/smisc.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtod.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtold.c` | C code | Simplified-BSD | OK | .
`gdtoa/strtof.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/sum.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtorQ.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtord.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/strtorx.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gdtoa/ulp.c` | C code | Lucent (similar to ISC and CMU) | OK | .
`gen/auth_subr.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/alarm.3` | ? | Modified-BSD | OK | .
**`gen/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 52 non empty lines; 57 total lines | .
`gen/__tfork_thread.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/alarm.c` | C code | Modified-BSD | OK | .
`gen/assert.c` | C code | Modified-BSD | OK | .
`gen/closedir.c` | C code | Modified-BSD | OK | .
`gen/clock.3` | ? | Modified-BSD | OK | .
`gen/auth_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`gen/authenticate.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/authenticate.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/basename.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/basename.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/cgetent.3` | ? | Modified-BSD | OK | .
`gen/charclass.h` | C header | Public-Domain (Todd C. Miller <millert@openbsd.org>) | OK | .
`gen/clock.c` | C code | Modified-BSD | OK | .
`gen/devname.3` | ? | Modified-BSD | OK | .
`gen/ctype_.c` | C code | Modified-BSD | OK | .
`gen/daemon.3` | ? | Modified-BSD | OK | .
`gen/clock_getcpuclockid.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/clock_getcpuclockid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/confstr.3` | ? | Modified-BSD | OK | .
`gen/confstr.c` | C code | Modified-BSD | OK | .
`gen/ctermid.3` | ? | Modified-BSD | OK | .
`gen/ctermid.c` | C code | Modified-BSD | OK | .
`gen/daemon.c` | C code | Modified-BSD | OK | .
`gen/dirname.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/dirfd.c` | C code | Public-Domain (Matthew Dempsky) | OK | .
`gen/devname.c` | C code | Modified-BSD | OK | .
`gen/errlist.c` | C code | Modified-BSD | OK | .
`gen/err.3` | ? | Modified-BSD | OK | .
`gen/dirname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/disklabel.c` | C code | Modified-BSD | OK | .
`gen/err.c` | C code | Modified-BSD | OK | .
`gen/errc.c` | C code | Modified-BSD | OK | .
`gen/fdatasync.c` | C code | Public-Domain (Matthew Dempsky) | OK | .
`gen/errno.c` | C code | Public-Domain (Marco S Hyman) | OK | .
`gen/errx.c` | C code | Modified-BSD | OK | .
`gen/exec.c` | C code | Modified-BSD | OK | .
`gen/execv.3` | ? | Modified-BSD | OK | .
`gen/fabs.3` | ? | Modified-BSD | OK | .
`gen/fts_open.3` | ? | Modified-BSD | OK | .
`gen/frexp.3` | ? | Modified-BSD | OK | .
`gen/fnmatch.3` | ? | Modified-BSD | OK | .
`gen/fnmatch.c` | C code | Modified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`gen/fpclassify.3` | ? | Simplified-BSD | OK | .
`gen/fpclassify.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/fpgetmask.3` | ? | Simplified-BSD | OK | .
`gen/frexp.c` | C code | Simplified-BSD | OK | .
`gen/fstab.c` | C code | Modified-BSD | OK | .
`gen/ftok.3` | ? | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`gen/ftok.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`gen/fts.c` | C code | Modified-BSD | OK | .
`gen/getbsize.3` | ? | Modified-BSD | OK | .
`gen/ftw.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/ftw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/getdiskbyname.3` | ? | Modified-BSD | OK | .
`gen/getcap.c` | C code | Modified-BSD | OK | .
`gen/getbsize.c` | C code | Modified-BSD | OK | .
`gen/getcwd.3` | ? | Modified-BSD | OK | .
`gen/getcwd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/initgroups.3` | ? | Modified-BSD | OK | .
`gen/glob.3` | ? | Modified-BSD | OK | .
`gen/getdomainname.3` | ? | Modified-BSD | OK | .
`gen/getdomainname.c` | C code | Modified-BSD | OK | .
`gen/getfsent.3` | ? | Modified-BSD | OK | .
`gen/getgrent.3` | ? | Modified-BSD | OK | .
`gen/getgrent.c` | C code | Modified-BSD | OK | .
`gen/getgrouplist.3` | ? | Modified-BSD | OK | .
`gen/getgrouplist.c` | C code | Modified-BSD | OK | .
`gen/gethostname.3` | ? | Modified-BSD | OK | .
`gen/gethostname.c` | C code | Modified-BSD | OK | .
`gen/getloadavg.3` | ? | Modified-BSD | OK | .
`gen/getloadavg.c` | C code | Modified-BSD | OK | .
`gen/getlogin.c` | C code | Modified-BSD | OK | .
`gen/getmntinfo.3` | ? | Modified-BSD | OK | .
`gen/getmntinfo.c` | C code | Modified-BSD | OK | .
`gen/getnetgrent.3` | ? | Modified-BSD | OK | .
`gen/getnetgrent.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/getpagesize.3` | ? | Modified-BSD | OK | .
`gen/getpagesize.c` | C code | Modified-BSD | OK | .
`gen/getpass.3` | ? | Modified-BSD | OK | .
`gen/getprogname.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/getprogname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/getpwent.3` | ? | Modified-BSD | OK | .
`gen/getpwent.c` | C code | Modified-BSD | OK | .
`gen/getpwnam.3` | ? | Modified-BSD | OK | .
`gen/getttyent.3` | ? | Modified-BSD | OK | .
`gen/getttyent.c` | C code | Modified-BSD | OK | .
`gen/getusershell.3` | ? | Modified-BSD | OK | .
`gen/getusershell.c` | C code | Modified-BSD | OK | .
`gen/glob.c` | C code | Modified-BSD | OK | .
`gen/isblank.3` | ? | Modified-BSD | OK | .
`gen/isatty.c` | C code | Modified-BSD | OK | .
`gen/initgroups.c` | C code | Modified-BSD | OK | .
`gen/isalnum.3` | ? | Modified-BSD | OK | .
`gen/isalpha.3` | ? | Modified-BSD | OK | .
`gen/isascii.3` | ? | Modified-BSD | OK | .
`gen/islower.3` | ? | Modified-BSD | OK | .
`gen/isinf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/iscntrl.3` | ? | Modified-BSD | OK | .
`gen/isctype.c` | C code | Modified-BSD | OK | .
`gen/isdigit.3` | ? | Modified-BSD | OK | .
`gen/isfdtype.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/isfdtype.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/isfinite.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/isgraph.3` | ? | Modified-BSD | OK | .
`gen/isgreater.3` | ? | Simplified-BSD | OK | .
`gen/isnormal.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/isnan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/login_cap.3` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/ldexp.3` | ? | Modified-BSD | OK | .
`gen/isprint.3` | ? | Modified-BSD | OK | .
`gen/ispunct.3` | ? | Modified-BSD | OK | .
`gen/isspace.3` | ? | Modified-BSD | OK | .
`gen/isupper.3` | ? | Modified-BSD | OK | .
`gen/isxdigit.3` | ? | Modified-BSD | OK | .
`gen/ldexp.c` | C code | SunPro | OK | .
`gen/lockf.3` | ? | Simplified-BSD | OK | .
`gen/lockf.c` | C code | Simplified-BSD | OK | .
`gen/opendir.3` | ? | Modified-BSD | OK | .
`gen/modf.3` | ? | Modified-BSD | OK | .
`gen/login_cap.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`gen/modf.c` | C code | CMU (similar to ISC) | OK | .
`gen/nftw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/nice.3` | ? | Modified-BSD | OK | .
`gen/nice.c` | C code | Modified-BSD | OK | .
`gen/nlist.3` | ? | Modified-BSD | OK | .
`gen/nlist.c` | C code | Modified-BSD | OK | .
`gen/posix_spawn.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/pause.3` | ? | Modified-BSD | OK | .
`gen/opendir.c` | C code | Modified-BSD | OK | .
`gen/pause.c` | C code | Public-Domain (Todd C. Miller) | OK | .
`gen/popen.3` | ? | Modified-BSD | OK | .
`gen/popen.c` | C code | Modified-BSD | OK | .
`gen/posix_spawnattr_init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/posix_spawn.c` | C code | Simplified-BSD | OK | .
`gen/pwcache.c` | C code | Modified-BSD | OK | .
`gen/pw_dup.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/psignal.3` | ? | Modified-BSD | OK | .
`gen/pw_dup.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/posix_spawn_file_actions_addopen.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/posix_spawn_file_actions_init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/posix_spawnattr_getflags.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/posix_spawnattr_getpgroup.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/psignal.c` | C code | Modified-BSD | OK | .
`gen/readdir.c` | C code | Modified-BSD | OK | .
`gen/raise.3` | ? | Modified-BSD | OK | .
`gen/raise.c` | C code | Modified-BSD | OK | .
`gen/setmode.3` | ? | Modified-BSD | OK | .
`gen/setjmp.3` | ? | Modified-BSD | OK | .
`gen/readdir_r.c` | C code | Modified-BSD | OK | .
`gen/readpassphrase.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/readpassphrase.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/rewinddir.c` | C code | Modified-BSD | OK | .
`gen/scandir.3` | ? | Modified-BSD | OK | .
`gen/scandir.c` | C code | Modified-BSD | OK | .
`gen/seekdir.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/setdomainname.c` | C code | Modified-BSD | OK | .
`gen/sethostname.c` | C code | Modified-BSD | OK | .
`gen/signame.c` | C code | Modified-BSD | OK | .
`gen/signal.3` | ? | Modified-BSD | OK | .
`gen/setmode.c` | C code | Modified-BSD | OK | .
`gen/setproctitle.3` | ? | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`gen/setproctitle.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`gen/setprogname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/shm_open.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/shm_open.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/sigaddset.3` | ? | Modified-BSD | OK | .
`gen/siginterrupt.3` | ? | Modified-BSD | OK | .
`gen/siginterrupt.c` | C code | Modified-BSD | OK | .
`gen/siglist.c` | C code | Modified-BSD | OK | .
`gen/signal.c` | C code | Modified-BSD | OK | .
`gen/statvfs.3` | ? | Modified-BSD | OK | .
`gen/sleep.3` | ? | Modified-BSD | OK | .
`gen/signbit.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/sigsetops.c` | C code | Modified-BSD | OK | .
`gen/sigwait.3` | ? | Public-Domain (David Leonard) | OK | .
`gen/sigwait.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/sleep.c` | C code | Modified-BSD | OK | .
`gen/syslog_r.c` | C code | Modified-BSD | OK | .
`gen/syslog.3` | ? | Modified-BSD | OK | .
`gen/statvfs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`gen/strtofflags.3` | ? | Modified-BSD | OK | .
`gen/strtofflags.c` | C code | Modified-BSD | OK | .
`gen/sysconf.3` | ? | Modified-BSD | OK | .
`gen/sysconf.c` | C code | Modified-BSD | OK | .
`gen/syslog.c` | C code | Modified-BSD | OK | .
`gen/timespec_get.3` | ? | Simplified-BSD | OK | .
`gen/time.3` | ? | Modified-BSD | OK | .
`gen/telldir.c` | C code | Modified-BSD | OK | .
`gen/telldir.h` | C header | Modified-BSD | OK | .
`gen/time.c` | C code | Modified-BSD | OK | .
`gen/times.3` | ? | Modified-BSD | OK | .
`gen/times.c` | C code | Modified-BSD | OK | .
`gen/ttyname.3` | ? | Modified-BSD | OK | .
`gen/tree.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`gen/timespec_get.c` | C code | Simplified-BSD | OK | .
`gen/toascii.3` | ? | Modified-BSD | OK | .
`gen/tolower.3` | ? | Modified-BSD | OK | .
`gen/tolower_.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`gen/toupper.3` | ? | Modified-BSD | OK | .
`gen/toupper_.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`gen/user_from_uid.3` | ? | Modified-BSD | OK | .
`gen/ualarm.3` | ? | Modified-BSD | OK | .
`gen/ttyname.c` | C code | Modified-BSD | OK | .
`gen/ttyslot.c` | C code | Modified-BSD | OK | .
`gen/ualarm.c` | C code | Modified-BSD | OK | .
`gen/uname.3` | ? | Modified-BSD | OK | .
`gen/uname.c` | C code | Modified-BSD | OK | .
`gen/unvis.3` | ? | Modified-BSD | OK | .
`gen/unvis.c` | C code | Modified-BSD | OK | .
`gen/waitpid.c` | C code | Modified-BSD | OK | .
`gen/usleep.3` | ? | Modified-BSD | OK | .
`gen/usleep.c` | C code | Modified-BSD | OK | .
`gen/utime.3` | ? | Modified-BSD | OK | .
`gen/utime.c` | C code | Modified-BSD | OK | .
`gen/valloc.3` | ? | Modified-BSD | OK | .
`gen/valloc.c` | C code | Modified-BSD | OK | .
`gen/verr.c` | C code | Modified-BSD | OK | .
`gen/verrc.c` | C code | Modified-BSD | OK | .
`gen/verrx.c` | C code | Modified-BSD | OK | .
`gen/vis.3` | ? | Modified-BSD | OK | .
`gen/vis.c` | C code | Modified-BSD | OK | .
`gen/vwarn.c` | C code | Modified-BSD | OK | .
`gen/vwarnc.c` | C code | Modified-BSD | OK | .
`gen/vwarnx.c` | C code | Modified-BSD | OK | .
`gen/wait.c` | C code | Modified-BSD | OK | .
`gen/wait3.c` | C code | Modified-BSD | OK | .
`gen/warn.c` | C code | Modified-BSD | OK | .
`gen/warnc.c` | C code | Modified-BSD | OK | .
`gen/warnx.c` | C code | Modified-BSD | OK | .
`gmon/moncontrol.3` | ? | Modified-BSD | OK | .
`gmon/gmon.c` | C code | Modified-BSD | OK | .
`gmon/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 3 commented lines; 15 total lines | .
`gmon/mcount.c` | C code | Modified-BSD | OK | .
`hash/siphash.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`hash/helper.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`hash/MD5Init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
**`hash/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 46 total lines | .
`hash/RMD160Init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`hash/SHA1Init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`hash/SHA256Init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`hash/SipHash24_Init.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`hash/md5.c` | C code | Public-Domain (Colin Plumb) | OK | .
`hash/rmd160.c` | C code | Simplified-BSD | OK | .
`hash/sha1.c` | C code | Public-Domain (Steve Reid) | OK | .
`hash/sha2.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`hidden/arpa/nameser.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/arpa/inet.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/machine/spinlock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/machine/sysarch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/net/if.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/net/if_dl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/netinet/in.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/netinet/if_ether.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/auth_unix.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/auth.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/pmap_clnt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/clnt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/rpc_msg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/rpc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/pmap_prot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/pmap_rmt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/svc_auth.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/svc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpc/xdr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpcsvc/yp.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rpcsvc/ypclnt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/resource.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/futex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/ioctl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/mman.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/mount.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/msg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/ptrace.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/select.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/sem.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/socket.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/stat.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/sysctl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/time.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/tree.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/uio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sys/wait.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/bsd_auth.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/asr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/assert.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/blf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ifaddrs.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ctype.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/db.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/dirent.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/dlfcn.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/err.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/errno.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/fcntl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/float.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/fstab.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/fts.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ftw.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/getopt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/grp.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/icdb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ieeefp.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/login_cap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/locale.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/inttypes.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/langinfo.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/link_elf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/netgroup.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/math.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/md5.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ndbm.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/netdb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/pthread.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/poll.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/nl_types.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/readpassphrase.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/pwd.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/siphash.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/regex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/resolv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/rmd160.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sched.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/search.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sha1.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/sha2.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/signal.h` | C header | Original-ISC ("and" is used instead of "and/or") with Added CR for Hyperbola | OK | .
`hidden/termios.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/spawn.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/softfloat.h` | C header | Public-Domain (notified in file) | OK | .
`hidden/stdio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/stdlib.h` | C header | Modified-BSD | OK | .
`hidden/string.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/syslog.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/tib.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/time.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/ttyent.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/unistd.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/utime.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/uuid.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/vis.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/wchar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`hidden/wctype.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/ctype_private.h` | C header | Public-Domain (Marc Espie) | OK | .
**`include/DETAILS`** | ? | __Unlicensed!!!__ | Non-Trivial; 106 non empty lines; 132 total lines | .
**`include/README`** | Readme | __Unlicensed!!!__ | Non-Trivial; 77 non empty lines; 96 total lines | .
`include/atfork.h` | C header | Custom-Simplified-BSD (second clause is an "endorsement clause") | OK | .
`include/cancel.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/namespace.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/mpool.h` | C header | Modified-BSD | OK | .
`include/localedef.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/thread_private.h` | C header | Public-Domain (Marco S Hyman) | OK | .
`locale/_CurrentRuneLocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`locale/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 21 non empty lines; 25 total lines | .
`locale/duplocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/btowc.3` | ? | Simplified-BSD | OK | .
`locale/___runetype_mb.c` | C code | Modified-BSD | OK | .
`locale/__mb_cur_max.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/_def_messages.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/_def_monetary.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/_def_numeric.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/_def_time.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/_get_locname.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/_wcstod.h` | C header | Simplified-BSD | OK | .
`locale/_wcstol.h` | C header | Modified-BSD | OK | .
`locale/_wcstoul.h` | C header | Modified-BSD | OK | .
`locale/_wctrans.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`locale/_wctrans_local.h` | C header | Simplified-BSD | OK | .
`locale/btowc.c` | C code | Simplified-BSD | OK | .
`locale/mbrtowc.3` | ? | Simplified-BSD | OK | .
`locale/mblen.3` | ? | Simplified-BSD | OK | .
`locale/freelocale.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`locale/isctype_l.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/iswalnum.3` | ? | Modified-BSD | OK | .
`locale/iswctype.3` | ? | Simplified-BSD | OK | .
`locale/iswctype.c` | C code | Modified-BSD | OK | .
`locale/iswctype_l.c` | C code | Modified-BSD | OK | .
`locale/localeconv.3` | ? | Modified-BSD | OK | .
`locale/localeconv.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/mblen.c` | C code | Simplified-BSD | OK | .
`locale/mbrlen.3` | ? | Simplified-BSD | OK | .
`locale/mbrlen.c` | C code | Simplified-BSD | OK | .
`locale/newlocale.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/mbtowc.3` | ? | Simplified-BSD | OK | .
`locale/mbsinit.3` | ? | Simplified-BSD | OK | .
`locale/mbsrtowcs.3` | ? | Simplified-BSD | OK | .
`locale/mbstowcs.3` | ? | Simplified-BSD | OK | .
`locale/mbstowcs.c` | C code | Simplified-BSD | OK | .
`locale/mbtowc.c` | C code | Simplified-BSD | OK | .
`locale/rune_local.h` | C header | Simplified-BSD | OK | .
`locale/rune.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`locale/rune.h` | C header | Modified-BSD | OK | .
`locale/multibyte_citrus.c` | C code | Simplified-BSD | OK | .
`locale/newlocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/nl_langinfo.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/nl_langinfo.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`locale/nl_langinfo_l.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/wcstoimax.c` | C code | "Unlicensed!!!" | Trivial; 13 code lines + 2 commented lines; 19 total lines | .
`locale/wcstod.3` | ? | Simplified-BSD | OK | .
`locale/runeglue.c` | C code | Simplified-BSD | OK | .
`locale/runetable.c` | C code | Modified-BSD | OK | .
`locale/runetype.h` | C header | Modified-BSD | OK | .
`locale/setlocale.3` | ? | Modified-BSD | OK | .
`locale/setlocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/towctrans.3` | ? | Simplified-BSD | OK | .
`locale/towlower.3` | ? | Modified-BSD | OK | .
`locale/uselocale.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/uselocale.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`locale/wcrtomb.3` | ? | Simplified-BSD | OK | .
`locale/wcscoll.3` | ? | Modified-BSD | OK | .
`locale/wcscoll.c` | C code | Simplified-BSD | OK | .
`locale/wcscoll_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`locale/wcsrtombs.3` | ? | Simplified-BSD | OK | .
`locale/wcstod.c` | C code | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 13 total lines | .
`locale/wcstof.c` | C code | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 13 total lines | .
`locale/wcstold.c` | C code | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented lines; 13 total lines | .
`locale/wcstol.3` | ? | Simplified-BSD | OK | .
`locale/wcstol.c` | C code | "Unlicensed!!!" | Trivial; 12 code lines + 2 commented lines; 18 total lines | .
`locale/wctoint.h` | C header | Simplified-BSD | OK | .
`locale/wctob.3` | ? | Simplified-BSD | OK | .
`locale/wcstoll.c` | C code | "Unlicensed!!!" | 12 code lines + 2 commented lines; 18 total lines | .
`locale/wcstombs.3` | ? | Simplified-BSD | OK | .
`locale/wcstombs.c` | C code | Simplified-BSD | OK | .
`locale/wcstoul.c` | C code | "Unlicensed!!!" | Trivial; 11 code lines + 2 commented lines; 17 total lines | .
`locale/wcstoull.c` | C code | "Unlicensed!!!" | Trivial; 11 code lines + 2 commented lines; 17 total lines | .
`locale/wcstoumax.c` | C code | "Unlicensed!!!" | Trivial; 12 code lines + 2 commented lines; 18 total lines | .
`locale/wcsxfrm.3` | ? | Modified-BSD | OK | .
`locale/wcsxfrm.c` | C code | Modified-BSD | OK | .
`locale/wcsxfrm_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`locale/wctob.c` | C code | Simplified-BSD | OK | .
`locale/wctrans.3` | ? | Simplified-BSD | OK | .
`locale/wctomb.3` | ? | Simplified-BSD | OK | .
`locale/wctomb.c` | C code | Simplified-BSD | OK | .
`locale/wcwidth.3` | ? | Simplified-BSD | OK | .
`locale/wctype.3` | ? | Simplified-BSD | OK | .
`net/ether_aton.3` | ? | Public-Domain (roland@frob.com) | OK | .
`net/base64.c` | C code | Original-ISC ("and" is used instead of "and/or") + IPL-1.0 | OK (Combined Licenses) | .
**`net/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 36 total lines | .
`net/freeaddrinfo.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`net/ethers.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/htobe64.3` | ? | Modified-BSD | OK | .
`net/herror.c` | C code | Modified-BSD | OK | .
`net/gai_strerror.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/gai_strerror.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`net/getaddrinfo.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/gethostbyname.3` | ? | Modified-BSD | OK | .
`net/getifaddrs.3` | ? | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`net/getifaddrs.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`net/getnameinfo.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/getnetent.3` | ? | Modified-BSD | OK | .
`net/getnetent.c` | C code | Modified-BSD | OK | .
`net/getpeereid.3` | ? | Modified-BSD | OK | .
`net/getpeereid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/getproto.c` | C code | Modified-BSD | OK | .
`net/getprotoent.3` | ? | Modified-BSD | OK | .
`net/getprotoent.c` | C code | Modified-BSD | OK | .
`net/getprotoname.c` | C code | Modified-BSD | OK | .
`net/getrrsetbyname.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/getservbyname.c` | C code | Modified-BSD | OK | .
`net/getservbyport.c` | C code | Modified-BSD | OK | .
`net/getservent.3` | ? | Modified-BSD | OK | .
`net/getservent.c` | C code | Modified-BSD | OK | .
`net/if_indextoname.3` | ? | Modified-BSD | OK | .
`net/htonl.3` | ? | Modified-BSD | OK | .
`net/htonl.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`net/htons.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`net/inet6_rth_space.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/if_indextoname.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`net/if_nameindex.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`net/if_nametoindex.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`net/inet6_opt_init.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/link_ntoa.3` | ? | Modified-BSD | OK | .
`net/ip6opt.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/inet_addr.3` | ? | Modified-BSD | OK | .
`net/inet_addr.c` | C code | Modified-BSD | OK | .
`net/inet_lnaof.3` | ? | Modified-BSD | OK | .
`net/inet_lnaof.c` | C code | Modified-BSD | OK | .
`net/inet_makeaddr.c` | C code | Modified-BSD | OK | .
`net/inet_net_ntop.3` | ? | Simplified-BSD | OK | .
`net/inet_net_ntop.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/inet_net_pton.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/inet_neta.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/inet_netof.c` | C code | Modified-BSD | OK | .
`net/inet_network.c` | C code | Modified-BSD | OK | .
`net/inet_ntoa.c` | C code | Modified-BSD | OK | .
`net/inet_ntop.3` | ? | Modified-BSD | OK | .
`net/inet_ntop.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/inet_pton.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/res_comp.c` | C code | Modified-BSD | OK | .
`net/ntohl.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`net/linkaddr.c` | C code | Modified-BSD | OK | .
`net/ntohs.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`net/rcmd.3` | ? | Modified-BSD | OK | .
`net/rcmd.c` | C code | Modified-BSD | OK | .
`net/rcmdsh.3` | ? | Modified-BSD | OK | .
`net/rcmdsh.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/recv.c` | C code | Modified-BSD | OK | .
`net/ruserok.c` | C code | Modified-BSD | OK | .
`net/rthdr.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/res_data.c` | C code | Modified-BSD | OK | .
`net/res_debug_syms.c` | C code | Modified-BSD | OK | .
`net/res_init.3` | ? | Modified-BSD | OK | .
`net/res_random.c` | C code | Simplified-BSD | OK | .
`net/rresvport.c` | C code | Modified-BSD | OK | .
`net/sockatmark.3` | ? | Simplified-BSD | OK | .
`net/send.c` | C code | Modified-BSD | OK | .
`net/vars6.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`net/sockatmark.c` | C code | Simplified-BSD | OK | .
`nls/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented lines; 6 total lines | .
`nls/catclose.3` | ? | Public-Domain (J.T. Conklin) | OK | .
`nls/catclose.c` | C code | Simplified-BSD | OK | .
`nls/catgets.3` | ? | Public-Domain (J.T. Conklin) | OK | .
`nls/catgets.c` | C code | Simplified-BSD | OK | .
`nls/catopen.3` | ? | Public-Domain (J.T. Conklin) | OK | .
`nls/catopen.c` | C code | Simplified-BSD | OK | .
`quad/ashldi3.c` | C code | Modified-BSD | OK | .
`quad/adddi3.c` | C code | Modified-BSD | OK | .
`quad/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 2 commented lines; 10 total lines | .
`quad/anddi3.c` | C code | Modified-BSD | OK | .
`quad/fixdfdi.c` | C code | Modified-BSD | OK | .
`quad/cmpdi2.c` | C code | Modified-BSD | OK | .
`quad/ashrdi3.c` | C code | Modified-BSD | OK | .
`quad/divdi3.c` | C code | Modified-BSD | OK | .
`quad/lshldi3.c` | C code | Modified-BSD | OK | .
`quad/iordi3.c` | C code | Modified-BSD | OK | .
`quad/fixsfdi.c` | C code | Modified-BSD | OK | .
`quad/fixunsdfdi.c` | C code | Modified-BSD | OK | .
`quad/fixunssfdi.c` | C code | Modified-BSD | OK | .
`quad/floatdidf.c` | C code | Modified-BSD | OK | .
`quad/floatdisf.c` | C code | Modified-BSD | OK | .
`quad/floatunsdidf.c` | C code | Modified-BSD | OK | .
`quad/qdivrem.c` | C code | Modified-BSD | OK | .
`quad/moddi3.c` | C code | Modified-BSD | OK | .
`quad/lshrdi3.c` | C code | Modified-BSD | OK | .
`quad/muldi3.c` | C code | Modified-BSD | OK | .
`quad/negdi2.c` | C code | Modified-BSD | OK | .
`quad/notdi2.c` | C code | Modified-BSD | OK | .
`quad/ucmpdi2.c` | C code | Modified-BSD | OK | .
`quad/quad.h` | C header | Modified-BSD | OK | .
`quad/subdi3.c` | C code | Modified-BSD | OK | .
`quad/xordi3.c` | C code | Modified-BSD | OK | .
`quad/udivdi3.c` | C code | Modified-BSD | OK | .
`quad/umoddi3.c` | C code | Modified-BSD | OK | .
`regex/re_format.7` | ? | Modified-BSD | OK | .
`regex/cclass.h` | C header | Modified-BSD | OK | .
`regex/COPYRIGHT` | License file | Spencer-94 + Modified-BSD | OK (Combined Licenses) | .
`regex/Makefile.inc` | Makefile | Spencer-94 + Modified-BSD | OK (Combined Licenses) | .
`regex/cname.h` | C header | Modified-BSD | OK | .
`regex/engine.c` | C code | Modified-BSD | OK | .
`regex/regexec.c` | C code | Modified-BSD | OK | .
`regex/regex.3` | ? | Modified-BSD | OK | .
`regex/regcomp.c` | C code | Modified-BSD | OK | .
`regex/regerror.c` | C code | Modified-BSD | OK | .
`regex/regex2.h` | C header | Modified-BSD | OK | .
`regex/utils.h` | C header | Modified-BSD | OK | .
`regex/regfree.c` | C code | Modified-BSD | OK | .
`rpc/auth_none.c` | C code | Modified-BSD |  OK | .
**`rpc/README`** | Readme | __Unlicensed!!!__ | Non-Trivial; 185 non empty lines; 233 total lines | .
`rpc/DISCLAIMER` | DISCLAIMER | Modified-BSD |  OK | .
`rpc/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 2 commented lines; 15 total lines | .
`rpc/authnone_create.3` | ? | Modified-BSD |  OK | .
`rpc/auth_unix.c` | C code | Modified-BSD |  OK | said as very weak cause no encryption
`rpc/rpc_callmsg.c` | C code | Modified-BSD |  OK | .
`rpc/rpc.3` | ? | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`rpc/authunix_prot.c` | C code | Modified-BSD |  OK | .
`rpc/bindresvport.3` | ? | Modified-BSD |  OK | .
`rpc/bindresvport.c` | C code | Simplified-BSD | OK | .
`rpc/clnt_generic.c` | C code | Modified-BSD |  OK | .
`rpc/clnt_perror.c` | C code | Modified-BSD |  OK | .
`rpc/clnt_raw.c` | C code | Modified-BSD |  OK | .
`rpc/clnt_simple.c` | C code | Modified-BSD |  OK | .
`rpc/clnt_tcp.c` | C code | Modified-BSD |  OK | .
`rpc/clnt_udp.c` | C code | Modified-BSD |  OK | .
`rpc/get_myaddress.c` | C code | Modified-BSD |  OK | .
`rpc/getrpcent.3` | ? | Modified-BSD |  OK | .
`rpc/getrpcent.c` | C code | Modified-BSD |  OK | .
`rpc/getrpcport.3` | ? | Modified-BSD |  OK | .
`rpc/getrpcport.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_clnt.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_getmaps.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_getport.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_prot.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_prot2.c` | C code | Modified-BSD |  OK | .
`rpc/pmap_rmt.c` | C code | Modified-BSD |  OK | .
`rpc/svc_auth.c` | C code | Modified-BSD |  OK | .
`rpc/svc.c` | C code | Modified-BSD |  OK | .
`rpc/rpc_commondata.c` | C code | Modified-BSD |  OK | .
`rpc/rpc_prot.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_array.c` | C code | Modified-BSD |  OK | .
`rpc/xdr.3` | ? | Modified-BSD |  OK | .
`rpc/svc_auth_unix.c` | C code | Modified-BSD |  OK | .
`rpc/svc_raw.c` | C code | Modified-BSD |  OK | .
`rpc/svc_run.c` | C code | Modified-BSD |  OK | .
`rpc/svc_simple.c` | C code | Modified-BSD |  OK | .
`rpc/svc_tcp.c` | C code | Modified-BSD |  OK | .
`rpc/svc_udp.c` | C code | Modified-BSD |  OK | .
`rpc/xdr.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_float.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_mem.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_rec.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_reference.c` | C code | Modified-BSD |  OK | .
`rpc/xdr_stdio.c` | C code | Modified-BSD |  OK | .
`softfloat/fpgetmask.c` | C code | Simplified-BSD | OK | .
`softfloat/eqdf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/eqsf2.c` | C code | Public-Domain (Ben Harris) | OK | .
**`softfloat/softfloat.c`** | C code | __Nonfree-License!!!__ (John R. Hauser) | __Nonfree!!!__ [it contains restrictions to use the code for any purpose] | .
`softfloat/gedf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/fpgetround.c` | C code | Simplified-BSD | OK | .
`softfloat/fpgetsticky.c` | C code | Simplified-BSD | OK | .
`softfloat/fpsetmask.c` | C code | Simplified-BSD | OK | .
`softfloat/fpsetround.c` | C code | Simplified-BSD | OK | .
`softfloat/fpsetsticky.c` | C code | Simplified-BSD | OK | .
`softfloat/gesf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/gtdf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/gtsf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/ledf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/lesf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/ltdf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/ltsf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/milieu.h` | C header | "Unlicensed!!!" | Trivial; 5 code lines + 0 commented lines; 5 total lines | .
`softfloat/nedf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/negdf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/negsf2.c` | C code | Public-Domain (Ben Harris) | OK | .
`softfloat/nesf2.c` | C code | Public-Domain (Ben Harris) | OK | .
**`softfloat/softfloat-for-gcc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 62 non empty lines; 66 total lines | .
**`softfloat/softfloat-macros.h`** | C header | __Nonfree-License!!!__ (John R. Hauser) | __Nonfree!!!__ [it contains restrictions to use the code for any purpose] | .
**`softfloat/softfloat-specialize.h`** | C header | __Nonfree-License!!!__ (John R. Hauser) | __Nonfree!!!__ [it contains restrictions to use the code for any purpose] | .
**`softfloat/softfloat.h`** | C header | __Nonfree-License!!!__ (John R. Hauser) | __Nonfree!!!__ [it contains restrictions to use the code for any purpose] | .
**`softfloat/timesoftfloat.c`** | C code | __Nonfree-License!!!__ (John R. Hauser) | __Nonfree!!!__ [it contains restrictions to use the code for any purpose] | .
`softfloat/unorddf2.c` | C code | Public-Domain (Richard Earnshaw) | OK | .
`softfloat/unordsf2.c` | C code | Public-Domain (Richard Earnshaw) | OK | .
`stdio/dprintf.c` | C code | Simplified-BSD | OK | .
`stdio/clrerr.c` | C code | Modified-BSD | OK | .
**`stdio/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 24 non empty lines; 28 total lines | .
`stdio/asprintf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/fgetpos.c` | C code | Modified-BSD | OK | .
`stdio/fclose.3` | ? | Modified-BSD | OK | .
`stdio/fclose.c` | C code | Modified-BSD | OK | .
`stdio/fdopen.c` | C code | Modified-BSD | OK | .
`stdio/feof.c` | C code | Modified-BSD | OK | .
`stdio/ferror.3` | ? | Modified-BSD | OK | .
`stdio/ferror.c` | C code | Modified-BSD | OK | .
`stdio/fflush.3` | ? | Modified-BSD | OK | .
`stdio/fflush.c` | C code | Modified-BSD | OK | .
`stdio/fgetc.c` | C code | Modified-BSD | OK | .
`stdio/fgetln.3` | ? | Modified-BSD | OK | .
`stdio/fgetln.c` | C code | Modified-BSD | OK | .
`stdio/fgetwln.3` | ? | Modified-BSD | OK | .
`stdio/fgets.3` | ? | Modified-BSD | OK | .
`stdio/fgets.c` | C code | Modified-BSD | OK | .
`stdio/fgetwc.c` | C code | Simplified-BSD | OK | .
`stdio/fileext.h` | C header | Simplified-BSD | OK | .
`stdio/fgetws.3` | ? | Modified-BSD | OK | .
`stdio/fgetwln.c` | C code | Simplified-BSD | OK | .
`stdio/fgetws.c` | C code | Simplified-BSD | OK | .
`stdio/floatio.h` | C header | Modified-BSD | OK | .
`stdio/fileno.c` | C code | Modified-BSD | OK | .
`stdio/findfp.c` | C code | Modified-BSD | OK | .
`stdio/flags.c` | C code | Modified-BSD | OK | .
`stdio/fprintf.c` | C code | Modified-BSD | OK | .
`stdio/fopen.3` | ? | Modified-BSD | OK | .
**`stdio/flockfile.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 29 total lines | .
`stdio/fmemopen.3` | ? | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`stdio/fmemopen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/fopen.c` | C code | Modified-BSD | OK | .
`stdio/freopen.c` | C code | Modified-BSD | OK | .
`stdio/fpurge.c` | C code | Modified-BSD | OK | .
`stdio/fputc.c` | C code | Modified-BSD | OK | .
`stdio/fputs.3` | ? | Modified-BSD | OK | .
`stdio/fputs.c` | C code | Modified-BSD | OK | .
`stdio/fputwc.c` | C code | Simplified-BSD | OK | .
`stdio/fputws.3` | ? | Modified-BSD | OK | .
`stdio/fputws.c` | C code | Simplified-BSD | OK | .
`stdio/fread.3` | ? | Modified-BSD | OK | .
`stdio/fread.c` | C code | Modified-BSD | OK | .
`stdio/fsetpos.c` | C code | Modified-BSD | OK | .
`stdio/fscanf.c` | C code | Modified-BSD | OK | .
`stdio/fseek.3` | ? | Modified-BSD | OK | .
`stdio/fseek.c` | C code | Modified-BSD | OK | .
`stdio/funopen.3` | ? | Modified-BSD | OK | .
`stdio/ftell.c` | C code | Modified-BSD | OK | .
`stdio/fwprintf.c` | C code | Modified-BSD | OK | .
`stdio/fwalk.c` | C code | Modified-BSD | OK | .
`stdio/funopen.c` | C code | Modified-BSD | OK | .
`stdio/fvwrite.c` | C code | Modified-BSD | OK | .
`stdio/fvwrite.h` | C header | Modified-BSD | OK | .
`stdio/fwide.3` | ? | Simplified-BSD | OK | .
`stdio/fwide.c` | C code | Simplified-BSD | OK | .
`stdio/fwscanf.c` | C code | Simplified-BSD | OK | .
`stdio/fwrite.c` | C code | Modified-BSD | OK | .
`stdio/getchar.c` | C code | Modified-BSD | OK | .
`stdio/getc.3` | ? | Modified-BSD | OK | .
`stdio/getc.c` | C code | Modified-BSD | OK | .
`stdio/getwchar.c` | C code | Simplified-BSD | OK | .
`stdio/getw.c` | C code | Modified-BSD | OK | .
`stdio/getdelim.3` | ? | Simplified-BSD | OK | .
`stdio/getdelim.c` | C code | Simplified-BSD | OK | .
`stdio/getline.c` | C code | Simplified-BSD | OK | .
`stdio/getwc.3` | ? | Modified-BSD | OK | .
`stdio/getwc.c` | C code | Simplified-BSD | OK | .
`stdio/makebuf.c` | C code | Modified-BSD | OK | .
`stdio/glue.h` | C header | Modified-BSD | OK | .
`stdio/local.h` | C header | Modified-BSD | OK | .
`stdio/open_memstream.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/mktemp.3` | ? | Modified-BSD | OK | .
`stdio/mktemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/open_wmemstream.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/open_memstream.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/putchar.c` | C code | Modified-BSD | OK | .
`stdio/perror.3` | ? | Modified-BSD | OK | .
`stdio/perror.c` | C code | Modified-BSD | OK | .
`stdio/printf.3` | ? | Modified-BSD | OK | .
`stdio/printf.c` | C code | Modified-BSD | OK | .
`stdio/putc.3` | ? | Modified-BSD | OK | .
`stdio/putc.c` | C code | Modified-BSD | OK | .
`stdio/putwchar.c` | C code | Simplified-BSD | OK | .
`stdio/puts.c` | C code | Modified-BSD | OK | .
`stdio/putw.c` | C code | Modified-BSD | OK | .
`stdio/putwc.3` | ? | Modified-BSD | OK | .
`stdio/putwc.c` | C code | Simplified-BSD | OK | .
`stdio/setbuffer.c` | C code | Modified-BSD | OK | .
`stdio/refill.c` | C code | Modified-BSD | OK | .
`stdio/remove.3` | ? | Modified-BSD | OK | .
`stdio/remove.c` | C code | Modified-BSD | OK | .
`stdio/rewind.c` | C code | Modified-BSD | OK | .
`stdio/rget.c` | C code | Modified-BSD | OK | .
`stdio/scanf.3` | ? | Modified-BSD | OK | .
`stdio/scanf.c` | C code | Modified-BSD | OK | .
`stdio/setbuf.3` | ? | Modified-BSD | OK | .
`stdio/setbuf.c` | C code | Modified-BSD | OK | .
`stdio/swprintf.c` | C code | Simplified-BSD | OK | .
`stdio/sscanf.c` | C code | Modified-BSD | OK | .
`stdio/setvbuf.3` | ? | Modified-BSD | OK | .
`stdio/setvbuf.c` | C code | Modified-BSD | OK | .
`stdio/snprintf.c` | C code | Modified-BSD | OK | .
`stdio/sprintf.c` | C code | Modified-BSD | OK | .
`stdio/stdio.3` | ? | Modified-BSD | OK | .
`stdio/stdio.c` | C code | Modified-BSD | OK | .
`stdio/ungetwc.3` | ? | Modified-BSD | OK | .
`stdio/tmpnam.3` | ? | Modified-BSD | OK | .
`stdio/swscanf.c` | C code | Simplified-BSD | OK | .
`stdio/tempnam.c` | C code | Modified-BSD | OK | .
`stdio/tmpfile.c` | C code | Modified-BSD | OK | .
`stdio/tmpnam.c` | C code | Modified-BSD | OK | .
`stdio/ungetc.3` | ? | Modified-BSD | OK | .
`stdio/ungetc.c` | C code | Modified-BSD | OK | .
`stdio/vsnprintf.c` | C code | Modified-BSD | OK | .
`stdio/vscanf.c` | C code | Modified-BSD | OK | .
`stdio/ungetwc.c` | C code | Simplified-BSD | OK | .
`stdio/vasprintf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdio/vdprintf.c` | C code | Simplified-BSD | OK | .
`stdio/vfprintf.c` | C code | Modified-BSD | OK | .
`stdio/vfscanf.c` | C code | Modified-BSD | OK | .
`stdio/vfwprintf.c` | C code | Modified-BSD | OK | .
`stdio/vfwscanf.c` | C code | Modified-BSD | OK | .
`stdio/vprintf.c` | C code | Modified-BSD | OK | .
`stdio/wprintf.3` | ? | Modified-BSD | OK | .
`stdio/wbuf.c` | C code | Modified-BSD | OK | .
`stdio/vsprintf.c` | C code | Modified-BSD | OK | .
`stdio/vsscanf.c` | C code | Modified-BSD | OK | .
`stdio/vswprintf.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`stdio/vswscanf.c` | C code | Modified-BSD | OK | .
`stdio/vwprintf.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdio/vwscanf.c` | C code | Simplified-BSD | OK | .
`stdio/wcio.h` | C header | Simplified-BSD | OK | .
`stdio/wscanf.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdio/wprintf.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdio/wscanf.c` | C code | Simplified-BSD | OK | .
`stdio/wsetup.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
**`stdlib/_rand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/_Exit.c` | C code | Public-Domain (Todd C. Miller) T2 | OK | .
**`stdlib/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 33 total lines | .
`stdlib/bsearch.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/a64l.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/a64l.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`stdlib/abort.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/abort.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/abs.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/abs.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/alloca.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/atexit.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/atexit.c` | C code | Simplified-BSD T2 | OK | .
`stdlib/atexit.h` | C header | Simplified-BSD T2 | OK | .
`stdlib/atof.3` | ? | Modified-BSD | OK | .
`stdlib/atof.c` | C code | Modified-BSD | OK | .
`stdlib/atoi.3` | ? | Modified-BSD | OK | .
`stdlib/atoi.c` | C code | Modified-BSD | OK | .
`stdlib/atol.3` | ? | Modified-BSD | OK | .
`stdlib/atol.c` | C code | Modified-BSD | OK | .
`stdlib/atoll.3` | ? | Modified-BSD | OK | .
`stdlib/atoll.c` | C code | Modified-BSD | OK | .
**`stdlib/drand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/div.3` | ? | Modified-BSD | OK | .
`stdlib/bsearch.c` | C code | Modified-BSD | OK | .
`stdlib/div.c` | C code | Modified-BSD | OK | .
**`stdlib/erand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/ecvt.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/ecvt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/getopt_long.3` | ? | Modified-BSD | OK | .
`stdlib/exit.3` | ? | Modified-BSD | OK | .
`stdlib/exit.c` | C code | Modified-BSD | OK | .
`stdlib/gcvt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/getenv.3` | ? | Modified-BSD | OK | .
`stdlib/getenv.c` | C code | Modified-BSD | OK | .
`stdlib/getopt.3` | ? | Modified-BSD | OK | .
`stdlib/icdb_new.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/icdb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/getopt_long.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`stdlib/getsubopt.3` | ? | Modified-BSD | OK | .
`stdlib/getsubopt.c` | C code | Modified-BSD | OK | .
`stdlib/hcreate.3` | ? | Simplified-BSD | OK | .
`stdlib/hcreate.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`stdlib/heapsort.c` | C code | Modified-BSD | OK | .
**`stdlib/jrand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/insque.3` | ? | Custom-Modified-BSD (3rd clause is an endorsement clause) | OK | .
`stdlib/imaxabs.3` | ? | Modified-BSD | OK | .
`stdlib/imaxabs.c` | C code | Modified-BSD | OK | .
`stdlib/imaxdiv.3` | ? | Modified-BSD | OK | .
`stdlib/imaxdiv.c` | C code | Modified-BSD | OK | .
`stdlib/insque.c` | C code | Custom-Modified-BSD (3rd clause is an endorsement clause) | OK | .
**`stdlib/lcong48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/l64a.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`stdlib/labs.3` | ? | Modified-BSD | OK | .
`stdlib/labs.c` | C code | Modified-BSD | OK | .
**`stdlib/lrand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/ldiv.3` | ? | Modified-BSD | OK | .
`stdlib/ldiv.c` | C code | Modified-BSD | OK | .
`stdlib/llabs.c` | C code | Modified-BSD | OK | .
`stdlib/lldiv.3` | ? | Modified-BSD | OK | .
`stdlib/lldiv.c` | C code | Modified-BSD | OK | .
**`stdlib/mrand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/malloc.3` | ? | Modified-BSD | OK | .
`stdlib/lsearch.3` | ? | Modified-BSD | OK | .
`stdlib/lsearch.c` | C code | Modified-BSD | OK | .
`stdlib/malloc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/merge.c` | C code | Modified-BSD | OK | .
`stdlib/radixsort.3` | ? | Modified-BSD | OK | .
`stdlib/qsort.3` | ? | Modified-BSD | OK | .
**`stdlib/nrand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/posix_memalign.3` | ? | Custom-Simplified-BSD (Addition of "first lines of the file unmodified ..." in the 1st clause) | OK | .
`stdlib/posix_openpt.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/posix_pty.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/ptsname.3` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`stdlib/qsort.c` | C code | Modified-BSD | OK | .
`stdlib/reallocarray.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/rand.3` | ? | Modified-BSD | OK | .
`stdlib/radixsort.c` | C code | Modified-BSD | OK | .
`stdlib/rand.c` | C code | Modified-BSD | OK | .
**`stdlib/rand48.3`** | ? | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
**`stdlib/rand48.h`** | C header | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/random.3` | ? | Modified-BSD | OK | .
`stdlib/random.c` | C code | Modified-BSD | OK | .
**`stdlib/srand48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/remque.c` | C code | Custom-Modified-BSD (3rd clause is an endorsement clause) | OK | .
`stdlib/realpath.3` | ? | Modified-BSD | OK | .
`stdlib/realpath.c` | C code | Original-ISC | OK | .
`stdlib/recallocarray.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`stdlib/seed48.c`** | C code | __Nonfree-License!!!__ (Martin Birgmeier) | __Nonfree!!!__ [doesn't contain conditions to allow to use and modify the code] | .
`stdlib/setenv.c` | C code | Modified-BSD | OK | .
`stdlib/strtoimax.c` | C code | Modified-BSD | OK | .
`stdlib/strtod.3` | ? | Modified-BSD | OK | .
`stdlib/strtoll.c` | C code | Modified-BSD | OK | .
`stdlib/strtol.3` | ? | Modified-BSD | OK | .
`stdlib/strtol.c` | C code | Modified-BSD | OK | .
`stdlib/thread_atexit.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/system.3` | ? | Modified-BSD | OK | .
`stdlib/strtonum.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/strtonum.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/strtoul.3` | ? | Modified-BSD | OK | .
`stdlib/strtoul.c` | C code | Modified-BSD | OK | .
`stdlib/strtoull.c` | C code | Modified-BSD | OK | .
`stdlib/strtoumax.c` | C code | Modified-BSD | OK | .
`stdlib/system.c` | C code | Modified-BSD | OK | .
`stdlib/tfind.c` | C code | Public-Domain (from Knuth (6.2.2) Algorithm T) | OK | .
`stdlib/tsearch.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`stdlib/tsearch.c` | C code | Public-Domain (from Knuth (6.2.2) Algorithm T) | OK | .
`string/explicit_bzero.c` | C code | Public-Domain (Matthew Dempsky) | OK | .
`string/bcmp.3` | ? | Modified-BSD | OK | .
**`string/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 35 total lines | .
`string/bcmp.c` | C code | Modified-BSD | OK | .
`string/bcopy.3` | ? | Modified-BSD | OK | .
`string/bcopy.c` | C code | Modified-BSD | OK | .
`string/bzero.3` | ? | Modified-BSD | OK | .
`string/bzero.c` | C code | Modified-BSD | OK | .
`string/memccpy.3` | ? | Modified-BSD | OK | .
`string/ffs.3` | ? | Modified-BSD | OK | .
`string/ffs.c` | C code | Public-Domain (Dale Rahn) | OK | .
`string/memmove.3` | ? | Modified-BSD | OK | .
`string/memchr.3` | ? | Modified-BSD | OK | .
`string/memccpy.c` | C code | Modified-BSD | OK | .
`string/memchr.c` | C code | Modified-BSD | OK | .
`string/memcmp.3` | ? | Modified-BSD | OK | .
`string/memcmp.c` | C code | Modified-BSD | OK | .
`string/memcpy.3` | ? | Modified-BSD | OK | .
`string/memcpy.c` | C code | Modified-BSD | OK | .
`string/memmem.3` | ? | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`string/memmem.c` | C code | Expat | OK | .
`string/stpncpy.c` | C code | Modified-BSD | OK | .
`string/memset.3` | ? | Modified-BSD | OK | .
`string/memmove.c` | C code | Modified-BSD | OK | .
`string/memrchr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/memset.c` | C code | Modified-BSD | OK | .
`string/stpcpy.3` | ? | Modified-BSD | OK | .
`string/stpcpy.c` | C code | Modified-BSD | OK | .
`string/strcoll.3` | ? | Modified-BSD | OK | .
`string/strcat.3` | ? | Modified-BSD | OK | .
`string/strcasecmp.3` | ? | Modified-BSD | OK | .
`string/strcasecmp.c` | C code | Modified-BSD | OK | .
`string/strcasecmp_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`string/strcasestr.c` | C code | Modified-BSD | OK | .
`string/strcat.c` | C code | Modified-BSD | OK | .
`string/strchr.3` | ? | Modified-BSD | OK | .
`string/strchr.c` | C code | Modified-BSD | OK | .
`string/strcmp.3` | ? | Modified-BSD | OK | .
`string/strcmp.c` | C code | Modified-BSD | OK | .
`string/strcspn.3` | ? | Modified-BSD | OK | .
`string/strcpy.3` | ? | Modified-BSD | OK | .
`string/strcoll.c` | C code | Modified-BSD | OK | .
`string/strcoll_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`string/strcpy.c` | C code | Modified-BSD | OK | .
`string/strerror.3` | ? | Modified-BSD | OK | .
`string/strdup.3` | ? | Modified-BSD | OK | .
`string/strcspn.c` | C code | Modified-BSD | OK | .
`string/strdup.c` | C code | Modified-BSD | OK | .
`string/strmode.3` | ? | Modified-BSD | OK | .
`string/strlen.3` | ? | Modified-BSD | OK | .
`string/strerror.c` | C code | Modified-BSD | OK | .
`string/strerror_l.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strerror_r.c` | C code | Public-Domain (marc@snafu.org) | OK | .
`string/strlcat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strlcpy.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strlcpy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strlen.c` | C code | Modified-BSD | OK | .
`string/strsignal.3` | ? | Modified-BSD | OK | .
`string/strsep.3` | ? | Modified-BSD | OK | .
`string/strmode.c` | C code | Modified-BSD | OK | .
`string/strncat.3` | ? | Modified-BSD | OK | .
`string/strncat.c` | C code | Modified-BSD | OK | .
`string/strncmp.c` | C code | Modified-BSD | OK | .
`string/strncpy.3` | ? | Modified-BSD | OK | .
`string/strncpy.c` | C code | Modified-BSD | OK | .
`string/strndup.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strnlen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/strpbrk.3` | ? | Modified-BSD | OK | .
`string/strpbrk.c` | C code | Modified-BSD | OK | .
`string/strrchr.3` | ? | Modified-BSD | OK | .
`string/strrchr.c` | C code | Modified-BSD | OK | .
`string/strsep.c` | C code | Modified-BSD | OK | .
`string/strxfrm.3` | ? | Modified-BSD | OK | .
`string/strspn.3` | ? | Modified-BSD | OK | .
`string/strsignal.c` | C code | Modified-BSD | OK | .
`string/strspn.c` | C code | Modified-BSD | OK | .
`string/strstr.3` | ? | Modified-BSD | OK | .
`string/strstr.c` | C code | Expat | OK | .
`string/strtok.3` | ? | Modified-BSD | OK | .
`string/strtok.c` | C code | Modified-BSD | OK | .
`string/wcscasecmp.3` | ? | Modified-BSD | OK | .
`string/swab.3` | ? | Modified-BSD | OK | .
`string/strxfrm.c` | C code | Modified-BSD | OK | .
`string/strxfrm_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`string/swab.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/wcscspn.3` | ? | Modified-BSD | OK | .
`string/wcscat.3` | ? | Modified-BSD | OK | .
`string/wcscat.c` | C code | Simplified-BSD | OK | .
`string/timingsafe_bcmp.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/timingsafe_bcmp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/timingsafe_memcmp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/wcscasecmp.c` | C code | Simplified-BSD | OK | .
`string/wcscasecmp_l.c` | C code | Simplified-BSD | OK | .
`string/wcschr.3` | ? | Modified-BSD | OK | .
`string/wcschr.c` | C code | Simplified-BSD | OK | .
`string/wcscmp.3` | ? | Modified-BSD | OK | .
`string/wcscmp.c` | C code | Modified-BSD | OK | .
`string/wcscpy.3` | ? | Modified-BSD | OK | .
`string/wcscpy.c` | C code | Simplified-BSD | OK | .
`string/wcslcat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/wcsdup.3` | ? | Modified-BSD | OK | .
`string/wcscspn.c` | C code | Simplified-BSD | OK | .
`string/wcsdup.c` | C code | Custom-Expat (Cheusov variant) | OK | .
`string/wcsncat.c` | C code | Simplified-BSD | OK | .
`string/wcslen.3` | ? | Modified-BSD | OK | .
`string/wcslcpy.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/wcslcpy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`string/wcslen.c` | C code | Simplified-BSD | OK | .
`string/wcswidth.3` | ? | Simplified-BSD | OK | .
`string/wcsspn.3` | ? | Modified-BSD | OK | .
`string/wcsncmp.c` | C code | Modified-BSD | OK | .
`string/wcsncpy.c` | C code | Simplified-BSD | OK | .
`string/wcspbrk.3` | ? | Modified-BSD | OK | .
`string/wcspbrk.c` | C code | Simplified-BSD | OK | .
`string/wcsrchr.3` | ? | Modified-BSD | OK | .
`string/wcsrchr.c` | C code | Simplified-BSD | OK | .
`string/wcsspn.c` | C code | Simplified-BSD | OK | .
`string/wcsstr.3` | ? | Modified-BSD | OK | .
`string/wcsstr.c` | C code | Simplified-BSD | OK | .
`string/wcstok.3` | ? | Original-BSD (Softweyr LLC) | OK | .
`string/wcstok.c` | C code | Original-BSD (Softweyr LLC) | OK | .
`string/wcswcs.c` | C code | "Unlicensed!!!" | Trivial; 2 code lines + 2 commented lines; 5 total lines | .
`string/wcswidth.c` | C code | Simplified-BSD | OK | .
`string/wmemchr.3` | ? | Modified-BSD | OK | .
`string/wmemchr.c` | C code | Simplified-BSD | OK | .
`string/wmemcmp.3` | ? | Modified-BSD | OK | .
`string/wmemcmp.c` | C code | Simplified-BSD | OK | .
`string/wmemcpy.3` | ? | Modified-BSD | OK | .
`string/wmemcpy.c` | C code | Simplified-BSD | OK | .
`string/wmemmove.3` | ? | Modified-BSD | OK | .
`string/wmemmove.c` | C code | Simplified-BSD | OK | .
`string/wmemset.3` | ? | Modified-BSD | OK | .
`string/wmemset.c` | C code | Simplified-BSD | OK | .
`sys/adjfreq.2` | ? | Simplified-BSD | OK | .
`sys/_exit.2` | ? | Modified-BSD | OK | .
**`sys/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 173 non empty lines; 199 total lines | .
`sys/__get_tcb.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/__thrsigdivert.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/__thrsleep.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/accept.2` | ? | Modified-BSD | OK | .
`sys/access.2` | ? | Modified-BSD | OK | .
`sys/acct.2` | ? | Modified-BSD | OK | .
`sys/canceled.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/bind.2` | ? | Modified-BSD | OK | .
`sys/adjtime.2` | ? | Modified-BSD | OK | .
`sys/brk.2` | ? | Modified-BSD | OK | .
`sys/chflags.2` | ? | Modified-BSD | OK | .
`sys/chdir.2` | ? | Modified-BSD | OK | .
`sys/clock_gettime.2` | ? | Modified-BSD | OK | .
`sys/chmod.2` | ? | Modified-BSD | OK | .
`sys/chown.2` | ? | Modified-BSD | OK | .
`sys/chroot.2` | ? | Modified-BSD | OK | .
`sys/closefrom.2` | ? | Simplified-BSD | OK | .
`sys/close.2` | ? | Modified-BSD | OK | .
`sys/ftruncate.c` | C code | Modified-BSD | OK | .
`sys/dup.2` | ? | Modified-BSD | OK | .
`sys/connect.2` | ? | Modified-BSD | OK | .
`sys/execve.2` | ? | Modified-BSD | OK | .
`sys/fcntl.2` | ? | Modified-BSD | OK | .
`sys/fhopen.2` | ? | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`sys/flock.2` | ? | Modified-BSD | OK | .
`sys/fork.2` | ? | Modified-BSD | OK | .
`sys/fsync.2` | ? | Modified-BSD | OK | .
`sys/getdents.2` | ? | Modified-BSD | OK | .
`sys/futex.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/getfsstat.2` | ? | Modified-BSD | OK | .
`sys/getfh.2` | ? | Modified-BSD | OK | .
`sys/getdtablecount.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/getentropy.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/getgroups.2` | ? | Modified-BSD | OK | .
`sys/getgid.2` | ? | Modified-BSD | OK | .
`sys/getpriority.2` | ? | Modified-BSD | OK | .
`sys/getpid.2` | ? | Modified-BSD | OK | .
`sys/getitimer.2` | ? | Modified-BSD | OK | .
`sys/getlogin.2` | ? | Modified-BSD | OK | .
`sys/getpeername.2` | ? | Modified-BSD | OK | .
`sys/getpgrp.2` | ? | Modified-BSD | OK | .
`sys/getsockname.2` | ? | Modified-BSD | OK | .
`sys/getsid.2` | ? | Simplified-BSD | OK | .
`sys/getrlimit.2` | ? | Modified-BSD | OK | .
`sys/getrtable.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/getrusage.2` | ? | Modified-BSD | OK | .
`sys/issetugid.2` | ? | Modified-BSD | OK | .
`sys/getuid.2` | ? | Modified-BSD | OK | .
`sys/getsockopt.2` | ? | Modified-BSD | OK | .
`sys/getthrid.2` | ? | Modified-BSD | OK | .
`sys/gettimeofday.2` | ? | Modified-BSD | OK | .
`sys/intro.2` | ? | Modified-BSD | OK | .
`sys/ioctl.2` | ? | Modified-BSD | OK | .
`sys/madvise.2` | ? | Modified-BSD | OK | .
`sys/kbind.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/kill.2` | ? | Modified-BSD | OK | .
`sys/kqueue.2` | ? | Simplified-BSD | OK | .
`sys/ktrace.2` | ? | Modified-BSD | OK | .
`sys/link.2` | ? | Modified-BSD | OK | .
`sys/listen.2` | ? | Modified-BSD | OK | .
`sys/lseek.2` | ? | Modified-BSD | OK | .
`sys/lseek.c` | C code | Modified-BSD | OK | .
`sys/mlockall.2` | ? | Simplified-BSD | OK | .
`sys/mkdir.2` | ? | Modified-BSD | OK | .
`sys/microtime.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/minherit.2` | ? | Modified-BSD | OK | .
`sys/mkfifo.2` | ? | Modified-BSD | OK | .
`sys/mknod.2` | ? | Modified-BSD | OK | .
`sys/mlock.2` | ? | Modified-BSD | OK | .
`sys/mprotect.2` | ? | Modified-BSD | OK | .
`sys/mmap.2` | ? | Modified-BSD | OK | .
`sys/mmap.c` | C code | Modified-BSD | OK | .
`sys/mount.2` | ? | Modified-BSD | OK | .
`sys/msyscall.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/mquery.2` | ? | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`sys/mquery.c` | C code | Public-Domain (Artur Grabowski) | OK | .
`sys/msgctl.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/msgget.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/msgrcv.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/msgsnd.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/msync.2` | ? | Modified-BSD | OK | .
`sys/nanosleep.2` | ? | Modified-BSD | OK | .
`sys/munmap.2` | ? | Modified-BSD | OK | .
`sys/pathconf.2` | ? | Modified-BSD | OK | .
`sys/nfssvc.2` | ? | Modified-BSD | OK | .
`sys/open.2` | ? | Modified-BSD | OK | .
`sys/posix_madvise.c` | C code | Public-Domain (Ted Unangst) | OK | .
`sys/pipe.2` | ? | Modified-BSD | OK | .
`sys/pledge.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/poll.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/pwritev.c` | C code | Modified-BSD | OK | .
`sys/pread.c` | C code | Modified-BSD | OK | .
`sys/preadv.c` | C code | Modified-BSD | OK | .
`sys/profil.2` | ? | Modified-BSD | OK | .
`sys/ptrace.2` | ? | Public-Domain (notified in file) | OK | .
`sys/ptrace.c` | C code | Public-Domain (David Leonard) | OK | .
`sys/pwrite.c` | C code | Modified-BSD | OK | .
`sys/read.2` | ? | Modified-BSD | OK | .
`sys/pthread_sigmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/sched_yield.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/reboot.2` | ? | Modified-BSD | OK | .
`sys/quotactl.2` | ? | Modified-BSD | OK | .
`sys/readlink.2` | ? | Modified-BSD | OK | .
`sys/recv.2` | ? | Modified-BSD | OK | .
`sys/rename.2` | ? | Modified-BSD | OK | .
`sys/revoke.2` | ? | Modified-BSD | OK | .
`sys/rmdir.2` | ? | Modified-BSD | OK | .
`sys/sendsyslog.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/select.2` | ? | Modified-BSD | OK | .
`sys/semctl.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/semctl.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/semget.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/semop.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/send.2` | ? | Modified-BSD | OK | .
`sys/shutdown.2` | ? | Modified-BSD | OK | .
`sys/setsid.2` | ? | Modified-BSD | OK | .
`sys/setgroups.2` | ? | Modified-BSD | OK | .
`sys/setpgid.2` | ? | Modified-BSD | OK | .
`sys/setregid.2` | ? | Modified-BSD | OK | .
`sys/setresuid.2` | ? | Simplified-BSD | OK | .
`sys/setreuid.2` | ? | Modified-BSD | OK | .
`sys/setuid.2` | ? | Modified-BSD | OK | .
`sys/shmat.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/shmctl.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/shmget.2` | ? | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/socketpair.2` | ? | Modified-BSD | OK | .
`sys/socket.2` | ? | Modified-BSD | OK | .
`sys/sigaction.2` | ? | Modified-BSD | OK | .
`sys/sigaltstack.2` | ? | Modified-BSD | OK | .
`sys/sigpending.2` | ? | Modified-BSD | OK | .
`sys/sigprocmask.2` | ? | Modified-BSD | OK | .
`sys/sigreturn.2` | ? | Modified-BSD | OK | .
`sys/sigsuspend.2` | ? | Modified-BSD | OK | .
`sys/swapctl.2` | ? | Modified-BSD | OK | .
`sys/stat.2` | ? | Modified-BSD | OK | .
`sys/statfs.2` | ? | Modified-BSD | OK | .
`sys/stack_protector.c` | C code | Simplified-BSD | OK | .
`sys/sysarch.2` | ? | Modified-BSD | OK | .
`sys/sync.2` | ? | Modified-BSD | OK | .
`sys/symlink.2` | ? | Modified-BSD | OK | .
`sys/thrkill.2` | ? | Modified-BSD | OK | .
`sys/sysctl.2` | ? | Modified-BSD | OK | .
`sys/syscall.2` | ? | Modified-BSD | OK | .
`sys/timer_getoverrun.c` | C code | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 14 total lines | .
`sys/timer_create.c` | C code | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented lines; 17 total lines | .
`sys/timer_delete.c` | C code | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 14 total lines | .
`sys/w_accept.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/umask.2` | ? | Modified-BSD | OK | .
`sys/timer_gettime.c` | C code | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 14 total lines | .
`sys/timer_settime.c` | C code | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented lines; 16 total lines | .
`sys/truncate.2` | ? | Modified-BSD | OK | .
`sys/truncate.c` | C code | Modified-BSD | OK | .
`sys/unlink.2` | ? | Modified-BSD | OK | .
`sys/unveil.2` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/utimes.2` | ? | Modified-BSD | OK | .
`sys/utrace.2` | ? | Simplified-BSD | OK | .
`sys/vfork.2` | ? | Modified-BSD | OK | .
`sys/w_clock_gettime.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_accept4.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_fsync.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_fork.c` | C code | Custom-Simplified-BSD (second clause is an "endorsement clause") | OK | .
`sys/w_close.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_closefrom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_connect.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_fcntl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_openat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_open.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_gettimeofday.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_msgrcv.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_msgsnd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_msync.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_nanosleep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_ppoll.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_poll.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_readv.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_read.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_pread.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_preadv.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_pselect.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_pwrite.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_pwritev.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/wait.2` | ? | Modified-BSD | OK | .
`sys/w_recvfrom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_recvmsg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_select.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_sendmsg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_sendto.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_sigaction.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_sigprocmask.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_sigsuspend.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_vfork.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_wait4.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_write.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/w_writev.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/write.2` | ? | Modified-BSD | OK | .
`termios/tcflush.c` | C code | Modified-BSD | OK | .
`termios/tcflow.c` | C code | Modified-BSD | OK | .
`termios/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented lines; 9 total lines | .
`termios/cfgetispeed.c` | C code | Modified-BSD | OK | .
`termios/cfgetospeed.c` | C code | Modified-BSD | OK | .
`termios/cfmakeraw.c` | C code | Modified-BSD | OK | .
`termios/cfsetispeed.c` | C code | Modified-BSD | OK | .
`termios/cfsetospeed.c` | C code | Modified-BSD | OK | .
`termios/cfsetspeed.c` | C code | Modified-BSD | OK | .
`termios/tcdrain.c` | C code | Modified-BSD | OK | .
`termios/tcgetattr.c` | C code | Modified-BSD | OK | .
`termios/tcgetpgrp.3` | ? | Modified-BSD | OK | .
`termios/tcgetpgrp.c` | C code | Modified-BSD | OK | .
`termios/tcgetsid.3` | ? | Modified-BSD | OK | .
`termios/tcgetsid.c` | C code | Modified-BSD | OK | .
`termios/tcsendbreak.3` | ? | Modified-BSD | OK | .
`termios/tcsendbreak.c` | C code | Modified-BSD | OK | .
`termios/tcsetattr.3` | ? | Modified-BSD | OK | .
`termios/tcsetattr.c` | C code | Modified-BSD | OK | .
`termios/tcsetpgrp.3` | ? | Modified-BSD | OK | .
`termios/tcsetpgrp.c` | C code | Modified-BSD | OK | .
`thread/callbacks.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/atfork.c` | C code | Custom-Simplified-BSD (second clause is an "endorsement clause") | OK | .
**`thread/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 28 non empty lines; 35 total lines | .
`thread/rthread_cleanup.c` | C code | "Unlicensed!!!" | Trivial; 0 code lines + 0 commented lines; 1 total lines | .
`thread/rthread.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_cb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_condattr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_cond.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/synch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_debug.c` | C code | Public-Domain (Marco S Hyman <marc@snafu.org>) | OK | .
`thread/rthread_file.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`thread/rthread_libc.c` | C code | Public-Domain (Marco S Hyman <marc@snafu.org>) | OK | .
`thread/rthread_mutex.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_once.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_sync.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`thread/rthread_tls.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`time/asctime.c` | C code | Public-Domain (Arthur David Olson) | OK | .
`time/README` | Readme | Public-Domain (Arthur David Olson) | OK | .
`time/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`time/Theory` | ? | Public-Domain (Arthur David Olson) | OK | .
`time/difftime.c` | C code | Public-Domain (Matthew Dempsky) | OK | .
**`time/ctime.3`** | ? | __Unlicensed!!!__ | Non-Trivial; 392 non empty lines; 392 total lines | .
`time/wcsftime.3` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`time/tzfile.5` | ? | Public-Domain (Arthur David Olson) | OK | .
`time/localtime.c` | C code | Public-Domain (Arthur David Olson) | OK | .
`time/private.h` | C header | Public-Domain (Arthur David Olson) | OK | .
`time/strftime.3` | ? | Modified-BSD | OK | .
`time/strftime.c` | C code | Modified-BSD | OK | .
`time/strftime_l.c` | C code | Public-Domain (Ingo Schwarze) | OK | .
`time/strptime.3` | ? | Simplified-BSD | OK | .
`time/strptime.c` | C code | Simplified-BSD | OK | .
`time/time2posix.3` | ? | Public-Domain (Arthur David Olson) | OK | .
`time/tzfile.h` | C header | Public-Domain (Arthur David Olson) | OK | .
`time/tzset.3` | ? | Public-Domain (Arthur David Olson) | OK | .
`time/wcsftime.c` | C code | Modified-BSD | OK | .
`uuid/uuid_create_nil.c` | C code | Simplified-BSD | OK | .
`uuid/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 3 commented lines; 12 total lines | .
`uuid/uuid_compare.3` | ? | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`uuid/uuid_compare.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_create.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_from_string.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_equal.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_hash.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_is_nil.c` | C code | Simplified-BSD | OK | .
`uuid/uuid_stream.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`uuid/uuid_to_string.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypbind_binding.c` | C code | Simplified-BSD | OK | .
`yp/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 2 commented lines; 16 total lines | .
`yp/_yp_check.c` | C code | Simplified-BSD | OK | .
`yp/xdr_domainname.c` | C code | Simplified-BSD | OK | .
`yp/xdr_keydat.c` | C code | Simplified-BSD | OK | .
`yp/xdr_mapname.c` | C code | Simplified-BSD | OK | .
`yp/xdr_peername.c` | C code | Simplified-BSD | OK | .
`yp/xdr_valdat.c` | C code | Simplified-BSD | OK | .
`yp/yp_bind.3` | ? | Simplified-BSD | OK | .
`yp/yp_all.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypbind_resp.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypbind_resptype.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypbind_setdom.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypmaplist.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypreq_key.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypreq_nokey.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_all.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_key_val.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_maplist.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_master.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_order.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypresp_val.c` | C code | Simplified-BSD | OK | .
`yp/xdr_ypstat.c` | C code | Simplified-BSD | OK | .
`yp/yp_get_default_domain.c` | C code | Simplified-BSD | OK | .
`yp/yp_bind.c` | C code | Simplified-BSD | OK | .
`yp/yp_first.c` | C code | Simplified-BSD | OK | .
`yp/yp_maplist.c` | C code | Simplified-BSD | OK | .
`yp/yp_master.c` | C code | Simplified-BSD | OK | .
`yp/yp_order.c` | C code | Simplified-BSD | OK | .
`yp/yperr_string.c` | C code | Simplified-BSD | OK | .
`yp/ypexclude.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`yp/ypexclude.h` | C header | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`yp/ypinternal.h` | C header | Simplified-BSD | OK | .
`yp/ypmatch_cache.c` | C code | Simplified-BSD | OK | .
`yp/ypprot_err.c` | C code | Simplified-BSD | OK | .
**`Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 non empty lines; 72 total lines | .
**`Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 37 non empty lines; 46 total lines | .
**`Symbols.list`** | ? | __Unlicensed!!!__ | Non-Trivial; 1738 non empty lines; 1766 total lines | .
`shlib_version` | ? | "Unlicensed!!!" | Trivial; 2 code lines + 2 commented lines; 4 total lines | .

---

## hyperblibc-file-list.md - HyperBLibC file list for Todo list

Written in 2023 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.info>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
