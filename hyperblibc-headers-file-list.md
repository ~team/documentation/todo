File name | File type | License(s) | Description(s) / Status | More
---|---|---|---|---
`include/arpa/nameser.h` | C header | Modified-BSD | OK | .
`include/arpa/ftp.h` | C header | Modified-BSD | OK | .
`include/arpa/inet.h` | C header | Modified-BSD | OK | .
`include/arpa/telnet.h` | C header | Modified-BSD | OK | .
`include/arpa/tftp.h` | C header | Modified-BSD | OK | .
`include/protocols/routed.h` | C header | Modified-BSD | OK | .
`include/protocols/dumprestore.h` | C header | Modified-BSD | OK | .
`include/protocols/rwhod.h` | C header | Modified-BSD | OK | .
`include/protocols/talkd.h` | C header | Modified-BSD | OK | .
`include/protocols/timed.h` | C header | Modified-BSD | OK | .
`include/rpc/auth_unix.h` | C header | Modified-BSD | OK | .
`include/rpc/auth.h` | C header | Modified-BSD | OK | .
`include/rpc/pmap_clnt.h` | C header | Modified-BSD | OK | .
`include/rpc/clnt.h` | C header | Modified-BSD | OK | .
`include/rpc/rpc_des.h` | C header | Original-BSD | OK | .
`include/rpc/rpc.h` | C header | Modified-BSD | OK | .
`include/rpc/pmap_prot.h` | C header | Modified-BSD | OK | .
`include/rpc/pmap_rmt.h` | C header | Modified-BSD | OK | .
`include/rpc/svc_auth.h` | C header | "Unlicensed!!!" | Trivial; 0 code lines + 1 commented lines; 1 total lines | .
`include/rpc/svc.h` | C header | Modified-BSD | OK | .
`include/rpc/rpc_msg.h` | C header | Modified-BSD | OK | .
`include/rpc/types.h` | C header | Modified-BSD | OK | .
`include/rpc/xdr.h` | C header | Modified-BSD | OK | .
`include/rpcsvc/ypclnt.h` | C header | Simplified-BSD | OK | .
`include/rpcsvc/yp_prot.h` | C header | Simplified-BSD | OK | .
`include/bitstring.h` | C header | Modified-BSD | OK | .
**`include/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 143 non empty lines; 163 total lines | .
`include/a.out.h` | C header | Modified-BSD | OK | .
`include/ar.h` | C header | Modified-BSD | OK | .
`include/asr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/assert.h` | C header | Modified-BSD | OK | .
`include/bsd_auth.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/blf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/disktab.h` | C header | Modified-BSD | OK | .
`include/cpio.h` | C header | Simplified-BSD | OK | .
`include/complex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/ctype.h` | C header | Modified-BSD | OK | .
`include/curses.h` | C header | Public-Domain (notified in file) | OK | .
`include/db.h` | C header | Modified-BSD | OK | .
`include/dirent.h` | C header | Modified-BSD | OK | .
`include/elf_abi.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`include/dlfcn.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/elf.h` | C header | Public-Domain (notified in file) | OK | .
`include/fnmatch.h` | C header | Modified-BSD | OK | .
`include/err.h` | C header | Modified-BSD | OK | .
`include/errno.h` | C header | Modified-BSD | OK | .
`include/fenv.h` | C header | Simplified-BSD | OK | .
`include/float.h` | C header | Modified-BSD | OK | .
`include/ifaddrs.h` | C header | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`include/fstab.h` | C header | Modified-BSD | OK | .
`include/fts.h` | C header | Modified-BSD | OK | .
`include/ftw.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/getopt.h` | C header | Simplified-BSD | OK | .
`include/glob.h` | C header | Modified-BSD | OK | .
`include/grp.h` | C header | Modified-BSD | OK | .
`include/icdb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`include/langinfo.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`include/iso646.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`include/inttypes.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/kvm.h` | C header | Modified-BSD | OK | .
`include/link_elf.h` | C header | Public-Domain (notified in file) | OK | .
`include/libgen.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/limits.h` | C header | Modified-BSD | OK | .
`include/link.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/login_cap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/locale.h` | C header | Modified-BSD | OK | .
`include/netgroup.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`include/math.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 428 non empty lines; 497 total lines | .
`include/md5.h` | C header | Public-Domain (Colin Plumb) | OK | .
`include/memory.h` | C header | Modified-BSD | OK | .
`include/ndbm.h` | C header | Modified-BSD | OK | .
`include/netdb.h` | C header | Modified-BSD + Custom-Modified-BSD (with differents names for copyrights) T2 | OK (Combined Licenses) | .
`include/pthread.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`include/nlist.h` | C header | Modified-BSD | OK | .
`include/nl_types.h` | C header | Simplified-BSD | OK | .
`include/paths.h` | C header | Modified-BSD | OK | .
`include/poll.h` | C header | Public-Domain (notified in file) | OK | .
`include/readpassphrase.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/pwd.h` | C header | Modified-BSD | OK | .
`include/pthread_np.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`include/ranlib.h` | C header | Modified-BSD | OK | .
`include/semaphore.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`include/regex.h` | C header | Modified-BSD | OK | .
`include/resolv.h` | C header | Custom-Modified-BSD (with differents names for copyrights) T2 + Modified-BSD | OK (Combined Licenses) | .
`include/rmd160.h` | C header | Simplified-BSD | OK | .
`include/sched.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`include/search.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`include/siphash.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`include/setjmp.h` | C header | Modified-BSD | OK | .
`include/sha1.h` | C header | Public-Domain (notified in file) | OK | .
`include/sha2.h` | C header | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`include/signal.h` | C header | Modified-BSD | OK | .
`include/stdbool.h` | C header | Public-Domain (notified in file) | OK | .
`include/sndio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/spawn.h` | C header | Simplified-BSD | OK | .
`include/strings.h` | C header | Modified-BSD | OK | .
`include/stddef.h` | C header | Modified-BSD | OK | .
`include/stdio.h` | C header | Modified-BSD | OK | .
`include/stdlib.h` | C header | Modified-BSD | OK | .
`include/string.h` | C header | Modified-BSD | OK | .
`include/time.h` | C header | Modified-BSD | OK | .
`include/unistd.h` | C header | Modified-BSD | OK | .
`include/tar.h` | C header | Simplified-BSD | OK | .
`include/sysexits.h` | C header | Modified-BSD | OK | .
`include/tgmath.h` | C header | Simplified-BSD | OK | .
`include/tib.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`include/vis.h` | C header | Modified-BSD | OK | .
`include/ttyent.h` | C header | Modified-BSD | OK | .
`include/wchar.h` | C header | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`include/utime.h` | C header | Modified-BSD | OK | .
`include/utmp.h` | C header | Modified-BSD | OK | .
`include/uuid.h` | C header | Simplified-BSD | OK | .
`include/wctype.h` | C header | Simplified-BSD | OK | .

---

## hyperblibc-headers-file-list.md - HyperBLibC Headers file list for Todo list

Written in 2023 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.info>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
