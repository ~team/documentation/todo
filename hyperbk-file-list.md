File name | File type | License(s) | Description(s) / Status | More
---|---|---|---|---
`arch/alpha/alpha/api_up1000.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/alpha/alpha/clock.c` | C code | Modified-BSD | OK | .
`arch/alpha/alpha/clockvar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/conf.c` | C code | Modified-BSD | OK | .
`arch/alpha/alpha/cpu.c` | C code | Simplified-BSD + CMU | OK | .
`arch/alpha/alpha/cpuconf.c` | C code | Modified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/alpha/db_disasm.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/db_instruction.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU | OK | .
`arch/alpha/alpha/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/db_trace.c` | C code | Simplified-BSD | OK | .
`arch/alpha/alpha/debug.s` | Assembler header (.h) | Simplified-BSD | OK | .
`arch/alpha/alpha/dec_1000a.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/alpha/dec_2100_a50.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_3000_300.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_3000_500.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_550.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_6600.c` | C code | Original-ISC ("and" is used instead of "and/or") + CMU | OK (Combined Licenses) | .
`arch/alpha/alpha/dec_alphabook1.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_axppci_33.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_eb164.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_eb64plus.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_kn20aa.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/dec_kn300.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/alpha/disksubr.c` | C code | Modified-BSD | OK | .
`arch/alpha/alpha/fp_complete.c` | C code | Original-BSD | OK | .
`arch/alpha/alpha/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/alpha/alpha/in_cksum.c` | C code | Modified-BSD | OK | .
`arch/alpha/alpha/interrupt.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/alpha/ipifuncs.c` | C code | Simplified-BSD | OK | .
`arch/alpha/alpha/lock_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/alpha/locore0.S` | Assembler code | Simplified-BSD + CMU | OK | .
`arch/alpha/alpha/locore.s` | Assembler code (.S) | Simplified-BSD + CMU | OK | .
`arch/alpha/alpha/machdep.c` | C code | Simplified-BSD + CMU | OK | .
`arch/alpha/alpha/mainbus.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/mcclock.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/mcclockvar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/mem.c` | C code | Modified-BSD | OK | .
`arch/alpha/alpha/multiproc.s` | Assembler header (.h) | Simplified-BSD | OK | .
`arch/alpha/alpha/pal.s` | Assembler header (.h) | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/pmap.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/alpha/alpha/process_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/alpha/alpha/prom.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/promcons.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/prom_disp.s` | Assembler header (.h) | CMU (similar to ISC) | OK | .
`arch/alpha/alpha/sys_machdep.c` | C code | Simplified-BSD + CMU | OK | .
`arch/alpha/alpha/trap.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/alpha/alpha/vm_machdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 15 code lines; 20 total lines | .
`arch/alpha/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 8 total lines | .
`arch/alpha/compile/RAMDISKBIG/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/compile/RAMDISKB/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/compile/RAMDISKC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/alpha/conf/files.alpha` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/GENERIC` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/GENERIC.MP` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/alpha/conf/Makefile.alpha`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 106 code lines + 20 commented lines; 163 total lines | .
`arch/alpha/conf/RAMDISKB` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/RAMDISKBIG` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/RAMDISK` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/conf/RAMDISKC` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/alpha/dev/bus_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/dev/sgmap_common.c` | C code | Simplified-BSD | OK | .
`arch/alpha/dev/sgmap_typedep.c` | C code | Simplified-BSD | OK | .
`arch/alpha/dev/sgmap_typedep.h` | C header | Simplified-BSD | OK | .
`arch/alpha/dev/sgmapvar.h` | C header | Simplified-BSD | OK | .
`arch/alpha/dev/shared_intr.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/eisa/eisa_machdep.c` | C code | Simplified-BSD | OK | .
`arch/alpha/eisa/eisa_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/alpha_cpu.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/asm.h` | Assembler header | CMU (similar to ISC) | OK | .
`arch/alpha/include/atomic.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/autoconf.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/bus.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/bwx.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/cdefs.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/conf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/include/cpuconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/include/cpu.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/alpha/include/db_machdep.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/exec.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/include/_float.h` | C header | Modified-BSD | OK | .
`arch/alpha/include/fpu.h` | C header | Original-BSD | OK | .
`arch/alpha/include/frame.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/alpha/include/ieee.h` | C header | Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/alpha/include/intr.h` | C header | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/include/ioctl_fd.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/kcore.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/limits.h` | C header | Modified-BSD | OK | .
`arch/alpha/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/lock.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/logout.h` | C header | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`arch/alpha/include/mplock.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/alpha/include/pal.h` | Assembler header | CMU (similar to ISC) | OK | .
`arch/alpha/include/param.h` | C header | Modified-BSD | OK | .
`arch/alpha/include/pcb.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/pmap.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/alpha/include/proc.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/profile.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/prom.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/pte.h` | C header | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/include/reg.h` | C header | CMU (similar to ISC) | OK | .
**`arch/alpha/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 32 code lines + 1 commented line; 38 total lines | .
`arch/alpha/include/rpb.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/setjmp.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/signal.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented line; 11 total lines | .
`arch/alpha/include/sysarch.h` | C header | Simplified-BSD | OK | .
`arch/alpha/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/include/tc_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/include/_types.h` | C header | Modified-BSD | OK | .
`arch/alpha/include/vmparam.h` | C header | Modified-BSD | OK | .
`arch/alpha/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-Original-BSD (Original-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/alpha/isa/isadma_bounce.c` | C code | Simplified-BSD | OK | .
`arch/alpha/isa/isafcns_jensen.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/isa/isa_machdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/isa/isa_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/isa/isapnp_machdep.c` | C code | Simplified-BSD | OK | .
`arch/alpha/isa/isapnp_machdep.h` | C header | Simplified-BSD | OK | .
`arch/alpha/isa/mcclock_isa.c` | C code | CMU (similar to ISC) | OK | .
**`arch/alpha/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 5 commented lines; 51 total lines | .
`arch/alpha/mcbus/mcbus.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/mcbus/mcbusreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/mcbus/mcbusvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/mcbus/mcmem.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/apecs_bus_io.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/apecs_bus_mem.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/apecs.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/apecs_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/apecs_pci.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/apecsreg.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/apecsvar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/cia_bus_io.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/cia_bus_mem.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/cia_bwx_bus_io.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/cia_bwx_bus_mem.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/cia.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/cia_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/cia_pci.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/ciareg.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/ciavar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/irongate_bus_io.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/irongate_bus_mem.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/irongate.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/irongate_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/irongate_pci.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/irongatereg.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/irongatevar.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/lca_bus_io.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/lca_bus_mem.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/lca.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/lca_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/lca_pci.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/lcareg.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/lcavar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/mcpcia_bus_io.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/mcpcia_bus_mem.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/mcpcia.c` | C code | Simplified-BSD + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/alpha/pci/mcpcia_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/mcpcia_pci.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/mcpciareg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/mcpciavar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/pci_1000a.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_1000a.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/pci_1000.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_1000.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/pci_2100_a50.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_2100_a50.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_550.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_550.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_6600.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/pci_6600.h` | C header | Public-Domain (notified in file) | OK | .
`arch/alpha/pci/pci_alphabook1.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_alphabook1.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_axppci_33.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_axppci_33.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_bwx_bus_io_chipdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_bwx_bus_mem_chipdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_eb164.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_eb164.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_eb164_intr.s` | Assembler code (.S) | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_eb64plus.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/pci_eb64plus.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_eb64plus_intr.s` | Assembler code (.S) | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/pci_kn20aa.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_kn20aa.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_kn300.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/pci_kn300.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/alpha/pci/pci_machdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_sgmap_pte64.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/pci_sgmap_pte64.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/pci_swiz_bus_io_chipdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_swiz_bus_mem_chipdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/pci_up1000.c` | C code | Simplified-BSD | OK | .
`arch/alpha/pci/pci_up1000.h` | C header | Simplified-BSD | OK | .
`arch/alpha/pci/sio.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/pci/sio_pic.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/pci/sioreg.h` | C header | BBN-Corporation (similar to Modified-BSD but with the "non-endorsement clause") | OK | .
`arch/alpha/pci/siovar.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/pci/tsc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsciic.c` | C code | Simplified-BSD | ok | .
`arch/alpha/pci/tsp_bus_io.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsp_bus_mem.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsp_dma.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/alpha/pci/tsp_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/bbinfo.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/stand/boot/boot.c` | C code | Modified-BSD | OK | .
`arch/alpha/stand/boot/conf.c` | C code | Modified-BSD | OK | .
`arch/alpha/stand/boot/devopen.c` | C code | Modified-BSD | OK | .
`arch/alpha/stand/boot/disk.c` | C code | Modified-BSD | OK | .
`arch/alpha/stand/boot/disk.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 3 commented lines; 9 total lines | .
`arch/alpha/stand/boot/filesystem.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 4 commented lines; 63 total lines | .
`arch/alpha/stand/boot/newvers.sh` | Shell script | Modified-BSD | OK | .
`arch/alpha/stand/boot/prom_swpal.S` | Assembler code | CMU (similar to ISC) | OK | .
**`arch/alpha/stand/boot/version`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 21 data lines + 2 commented lines; 30 total lines | .
`arch/alpha/stand/bootxx.c` | C code | CMU (similar to ISC) | OK | .
**`arch/alpha/stand/bootxx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 2 commented lines; 43 total lines | .
`arch/alpha/stand/headersize.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/stand/installboot.8` | troff (manual) | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/installboot.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/installboot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 13 code lines + 2 commented lines; 20 total lines | .
**`arch/alpha/stand/libsa/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 code lines + 4 commented lines; 40 total lines | .
`arch/alpha/stand/libsa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented line; 6 total lines | .
**`arch/alpha/stand/libz/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 46 code lines + 16 commented lines; 72 total lines | .
`arch/alpha/stand/libz/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented line; 6 total lines | .
`arch/alpha/stand/loadfile_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/alpha/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 11 code lines + 8 commented line; 27 total lines | .
`arch/alpha/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 11 total lines | .
**`arch/alpha/stand/netboot/conf.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 2 commented lines; 27 total lines | .
`arch/alpha/stand/netboot/dev_net.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause", four clause is the advertising) | OK | .
`arch/alpha/stand/netboot/dev_net.h` | C header | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 9 total lines | .
`arch/alpha/stand/netboot/devopen.c` | C code | Modified-BSD | OK | .
**`arch/alpha/stand/netboot/getsecs.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 35 total lines | .
`arch/alpha/stand/netboot/if_prom.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/stand/netboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 4 commented lines; 73 total lines | .
`arch/alpha/stand/netboot/netboot.c` | C code | Modified-BSD | OK | .
`arch/alpha/stand/netboot/newvers.sh` | Shell script | Modified-BSD | OK | .
`arch/alpha/stand/netboot/rpcc.S` | Assembler code | "Unlicensed!!!" | Trivial; 6 code lines + 2 commented lines; 11 total lines | .
`arch/alpha/stand/netboot/version` | ASCII text | "Unlicensed!!!" | Trivial; 10 data lines + 2 commented lines; 16 total lines | .
`arch/alpha/stand/OSFpal.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/stand/prom.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/stand/prom_disp.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/stand/puts.c` | C code | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 12 total lines | .
`arch/alpha/stand/setnetbootinfo/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 2 commented lines; 20 total lines | .
`arch/alpha/stand/setnetbootinfo/setnetbootinfo.8` | troff (manual) | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/setnetbootinfo/setnetbootinfo.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/start.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/ioasic.c` | C code | Simplified-BSD + CMU | OK (Combined Licenses) | .
`arch/alpha/tc/mcclock_ioasic.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_3000_300.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_3000_300.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_3000_500.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_3000_500.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tcasic.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_bus_mem.c` | C code | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_conf.h` | C header | CMU (similar to ISC) | OK | .
`arch/alpha/tc/tc_dma_3000_300.c` | C code | Simplified-BSD | OK | .
`arch/alpha/tc/tc_dma_3000_300.h` | C header | Simplified-BSD | OK | .
`arch/alpha/tc/tc_dma_3000_500.c` | C code | Simplified-BSD | OK | .
`arch/alpha/tc/tc_dma_3000_500.h` | C header | Simplified-BSD | OK | .
`arch/alpha/tc/tc_dma.c` | C code | Simplified-BSD | OK | .
`arch/alpha/tc/tc_sgmap.c` | C code | Simplified-BSD | OK | .
`arch/alpha/tc/tc_sgmap.h` | C header | Simplified-BSD | OK | .
`arch/amd64/amd64/acpi_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/acpi_wakecode.S` | Assembler code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/amd64/amd64/aes_intel.S` | Assembler code | Modified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/amd64/amd64/aesni.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/amd64errata.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/amd64_mem.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/apic.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/autoconf.c` | C code | Modified-BSD | OK | .
`arch/amd64/amd64/bios.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/bus_dma.c` | C code | Simplified-BSD + Custom-Original-BSD (Original-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/amd64/amd64/bus_space.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/cacheinfo.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/codepatch.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/consinit.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/copy.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/cpu.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/db_disasm.c` | C code | CMU (similar to ISC) | OK | .
`arch/amd64/amd64/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/amd64/amd64/db_memrw.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/amd64/amd64/disksubr.c` | C code | Modified-BSD | OK | .
`arch/amd64/amd64/dkcsum.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/efifb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/est.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/fpu.c` | C code | Modified-BSD | OK | .
`arch/amd64/amd64/gdt.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/genassym.cf` | C code generator | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/amd64/hibernate_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/i8259.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/identcpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/intr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/ioapic.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/ipi.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/ipifuncs.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/k1x-pstate.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/lapic.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/machdep.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/mainbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/mds.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/mem.c` | C code | Original-BSD | OK | .
`arch/amd64/amd64/mpbios.c` | C code | Simplified-BSD + Original-BSD + Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK (Combined Licenses) | .
`arch/amd64/amd64/mpbios_intr_fixup.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/mp_setperf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/mptramp.S` | Assembler code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/mtrr.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/nvram.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/pctr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/pmap.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/amd64/amd64/powernow-k8.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/process_machdep.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/rbus_machdep.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/softintr.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/spl.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/sys_machdep.c` | C code | Simplified-BSD | OK | .
`arch/amd64/amd64/trap.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/tsc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/ucode.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/vector.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK | .
`arch/amd64/amd64/via.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/vm_machdep.c` | C code | Modified-BSD | OK | .
`arch/amd64/amd64/vmm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/vmm_support.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/amd64/wscons_machdep.c` | C code | Simplified-BSD | OK | .
`arch/amd64/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/amd64/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/amd64/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 15 code lines; 20 total lines | .
`arch/amd64/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 8 total lines | .
`arch/amd64/compile/RAMDISK_CD/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/amd64/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/amd64/conf/files.amd64` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/amd64/conf/GENERIC` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/amd64/conf/GENERIC.MP` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/amd64/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/amd64/conf/Makefile.amd64`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 202 code lines + 20 commented lines; 266 total lines | .
`arch/amd64/conf/RAMDISK` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/amd64/conf/RAMDISK_CD` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/amd64/include/apicvar.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/asm.h` | Assembler header | Modified-BSD | OK | .
`arch/amd64/include/atomic.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/biosvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`arch/amd64/include/cacheinfo.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 43 commented lines; 112 total lines | .
`arch/amd64/include/cdefs.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/include/codepatch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/cpu_full.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/cpufunc.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/cpu.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/cpuvar.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/amd64/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/efifbvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/exec.h` | C header | Public-Domain (Artur Grabowski) | OK | .
`arch/amd64/include/fenv.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/_float.h` | C header | Modified-BSD | OK | .
**`arch/amd64/include/fpu.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 66 code lines + 15 commented lines ; 100 total lines | .
**`arch/amd64/include/frameasm.h`** | Assembler header | __Unlicensed!!!__ | Non-Trivial; 138 commented lines + 42 commented lines; 198 total lines | .
`arch/amd64/include/frame.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/include/gdt.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/hibernate.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/hibernate_var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/i82093reg.h` | Assembler header | Simplified-BSD | OK | .
`arch/amd64/include/i82093var.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/i82489reg.h` | Assembler header | Simplified-BSD | OK | .
`arch/amd64/include/i82489var.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/i8259.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/amd64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/amd64/include/intrdefs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 47 code lines + 32 commented lines ; 93 total lines | .
`arch/amd64/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/ioctl_fd.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/isa_machdep.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/include/kcore.h` | C header | CMU (similar to ISC) | OK | .
`arch/amd64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/amd64/include/mpbiosreg.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/mpbiosvar.h` | C header | Simplified-BSD | OK | .
**`arch/amd64/include/mpconfig.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 35 code lines + 6 commented lines; 50 total lines | .
`arch/amd64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/amd64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/amd64/include/param.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/pcb.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/include/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/amd64/include/pctr.h`** | C header | __Nonfree-License!!!__ | (David Mazieres; it does not have the "use" permission in the license) | .
**`arch/amd64/include/pic.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 29 code lines + 8 commented lines; 45 total lines | .
`arch/amd64/include/pio.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/pmap.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/proc.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/profile.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/psl.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/rbus_machdep.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/reg.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/reloc.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/segments.h` | C header | Modified-BSD | OK | .
**`arch/amd64/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 12 code lines + 7 commented lines; 23 total lines | .
`arch/amd64/include/signal.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/smbiosvar.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/specialreg.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented line; 11 total lines | .
**`arch/amd64/include/sysarch.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 29 total lines | .
`arch/amd64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/trap.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/tss.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/_types.h` | C header | Modified-BSD | OK | .
`arch/amd64/include/vga_post.h` | C header | Simplified-BSD | OK | .
`arch/amd64/include/vmmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/include/vmparam.h` | C header | Modified-BSD | OK | .
`arch/amd64/isa/clock.c` | C code | Modified-BSD + CMU + Intel (similar to ISC and CMU but without endorsement) | OK (Combined Licenses) | .
`arch/amd64/isa/isa_machdep.c` | C code | Simplified-BSD + Modified-BSD | OK | .
`arch/amd64/isa/nvram.h` | C header | Modified-BSD | OK | .
**`arch/amd64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines (1 long line) + 5 commented lines; 51 total lines | .
**`arch/amd64/pci/aapic.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 58 code lines +  5 commented lines; 82 total lines | .
`arch/amd64/pci/acpipci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/pci/agp_machdep.c` | C code | Original-ISC + Simplified-BSD | OK | .
`arch/amd64/pci/pchb.c` | C code | Simplified-BSD + Simplified-BSD | OK | .
`arch/amd64/pci/pcib.c` | C code | Simplified-BSD | OK | .
`arch/amd64/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/pci/pci_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/amd64/pci/vga_post.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/biosboot/biosboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/amd64/stand/biosboot/biosboot.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/biosboot/ld.script` | Linker script | "Unlicensed!!!" | Trivial; 11 code lines; 13 total lines | .
**`arch/amd64/stand/biosboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 35 total lines | .
`arch/amd64/stand/boot/boot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/amd64/stand/boot/check-boot.pl` | Perl script | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/boot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 71 code lines + 2 commented lines; 89 total lines | .
`arch/amd64/stand/boot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/cdboot/cdboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/amd64/stand/cdboot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 59 code lines + 1 commented line; 74 total lines | .
`arch/amd64/stand/cdboot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/cdbr/cdbr.S` | Assembler code | Modified-BSD | OK | .
**`arch/amd64/stand/cdbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 3 commented lines; 36 total lines | .
`arch/amd64/stand/efi32/bootia32/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented line; 19 total lines | .
`arch/amd64/stand/efi32/cmd_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/conf.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/dev_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/diskprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/eficall.h` | Assembler header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/eficall.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/efidev.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/efidev.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/efirng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/exec_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/amd64/stand/efi32/ldscript.i386`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 76 total lines | .
`arch/amd64/stand/efi32/machdep.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/efi32/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
`arch/amd64/stand/efi32/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/amd64/stand/efi32/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 10 total lines | .
`arch/amd64/stand/efi32/memprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/run_i386.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/run_i386.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi32/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi32/start_i386.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/bootx64/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented line; 15 total lines | .
`arch/amd64/stand/efi64/cmd_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/conf.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/dev_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/diskprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/eficall.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/eficall.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/efidev.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/efidev.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/efirng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/exec_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/amd64/stand/efi64/ldscript.amd64`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 66 total lines | .
`arch/amd64/stand/efi64/machdep.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/efi64/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
`arch/amd64/stand/efi64/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/amd64/stand/efi64/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 10 total lines | .
`arch/amd64/stand/efi64/memprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/run_i386.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/run_i386.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efi64/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efi64/start_amd64.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/stand/efiboot/bootia32/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented line; 19 total lines | .
`arch/amd64/stand/efiboot/bootx64/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 1 commented line; 15 total lines | .
`arch/amd64/stand/efiboot/cmd_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/conf.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/dev_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/diskprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/efidev.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/efidev.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/efirng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/exec_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/amd64/stand/efiboot/ldscript.amd64`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 66 total lines | .
**`arch/amd64/stand/efiboot/ldscript.i386`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 76 total lines | .
`arch/amd64/stand/efiboot/machdep.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/efiboot/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
`arch/amd64/stand/efiboot/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/amd64/stand/efiboot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 10 total lines | .
`arch/amd64/stand/efiboot/memprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/run_i386.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/run_i386.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/efiboot/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/efiboot/start_amd64.S` | Assembler code | Modified-BSD | OK | .
`arch/amd64/stand/efiboot/start_i386.S` | Assembler code | Simplified-BSD | OK | .
**`arch/amd64/stand/etc/genassym.cf`** | C code generator | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 31 total lines [False-Positive: this file is used by "kern/genassym.sh" to generate C code files; "Simplified-BSD"] | .
`arch/amd64/stand/fdboot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 13 total lines | .
`arch/amd64/stand/libsa/bioscons.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/biosdev.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/biosdev.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/biosprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/cmd_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/dev_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/disk.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/diskprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/elf32.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/elf64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/exec_i386.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/gateA20.c` | C code | CMU (similar to ISC) | OK | .
`arch/amd64/stand/libsa/gidt.h` | Assembler header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/gidt.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/libsa.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/machdep.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/mdrandom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/memprobe.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/pxeboot.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/stand/libsa/pxe_call.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/stand/libsa/pxe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/amd64/stand/libsa/pxe.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/pxe_net.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/pxe_net.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/pxe_netif.h` | C header | Simplified-BSD | OK | .
`arch/amd64/stand/libsa/run_amd64.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/softraid_amd64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/softraid_amd64.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/amd64/stand/libsa/time.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 11 commented lines; 46 total lines | .
`arch/amd64/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
**`arch/amd64/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 code lines + 5 commented lines; 36 total lines | .
`arch/amd64/stand/mbr/mbr.S` | Assembler code | Simplified-BSD | OK | .
`arch/amd64/stand/pxeboot/conf.c` | C code | Simplified-BSD | OK | .
`arch/amd64/stand/pxeboot/devopen.c` | C code | Simplified-BSD | OK | .
**`arch/amd64/stand/pxeboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 79 total lines | .
`arch/amd64/stand/pxeboot/open.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`arch/amd64/stand/pxeboot/pxeboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/amd64/stand/pxeboot/pxe_udp.c` | C code | Original-BSD | OK | .
`arch/amd64/stand/pxeboot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/acpiapm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/acpi_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/arm64/aesv8-armx.S` | Assembler code | Modified-BSD or GPL | OK | .
`arch/arm64/arm64/arm64_machdep.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/arm64var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/ast.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/autoconf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/bus_dma.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm64/arm64/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm64/arm64/copy.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/copystr.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/cpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/cpufunc_asm.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/cpuswitch.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/arm64/arm64/cryptox.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/db_disasm.c` | C code | Simplified-BSD | OK | .
`arch/arm64/arm64/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/arm64/arm64/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/arm64/arm64/disasm.c` | C code | Simplified-BSD | OK | .
`arch/arm64/arm64/disasm.h` | C header | Simplified-BSD | OK | .
`arch/arm64/arm64/disksubr.c` | C code | Modified-BSD | OK | .
`arch/arm64/arm64/exception.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/arm64/arm64/intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/locore0.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/locore.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/arm64/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/arm64/arm64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm64/arm64/pmap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/process_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/sig_machdep.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm64/arm64/softintr.c` | C code | Simplified-BSD | OK | .
`arch/arm64/arm64/support.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/arm64/syscall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/sys_machdep.c` | C code | Original-BSD | OK | .
`arch/arm64/arm64/trampoline.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/trap.c` | C code | Simplified-BSD | OK | .
`arch/arm64/arm64/vfp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/arm64/vm_machdep.c` | C code | Modified-BSD | OK | .
`arch/arm64/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented lines; 1 total line | .
`arch/arm64/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented lines; 1 total line | .
`arch/arm64/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 7 total lines | .
`arch/arm64/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/arm64/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented lines; 1 total line | .
**`arch/arm64/conf/files.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 163 non empty lines; 212 total lines | .
**`arch/arm64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 472 non empty lines; 508 total lines | .
`arch/arm64/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
**`arch/arm64/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 81 non empty lines; 88 total lines | .
**`arch/arm64/conf/Makefile.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 157 non empty lines; 192 total lines | .
**`arch/arm64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 307 non empty lines; 340 total lines | .
`arch/arm64/dev/acpiiort.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/acpiiort.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/acpipci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/agintc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/agtimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/ampintc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/apldart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/apldog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/apldwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplintc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplns.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplpcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplpinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplpmu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/aplspmi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/apm.c` | C code | Modified-BSD | OK | .
`arch/arm64/dev/arm64_bus_space.c` | C code | Simplified-BSD | OK | .
`arch/arm64/dev/bcm2836_intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/efi.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/dev/mainbus.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/dev/mainbus.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/pci_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/simplebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/simplebusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smbios.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smmu_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smmu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smmu_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smmureg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/dev/smmuvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/acpiapm.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/arm64/include/armreg.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/asm.h` | C header | Modified-BSD with Added CR for hyperbola | OK | .
`arch/arm64/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm64/include/bootconfig.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/cdefs.h` | C header | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 16 total lines | .
`arch/arm64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm64/include/cpufunc.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/cpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/arm64/include/disklabel.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/endian.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/exec.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/_float.h` | C header | Original-BSD (2X advertising clause, above and 3rd clause) | OK | .
`arch/arm64/include/float.h` | C header | Original-BSD (2X advertising clause, above and 3rd clause) | OK | .
`arch/arm64/include/frame.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/hypervisor.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm64/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/arm64/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/lock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/arm64/include/machdep.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 27 total lines | .
`arch/arm64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/arm64/include/openpromio.h` | C header | Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm64/include/param.h` | C header | Modified-BSD | OK | .
`arch/arm64/include/pcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/pmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/proc.h` | C header | Modified-BSD | OK | .
`arch/arm64/include/profile.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/pte.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/ptrace.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/arm64/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 106 non empty lines; 123 total lines | .
**`arch/arm64/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 75 non empty lines; 83 total lines | .
`arch/arm64/include/signal.h` | C header | Modified-BSD | OK | .
`arch/arm64/include/smbiosvar.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/softintr.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/spinlock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/sysarch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/trap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/include/_types.h` | C header | Modified-BSD | OK | .
`arch/arm64/include/vfp.h` | C header | Simplified-BSD | OK | .
`arch/arm64/include/vmparam.h` | C header | Modified-BSD | OK | .
**`arch/arm64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/arm64/stand/efiboot/acpi.dts`** | Device Tree Source File | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 56 total lines | .
`arch/arm64/stand/efiboot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/arm64/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
**`arch/arm64/stand/efiboot/dt_blob.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 380 non empty lines; 381 total lines | .
`arch/arm64/stand/efiboot/efiacpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/stand/efiboot/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/efidev.c` | C code | Simplified-BSD | OK | .
`arch/arm64/stand/efiboot/efidev.h` | C header | Simplified-BSD | OK | .
`arch/arm64/stand/efiboot/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/efirng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/exec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/arm64/stand/efiboot/ldscript.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 79 non empty lines; 82 total lines | .
`arch/arm64/stand/efiboot/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/arm64/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 71 non empty lines; 89 total lines | .
`arch/arm64/stand/efiboot/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/arm64/stand/efiboot/softraid_arm64.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/arm64/stand/efiboot/softraid_arm64.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm64/stand/efiboot/start.S` | Assembler code | Simplified-BSD | OK | .
`arch/arm64/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
`arch/arm/arm/arm32_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/arm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm/arm/ast.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bcopyinout.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bcopy_page.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bus_dma.c` | C code | Simplified-BSD | OK | .
`arch/arm/arm/bus_space_asm_generic.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/bus_space_notimpl.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/conf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/copystr.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpu.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpufunc_asm_armv7.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/arm/cpufunc_asm.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpufunc.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpuswitch7.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/db_disasm.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/db_interface.c` | C code | CMU (similar to ISC without full name in the penultimate line of the disclaimer) | OK | .
`arch/arm/arm/db_machdep.c` | C code | CMU (similar to ISC without full name in the penultimate line of the disclaimer) | OK | .
`arch/arm/arm/db_trace.c` | C code | CMU (similar to ISC without full name in the penultimate line of the disclaimer) | OK | .
`arch/arm/arm/disassem.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/disassem.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/disksubr.c` | C code | Modified-BSD | OK | .
`arch/arm/arm/exception.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/fault.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/fiq.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/fiq_subr.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/arm/arm/in_cksum_arm.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/irq_dispatch.S` | Assembler code | Modified-BSD (with differents names for copyrights) + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/mem.c` | C code | Modified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm/arm/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm/arm/pmap7.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non endorsement clause") + Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/process_machdep.c` | C code | Modified-BSD | OK | .
`arch/arm/arm/setstack.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/sigcode.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/sig_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/stubs.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/syscall.c` | C code | Simplified-BSD | OK | .
`arch/arm/arm/sys_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/undefined.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/armv7/armv7_space.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/armv7/armv7var.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 0 commented lines; 3 total lines | .
`arch/arm/armv7/bus_space_asm_armv7.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/vectors.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/vfp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/arm/vm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/arm/conf/files.arm`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 88 non empty lines; 105 total lines | .
**`arch/arm/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 81 non empty lines; 88 total lines | .
`arch/arm/cortex/agtimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/cortex/ampintc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/cortex/amptimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/cortex/arml2cc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/cortex/cortex.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/cortex/cortex.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/arm/cortex/files.cortex`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 24 total lines | .
`arch/arm/cortex/smc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/arm/include/armreg.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/asm.h` | C header | Modified-BSD | OK | .
`arch/arm/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm/include/bus.h` | C header | Simplified-BSD with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/include/cdefs.h` | C header | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 16 total lines | .
`arch/arm/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/cpuconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/cpufunc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/arm/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/endian.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/include/exec.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/arm/include/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/include/fiq.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/_float.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
`arch/arm/include/fp.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm/include/ieee.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
`arch/arm/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm/include/limits.h` | C header | Modified-BSD | OK | .
`arch/arm/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
**`arch/arm/include/machdep.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 27 total lines | .
`arch/arm/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/arm/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | ok | .
`arch/arm/include/param.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/pcb.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/arm/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/include/proc.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/profile.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/arm/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 53 total lines | .
**`arch/arm/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 59 non empty lines; 67 total lines | .
`arch/arm/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/softintr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/arm/include/swi.h` | C header | Public-Domain (notified in file) | OK | .
`arch/arm/include/sysarch.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/sysreg.h` | C header | Simplified-BSD | OK | .
`arch/arm/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/_types.h` | C header | Modified-BSD | OK | .
`arch/arm/include/undefined.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/vfp.h` | C header | Simplified-BSD | OK | .
`arch/arm/include/vmparam.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/mainbus/mainbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/mainbus/mainbus.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/simplebus/simplebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/arm/simplebus/simplebusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/armv7/armv7.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/armv7/armv7_machdep.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/armv7/armv7/armv7_machdep.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/armv7/armv7_start.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" ) | OK | .
`arch/armv7/armv7/armv7var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/armv7/autoconf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/armv7/armv7/genassym.cf` | "$OpenBSD: genassym.cf,v 1.1 2013/09/04 14:38:25 patrick Exp $" | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 1 total line | .
`arch/armv7/armv7/intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/armv7/locore0.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" ) | OK | .
`arch/armv7/armv7/platform.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/broadcom/bcm2836_intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/broadcom/files.broadcom` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented line; 5 total lines | .
`arch/armv7/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented line; 1 total line | .
`arch/armv7/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/armv7/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/armv7/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
**`arch/armv7/conf/files.armv7`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 58 non empty lines; 78 total lines | .
**`arch/armv7/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 380 non empty lines; 416 total lines | .
**`arch/armv7/conf/Makefile.armv7`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 134 non empty lines; 170 total lines | .
**`arch/armv7/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 262 non empty lines; 297 total lines | .
`arch/armv7/exynos/crosec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/crosec_kbd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/crosecvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/ec_commands.h` | C header | BSD-style license | ? | .
`arch/armv7/exynos/exclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exclockvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exdisplay.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exdisplayvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exdwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exehci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/exmct.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/exynos/expower.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/armv7/exynos/files.exynos`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 46 total lines | .
`arch/armv7/exynos/tps65090.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/imx/files.imx` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/armv7/imx/imxahci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/imx/imxtemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/include/apmvar.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 2 total lines | .
`arch/armv7/include/asm.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/bootconfig.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/armv7/include/bus.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/cdefs.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/conf.h` | C header | "Unlicensed!!!" | Trival; 6 code lines + 6 commented lines; 17 total lines | .
`arch/armv7/include/cpufunc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/cpu.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/db_machdep.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/disklabel.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/endian.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/exec.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/fdt.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/fp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/frame.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/ieeefp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/ieee.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/armv7/include/limits.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/armv7/include/lock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/machine_reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" V3) | OK | .
`arch/armv7/include/math.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/mplock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/mutex.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/openpromio.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/param.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/armv7/include/pcb.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/pci_machdep.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/armv7/include/pmap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/proc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/profile.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/ptrace.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/reg.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/reloc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 2 total lines | .
`arch/armv7/include/rtc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/signal.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/sysarch.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/armv7/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/include/trap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 2 commented lines; 4 total lines | .
`arch/armv7/include/_types.h` | C header | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`arch/armv7/include/vmparam.h` | C header | Modified-BSD | OK | .
**`arch/armv7/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/armv7/marvell/files.marvell`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 28 non empty lines; 37 total lines | .
`arch/armv7/marvell/mvacc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvagc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvahci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvmbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvmbusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvmpic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvpcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvpxa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvsysctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/marvell/mvxhci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/am335x.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/am335x_prcmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/amdisplay.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/amdisplayreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/dmtimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/edma.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/edmavar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/armv7/omap/files.omap`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 76 non empty lines; 99 total lines | .
`arch/armv7/omap/gptimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/if_cpsw.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/armv7/omap/if_cpswreg.h` | C header | Simplified-BSD | OK | .
`arch/armv7/omap/intc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/intc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/nxphdmi.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/armv7/omap/nxphdmivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap3.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap3_prcmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap4.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap4_prcmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omapid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omap_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omcm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omdisplay.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omehci.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
**`arch/armv7/omap/omehcivar.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 203 non empty lines; 232 total lines | .
`arch/armv7/omap/omgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omgpiovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/ommmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omohci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omrng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omsysc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omusbtll.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/omwugen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/prcm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/prcmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/omap/ti_iic.c` | C code | Simplified-BSD + Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK (Combined Licenses) | .
`arch/armv7/omap/ti_iicreg.h` | C header | Simplified-BSD | OK | .
`arch/armv7/stand/efiboot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/armv7/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
`arch/armv7/stand/efiboot/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/armv7/stand/efiboot/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/efidev.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/armv7/stand/efiboot/efidev.h` | C header | Simplified-BSD | OK | .
`arch/armv7/stand/efiboot/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/exec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/stand/efiboot/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/armv7/stand/efiboot/ldscript.arm`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 non empty lines; 63 total lines | .
`arch/armv7/stand/efiboot/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/armv7/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 67 non empty lines; 84 total lines | .
`arch/armv7/stand/efiboot/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/armv7/stand/efiboot/start.S` | Assembler code | Simplified-BSD | OK | .
`arch/armv7/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented lines; 5 total lines | .
`arch/armv7/sunxi/files.sunxi` | BSD kernel config. | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 17 total lines | .
`arch/armv7/sunxi/sxiahci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/sunxi/sxie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/sunxi/sxiintc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/sunxi/sxiintc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/sunxi/sxitimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/vexpress/files.vexpress` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/armv7/vexpress/pciecam.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/vexpress/sysreg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/armv7/xilinx/files.xilinx` | ? | "Unlicensed!!!" |  | .
`arch/armv7/xilinx/slcreg.h` | C header | Original-ISC | OK | .
`arch/armv7/xilinx/zqclock.c` | C code | Original-ISC | OK | .
`arch/armv7/xilinx/zqreset.c` | C code | Original-ISC | OK | .
`arch/hppa/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/hppa/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/hppa/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`arch/hppa/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/hppa/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
**`arch/hppa/conf/files.hppa`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 285 non empty lines; 350 total lines | .
**`arch/hppa/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 328 non empty lines; 361 total lines | .
`arch/hppa/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 8 total lines | .
`arch/hppa/conf/ld.script` | Linker script | Simplified-BSD | OK | .
**`arch/hppa/conf/Makefile.hppa`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 137 non empty lines; 174 total lines | .
**`arch/hppa/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 187 non empty lines; 216 total lines | .
`arch/hppa/dev/apic.c` | C code | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/dev/asp.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/astro.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/clock.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/com_dino.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/com_ssio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/cpu.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/cpudevs` | BSD kernel config. | Simplified-BSD | OK | .
**`arch/hppa/dev/cpudevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 353 non empty lines; 354 total lines | .
**`arch/hppa/dev/cpudevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 372 non empty lines; 373 total lines | .
`arch/hppa/dev/devlist2h.awk` | Awk code | Simplified-BSD | OK | .
`arch/hppa/dev/dino.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/elroy.c` | C code | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/dev/elroyreg.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/dev/elroyvar.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/dev/gecko.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/lasi.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/lcd.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/hppa/dev/lpt_ssio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 7 total lines | .
`arch/hppa/dev/mongoose.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/mongoosereg.h` | C header | Simplified-BSD | OK | .
`arch/hppa/dev/mongoosevar.h` | C header | Simplified-BSD | OK | .
`arch/hppa/dev/pdc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/phantomas.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/power.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/siop_sgc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/ssio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/ssiovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/sti_pci_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/dev/sti_sgc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/uturn.c` | C code | Simplified-BSD | OK | .
`arch/hppa/dev/viper.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/dev/wax.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/arcofi_gsc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/gsc/com_gsc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/fdc_gsc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/gscbus.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/gscbusvar.h` | C header | Simplified-BSD | OK | .
`arch/hppa/gsc/gsckbc.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/hppa/gsc/gsckbcreg.h` | C header | Simplified-BSD | OK | .
`arch/hppa/gsc/harmony.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/harmonyreg.h` | C header | Simplified-BSD | OK | .
`arch/hppa/gsc/harmonyvar.h` | C header | Simplified-BSD | OK | .
`arch/hppa/gsc/hil_gsc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/if_ie_gsc.c` | C code | Simplified-BSD | OK | .
**`arch/hppa/gsc/lpt_gsc.c`** | C code | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains restrictions for commercial use] | .
`arch/hppa/gsc/mongoose_gsc.c` | C code | Simplified-BSD | OK | .
`arch/hppa/gsc/oosiop_gsc.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/hppa/gsc/osiop_gsc.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/hppa/gsc/siop_gsc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/hppa/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/hppa/hppa/conf.c` | C code | Modified-BSD | OK | .
`arch/hppa/hppa/db_disasm.c` | C code | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") + Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK (Combined Licenses) | .
`arch/hppa/hppa/db_interface.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/disksubr.c` | C code | Custom-Modified-BSD with Added CR for hyperbola | OK | .
`arch/hppa/hppa/fpemu.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/hppa/fpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/hppa/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/hppa/hppa/in_cksum.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/intr.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/ipi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/hppa/lock_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/hppa/locore0.S` | Assembler code | Simplified-BSD + Hewlett-Packard (similar to Original-ISC but without endorsement clause) + CSL (similar to ISC) | OK (Combined Licenses) | .
`arch/hppa/hppa/locore.S` | Assembler code | Simplified-BSD + Hewlett-Packard (similar to Original-ISC but without endorsement clause) + CSL (similar to ISC) | OK (Combined Licenses) | .
`arch/hppa/hppa/machdep.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/mainbus.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/mem.c` | C code | Simplified-BSD + CMU (similar to ISC) + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/hppa/hppa/mutex.c` | C code | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`arch/hppa/hppa/pmap.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/process_machdep.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/sys_machdep.c` | C code | Modified-BSD | OK | .
`arch/hppa/hppa/trap.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/vm_machdep.c` | C code | Simplified-BSD | OK | .
`arch/hppa/hppa/wscons_machdep.c` | C code | Simplified-BSD | OK | .
`arch/hppa/include/asm.h` | Assembler header | CSL (similar to ISC) | OK | .
`arch/hppa/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/hppa/include/autoconf.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/cdefs.h` | C header | Custom-CMU (similar to ISC) | OK | .
`arch/hppa/include/conf.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
**`arch/hppa/include/cpufunc.h`** | C header | Simplified-BSD + Hewlett-Packard (similar to Original-ISC but without endorsement clause) + __Nonfree-License!!!__ (CSL disclaimer only) | __Nonfree!!!__ (CSL disclaimer only) | .
`arch/hppa/include/cpu.h` | C header | Simplified-BSD + CSL (similar to ISC) | OK (Combined Licenses) | .
`arch/hppa/include/db_machdep.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/hppa/include/eisa_machdep.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/exec.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/include/_float.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/fpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/include/frame.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/hil_machdep.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/hppa/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/hppa/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/iomod.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/isa_machdep.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/kcore.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/limits.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/hppa/include/mplock.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/mutex.h` | C header | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`arch/hppa/include/nvm.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/param.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/pcb.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/pdc.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/pmap.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/proc.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/profile.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/psl.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/pte.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/ptrace.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/include/rbus_machdep.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/include/reg.h` | C header | Simplified-BSD + CSL (similar to ISC) | OK (Combined Licenses) | .
`arch/hppa/include/reloc.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/hppa/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 4 commented lines; 7 total lines | .
`arch/hppa/include/signal.h` | C header | CSL (similar to ISC) | OK | .
`arch/hppa/include/som.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/hppa/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/include/trap.h` | C header | Simplified-BSD | OK | .
`arch/hppa/include/_types.h` | C header | Modified-BSD | OK | .
`arch/hppa/include/vmparam.h` | C header | CSL (similar to ISC) | OK | .
**`arch/hppa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/hppa/spmath/cnv_float.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dbl_float.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfadd.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfcmp.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfdiv.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfmpy.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfrem.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfsqrt.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/dfsub.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divsfm.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divsfr.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divsim.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divsir.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divufr.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divuir.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/divu.S` | Assembler code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/fcnvff.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/fcnvfx.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/fcnvfxt.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/fcnvxf.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/float.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/fpbits.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/frnd.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/hppa.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/impys.S` | Assembler code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/impyu.S` | Assembler code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/md.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpyaccs.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpyaccu.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpys.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpyscv.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpyu.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/mpyucv.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/quad_float.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfadd.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfcmp.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfdiv.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfmpy.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfrem.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfsqrt.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sfsub.c` | C code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/spmath/sgl_float.h` | C header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`arch/hppa/stand/boot/boot.8` | troff (manual) | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/hppa/stand/boot/conf.c` | C code | Simplified-BSD | OK | .
`arch/hppa/stand/boot/exec.c` | C code | Simplified-BSD | OK | .
**`arch/hppa/stand/boot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 49 total lines | .
**`arch/hppa/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 64 total lines | .
`arch/hppa/stand/boot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/hppa/stand/cdboot/cdboot.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/hppa/stand/cdboot/elf64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/hppa/stand/cdboot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 49 total lines | .
**`arch/hppa/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 66 total lines | .
`arch/hppa/stand/libsa/cmd_hppa.c` | C code | Simplified-BSD | OK | .
`arch/hppa/stand/libsa/ct.c` | C code | Simplified-BSD + OSF (similar to ISC and CMU) | OK (Combined Licenses) | .
`arch/hppa/stand/libsa/dev_hppa.c` | C code | Simplified-BSD | OK | .
**`arch/hppa/stand/libsa/dev_hppa.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 42 total lines | .
`arch/hppa/stand/libsa/dk.c` | C code | OSF (similar to ISC and CMU) | OK | .
`arch/hppa/stand/libsa/elf32.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/stand/libsa/elf64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/hppa/stand/libsa/itecons.c` | C code | Simplified-BSD + OSF (similar to ISC and CMU) | OK (Combined Licenses) | .
`arch/hppa/stand/libsa/lf.c` | C code | Simplified-BSD | OK | .
`arch/hppa/stand/libsa/libsa.h` | C header | Simplified-BSD | OK | .
`arch/hppa/stand/libsa/lif.c` | C code | Simplified-BSD | OK | .
`arch/hppa/stand/libsa/machdep.c` | C code | Simplified-BSD | OK | .
**`arch/hppa/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 44 total lines | .
**`arch/hppa/stand/libsa/pdc.c`** | C code | Simplified-BSD + OSF (similar to ISC and CMU) + __Nonfree-License!!!__ (Custom-BSD license) T2 | __Nonfree!!!__ [License contains distribution only] | .
`arch/hppa/stand/libsa/time.c` | C code | Simplified-BSD | OK | .
`arch/hppa/stand/libz/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/hppa/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 8 code lines + 1 commented lines; 11 total lines | .
**`arch/hppa/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 37 non empty lines; 41 total lines | .
`arch/hppa/stand/mkboot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented line; 14 total lines | .
`arch/hppa/stand/mkboot/mkboot.8` | troff (manual) | Modified-BSD | OK | .
`arch/hppa/stand/mkboot/mkboot.c` | C code | Modified-BSD | OK | .
`arch/i386/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/i386/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/i386/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line ; 8 total lines | .
`arch/i386/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 15 code lines; 20 total lines | .
`arch/i386/compile/RAMDISK_CD/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/i386/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line; 2 total lines | .
`arch/i386/conf/files.i386` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/i386/conf/GENERIC` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/i386/conf/GENERIC.MP` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/i386/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/i386/conf/Makefile.i386`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 131 code lines + 20 commented lines; 191 total lines | .
`arch/i386/conf/RAMDISK` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/i386/conf/RAMDISK_CD` | BSD kernel config. | Non-Copyrightable | OK | .
`arch/i386/eisa/eisa_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/eisa/eisa_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/acpiapm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/acpi_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/acpi_wakecode.S` | Assembler code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/i386/i386/amd64errata.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/amdmsr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/apic.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/apicvec.s` | Assembler code | Simplified-BSD | OK | .
`arch/i386/i386/apm.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/apmcall.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/i386/autoconf.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/bios.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/bus_dma.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/i386/i386/bus_space.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/i386/codepatch.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/cpu.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/db_disasm.c` | C code | CMU (similar to ISC) | OK | .
`arch/i386/i386/db_interface.c` | C code  | CMU (similar to ISC) | OK | .
`arch/i386/i386/db_memrw.c` | C code | CMU (similar to ISC) | OK | .
`arch/i386/i386/db_mp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/i386/i386/disksubr.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/dkcsum.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/esm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/esmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/esmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/est.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/gdt.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/i386/i386/hibernate_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/i686_mem.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/in_cksum.s` | Assembler code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/i386/i386/ioapic.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/ipifuncs.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/k1x-pstate.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/k6_mem.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/lapic.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/locore0.S` | Assembler code | Modified-BSD | OK | .
`arch/i386/i386/locore.s` | Assembler code | Modified-BSD | OK | .
`arch/i386/i386/longrun.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/machdep.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/i386/i386/mainbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/mem.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/mpbios.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/mpbios_intr_fixup.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/mp_setperf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/mptramp.s` | Assembler code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/mtrr.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/nvram.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/p4tcc.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/pctr.c` | C code | Mazieres-BSD-style | OK | .
`arch/i386/i386/pmapae.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/i386/pmap.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/powernow.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/powernow-k7.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/powernow-k8.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/process_machdep.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/rbus_machdep.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/softintr.c` | C code | Simplified-BSD | OK | .
`arch/i386/i386/sys_machdep.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/trap.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/ucode.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/vector.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/via.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/i386/vm_machdep.c` | C code | Modified-BSD | OK | .
`arch/i386/i386/wscons_machdep.c` | C code | Simplified-BSD | OK | .
`arch/i386/include/acpiapm.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/amdmsr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/apicvar.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/include/asm.h` | C header | Modified-BSD | OK | .
`arch/i386/include/atomic.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/biosvar.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/include/cdefs.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/include/codepatch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/cpu_full.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/cpufunc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/cpu.h` | C header | Modified-BSD | OK | .
`arch/i386/include/cputypes.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/include/cpuvar.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/i386/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/exec.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/include/fenv.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/_float.h` | C header | Modified-BSD | OK | .
`arch/i386/include/frame.h` | C header | Modified-BSD | OK | .
`arch/i386/include/gdt.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/hibernate.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/hibernate_var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/i82093reg.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/i82093var.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/i82489reg.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/i82489var.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/i8259.h` | C header | Modified-BSD | OK | .
`arch/i386/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/i386/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/i386/include/intrdefs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 62 commented line ; 131 total lines | .
`arch/i386/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/ioctl_fd.h` | C header | Simplified-BSD | OK | .
**`arch/i386/include/joystick.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 2 commented lines ; 24 total lines | .
`arch/i386/include/kcore.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/limits.h` | C header | Modified-BSD | OK | .
`arch/i386/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/i386/include/mouse.h` | C header | Custom-Simplified-BSD (second clause removed) | OK | .
`arch/i386/include/mpbiosreg.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/mpbiosvar.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/i386/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`arch/i386/include/npx.h` | C header | Modified-BSD | OK | .
`arch/i386/include/param.h` | C header | Modified-BSD | OK | .
`arch/i386/include/pcb.h` | C header | Modified-BSD | OK | .
`arch/i386/include/pctr.h` | C header | Mazieres-BSD-style | OK | .
**`arch/i386/include/pic.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 8 commented lines; 43 total lines | .
`arch/i386/include/pio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/pmap.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/proc.h` | C header | Modified-BSD | OK | .
`arch/i386/include/profile.h` | C header | Modified-BSD | OK | .
`arch/i386/include/psl.h` | C header | Modified-BSD | OK | .
`arch/i386/include/pte.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/rbus_machdep.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/reg.h` | C header | Modified-BSD | OK | .
`arch/i386/include/reloc.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/segments.h` | C header | Modified-BSD | OK | .
`arch/i386/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 10 code lines + 5 commented lines; 19 total lines | .
`arch/i386/include/signal.h` | C header | Modified-BSD | OK | .
`arch/i386/include/smbiosvar.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/specialreg.h` | C header | Modified-BSD | OK | .
`arch/i386/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 11 total lines | .
**`arch/i386/include/sysarch.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 5 commented lines; 39 total lines | .
`arch/i386/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/trap.h` | C header | Modified-BSD | OK | .
`arch/i386/include/tss.h` | C header | Modified-BSD | OK | .
`arch/i386/include/_types.h` | C header | Modified-BSD | OK | .
`arch/i386/include/vga_post.h` | C header | Simplified-BSD | OK | .
`arch/i386/include/vmmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/include/vmparam.h` | C header | Modified-BSD | OK | .
`arch/i386/isa/ahc_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/clock.c` | C code | Modified-BSD + CMU + Intel (similar to ISC and CMU but without endorsement) | OK (Combined Licenses) | .
`arch/i386/isa/icu.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/isa_machdep.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/i386/isa/isa_machdep.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/i386/isa/isapnp_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/isa/isapnp_machdep.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/isa/joy.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/joy_isa.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/joy_isapnp.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/joyreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/lms.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/i386/isa/mms.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/i386/isa/npx.c` | C code | Modified-BSD | OK | .
`arch/i386/isa/nvram.h` | C header | Modified-BSD | OK | .
**`arch/i386/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 5 commented lines; 51 total lines | .
`arch/i386/pci/agp_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/pci/ali1543.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/pci/amd756.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/amd756reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/elan520.c` | C code | Simplified-BSD | OK | .
`arch/i386/pci/elan520reg.h` | C header | Simplified-BSD | OK | .
`arch/i386/pci/geodesc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/geodescreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/glxsb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/gscpcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/gscpcibreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/gscpm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/gscpmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/ichpcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/pci/opti82c558.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/opti82c558reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/opti82c700.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/opti82c700reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/pchb.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/pci/pci_addr_fixup.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/pcib.c` | C code | Simplified-BSD | OK | .
`arch/i386/pci/pcibios.c` | C code | Simplified-BSD + Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/pcibiosvar.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/pci_bus_fixup.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/pcic_pci_machdep.c` | C code | Simplified-BSD | OK | .
`arch/i386/pci/pciide_gcsc_reg.h` | C header | Simplified-BSD | OK | .
`arch/i386/pci/pciide_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/pci_intr_fixup.c` | C code | Simplified-BSD + Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/pci/pci_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/piix.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/piixreg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/piixvar.h` | C header | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/rccosb4.c` | C code | Simplified-BSD | OK | .
`arch/i386/pci/rccosb4reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/sis85c503.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/sis85c503reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/vga_post.c` | C code | Simplified-BSD | OK | .
`arch/i386/pci/via8231.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/via8231reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/pci/via82c586.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/via82c586reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`arch/i386/stand/biosboot/biosboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/i386/stand/biosboot/biosboot.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/biosboot/ld.script` | Linker script | "Unlicensed!!!" | Trivial; 11 code lines; 13 total lines | .
**`arch/i386/stand/biosboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 code lines + 2 commented lines; 34 total lines | .
`arch/i386/stand/boot/boot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/i386/stand/boot/check-boot.pl` | Perl script | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/boot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/i386/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 94 total lines | .
`arch/i386/stand/boot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/cdboot/cdboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/i386/stand/cdboot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/i386/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 81 total lines | .
`arch/i386/stand/cdboot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/cdbr/cdbr.S` | Assembler code | Modified-BSD | OK | .
**`arch/i386/stand/cdbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 23 code lines + 3 commented lines; 33 total lines | .
**`arch/i386/stand/etc/genassym.cf`** | C code generator | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 31 total lines | .
`arch/i386/stand/fdboot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 13 total lines | .
`arch/i386/stand/libsa/apmprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/bioscons.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/biosdev.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/biosdev.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/biosprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/cmd_i386.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/cpuprobe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/debug.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/debug.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/debug_i386.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/debug_md.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/dev_i386.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/disk.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/diskprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/elf32.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/elf64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/exec_i386.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/gateA20.c` | C code | CMU (similar to ISC) | OK | .
`arch/i386/stand/libsa/gidt.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/gidt.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/libsa.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/machdep.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/mdrandom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/memprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pciprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/ps2probe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pslid.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pxeboot.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/stand/libsa/pxe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/stand/libsa/pxe_call.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/stand/libsa/pxe.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pxe_net.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pxe_net.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/pxe_netif.h` | C header | Simplified-BSD | OK | .
`arch/i386/stand/libsa/smpprobe.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/libsa/softraid_i386.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/softraid_i386.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/i386/stand/libsa/time.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
**`arch/i386/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 11 commented lines; 46 total lines | .
**`arch/i386/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 5 commented lines; 34 total lines | .
`arch/i386/stand/mbr/mbr.S` | Assembler code | Simplified-BSD | OK | .
`arch/i386/stand/pxeboot/conf.c` | C code | Simplified-BSD | OK | .
`arch/i386/stand/pxeboot/devopen.c` | C code | Simplified-BSD | OK | .
**`arch/i386/stand/pxeboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 3 commented lines; 80 total lines | .
`arch/i386/stand/pxeboot/open.c` | C code | Modified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/i386/stand/pxeboot/pxeboot.8` | troff (manual) | Simplified-BSD | OK | .
`arch/i386/stand/pxeboot/pxe_udp.c` | C code | Original-BSD | OK | .
`arch/i386/stand/pxeboot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/landisk/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/landisk/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/landisk/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/landisk/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/landisk/conf/files.landisk`** | ? | __Unlicensed!!!__ | Non-Trivial; 77 non empty lines; 98 total lines | .
**`arch/landisk/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 162 non empty lines; 186 total lines | .
`arch/landisk/conf/ld.script` | Linker script | "Unlicensed!!!" | Trivial; 0 code line + 0 commented line; 0 total line | **0 line**
**`arch/landisk/conf/Makefile.landisk`** | ? | __Unlicensed!!!__ | Non-Trivial; 128 non empty lines; 163 total lines | .
**`arch/landisk/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 78 non empty lines; 97 total lines | .
`arch/landisk/dev/obio.c` | C code | Simplified-BSD | OK | .
`arch/landisk/dev/obiovar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/landisk/dev/power.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/landisk/dev/rs5c313.c` | C code | Simplified-BSD | OK | .
`arch/landisk/dev/rs5c313reg.h` | C header | Simplified-BSD | OK | .
`arch/landisk/dev/wdc_obio.c` | C code | Simplified-BSD | OK | .
`arch/landisk/include/asm.h` | Assembler header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/landisk/include/autoconf.h` | C header | Simplified-BSD | OK | .
`arch/landisk/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/landisk/include/cdefs.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/landisk/include/cpu.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 26 total lines | .
`arch/landisk/include/cputypes.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/db_machdep.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/disklabel.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/endian.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/exec.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/landisk/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/fpu.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/frame.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/ieeefp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/ieee.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/landisk/include/kcore.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/limits.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/landisk/include/lock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/param.h` | C header | Modified-BSD | OK | .
`arch/landisk/include/pcb.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/landisk/include/pmap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/proc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/profile.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/psl.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/pte.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/ptrace.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/reg.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/reloc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/signal.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/landisk/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/landisk/include/trap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/_types.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/landisk/include/vmparam.h` | C header | "Unlicensed!!!" | Trivial; 8 code lines + 2 commented lines; 15 total lines | .
`arch/landisk/landisk/autoconf.c` | C code | Simplified-BSD | OK | .
`arch/landisk/landisk/bus_dma.c` | C code | Simplified-BSD | OK | .
`arch/landisk/landisk/clock_machdep.c` | C code | Simplified-BSD | OK | .
`arch/landisk/landisk/conf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/landisk/landisk/consinit.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/landisk/landisk/disksubr.c` | C code | Modified-BSD | OK | .
`arch/landisk/landisk/genassym.cf` | C code generator | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 1 total line | .
`arch/landisk/landisk/intr.c` | C code | Simplified-BSD | OK | .
`arch/landisk/landisk/landiskreg.h` | C header | Simplified-BSD | OK | .
`arch/landisk/landisk/locore0.S` | Assembler code | Simplified-BSD | OK | .
`arch/landisk/landisk/locore.S` | Assembler code | Simplified-BSD | OK | .
`arch/landisk/landisk/machdep.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/landisk/landisk/mainbus.c` | C code | Simplified-BSD | OK | .
`arch/landisk/landisk/shpcic_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/landisk/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/landisk/stand/boot/boot.8` | troff (manual) | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/landisk/stand/boot/conf.c` | C code | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/landisk/stand/boot/delay.c` | C code | Simplified-BSD | OK | .
`arch/landisk/stand/boot/devs.c` | C code | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
**`arch/landisk/stand/boot/getsecs.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 168 non empty lines; 206 total lines | .
`arch/landisk/stand/boot/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
**`arch/landisk/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 42 non empty lines; 54 total lines | .
`arch/landisk/stand/boot/scifcons.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" V3) + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/landisk/stand/boot/srt0.S` | Assembler code | Simplified-BSD | OK | .
`arch/landisk/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 6 total lines | .
`arch/landisk/stand/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 10 code lines + 1 commented lines; 13 total lines | .
**`arch/landisk/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 34 total lines | .
`arch/landisk/stand/mbr/mbr.8` | troff (manual) | Original-ISC ("and" is used instead of "and/or") ("LOSS OF MIND" instead of "loss of use") | OK | .
`arch/landisk/stand/mbr/mbr.S` | Assembler code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/landisk/stand/xxboot/boot1.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
**`arch/landisk/stand/xxboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 54 total lines | .
`arch/landisk/stand/xxboot/pbr.S` | Assembler code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/landisk/stand/xxboot/xxboot.8` | troff (manual) | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/landisk/stand/xxboot/xxboot.S` | Assembler code | Simplified-BSD | OK | .
`arch/loongson/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total line | .
`arch/loongson/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line  + 0 commented line; 1 total line | .
`arch/loongson/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/loongson/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/loongson/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
**`arch/loongson/conf/files.loongson`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 114 non empty lines; 140 total lines | .
**`arch/loongson/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 226 non empty lines; 249 total lines | .
`arch/loongson/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 10 total lines | .
**`arch/loongson/conf/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 55 non empty lines; 61 total lines | .
**`arch/loongson/conf/Makefile.loongson`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 143 non empty lines; 180 total lines | .
**`arch/loongson/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 134 non empty lines; 155 total lines | .
`arch/loongson/dev/apm.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`arch/loongson/dev/bonito.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/loongson/dev/bonito_irq.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/loongson/dev/bonitoreg.h`** | C header | __Nonfree-License!!!__ (Custom license) Bonito Register Map | __Nonfree!!!__ [License contains use and modification only] | .
`arch/loongson/dev/bonitovar.h` | C header | Simplified-BSD | OK | .
`arch/loongson/dev/com_leioc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/gdiumiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/glx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/glxclk.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/htb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/htbreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/htbvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/kb3310.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/kb3310var.h` | C header | Original-ISC | OK | .
`arch/loongson/dev/leioc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/leiocreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/leiocvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/mainbus.c` | C code | Simplified-BSD | OK | .
`arch/loongson/dev/mcclock.c` | C code | CMU (similar to ISC) | OK | .
`arch/loongson/dev/mcclock_isa.c` | C code | CMU (similar to ISC) | OK | .
`arch/loongson/dev/mcclockvar.h` | C header | CMU (similar to ISC) | OK | .
`arch/loongson/dev/ohci_voyager.c` | C code | Simplified-BSD | OK | .
`arch/loongson/dev/pcib.c` | C code | Simplified-BSD | OK | .
`arch/loongson/dev/radeonfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/sisfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/smfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/smfbreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/stsec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/voyager.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/voyagerreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/dev/voyagervar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/loongson/include/asm.h` | Assembler header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/autoconf.h` | C header | Simplified-BSD | OK | .
`arch/loongson/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/loongson/include/cdefs.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/loongson/include/cpu.h` | C header | Modified-BSD + Custom-Modified-Expat | OK (Combined Licenses) | .
`arch/loongson/include/cpustate.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/db_machdep.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/disklabel.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/endian.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/exec.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/loongson/include/fpu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/frame.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/hibernate.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/include/hibernate_var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/ieee.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/loongson/include/isa_machdep.h` | C header | Old-Expat (with legal disclaimer 2) | OK | .
`arch/loongson/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/limits.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/loongson/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/loongson2.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/loongson3.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/memconf.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/mips_opcode.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/mplock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/loongson/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/loongson/include/param.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/pcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/loongson/include/pmap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/pmon.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/include/proc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/profile.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/pte.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/ptrace.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/regdef.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/reg.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/regnum.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/reloc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/setjmp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/signal.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/sysarch.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/include/trap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/_types.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/include/vmparam.h` | C header | Public-Domain (notified in file) | OK | .
`arch/loongson/loongson/autoconf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/bus_dma.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/loongson/loongson/bus_space.c` | C code | Simplified-BSD | OK | .
`arch/loongson/loongson/conf.c` | C code | Original-BSD | OK | .
`arch/loongson/loongson/disksubr.c` | C code | Modified-BSD | OK | .
`arch/loongson/loongson/gdium_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/genassym.cf` | C code generator | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 1 total line | .
`arch/loongson/loongson/generic2e_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/loongson/loongson/generic3a_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/hibernate_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/isa_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/locore0.S` | Assembler code | Simplified-BSD | OK | .
`arch/loongson/loongson/locore.S` | Assembler code | Simplified-BSD | OK | .
`arch/loongson/loongson/loongson2_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/loongson3_intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/loongson3_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola + Simplified-BSD | OK (Combined Licenses) | .
`arch/loongson/loongson/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/loongson/loongson/pmon32.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/pmon.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/loongson/wscons_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/loongson/loongson/yeeloong_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/loongson/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/loongson/stand/boot/conf.c` | C code | Modified-BSD | OK | .
`arch/loongson/stand/boot/cons.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/stand/boot/dev.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/loongson/stand/boot/devopen.c` | C code | Simplified-BSD | OK | .
`arch/loongson/stand/boot/exec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/loongson/stand/boot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 27 total lines | .
`arch/loongson/stand/boot/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/stand/boot/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/loongson/stand/boot/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 12 code lines + 1 commented line; 15 total lines | .
**`arch/loongson/stand/boot/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 44 total lines | .
`arch/loongson/stand/boot/rd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/loongson/stand/boot/start.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/loongson/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 50 total lines | .
`arch/loongson/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 8 total lines | .
**`arch/loongson/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 38 total lines | .
`arch/loongson/stand/mbr/Makefile` | Makefile | "Unlicensed!!!" | Trival; 11 code lines + 1 commented lines; 19 total lines | .
`arch/loongson/stand/mbr/mbr.uu` | ? | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 15 total lines | .
`arch/luna88k/cbus/cbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/cbus/cbusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/cbus/i82365_cbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/cbus/if_ne_cbus.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/cbus/nec86.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/nec86hw.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/nec86hwvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/nec86reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/nec86var.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/necsb.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/cbus/pcex.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/luna88k/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`arch/luna88k/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/luna88k/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/luna88k/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
**`arch/luna88k/conf/files.luna88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 88 non empty lines; 111 total lines | .
**`arch/luna88k/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 78 non empty lines; 97 total lines | .
`arch/luna88k/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 3 code lines + 10 commented lines; 15 total lines | .
`arch/luna88k/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
**`arch/luna88k/conf/Makefile.luna88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 131 non empty lines; 166 total lines | .
**`arch/luna88k/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 45 total lines | .
`arch/luna88k/dev/if_le.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/lcd.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/luna88k/dev/lunafb.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/lunaws.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/maskbits.h` | C header | Modified-BSD | OK | .
`arch/luna88k/dev/mb89352.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") without disclaimer + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`arch/luna88k/dev/mb89352reg.h` | C header | Modified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/luna88k/dev/mb89352var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") without disclaimer + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`arch/luna88k/dev/omkbdmap.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/omkbdmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/dev/omrasops1.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/dev/omrasops.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/omrasops.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/dev/sio.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/sioreg.h` | C header | Original-BSD | OK | .
`arch/luna88k/dev/siotty.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/siovar.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/dev/spc.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/timekeeper.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/dev/timekeeper.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/dev/xp.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/include/asm.h` | Assembler header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/asm_macro.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/autoconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/include/board.h` | C header | CMU (similar to ISC) | OK | .
`arch/luna88k/include/bus.h` | C header | Simplified-BSD + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`arch/luna88k/include/cdefs.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/cmmu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/conf.h` | C header | Custom-BSD (modified source clause and not endorsement clause) | OK | .
`arch/luna88k/include/cpu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/db_machdep.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/include/endian.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/exec.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/luna88k/include/fpu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/frame.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/ieee.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/intr.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/luna88k/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/lcd.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/luna88k/include/limits.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/m88100.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/m8820x.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/mmu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/mutex.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/param.h` | C header | Modified-BSD | OK | .
`arch/luna88k/include/pcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/pcex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/include/pmap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/pmap_table.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/proc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/profile.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/psl.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/ptrace.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/reg.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/reloc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/setjmp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/signal.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/include/trap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/_types.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/vmparam.h` | C header | Public-Domain (notified in file) | OK | .
`arch/luna88k/include/xpio.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/luna88k/autoconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/luna88k/clock.c` | C code | Original-BSD | OK | .
`arch/luna88k/luna88k/clockvar.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/luna88k/conf.c` | C code | Modified-BSD | OK | .
`arch/luna88k/luna88k/disksubr.c` | C code | Original-BSD | OK | .
`arch/luna88k/luna88k/eh.S` | Assembler code | Simplified-BSD | OK | .
`arch/luna88k/luna88k/genassym.cf` | C code generator | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 1 total lines | .
`arch/luna88k/luna88k/isr.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/luna88k/isr.h` | C header | Simplified-BSD | OK | .
`arch/luna88k/luna88k/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/m8820x.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/mainbus.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/luna88k/pmap_table.c` | C code | CMU (similar to ISC) | OK | .
**`arch/luna88k/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/luna88k/stand/boot/awaitkey.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/stand/boot/bcd.c` | C code | Public-Domain (notified in file) | OK | .
`arch/luna88k/stand/boot/bmc.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/bmd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/boot.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/boot.ldscript` | ? | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/stand/boot/conf.c` | C code | Modified-BSD | OK | .
`arch/luna88k/stand/boot/dev_net.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/stand/boot/dev_net.h` | C header | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented lines; 10 total lines | .
`arch/luna88k/stand/boot/devopen.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/fault.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/luna88k/stand/boot/font.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/getline.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/getsecs.c` | C code | Simplified-BSD | OK | .
`arch/luna88k/stand/boot/if_le.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/init_main.c` | C code | Original-BSD + Modified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/kbd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/kbdreg.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/lance.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/lance.h` | C header | Modified-BSD | OK | .
`arch/luna88k/stand/boot/locore.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") + CMU (similar to ISC) | OK (Combined Licenses) | .
**`arch/luna88k/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 58 non empty lines; 75 total lines | .
`arch/luna88k/stand/boot/parse.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/prf.c` | C code | Modified-BSD | OK | .
`arch/luna88k/stand/boot/rcvbuf.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/samachdep.h` | C header | Modified-BSD | OK | .
`arch/luna88k/stand/boot/sc.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/scsireg.h` | C header | Modified-BSD | ok | .
`arch/luna88k/stand/boot/scsivar.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/setjmp.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/luna88k/stand/boot/sio.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sioreg.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/status.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/ufs_disksubr.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 8 total lines | .
`arch/luna88k/stand/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 2 commented lines; 8 total lines | .
**`arch/m88k/conf/files.m88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 55 total lines | .
`arch/m88k/include/asm.h` | Assembler header | CMU (similar to ISC) | OK | .
`arch/m88k/include/asm_macro.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/m88k/include/cdefs.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/m88k/include/cmmu.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/cpu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/m88k/include/exec.h` | C header | "Unlicensed!!!" | Trival; 13 code lines + 2 commented lines; 21 total lines | .
`arch/m88k/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/include/_float.h` | C header | Modified-BSD | OK | .
`arch/m88k/include/fpu.h` | C header | Old-Expat (with legal disclaimer 2) | OK | .
`arch/m88k/include/frame.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/ieeefp.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`arch/m88k/include/kcore.h` | C header | Simplified-BSD | OK | .
`arch/m88k/include/limits.h` | C header | Modified-BSD | OK | .
`arch/m88k/include/lock.h` | C header | Simplified-BSD | OK | .
`arch/m88k/include/m88100.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/m8820x.h` | C header | Simplified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/m88k/include/mmu.h` | C header | Modified-BSD | OK | .
`arch/m88k/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/m88k/include/mutex.h` | C header | Simplified-BSD | OK | .
`arch/m88k/include/param.h` | C header | Modified-BSD | OK | .
`arch/m88k/include/pcb.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/pmap.h` | C header | Old-Expat (without disclaimer 2) | OK | .
`arch/m88k/include/pmap_table.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/proc.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/profile.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/m88k/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/ptrace.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/reloc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/m88k/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 4 code lines + 4 commented lines; 9 total lines | .
`arch/m88k/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/m88k/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/include/trap.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/include/_types.h` | C header | Modified-BSD | OK | .
`arch/m88k/include/vmparam.h` | C header | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/atomic.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/m88k/db_disasm.c` | C code | Simplified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/m88k/m88k/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/db_sstep.c` | C code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/eh_common.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/fpu.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/m88k/m88k/fpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/m88k/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/m88k/m88k/in_cksum.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/m88100_fp.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/m88k/m88k/m88100_machdep.c` | C code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/m88110_fp.c` | C code | Old-Expat (with legal disclaimer 2) | OK | .
`arch/m88k/m88k/m88110_mmu.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/m8820x_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/m88k/m88k/m88k_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/mem.c` | C code | Modified-BSD | OK | .
`arch/m88k/m88k/mutex.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/m88k/m88k/pmap.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/process_machdep.c` | C code | Modified-BSD | OK | .
`arch/m88k/m88k/process.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/sig_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/subr.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/trap.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/vectors_88100.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/vectors_88110.S` | Assembler code | CMU (similar to ISC) | OK | .
`arch/m88k/m88k/vm_machdep.c` | C code | Modified-BSD | OK | .
`arch/macppc/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/macppc/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/macppc/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/macppc/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/macppc/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/macppc/conf/files.macppc`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 228 non empty lines; 296 total lines | .
**`arch/macppc/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 376 non empty lines; 413 total lines | .
`arch/macppc/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
`arch/macppc/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
**`arch/macppc/conf/Makefile.macppc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 144 non empty lines; 181 total lines | .
**`arch/macppc/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 179 non empty lines; 202 total lines | .
`arch/macppc/dev/abtn.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/adb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/macppc/dev/adbvar.h` | C header | Simplified-BSD | OK | .
`arch/macppc/dev/akbd_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/aoa.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/apm.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`arch/macppc/dev/asms.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/awacs.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/daca.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/dbdma.c` | C code | OSF (similar to ISC and CMU) | OK | .
`arch/macppc/dev/dbdma.h` | C header | OSF (similar to ISC and CMU) | OK | .
`arch/macppc/dev/dfs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/i2s.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/i2sreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/i2svar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/if_bm.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/if_bmreg.h` | C header | OSF (similar to ISC and CMU) | OK | .
`arch/macppc/dev/if_mc.c` | C code | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`arch/macppc/dev/if_wi_obio.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/macppc/dev/kiic.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/kiicvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/macgpio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/maci2c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/maci2cvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/macintr.c` | C code | Modified-BSD | OK | .
`arch/macppc/dev/mediabay.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/onyx.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/openpic.c` | C code | Modified-BSD | OK | .
`arch/macppc/dev/openpicreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/pgs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/piic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/pm_direct.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/pm_direct.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/smu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/snapper.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/sysbutton.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/thermal.c` | C code | Simplified-BSD | OK | .
`arch/macppc/dev/thermal.h` | C header | Simplified-BSD | OK | .
`arch/macppc/dev/tumbler.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/dev/uni_n.c` | C code | Simplified-BSD | OK | .
`arch/macppc/dev/viareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/wdc_obio.c` | C code | Simplified-BSD | OK | .
`arch/macppc/dev/xlights.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/dev/zs.c` | C code | Custom-Original-BSD (third clause is a "non endorsement clause" and four clause is the "advertising clause") T1 | OK | .
`arch/macppc/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/include/asm.h` | Assembler header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/macppc/include/autoconf.h` | C header | Simplified-BSD | OK | .
`arch/macppc/include/bus.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/cdefs.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/db_machdep.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/endian.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/exec.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/macppc/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/fpu.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/frame.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/ieeefp.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/ieee.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/intr.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/macppc/include/ipkdb.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/kbio.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/kcore.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/limits.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/macppc/include/lock.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/mplock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/macppc/include/param.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/pcb.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/macppc/include/pio.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/macppc/include/pmap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/proc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/profile.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/psl.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/pte.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/ptrace.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/rbus_machdep.h` | C header | Simplified-BSD | OK | .
`arch/macppc/include/reg.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/reloc.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/signal.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/macppc/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/include/trap.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/macppc/include/_types.h` | C header | Public-Domain (notified in file) | OK | .
`arch/macppc/include/vmparam.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/macppc/macppc/autoconf.c` | C code | Modified-BSD | OK | .
`arch/macppc/macppc/clock.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/cpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/disksubr.c` | C code | Custom-Modified-BSD with Added CR for hyperbola | OK | .
`arch/macppc/macppc/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/macppc/macppc/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/mainbus.c` | C code | CMU (similar to ISC) | OK | .
`arch/macppc/macppc/mem.c` | C code | Modified-BSD | OK | .
`arch/macppc/macppc/ofw_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/ofw_machdep.h` | C header | Simplified-BSD | OK | .
`arch/macppc/macppc/ofwreal.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/opendev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/openfirm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/macppc/macppc/rbus_machdep.c` | C code | Simplified-BSD | OK | .
`arch/macppc/macppc/wscons_machdep.c` | C code | Simplified-BSD | OK | .
**`arch/macppc/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/macppc/pci/agp_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/pci/hpb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/pci/ht.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/pci/kauaiata.c` | C code | Simplified-BSD | OK | .
`arch/macppc/pci/macobio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/pci/macobio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/pci/mpcpcibus.c` | C code | Simplified-BSD | OK | .
`arch/macppc/pci/pchb.c` | C code | Simplified-BSD | OK | .
`arch/macppc/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/pci/pci_machdep.c` | C code | Simplified-BSD | OK | .
`arch/macppc/pci/vgafb.c` | C code | CMU (similar to ISC) | OK | .
`arch/macppc/README` | Readme | "Unlicensed!!!" | Trivial; 0 code line + 12 'text' lines;  14 total lines | .
`arch/macppc/stand/alloc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/boot.8` | troff (manual) | Simplified-BSD with Added CR for hyperbola | OK | .
**`arch/macppc/stand/cache.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 18 non empty lines; 20 total lines | .
`arch/macppc/stand/conf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/stand/hfs.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/macppc/stand/hfs.h` | C header | "Unlicensed!!!" | Trivial; 7 code lines + 2 commented lines; 10 total lines | .
`arch/macppc/stand/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/macppc/stand/Locore.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/main.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/macppc/stand/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 38 total lines | .
**`arch/macppc/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 34 total lines | .
`arch/macppc/stand/mbr/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 14 total lines | .
`arch/macppc/stand/mbr/mbr.uu` | ? | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 15 total lines | .
`arch/macppc/stand/net.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/netif_of.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/ofdev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/ofdev.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/ofwboot/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/macppc/stand/ofwboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 46 total lines | .
`arch/macppc/stand/ofwmagic.S` | Assembler code | Simplified-BSD | OK | .
`arch/macppc/stand/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/tbxidata/bsd.tbxi` | ? | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/macppc/stand/tbxidata/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 11 total lines | .
**`arch/mips64/conf/files.mips64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 32 non empty lines; 38 total lines | .
`arch/mips64/include/archtype.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/asm.h` | Assembler header | Simplified-BSD | OK | .
`arch/mips64/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/mips64/include/autoconf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/cache.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/cdefs.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/cpu.h` | C header | Modified-BSD + Custom-Modified-Expat | OK (Combined Licenses) | .
`arch/mips64/include/cpustate.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/db_machdep.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/exec.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/_float.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/fpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/frame.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/mips64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/mips64/include/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/mips64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/mips64/include/loongson2.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/loongson3.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/memconf.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/mips_cpu.h` | C header | Modified-BSD + Custom-Modified-Expat | OK (Combined Licenses) | .
`arch/mips64/include/mips_opcode.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/mips64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/mips64/include/param.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/pcb.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/pmap.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/proc.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/profile.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/pte.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/ptrace.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/regdef.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/reg.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/regnum.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/reloc.h` | C header | Simplified-BSD | OK | .
`arch/mips64/include/setjmp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/mips64/include/signal.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/mips64/include/sysarch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/include/trap.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/_types.h` | C header | Modified-BSD | OK | .
`arch/mips64/include/vmparam.h` | C header | Modified-BSD | OK | .
`arch/mips64/mips64/cache_loongson2.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/mips64/cache_loongson3.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/mips64/cache_mips64r2.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/mips64/cache_octeon.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/mips64/mips64/clock.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/context.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/cp0access.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/cpu.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/db_disasm.c` | C code | Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
`arch/mips64/mips64/db_machdep.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/exception.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/fp_emulate.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/mips64/genassym.cf` | C code generator | Simplified-BSD | OK | .
`arch/mips64/mips64/interrupt.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/ipifuncs.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/lcore_access.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/lcore_ddb.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/lcore_float.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/mem.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/mips64_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/mips64/mips64/mips64r2.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/mips64/mips64/pmap.c` | C code | Simplified-BSD | OK | .
`arch/mips64/mips64/process_machdep.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/sendsig.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/mips64/mips64/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/mips64/mips64/sys_machdep.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/tlbhandler.S` | Assembler code | Simplified-BSD | OK | .
`arch/mips64/mips64/trap.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/vm_machdep.c` | C code | Original-BSD | OK | .
`arch/octeon/compile/BOOT/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/octeon/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total lines | .
`arch/octeon/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/octeon/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code line + 1 commented line; 8 total lines | .
`arch/octeon/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/octeon/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/octeon/conf/BOOT`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 60 non empty lines; 79 total lines | .
**`arch/octeon/conf/files.octeon`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 149 non empty lines; 188 total lines | .
**`arch/octeon/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 192 non empty lines; 216 total lines | .
`arch/octeon/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line;  8 total lines | .
**`arch/octeon/conf/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 59 non empty lines; 66 total lines | .
**`arch/octeon/conf/Makefile.octeon`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 152 non empty lines; 189 total lines | .
**`arch/octeon/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 110 non empty lines; 133 total lines | .
`arch/octeon/dev/amdcf.c` | C code | Custom-Modified-BSD (with differents names for copyrights) T2 + Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/octeon/dev/cn30xxasx.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxasxreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxasxvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxbootbusreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxciureg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxcorereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/cn30xxfau.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxfaureg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxfauvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxfpa.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxfpareg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxfpavar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxgmx.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxgmxreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxgmxvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxipd.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxipdreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxipdvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxnpireg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpip.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpipreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpipvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpko.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpkoreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpkovar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpow.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpowreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxpowvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxsmi.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxsmireg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxsmivar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxuart.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/cn30xxuartreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/if_cnmac.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/if_cnmacvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/if_ogx.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/iobusvar.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/mainbus.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/octboot.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octcf.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/octeon/dev/octcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octcit.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octciu.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/octcrypto_asm.S` | Assembler code | Original-ISC | OK | .
`arch/octeon/dev/octcrypto.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octdwctwo.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octehci.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octeon_bus_space.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/octeon_intr.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/octeon_iobus.c` | C code | Simplified-BSD | OK | .
`arch/octeon/dev/octeon_pcibus.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/octeon/dev/octeon_pcibus.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/octgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octhcireg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/dev/octiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octmmc.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octmmcreg.h` | C header | Original-ISC | OK | .
`arch/octeon/dev/octohci.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octpcie.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octpip.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octrng.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octsctl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octuctl.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/octuctlreg.h` | C header | Original-ISC | OK | .
`arch/octeon/dev/octuctlvar.h` | C header | Original-ISC | OK | .
`arch/octeon/dev/octxctl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/octxctlreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/ogxnexus.c` | C code | Original-ISC | OK | .
`arch/octeon/dev/ogxreg.h` | C header | Original-ISC | OK | .
`arch/octeon/dev/ogxvar.h` | C header | Original-ISC | OK | .
`arch/octeon/dev/simplebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/dev/simplebusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/include/asm.h` | Assembler header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/atomic.h` | C header | Original-ISC | OK | .
`arch/octeon/include/autoconf.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/cdefs.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/octeon/include/cpu.h` | C header | Modified-BSD + Custom-Modified-Expat | OK (Combined Licenses) | .
`arch/octeon/include/cpustate.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/db_machdep.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/disklabel.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/endian.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/exec.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/include/fenv.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/_float.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/octeon/include/fpu.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/frame.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/ieeefp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/ieee.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/limits.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/lock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/memconf.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/mips_opcode.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/mplock.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/octeon/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/octeon/include/octboot.h` | C header | Original-ISC | OK | .
`arch/octeon/include/octeon_model.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/octeonreg.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/octeonvar.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/octeon/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/octeon/include/param.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/pcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/octeon/include/pmap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/proc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/profile.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/pte.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/ptrace.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/regdef.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/reg.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/regnum.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/reloc.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/setjmp.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/signal.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/sysarch.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/tcb.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/include/trap.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/_types.h` | C header | Public-Domain (notified in file) | OK | .
`arch/octeon/include/vmparam.h` | C header | Public-Domain (notified in file) | OK | .
**`arch/octeon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/octeon/octeon/autoconf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/octeon/octeon/bus_dma.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/octeon/octeon/cn3xxx.dts` | Device Tree Source File | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/octeon/octeon/cn3xxx_dts.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 330 non empty lines; 331 total lines | .
`arch/octeon/octeon/conf.c` | C code | Original-BSD | OK | .
`arch/octeon/octeon/disksubr.c` | C code | Modified-BSD | OK | .
`arch/octeon/octeon/genassym.cf` | C code generator | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 1 total line | .
`arch/octeon/octeon/locore0.S` | Assembler code | Simplified-BSD | OK | .
`arch/octeon/octeon/locore.S` | Assembler code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/octeon/octeon/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/octeon/octeon/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/octeon/octeon/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/octeon/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 39 total lines | .
**`arch/octeon/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 51 total lines | .
`arch/octeon/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
**`arch/octeon/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 45 total lines | .
`arch/octeon/stand/rdboot/cmd.c` | C code | Simplified-BSD | OK | .
`arch/octeon/stand/rdboot/cmd.h` | C header | Simplified-BSD | OK | .
`arch/octeon/stand/rdboot/disk.c` | C code | Original-ISC | OK | .
`arch/octeon/stand/rdboot/disk.h` | C header | Original-ISC | OK | .
`arch/octeon/stand/rdboot/Makefile` | Makefile | "Unlicensed!!!" | Trival; 12 code lines + 2 commented lines; 19 total lines | .
`arch/octeon/stand/rdboot/rdboot.c` | C code | Original-ISC | OK | .
`arch/octeon/stand/rdboot/vars.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/compile/BOOT/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/powerpc64/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/powerpc64/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/powerpc64/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/powerpc64/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/powerpc64/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/powerpc64/conf/BOOT`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 61 non empty lines; 73 total lines | .
**`arch/powerpc64/conf/files.powerpc64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 92 non empty lines; 119 total lines | .
**`arch/powerpc64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 182 non empty lines; 192 total lines | .
`arch/powerpc64/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
`arch/powerpc64/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
**`arch/powerpc64/conf/Makefile.powerpc64`** | ? | __Unlicensed!!!__ | Non-Trivial; 153 non empty lines; 189 total lines | .
**`arch/powerpc64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 75 total lines | .
`arch/powerpc64/dev/astfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/ipmi_opal.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/kexec.c` | C code | Original-ISC with Added CR for hyperbola | OK | .
`arch/powerpc64/dev/kexec_subr.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/mainbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/opal.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/opalcons.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/opalsens.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/pci_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/phb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/xicp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/xics.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/dev/xive.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/powerpc64/include/asm.h` | Assembler header | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/powerpc64/include/atomic.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/include/cdefs.h` | C header | Public-Domain (J.T. Conklin) | OK | .
**`arch/powerpc64/include/conf.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 24 total lines | .
`arch/powerpc64/include/cpufunc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/cpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/powerpc64/include/disklabel.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/include/exec.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/_float.h` | C header | Modified-BSD | OK | .
`arch/powerpc64/include/fpu.h` | C header | "Unlicensed!!!" | Trivial; 8 code lines + 0 commented line; 13 total lines | .
`arch/powerpc64/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/powerpc64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/include/intr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/kcore.h` | C header | "Unlicensed!!!" | 1 commented line as 'EMPTY'; 1 total line | .
`arch/powerpc64/include/kexec.h` | C header | Original-ISC | OK | .
`arch/powerpc64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/powerpc64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/powerpc64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/powerpc64/include/opal.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/include/param.h` | C header | Modified-BSD | OK | .
`arch/powerpc64/include/pcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/include/pmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/proc.h` | C header | Modified-BSD | OK | .
`arch/powerpc64/include/profile.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/pte.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/reloc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 2 commented lines; 4 total lines | .
`arch/powerpc64/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/smbiosvar.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/include/softintr.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/include/spinlock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/_types.h` | C header | Modified-BSD | OK | .
**`arch/powerpc64/include/vmparam.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 non empty lines; 50 total lines | .
**`arch/powerpc64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 44 non empty lines; 51 total lines | .
`arch/powerpc64/powerpc64/autoconf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/bus_dma.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/powerpc64/powerpc64/bus_space.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/powerpc64/clock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/conf.c` | C code | Modified-BSD | OK | .
`arch/powerpc64/powerpc64/cpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/db_disasm.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/powerpc64/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/powerpc64/powerpc64/db_memrw.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/powerpc64/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/powerpc64/powerpc64/disksubr.c` | C code | Modified-BSD | OK | .
`arch/powerpc64/powerpc64/fpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/genassym.cf` | C code generator | Modified-BSD | OK | .
`arch/powerpc64/powerpc64/intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/locore0.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/locore.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/powerpc64/powerpc64/mem.c` | C code | Modified-BSD | OK | .
`arch/powerpc64/powerpc64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/powerpc64/pmap.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`arch/powerpc64/powerpc64/process_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/softintr.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/powerpc64/syncicache.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/powerpc64/syscall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/sys_machdep.c` | C code | Original-BSD | OK | .
`arch/powerpc64/powerpc64/trap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc64/powerpc64/trap_subr.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/powerpc64/vm_machdep.c` | C code | Modified-BSD | OK | .
**`arch/powerpc64/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 39 total lines | .
`arch/powerpc64/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
**`arch/powerpc64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 44 total lines | .
`arch/powerpc64/stand/rdboot/cmd.c` | C code | Simplified-BSD | OK | .
`arch/powerpc64/stand/rdboot/cmd.h` | C header | Simplified-BSD | OK | .
`arch/powerpc64/stand/rdboot/disk.c` | C code | Original-ISC | OK | .
`arch/powerpc64/stand/rdboot/disk.h` | C header | Original-ISC | OK | .
`arch/powerpc64/stand/rdboot/Makefile` | Makefile | "Unlicensed!!!" | Trival; 12 code lines + 2 commented lines; 19 total lines | .
`arch/powerpc64/stand/rdboot/rdboot.c` | C code | Original-ISC with Added CR for hyperbola | OK | .
`arch/powerpc64/stand/rdboot/vars.c` | C code | Simplified-BSD | OK | .
**`arch/powerpc/conf/files.powerpc`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 41 non empty lines; 44 total lines | .
`arch/powerpc/ddb/db_disasm.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/ddb/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/powerpc/ddb/db_memrw.c` | C code | CMU (similar to ISC) | OK | .
`arch/powerpc/ddb/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/powerpc/include/asm.h` | Assembler header | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/powerpc/include/atomic.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/include/bat.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/cdefs.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/powerpc/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/powerpc/include/endian.h` | C header | Simplified-BSD | OK | .
`arch/powerpc/include/exec.h` | C header | Simplified-BSD | OK | .
`arch/powerpc/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/include/_float.h` | C header | Modified-BSD | OK | .
`arch/powerpc/include/fpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/hid.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/powerpc/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/powerpc/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/ipkdb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/limits.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/lock.h` | C header | Simplified-BSD | OK | .
`arch/powerpc/include/mplock.h` | C header | Simplified-BSD | OK | .
`arch/powerpc/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/powerpc/include/param.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pcb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/powerpc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/proc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/profile.h` | C header | Simplified-BSD | OK | .
`arch/powerpc/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/reg.h` | C header | Modified-BSD | OK | .
`arch/powerpc/include/reloc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 2 commented lines; 4 total lines | .
`arch/powerpc/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented line; 10 total lines | .
`arch/powerpc/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/_types.h` | C header | Modified-BSD | OK | .
`arch/powerpc/isa/isa_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/bus_dma.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/powerpc/bus_space.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/copystr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/cpu_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/powerpc/fpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/genassym.cf` | C code generator | "Unlicensed!!!" | Trivial; 0 code line + 0 commented line; 1 total line | .
`arch/powerpc/powerpc/in_cksum.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/intr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/lock_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/powerpc/powerpc/pmap.c` | C code | Simplified-BSD | OK | .
`arch/powerpc/powerpc/process_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/setjmp.S` | Assembler code | Simplified-BSD | OK | .
`arch/powerpc/powerpc/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/sys_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/trap.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/vm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/riscv64/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/riscv64/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/riscv64/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/riscv64/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/riscv64/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/riscv64/conf/files.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 116 non empty lines; 143 total lines | .
**`arch/riscv64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 155 non empty lines; 185 total lines | .
`arch/riscv64/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
**`arch/riscv64/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 76 non empty lines; 83 total lines | .
**`arch/riscv64/conf/Makefile.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 138 non empty lines; 173 total lines | .
**`arch/riscv64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 129 non empty lines; 156 total lines | .
`arch/riscv64/dev/mainbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/mainbus.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/pci_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/plic.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/riscv64/dev/plic.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/riscv_cpu_intc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/riscv_cpu_intc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/sfcc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/sfclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/sfuart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/simplebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/dev/simplebusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/asm.h` | Assembler header | Modified-BSD | OK | .
`arch/riscv64/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/riscv64/include/bootconfig.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/bus.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/cdefs.h` | C header | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 16 total lines | .
`arch/riscv64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/riscv64/include/cpufunc.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/cpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/db_machdep.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/disklabel.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/endian.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/exec.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/_float.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
`arch/riscv64/include/frame.h` | C header | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`arch/riscv64/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/riscv64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/riscv64/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/kcore.h` | C header | Public-Domain (notified in file) | OK | .
`arch/riscv64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/riscv64/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/riscv64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/riscv64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/riscv64/include/param.h` | C header | Modified-BSD | OK | .
`arch/riscv64/include/pcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/pmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/proc.h` | C header | Modified-BSD | OK | .
`arch/riscv64/include/profile.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/pte.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/ptrace.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/reg.h` | C header | Simplified-BSD | OK | .
**`arch/riscv64/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 19 total lines | .
`arch/riscv64/include/riscv64var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/riscvreg.h` | C header | Custom-Modified-BSD (with differents names for copyrights) T2 | OK | .
`arch/riscv64/include/sbi.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 4 commented lines; 8 total lines | .
`arch/riscv64/include/signal.h` | C header | Modified-BSD | OK | .
`arch/riscv64/include/softintr.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented line; 10 total lines | .
`arch/riscv64/include/sysarch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/syscall.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/trap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/include/_types.h` | C header | Modified-BSD | OK | .
`arch/riscv64/include/vmparam.h` | C header | Custom-Modified-BSD with Added CR for hyperbola | OK | .
**`arch/riscv64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/riscv64/riscv64/ast.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/autoconf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/bus_dma.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/riscv64/riscv64/bus_space.c` | C code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/clock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/riscv64/riscv64/copy.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/copystr.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/cpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/cpufunc_asm.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/cpuswitch.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/db_disasm.c` | C code | Simplified-BSD | OK | .
**`arch/riscv64/riscv64/db_instruction.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3407 non empty lines; 3409 total lines | .
`arch/riscv64/riscv64/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/riscv64/riscv64/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/riscv64/riscv64/disksubr.c` | C code | Modified-BSD | OK | .
`arch/riscv64/riscv64/exception.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/fpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/genassym.cf` | C code generator | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/intr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/locore0.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/locore.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/riscv64/riscv64/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/riscv64/riscv64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/riscv64/riscv64/pagezero.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/pmap.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/riscv64/riscv64/process_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/sbi.c` | C code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/sig_machdep.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/riscv64/riscv64/softintr.c` | C code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/support.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/riscv64/syscall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/trap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/riscv64/vm_machdep.c` | C code | Modified-BSD | OK | .
`arch/riscv64/stand/efiboot/conf.c` | C code | Simplified-BSD | OK | .
**`arch/riscv64/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
`arch/riscv64/stand/efiboot/efiboot.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/riscv64/stand/efiboot/efiboot.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/efidev.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/riscv64/stand/efiboot/efidev.h` | C header | Simplified-BSD | OK | .
`arch/riscv64/stand/efiboot/efipxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/efipxe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/efirng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/exec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/heap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/riscv64/stand/efiboot/ldscript.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 83 non empty lines; 85 total lines | .
`arch/riscv64/stand/efiboot/libsa.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`arch/riscv64/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 85 total lines | .
`arch/riscv64/stand/efiboot/self_reloc.c` | C code | Simplified-BSD | OK | .
`arch/riscv64/stand/efiboot/softraid_riscv64.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/riscv64/stand/efiboot/softraid_riscv64.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/riscv64/stand/efiboot/start.S` | Assembler code | Simplified-BSD | OK | .
`arch/riscv64/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
**`arch/sh/conf/files.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 52 non empty lines; 57 total lines | .
`arch/sh/conf/files.shb` | ? | "Unlicensed!!!" | Trivial; 6 code lines + 3 commented lines; 13 total lines | .
`arch/sh/conf/files.shpcic` | ? | "Unlicensed!!!" | Trivial; 4 code lines + 3 commented lines; 11 total lines | .
`arch/sh/dev/pcicreg.h` | C header | Simplified-BSD | OK | .
`arch/sh/dev/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sh/dev/scif.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/sh/dev/scifreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/dev/scireg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/dev/shb.c` | C code | Simplified-BSD | OK | .
`arch/sh/dev/shpcic.c` | C code | Simplified-BSD | OK | .
`arch/sh/dev/shpcicvar.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/asm.h` | Assembler header | Modified-BSD | OK | .
`arch/sh/include/atomic.h` | C header | Public-Domain (notified in file) | OK | .
`arch/sh/include/bscreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/cache.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/cache_sh3.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/cache_sh4.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/cdefs.h` | C header | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 16 total lines | .
`arch/sh/include/clock.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/cpgreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/cpu.h` | C header | Modified-BSD | OK | .
`arch/sh/include/cputypes.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/sh/include/devreg.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/sh/include/endian.h` | C header | Public-Domain (notified in file) | OK | .
**`arch/sh/include/exec.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 30 total lines | .
`arch/sh/include/fenv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sh/include/_float.h` | C header | Modified-BSD | OK | .
`arch/sh/include/fpu.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/frame.h` | C header | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/sh/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sh/include/intcreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/kcore.h` | C header | Old-Expat (with legal disclaimer 2) | OK | .
`arch/sh/include/limits.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sh/include/lock.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/locore.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/mmu.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/mmu_sh3.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/mmu_sh4.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 3 total lines | .
`arch/sh/include/param.h` | C header | Modified-BSD | OK | .
`arch/sh/include/pcb.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/pmap.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sh/include/proc.h` | C header | Modified-BSD | OK | .
`arch/sh/include/profile.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/psl.h` | C header | Modified-BSD | OK | .
`arch/sh/include/pte.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sh/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sh/include/reg.h` | C header | Modified-BSD | OK | .
**`arch/sh/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 106 non empty lines; 110 total lines | .
`arch/sh/include/rtcreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code line + 5 commented lines; 8 total lines | .
`arch/sh/include/signal.h` | C header | Modified-BSD | OK | .
`arch/sh/include/spinlock.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sh/include/tmureg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/trap.h` | C header | Simplified-BSD | OK | .
`arch/sh/include/_types.h` | C header | Modified-BSD | OK | .
`arch/sh/include/ubcreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/include/vmparam.h` | C header | Simplified-BSD | OK | .
`arch/sh/sh/cache.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/cache_sh3.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/cache_sh4.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/clock.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sh/sh/cpu.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/db_disasm.c` | C code | Custom-BSD (source clause, functional clause, modified binary clause and the advertising clause) | OK | .
`arch/sh/sh/db_interface.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/sh/db_memrw.c` | C code | CMU (similar to ISC) | OK | .
`arch/sh/sh/db_trace.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/sh/devreg.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/genassym.cf` | C code generator | Simplified-BSD | OK | .
`arch/sh/sh/in_cksum.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sh/sh/interrupt.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/locore_c.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/locore_subr.S` | Assembler code | Old-Expat (with legal disclaimer 2) + Simplified-BSD | OK (Combined Licenses) | .
`arch/sh/sh/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/mmu.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/mmu_sh3.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sh/sh/mmu_sh4.c` | C code | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sh/sh/pmap.c` | C code | Simplified-BSD | OK | .
`arch/sh/sh/process_machdep.c` | C code | Old-Expat (with legal disclaimer 2) + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/sh_machdep.c` | C code | Old-Expat (with legal disclaimer 2) + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/sh/sh/sys_machdep.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/trap.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/vectors.S` | Assembler code | Simplified-BSD | OK | .
`arch/sh/sh/vm_machdep.c` | C code | Old-Expat (with legal disclaimer 2) + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sparc64/compile/GENERIC/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/sparc64/compile/GENERIC.MP/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/sparc64/compile/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 8 total lines | .
`arch/sparc64/compile/Makefile.inc` | Makefile | "Unlicensed!!!" | Trival; 15 code lines + 0 commented lines; 18 total lines | .
`arch/sparc64/compile/RAMDISK/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/sparc64/compile/RAMDISKU1/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
`arch/sparc64/compile/RAMDISKU5/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 1 code line + 0 commented line; 1 total line | .
**`arch/sparc64/conf/files.sparc64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 369 non empty lines; 476 total lines | .
**`arch/sparc64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 503 non empty lines; 562 total lines | .
`arch/sparc64/conf/GENERIC.MP` | BSD kernel config. | "Unlicensed!!!" | Trivial; 7 code lines + 1 commented line; 11 total lines | .
`arch/sparc64/conf/ld.script` | Linker script | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
**`arch/sparc64/conf/Makefile.sparc64`** | ? | __Unlicensed!!!__ | Non-Trivial; 135 non empty lines; 170 total lines | .
**`arch/sparc64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 261 non empty lines; 293 total lines | .
**`arch/sparc64/conf/RAMDISKU1`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 72 non empty lines; 89 total lines | .
**`arch/sparc64/conf/RAMDISKU5`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 90 total lines | .
`arch/sparc64/dev/auxio.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/auxioreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/auxiovar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/bbc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/beep.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/beeper.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/cbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/cbusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ce4231.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/ce4231var.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/central.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/centralvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/clkbrd.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/clkbrdreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/clkbrdvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/cmp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/com_ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/comkbd_ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/comms_ebus.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`arch/sparc64/dev/cons.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/consinit.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/core.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/creator.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/creatorreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/creatorvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/ebus_mainbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ebusreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/ebusvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/environ.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/fb.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/dev/fd.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sparc64/dev/fdreg.h` | C header | Modified-BSD | OK | .
`arch/sparc64/dev/fdvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/fhc.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/fhc_central.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/fhc_mainbus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/fhcreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/fhcvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/gfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/gfxp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ifb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ifb_ident.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/iommu.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/iommureg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/dev/iommuvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/ldc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ldcvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/led.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/lom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/lpt_ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/machfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/mgiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/msi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/msivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ofwi2c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/ofwi2cvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/pcf8591_envctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/pcf8591_ofw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/pcfiic_ebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/pci_machdep.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/pckbc_ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/pcons.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") with Added CR for hyperbola | OK | .
`arch/sparc64/dev/pmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/power.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/ppm.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/prtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/psycho.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/psychoreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/psychovar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/pyro.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/pyrovar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/radeonfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/raptor.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/rtc.c` | C code | Custom-Original-BSD (Original-BSD with a 2nd advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/dev/sab82532reg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/sab.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/sbbc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/sbus.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK (Combined Licenses) | .
`arch/sparc64/dev/sbusreg.h` | C header | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/sparc64/dev/sbusvar.h` | C header | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/dev/schizo.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/schizoreg.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/schizovar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/ssm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/starfire.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/starfire.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/stp_sbus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/tda.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/upa.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/uperf.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/uperf_ebus.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/uperfvar.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/dev/vbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vbusvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vcc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vcctty.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vcons.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vds.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vdsk.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vdsp.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola | OK | .
`arch/sparc64/dev/vgafb.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/dev/viommu.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/dev/viommuvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/viovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vldc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vldcp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vnet.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vpci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vrng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/vsw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/wbsd_ebus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/dev/z8530kbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/dev/zs.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_add.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_arith.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/fpu/fpu_compare.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_div.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_emu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_explode.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_extern.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/fpu/fpu_implode.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_mul.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_sqrt.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_subr.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/apmvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/include/asm.h` | Assembler header | Modified-BSD | OK | .
`arch/sparc64/include/atomic.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/autoconf.h` | C header | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/include/boot_flag.h` | C header | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sparc64/include/bppioctl.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/sparc64/include/cdefs.h` | C header | "Unlicensed!!!" | Trival; 12 code lines + 1 commented lines; 16 total lines | .
`arch/sparc64/include/conf.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/cpu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/ctlreg.h` | C header | Custom-BSD (similar to Simplified-BSD but without the "binary clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/sparc64/include/db_machdep.h` | C header | CMU (similar to ISC) | OK | .
`arch/sparc64/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/sparc64/include/endian.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 62 non empty lines; 84 total lines | .
**`arch/sparc64/include/exec.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 90 non empty lines; 99 total lines | .
`arch/sparc64/include/fbvar.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/fenv.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/_float.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/frame.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/fsr.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/hypervisor.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/idprom.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/ieeefp.h` | C header | Public-Domain (J.T. Conklin) | OK | .
`arch/sparc64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/instr.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/intr.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/ioctl_fd.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/kcore.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/limits.h` | C header | Modified-BSD | OK | .
`arch/sparc64/include/loadfile_machdep.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/lock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/mdesc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/mplock.h` | C header | Public-Domain (notified in file) | OK | .
`arch/sparc64/include/mutex.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented lines; 3 total lines | .
`arch/sparc64/include/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/param.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK (Combined Licenses) | .
`arch/sparc64/include/pcb.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/pci_machdep.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/proc.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/profile.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/psl.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/pte.h` | C header | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/sparc64/include/ptrace.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/rbus_machdep.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/reg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/reloc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/setjmp.h` | C header | "Unlicensed!!!" | Trivial; 1 code lines + 4 commented lines; 8 total lines | .
`arch/sparc64/include/signal.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/sparc64.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/spinlock.h` | C header | "Unlicensed!!!" | Trivial; 6 code lines + 1 commented lines; 10 total lines | .
`arch/sparc64/include/tcb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/include/trap.h` | C header | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/sparc64/include/_types.h` | C header | Modified-BSD | OK | .
`arch/sparc64/include/vmparam.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/sparc64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/sparc64/sparc64/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) with added CR for Hyperbola | OK | .
`arch/sparc64/sparc64/busop.awk` | Awk code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/busop.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/busop_c.in` | Input file | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/busop.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/busop_h.in` | Input file | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/cache.c` | C code | Custom-Original-BSD (Original-BSD with a 2nd advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/cache.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") T with double authors | OK | .
`arch/sparc64/sparc64/clock.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for double authors | OK | .
`arch/sparc64/sparc64/conf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/cpu.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/db_disasm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/db_interface.c` | C code | CMU (similar to ISC) | OK | .
`arch/sparc64/sparc64/db_trace.c` | C code | CMU (similar to ISC) | OK | .
`arch/sparc64/sparc64/disksubr.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK | .
`arch/sparc64/sparc64/emul.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/sparc64/genassym.cf` | C code generator | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/hvcall.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/sparc64/in4_cksum.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/in_cksum.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/intr.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/intreg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/ipifuncs.c` | C code | Simplified-BSD | OK | .
`arch/sparc64/sparc64/locore.s` | Assembler code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
`arch/sparc64/sparc64/mdesc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/sparc64/mem.c` | C code | Modified-BSD | OK | .
`arch/sparc64/sparc64/ofw_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/openfirm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/pmap.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
`arch/sparc64/sparc64/process_machdep.c` | C code | Modified-BSD | OK | .
`arch/sparc64/sparc64/rbus_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/sparc64/sys_machdep.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/timerreg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/trap.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/vm_machdep.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/stand/bootblk/bootblk.fth` | ? | Simplified-BSD with Added CR for hyperbola | OK | .
`arch/sparc64/stand/bootblk/genassym.sh` | Shell script | Simplified-BSD | OK | .
`arch/sparc64/stand/bootblk/genfth.cf` | C code generator | Simplified-BSD + Original-BSD (Double advertising clause: above and 3rd clause) | OK (Combined Licenses) | .
**`arch/sparc64/stand/bootblk/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 40 non empty lines; 54 total lines | .
**`arch/sparc64/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 33 non empty lines; 48 total lines | .
`arch/sparc64/stand/libz/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 5 code lines + 1 commented line; 9 total lines | .
`arch/sparc64/stand/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 9 total lines | .
**`arch/sparc64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 32 non empty lines; 45 total lines | .
`arch/sparc64/stand/ofwboot/alloc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/boot.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/sparc64/stand/ofwboot/disk.h` | C header | Simplified-BSD | OK | .
`arch/sparc64/stand/ofwboot/diskprobe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/stand/ofwboot/elf64_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/sparc64/stand/ofwbootfd/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented line; 6 total lines | .
`arch/sparc64/stand/ofwboot/Locore.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/sparc64/stand/ofwboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 65 non empty lines; 85 total lines | .
`arch/sparc64/stand/ofwboot/md5.h` | C header | RSA Message-Digest License | OK | .
`arch/sparc64/stand/ofwboot/net.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/netif_of.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot.net/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 7 total lines | .
`arch/sparc64/stand/ofwboot/ofdev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/ofdev.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/softraid_sparc64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/stand/ofwboot/softraid_sparc64.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`arch/sparc64/stand/ofwboot/srt0.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/vers.c` | C code | "Unlicensed!!!" | Trivial; 1 code lines + 0 commented lines; 1 total lines | .
`conf/defines` | ASCII text | "Unlicensed!!!" | Trivial; 9 data lines; 10 total lines | .
`conf/files` | BSD kernel config. | Non-Copyrightable | OK | .
`conf/GENERIC` | BSD kernel config. | Non-Copyrightable | OK | .
**`conf/makegap.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 1 commented line; 72 total lines [this generates the "gap.link" (linker script) file; and with the "ld" command and the "gap.link" file, generate the "gap.o" (object code) file; most part of the "gap.link" code are included in this script] | .
`conf/newvers.sh` | Shell script | Modified-BSD | OK | .
`conf/param.c` | C code | Modified-BSD | OK | .
`conf/swapgeneric.c` | C code | Modified-BSD | OK | .
`crypto/aes.c` | C code | Expat | OK | .
`crypto/aes.h` | C header | Expat | OK | .
`crypto/arc4.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/arc4.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/blake2s.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/blake2s.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/blf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypto/blf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypto/cast.c` | C code | Public-Domain (Steve Reid) | OK | .
`crypto/cast.h` | C header | Public-Domain (Steve Reid) | OK | .
`crypto/castsb.h` | C header | Public-Domain (Steve Reid) | OK | .
`crypto/chachapoly.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/chachapoly.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/chacha_private.h` | C header | Public-Domain (D. J. Bernstein) | OK | .
`crypto/cmac.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/cmac.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/criov.c` | C code | Simplified-BSD | OK | .
`crypto/crypto.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/cryptodev.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`crypto/cryptosoft.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/cryptosoft.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/curve25519.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/curve25519.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/des_locl.h` | C header | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/ecb3_enc.c` | C code | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/ecb_enc.c` | C code | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/gmac.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/gmac.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/hmac.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/hmac.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/idgen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/idgen.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/key_wrap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/key_wrap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/md5.c` | C code | Public-Domain (Ron Rivest) | OK | .
`crypto/md5.h` | C header | Public-Domain (Colin Plumb) | OK | .
`crypto/michael.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/michael.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/podd.h` | C header | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/poly1305.c` | C code | Public-Domain (Andrew Moon) | OK | .
`crypto/poly1305.h` | C header | Public-Domain (Andrew Moon) | OK | .
`crypto/rijndael.c` | C code | Public-Domain (Vincent Rijmen, Antoon Bosselaers and Paulo Barreto) | OK | .
`crypto/rijndael.h` | C header | Public-Domain (Vincent Rijmen, Antoon Bosselaers and Paulo Barreto) | OK | .
`crypto/rmd160.c` | C code | Simplified-BSD | OK | .
`crypto/rmd160.h` | C header | Simplified-BSD | OK | .
`crypto/set_key.c` | C code | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/sha1.c` | C code | Public-Domain (Steve Reid) | OK | .
`crypto/sha1.h` | C header | Public-Domain (Steve Reid) | OK | .
`crypto/sha2.c` | C code | Modified-BSD | OK | .
`crypto/sha2.h` | C header | Modified-BSD | OK | .
`crypto/siphash.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`crypto/siphash.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`crypto/sk.h` | C header | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/spr.h` | C header | Custom-Modified-BSD (third clause is the "advertising clause") | OK | .
`crypto/xform.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/xform.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`crypto/xform_ipcomp.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`ddb/db_access.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_access.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_break.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_break.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_command.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_command.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_ctf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`ddb/db_dwarf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`ddb/db_elf.c` | C code | Simplified-BSD | OK | .
`ddb/db_elf.h` | C header | Simplified-BSD | OK | .
`ddb/db_examine.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_expr.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_extern.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`ddb/db_hangman.c` | C code | Simplified-BSD | OK | .
`ddb/db_input.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_interface.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`ddb/db_lex.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_lex.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_output.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_output.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_prof.c` | C code | Modified-BSD | OK | .
`ddb/db_run.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_run.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_sym.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_sym.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_trap.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_usrreq.c` | C code | Simplified-BSD | OK | .
`ddb/db_var.h` | C header | Simplified-BSD | OK | .
`ddb/db_variables.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_variables.h` | C header | CMU (similar to ISC) | OK | .
`ddb/db_watch.c` | C code | CMU (similar to ISC) | OK | .
`ddb/db_watch.h` | C header | CMU (similar to ISC) | OK | .
`dev/acpi/abl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpiac.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpials.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpiasus.c` | C code | Simplified-BSD | OK | .
`dev/acpi/acpibat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpibtn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpicbkbd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpicmos.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpicpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidebug.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidebug.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidev.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidmar.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidmar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpidock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpiec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpige.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpihid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpihpet.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpihve.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpimadt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpimcfg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpiprt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpipwrres.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpisbs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpisony.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpisurface.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpithinkpad.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpitimer.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpitoshiba.c` | C code | Simplified-BSD | OK | .
`dev/acpi/acpitz.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpiutil.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpivideo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/acpivout.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/ahci_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/amdgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/amd_iommu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/amltypes.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/aplgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/asmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/atk0110.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/bytgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/ccp_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/ccpmic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/chvgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/com_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/dsdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/dsdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/dwgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/dwiic_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/efi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/acpi/files.acpi` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/acpi/glkgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/if_bse_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/imxiic_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/ipmi_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/pchgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/pluart_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/sdhc_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/smbus.h` | C header | Simplified-BSD | OK | .
`dev/acpi/tipmic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/tpm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/acpi/xhci_acpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/adb/adb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/adb_subr.c` | C code | Simplified-BSD | OK | .
`dev/adb/akbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/akbdmap.h` | C header | Simplified-BSD | OK | .
`dev/adb/akbdvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/ams.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/amsvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/files.adb` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/adb/keyboard.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ata/ata.c` | C code | Simplified-BSD | OK | .
`dev/ata/atareg.h` | C header | Simplified-BSD | OK | .
`dev/ata/atascsi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ata/atascsi.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ata/atavar.h` | C header | Simplified-BSD | OK | .
`dev/ata/ata_wdc.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/ata/files.ata` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/atapiscsi/atapiscsi.c` | C code | Simplified-BSD | OK | .
`dev/atapiscsi/files.atapiscsi` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/ata/pmreg.h` | C header | Modified-BSD | OK | .
`dev/ata/satareg.h` | C header | Simplified-BSD | OK | .
`dev/ata/wd.c` | C code | Simplified-BSD + Simplified-BSD | OK | .
`dev/ata/wdvar.h` | C header | Simplified-BSD | OK | .
`dev/audio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/audio_if.h` | C header | Original-BSD | OK | .
`dev/bio.c` | C code | Simplified-BSD | OK | .
`dev/biovar.h` | C header | Simplified-BSD | OK | .
`dev/cardbus/cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/cardbus_exrom.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/cardbus_exrom.h` | C header | Simplified-BSD | OK | .
`dev/cardbus/cardbus_map.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/cardbusvar.h` | C header | Simplified-BSD | OK | .
`dev/cardbus/cardslot.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/cardslotvar.h` | C header | Simplified-BSD | OK | .
`dev/cardbus/com_cardbus.c` | C code | Modified-BSD | OK | .
`dev/cardbus/ehci_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/files.cardbus` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/cardbus/if_acx_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_ath_cardbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/cardbus/if_athn_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_atw_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/if_bwi_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_dc_cardbus.c` | C code | Original-BSD | OK | .
`dev/cardbus/if_fxp_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/if_malo_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_pgt_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_ral_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_re_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/if_rl_cardbus.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/cardbus/if_rtw_cardbus.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Simplified-BSD | OK | .
`dev/cardbus/if_xl_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/ohci_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/puc_cardbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/cardbus/rbus.c` | C code | Simplified-BSD | OK | .
`dev/cardbus/rbus.h` | C header | Simplified-BSD | OK | .
`dev/cardbus/uhci_cardbus.c` | C code | Simplified-BSD | OK | .
`dev/clock_subr.h` | C header | Simplified-BSD | OK | .
`dev/cninit.c` | C code | Modified-BSD | OK | .
`dev/cons.c` | C code | Modified-BSD | OK | .
`dev/cons.h` | C header | Modified-BSD | OK | .
`dev/dec/clockvar.h` | C header | CMU (similar to ISC) | OK | .
`dev/dec/files.dec` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/dec/if_le_dec.c` | C code | Modified-BSD | OK | .
`dev/dec/mcclockvar.h` | C header | CMU (similar to ISC) | OK | .
`dev/diskmap.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dt_dev.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dt_prov_kprobe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dt_prov_profile.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dt_prov_static.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dt_prov_syscall.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/dt/dtvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/eisa/ahc_eisa.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/eisa/cac_eisa.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/eisa/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs_data.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/files.eisa` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/eisa/if_ep_eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`dev/eisa/uha_eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/fdt/acrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ahci_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amldwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amliic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlmmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlpciephy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlpinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlpwm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlpwrc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlreset.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlrng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlsm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amltemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amluart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/amlusbphy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/axppmic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2711_pcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2711_rng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2711_tmon.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_aux.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_bsc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_clock.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/fdt/bcm2835_dmac.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/fdt/bcm2835_dog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_dwctwo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_gpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_mbox.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/fdt/bcm2835_rng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bcm2835_sdhost.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/fdt/bcm2835_temp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/bd718x7.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ccp_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/cduart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/com_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/cwfg.c` | C code | Simplified-BSD | OK | .
`dev/fdt/dapmic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/dwdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/dwmmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/dwpcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ehci_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/es8316ac.c` | C code | Simplified-BSD | OK | .
`dev/fdt/exrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/exuart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/exuartreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/fanpwr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/files.fdt` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/fdt/fusbtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/gfrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/graphaudio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/hiclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/hidwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/hireset.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/hitemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_bse_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_cad.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_dwge.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_dwxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_fec.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/if_mvneta.c` | C code | Simplified-BSD | OK | .
`dev/fdt/if_mvnetareg.h` | C header | Simplified-BSD | OK | .
`dev/fdt/if_mvpp.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/fdt/if_mvppreg.h` | C header | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/fdt/imxanatop.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxanatopvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxccm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxccm_clocks.h` | C header | Public-Domain (notified in file) | OK | .
`dev/fdt/imxdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxdwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxehci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxesdhc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxgpc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxiic_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxiomuxc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxpciephy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxpwm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxspi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxsrc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxtmu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxuart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/imxuartreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ipmi_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/moxtet.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvgicp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvicu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mviic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvkpcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvmdio.c` | C code | Simplified-BSD | OK | .
`dev/fdt/mvpinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvpinctrl_pins.h` | C header | Public-Domain (notified in file) | OK | .
`dev/fdt/mvrng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvspi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvsw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvtemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/mvuart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ociic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ohci_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pciecam.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/plgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/plrtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pluart_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/psci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pscivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pwmbl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pwmfan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/pwmreg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkanxdp.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rkclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkclock_clocks.h` | C header | Public-Domain (notified in file) | OK | .
`dev/fdt/rkdrm.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rkdrm.h` | C header | Simplified-BSD | OK | .
`dev/fdt/rkdwhdmi.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rkdwusb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkemmcphy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkgrf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkiis.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rkpcie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkpinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkpmic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkpwm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkrng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rktcphy.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rktemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/rkvop.c` | C code | Simplified-BSD | OK | .
`dev/fdt/rsbvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sdhc_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sfp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/simpleamp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/simpleaudio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/simplefb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/simplepanel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/spmivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/ssdfb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sunxireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxiccmu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxiccmu_clocks.h` | C header | Public-Domain (notified in file) | OK | .
`dev/fdt/sxidog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sximmc.c` | C code | Simplified-BSD | OK | .
`dev/fdt/sxipio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxipio_pins.h` | C header | Public-Domain (notified in file) | OK | .
`dev/fdt/sxipiovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxipwm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxirsb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxirtc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxisid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxisyscon.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxitemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxits.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/sxitwi.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/fdt/sypwr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/syscon.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/tcpci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/fdt/virtio_mmio.c` | C code | Simplified-BSD | OK | .
`dev/fdt/xhci_fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/firmload.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/files.gpio` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/gpio/gpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/gpiodcf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/gpioiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/gpioow.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/gpiosim.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/gpio/gpiovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/hid/files.hid` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/hid/hid.c` | C code | Simplified-BSD | OK | .
`dev/hid/hid.h` | C header | Simplified-BSD | OK | .
`dev/hid/hidkbd.c` | C code | Simplified-BSD | OK | .
`dev/hid/hidkbdsc.h` | C header | Simplified-BSD | OK | .
`dev/hid/hidkbdvar.h` | C header | Simplified-BSD | OK | .
`dev/hid/hidms.c` | C code | Simplified-BSD | OK | .
`dev/hid/hidmsvar.h` | C header | Simplified-BSD | OK | .
`dev/hid/hidmt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/hid/hidmtvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/hil/devlist2h.awk` | AWK script | Simplified-BSD | OK | .
`dev/hil/files.hil` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/hil/hil.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/hil/hildevs` | AWK script | Simplified-BSD | OK | .
`dev/hil/hildevs_data.h` | C header | Simplified-BSD | OK | .
`dev/hil/hildevs.h` | C header | Simplified-BSD | OK | .
`dev/hil/hilid.c` | C code | Simplified-BSD | OK | .
`dev/hil/hilkbd.c` | C code | Simplified-BSD | OK | .
`dev/hil/hilkbdmap.c` | C code | Simplified-BSD | OK | .
`dev/hil/hilkbdmap.h` | C header | Simplified-BSD | OK | .
`dev/hil/hilms.c` | C code | Simplified-BSD | OK | .
`dev/hil/hilreg.h` | C header | Modified-BSD | OK | .
`dev/hil/hilvar.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/hil/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code line + 1 commented line; 10 total lines | .
`dev/hotplug.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/abx80x.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ad741x.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1021.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1024.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1025.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1026.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1030.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adm1031.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adt7460.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/adt7462.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/asb100.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/asc7611.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/asc7621.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/bmc150.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ds1307.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ds1631.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ds3231.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/fcu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/files.i2c` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/i2c/fintek.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/gl518sm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/i2c_bitbang.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_bitbang.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_io.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_scan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/i2cvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/iatp.c` | C code | Simplified-BSD | OK | .
`dev/i2c/ihidev.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ihidev.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ikbd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ims.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/imt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/ipmi_i2c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/isl1208.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/lis331dl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/lm75.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/lm78_i2c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/lm87.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/lm93.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/m41t8x.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/maxim6690.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/mcp794xx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/pca9532.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/pca9548.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/pca9554.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/pcf8523.c` | C code | Simplified-BSD | OK | .
`dev/i2c/pcf8563.c` | C code | Simplified-BSD | OK | .
`dev/i2c/rs5c372.c` | C code | Simplified-BSD | OK | .
`dev/i2c/sdtemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/spdmem_i2c.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/i2c/thmc50.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/tmp451.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/tsl2560.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/w83793g.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/w83795g.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/i2c/w83l784r.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/aac.c` | C code | Simplified-BSD | OK | .
`dev/ic/aacreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/aac_tables.h` | C header | Simplified-BSD | OK | .
`dev/ic/aacvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/ac97.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/ac97.h` | C header | Simplified-BSD | OK | .
`dev/ic/acx100.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/acx111.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/acx.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/acxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/acxvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/ad1843reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ad1848reg.h` | C header | Original-BSD | OK | .
`dev/ic/adv.c` | C code | Simplified-BSD | OK | .
`dev/ic/adv.h` | C header | Simplified-BSD | OK | .
`dev/ic/advlib.c` | C code | Simplified-BSD | OK | .
`dev/ic/advlib.h` | C header | Simplified-BSD | OK | .
`dev/ic/adw.c` | C code | Simplified-BSD | OK | .
`dev/ic/adw.h` | C header | Simplified-BSD | OK | .
`dev/ic/adwlib.c` | C code | Simplified-BSD | OK | .
`dev/ic/adwlib.h` | C header | Simplified-BSD | OK | .
`dev/ic/ahci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ahcireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ahcivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/aic6250.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6250reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/aic6250var.h` | C header | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6915.c` | C code | Simplified-BSD | OK | .
`dev/ic/aic6915.h` | C header | Simplified-BSD | OK | .
`dev/ic/aic79xx.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/aic79xx.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/aic79xx_inline.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/aic79xx_openbsd.c` | C code | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic79xx_openbsd.h` | C header | Simplified-BSD + Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic7xxx_cam.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/aic7xxx.c` | C code | Modified-BSD | OK | .
`dev/ic/aic7xxx_inline.h` | C header | Modified-BSD | OK | .
`dev/ic/aic7xxx_openbsd.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/aic7xxx_openbsd.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/aic7xxx_seeprom.c` | C code | Modified-BSD | OK | .
`dev/ic/aic7xxxvar.h` | C header | Modified-BSD | OK | .
`dev/ic/am7930.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/am7930reg.h` | C header | Modified-BSD | OK | .
`dev/ic/am7930var.h` | C header | Modified-BSD | OK | .
`dev/ic/am79900.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/am79900reg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/am79900var.h` | C header | Simplified-BSD | OK | .
`dev/ic/am7990.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/am7990reg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/am7990var.h` | C header | Simplified-BSD | OK | .
`dev/ic/ami.c` | C code | Simplified-BSD | OK | .
`dev/ic/amireg.h` | C header | Simplified-BSD | OK | .
`dev/ic/amivar.h` | C header | Simplified-BSD | OK | .
`dev/ic/an.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/anreg.h` | C header | Original-BSD | OK | .
`dev/ic/anvar.h` | C header | Original-BSD | OK | .
`dev/ic/anxdp.c` | C code | Simplified-BSD | OK | .
`dev/ic/anxdp.h` | C header | Simplified-BSD | OK | .
`dev/ic/apcdmareg.h` | C header | Simplified-BSD | OK | .
`dev/ic/ar5008.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5008reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5210.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5210reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5210var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5211.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5211reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5211var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5212.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5212reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5212var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5416.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5416reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5xxx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar5xxx.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9003.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9003reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9280.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9280reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9285.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9285reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9287.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9287reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9380.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ar9380reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/arcofi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/arcofivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ath.c` | C code | Modified-BSD | OK | .
`dev/ic/athn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/athnreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/athnvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/athvar.h` | C header | Modified-BSD | OK | .
`dev/ic/atw.c` | C code | Simplified-BSD | OK | .
`dev/ic/atwreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/atwvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/atxxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ax88190.c` | C code | Simplified-BSD | OK | .
`dev/ic/ax88190reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/ax88190var.h` | C header | Simplified-BSD | OK | .
`dev/ic/bcm2835_dmac.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/bcm2835_mbox.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/bcm2835_vcprop.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/bcmgenet.c` | C code | Simplified-BSD | OK | .
`dev/ic/bcmgenetreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/bcmgenetvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/bt458reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/bt463.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/ic/bt463reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/bt463var.h` | C header | Simplified-BSD | OK | .
`dev/ic/bt485.c` | C code | CMU (similar to ISC) | OK | .
`dev/ic/bt485reg.h` | C header | CMU (similar to ISC) | OK | .
`dev/ic/bt485var.h` | C header | CMU (similar to ISC) | OK | .
`dev/ic/bt8xx.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/bwfm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/bwfmreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/bwfmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/bwi.c` | C code | Modified-BSD | OK | .
`dev/ic/bwireg.h` | C header | Modified-BSD | OK | .
`dev/ic/bwivar.h` | C header | Modified-BSD | OK | .
`dev/ic/cac.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/cacreg.h` | C header | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/cacvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/ccp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ccpvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/cd1190reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/cd1400reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/ciss.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/cissreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/cissvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/com.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/comreg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/comvar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/cs4231reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/cy.c` | C code | Modified-BSD | OK | .
`dev/ic/cyreg.h` | C header | Modified-BSD | OK | .
`dev/ic/dc21040reg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/dc.c` | C code | Original-BSD | OK | .
`dev/ic/dcreg.h` | C header | Original-BSD | OK | .
`dev/ic/decmonitors.c` | C code | Simplified-BSD | OK | .
`dev/ic/dl10019.c` | C code | Simplified-BSD | OK | .
`dev/ic/dl10019reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/dl10019var.h` | C header | Simplified-BSD | OK | .
`dev/ic/dp8390.c` | C code | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/ic/dp8390reg.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/ic/dp8390var.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/ic/dp8573areg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/ds1286reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`dev/ic/ds1687reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/dwhdmi.c` | C code | Simplified-BSD | OK | .
`dev/ic/dwhdmi.h` | C header | Simplified-BSD | OK | .
`dev/ic/dwhdmiphy.c` | C code | Simplified-BSD | OK | .
`dev/ic/dwiic.c` | C code | Original-ISC | OK | .
`dev/ic/dwiicreg.h` | C header | Original-ISC | OK | .
`dev/ic/dwiicvar.h` | C header | Original-ISC | OK | .
`dev/ic/elink3.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/elink3reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/elink3var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/fxp.c` | C code | Simplified-BSD | OK | .
`dev/ic/fxpreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/fxpvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/gdt_common.c` | C code | Simplified-BSD | OK | .
`dev/ic/gdtreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/gdtvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/gem.c` | C code | Simplified-BSD | OK | .
`dev/ic/gemreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/gemvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/hme.c` | C code | Simplified-BSD | OK | .
`dev/ic/hmereg.h` | C header | Simplified-BSD | OK | .
`dev/ic/hmevar.h` | C header | Simplified-BSD | OK | .
**`dev/ic/i8042reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 43 code lines + 2 commented lines; 52 total lines | .
`dev/ic/i82365.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i82365reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i82365var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i8237reg.h` | C header | "Unlicensed!!!" | Trivial; 8 code lines + 5 commented line; 17 total lines | .
`dev/ic/i8253reg.h` | C header | Modified-BSD | OK | .
`dev/ic/i82586reg.h` | C header | Original-BSD | OK | .
`dev/ic/i82596.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/i82596reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/i82596var.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/i8259reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/i82802reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/ibm561.c` | C code | Simplified-BSD | OK | .
`dev/ic/ibm561reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/ibm561var.h` | C header | Simplified-BSD | OK | .
`dev/ic/ics2101reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/if_wi.c` | C code | Original-BSD | OK | .
`dev/ic/if_wi_hostap.c` | C code | Original-BSD | OK | .
`dev/ic/if_wi_hostap.h` | C header | Original-BSD | OK | .
`dev/ic/if_wi_ieee.h` | C header | Original-BSD | OK | .
`dev/ic/if_wireg.h` | C header | Original-BSD | OK | .
`dev/ic/if_wivar.h` | C header | Original-BSD | OK | .
`dev/ic/iha.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/iha.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/imxiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/imxiicvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/lance.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/lancereg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/lancevar.h` | C header | Simplified-BSD | OK | .
`dev/ic/lemac.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/lemacreg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/lemacvar.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/lm700x.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/lm700x.h` | C header | Simplified-BSD | OK | .
`dev/ic/lm78.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/lm78var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/lpt.c` | C code | Original-BSD | OK | .
`dev/ic/lptreg.h` | C header | Modified-BSD | OK | .
`dev/ic/lptvar.h` | C header | Original-BSD | OK | .
`dev/ic/lsi64854.c` | C code | Simplified-BSD | OK | .
`dev/ic/lsi64854reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/lsi64854var.h` | C header | Simplified-BSD | OK | .
`dev/ic/m41t8xreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/malo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/malo.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/max2820reg.h` | C header | Modified-BSD | OK | .
`dev/ic/mc146818reg.h` | C header | CMU (similar to ISC) | OK | .
`dev/ic/mc6845.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/mc6845reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/mfi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mfireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mfivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mk48txx.c` | C code | Simplified-BSD | OK | .
`dev/ic/mk48txxreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/monitors.h` | C header | Simplified-BSD | OK | .
`dev/ic/mpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mpireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mpivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/mpuvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/mtd8xx.c` | C code | Simplified-BSD | OK | .
`dev/ic/mtd8xxreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/mtd8xxvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/ncr53c9x.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/ncr53c9xreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/ncr53c9xvar.h` | C header | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/ne2000.c` | C code | Simplified-BSD | OK | .
`dev/ic/ne2000reg.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/ic/ne2000var.h` | C header | Simplified-BSD | OK | .
`dev/ic/nec765reg.h` | C header | Modified-BSD | OK | .
`dev/ic/ns16450reg.h` | C header | Modified-BSD | OK | .
`dev/ic/ns16550reg.h` | C header | Modified-BSD | OK | .
`dev/ic/nvme.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/nvmereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/nvmevar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/oosiop.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/oosiopreg.h` | C header | Modified-BSD | OK | .
`dev/ic/oosiopvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/osiop.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/osiopreg.h` | C header | Modified-BSD | OK | .
`dev/ic/osiopvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/p9000.h` | C header | Simplified-BSD | OK | .
`dev/ic/pcdisplay_chars.c` | C code | Simplified-BSD | OK | .
**`dev/ic/pcdisplay.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 30 code lines + 9 commented line; 49 total lines | .
`dev/ic/pcdisplay_subr.c` | C code | CMU (similar to ISC) | OK | .
`dev/ic/pcdisplayvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/pcf8584.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/pcf8584var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/pckbc.c` | C code | Simplified-BSD | OK | .
`dev/ic/pckbcvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/pgt.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/pgtreg.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/pgtvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/pluart.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/pluartvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/pt2254a.c` | C code | Simplified-BSD | OK | .
`dev/ic/pt2254a.h` | C header | Simplified-BSD | OK | .
`dev/ic/qla.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/qlareg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/qlavar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/qlw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/qlwreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/qlwvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/r92creg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/ramdac.h` | C header | Simplified-BSD | OK | .
`dev/ic/re.c` | C code | Original-BSD | OK | .
`dev/ic/revar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rf3000reg.h` | C header | Modified-BSD | OK | .
`dev/ic/rt2560.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2560reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2560var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2661.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2661reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2661var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2860.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2860reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rt2860var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtl80x9.c` | C code | Simplified-BSD | OK | .
`dev/ic/rtl80x9reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/rtl80x9var.h` | C header | Simplified-BSD | OK | .
`dev/ic/rtl81x9.c` | C code | Original-BSD | OK | .
`dev/ic/rtl81x9reg.h` | C header | Original-BSD | OK | .
`dev/ic/rtl8225reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtsx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtsxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtsxvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtw.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/rtwn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtwnvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/rtwreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/rtwvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/s3_617.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/sa2400reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/seeq8003reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/si4136reg.h` | C header | Modified-BSD | OK | .
`dev/ic/sili.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/silireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/silivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/siop.c` | C code | Simplified-BSD | OK | .
`dev/ic/siop_common.c` | C code | Simplified-BSD | OK | .
`dev/ic/siopreg.h` | C header | Simplified-BSD | OK | .
`dev/ic/siopvar_common.h` | C header | Simplified-BSD | OK | .
`dev/ic/siopvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/smc83c170.c` | C code | Simplified-BSD | OK | .
`dev/ic/smc83c170reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/smc83c170var.h` | C header | Simplified-BSD | OK | .
`dev/ic/smc91cxx.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/smc91cxxreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/smc91cxxvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/smc93cx6.c` | C code | Custom-Original-BSD (third and four clauses are custom clauses) | OK | .
`dev/ic/smc93cx6var.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/sti.c` | C code | Simplified-BSD | OK | .
`dev/ic/stireg.h` | C header | Simplified-BSD | OK | .
`dev/ic/stivar.h` | C header | Simplified-BSD | OK | .
`dev/ic/tc921x.c` | C code | Simplified-BSD | OK | .
`dev/ic/tc921x.h` | C header | Simplified-BSD | OK | .
`dev/ic/tcic2.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/tcic2reg.h` | C header | Simplified-BSD | OK | .
`dev/ic/tcic2var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/tea5757.c` | C code | Simplified-BSD | OK | .
`dev/ic/tea5757.h` | C header | Simplified-BSD | OK | .
`dev/ic/ti.c` | C code | Original-BSD | OK | .
`dev/ic/tireg.h` | C header | Original-BSD | OK | .
`dev/ic/tivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ic/trm.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/trm.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/ic/twe.c` | C code | Simplified-BSD | OK | .
`dev/ic/twereg.h` | C header | Simplified-BSD | OK | .
`dev/ic/twevar.h` | C header | Simplified-BSD | OK | .
`dev/ic/uha.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/uhareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/uhavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/vga.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`dev/ic/vgareg.h` | C header | Simplified-BSD | OK | .
`dev/ic/vga_subr.c` | C code | Simplified-BSD | OK | .
`dev/ic/vgavar.h` | C header | CMU (similar to ISC) | OK | .
`dev/ic/w83l518d.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/w83l518dreg.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/w83l518d_sdmmc.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/w83l518d_sdmmc.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/w83l518dvar.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/ic/wd33c93.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/wd33c93reg.h` | C header | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/wd33c93var.h` | C header | Mofified-BSD | OK | .
`dev/ic/wdc.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/ic/wdcevent.h` | C header | Custom-Simplified-BSD (second clause is an "endorsement clause") | OK | .
`dev/ic/wdcreg.h` | C header | Modified-BSD | OK | .
`dev/ic/wdcvar.h` | C header | Simplified-BSD | OK | .
`dev/ic/xl.c` | C code | Original-BSD | OK | .
`dev/ic/xlreg.h` | C header | Original-BSD | OK | .
`dev/ic/z8530reg.h` | C header | Modified-BSD | OK | .
`dev/ic/z8530sc.c` | C code | Modified-BSD | OK | .
`dev/ic/z8530sc.h` | C header | Modified-BSD | OK | .
`dev/ic/z8530tty.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/ipmi.c` | C code | Simplified-BSD | OK | .
`dev/ipmi.h` | C header | Simplified-BSD | OK | .
`dev/ipmivar.h` | C header | Simplified-BSD | OK | .
`dev/isa/ad1848.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/isa/ad1848var.h` | C header | Original-BSD | OK | .
`dev/isa/addcom_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/aic_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/isa/aic_isapnp.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/isa/aps.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/ast.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/boca.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/com_commulti.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/isa/com_isa.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/isa/com_isapnp.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/isa/cs4231var.h` | C header | Simplified-BSD | OK | .
`dev/isa/cy_isa.c` | C code | Modified-BSD | OK | .
`dev/isa/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/elink.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/elink.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/ess.c` | C code | Custom-BSD (similar to Modified-BSD but without the "binary clause") | OK | .
`dev/isa/ess_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/essreg.h` | C header | Custom-BSD (similar to Modified-BSD but without the "binary clause") | OK | .
`dev/isa/essvar.h` | C header | Custom-BSD (similar to Modified-BSD but without the "binary clause") | OK | .
`dev/isa/fdc.c` | C code | Modified-BSD | OK | .
`dev/isa/fd.c` | C code | Modified-BSD | OK | .
`dev/isa/fdlink.h` | C header | Modified-BSD | OK | .
`dev/isa/fdreg.h` | C header | Modified-BSD | OK | .
`dev/isa/files.isa` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/isa/files.isapnp` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/isa/fins.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/gscsio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/gscsioreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/gus.c` | C code | Simplified-BSD | OK | .
`dev/isa/gus_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/gus_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/gusreg.h` | C header | Simplified-BSD | OK | .
`dev/isa/gusvar.h` | C header | Simplified-BSD | OK | .
`dev/isa/hsq.c` | C code | Simplified-BSD | OK | .
`dev/isa/i82365_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isasubr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/ics2101.c` | C code | Simplified-BSD | OK | .
`dev/isa/ics2101var.h` | C header | Simplified-BSD | OK | .
`dev/isa/if_an_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_ec.c` | C code | Simplified-BSD + Custom-BSD (Similar to Modified-BSD) | OK (Combined Licenses) | .
`dev/isa/if_ecreg.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/isa/if_ef_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_eg.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_egreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_el.c` | C code | Expat | OK | .
`dev/isa/if_elreg.h` | C header | Expat | OK | .
`dev/isa/if_ep_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_ep_isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_ex.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_exreg.h` | C header | Simplified-BSD | OK | .
**`dev/isa/if_ie507.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 15 code lines + 5 commented lines; 24 total lines | .
**`dev/isa/if_ieatt.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 27 total lines | .
`dev/isa/if_ie.c` | C code | Original-BSD | OK | .
`dev/isa/if_iee16.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_lc_isa.c` | C code | Custom-Simplified-BSD (second clause is an "endorsement clause") | OK | .
`dev/isa/if_le.c` | C code | Modified-BSD | OK | .
`dev/isa/if_le_isa.c` | C code | Modified-BSD | OK | .
`dev/isa/if_le_isapnp.c` | C code | Modified-BSD | OK | .
`dev/isa/if_levar.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/isa/if_ne_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_ne_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_sm_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/if_we.c` | C code | Simplified-BSD + Custom-BSD (Similar to Modified-BSD) | OK (Combined Licenses) | .
`dev/isa/if_wereg.h` | C header | Custom-BSD (Similar to Modified-BSD) | OK | .
`dev/isa/isa.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/isa/isadma.c` | C code | Simplified-BSD | OK | .
**`dev/isa/isadmareg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 13 code lines + 8 commented lines; 27 total lines | .
`dev/isa/isadmavar.h` | C header | Simplified-BSD | OK | .
`dev/isa/isagpio.c` | C code | Simplified-BSD | OK | .
`dev/isa/isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpdebug.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpres.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isareg.h` | C header | Modified-BSD | OK | .
`dev/isa/isavar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/isa/it.c` | C code | Simplified-BSD | OK | .
`dev/isa/itvar.h` | C header | Simplified-BSD | OK | .
`dev/isa/lm78_isa.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/lpt_isa.c` | C code | Original-BSD | OK | .
`dev/isa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 4 commented lines; 9 total lines | .
`dev/isa/mpu401.c` | C code | Simplified-BSD | OK | .
`dev/isa/mpu_isa.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
**`dev/isa/mpu_isapnp.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 1 commented line; 73 total lines | .
`dev/isa/nsclpcsio_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/pas.c` | C code | Original-BSD | OK | .
**`dev/isa/pasreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 193 code lines + 34 commented lines; 256 total lines | .
`dev/isa/pcdisplay.c` | C code | Simplified-BSD | OK | .
`dev/isa/pcdisplayvar.h` | C header | Simplified-BSD | OK | .
`dev/isa/pckbc_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/pcppi.c` | C code | CMU (similar to ISC) | OK | .
`dev/isa/pcppireg.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 5 commented lines; 12 total lines | .
`dev/isa/pcppivar.h` | C header | CMU (similar to ISC) | OK | .
**`dev/isa/pnpdevs`** | AWK script | __Unlicensed!!!__ | Non-Trivial; 96 code lines + 364 commented lines; 487 total lines | .
**`dev/isa/pnpdevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 98 code lines + 6 commented lines; 106 total lines [False-Positive: this file is generated by "dev/isa/pnpdevs.h"] | .
`dev/isa/sb.c` | C code | Original-BSD | OK | .
`dev/isa/sbdsp.c` | C code | Original-BSD | OK | .
`dev/isa/sbdspvar.h` | C code | Original-BSD | OK | .
`dev/isa/sb_isa.c` | C code | Original-BSD | OK | .
`dev/isa/sb_isapnp.c` | C code | Original-BSD | OK | .
`dev/isa/sbreg.h` | C header | Original-BSD | OK | .
`dev/isa/sbvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/sch311x.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/skgpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/spkr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/spkrio.h` | C header | "Unlicensed!!!" | Trivial; 9 code lines + 5 commented lines; 20 total lines | .
`dev/isa/tcic2_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/uguru.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/uha_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/vga_isa.c` | C code | CMU (similar to ISC) | OK | .
`dev/isa/vga_isavar.h` | C header | CMU (similar to ISC) | OK | .
`dev/isa/viasio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/viasioreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/wbsio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/wbsioreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/isa/wdc_isa.c` | C code | Simplified-BSD | OK | .
`dev/isa/wdc_isapnp.c` | C code | Simplified-BSD | OK | .
`dev/isa/wds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/isa/wdsreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 119 code lines + 13 commented lines; 154 total lines | .
`dev/kcov.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/kstat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ksyms.c` | C code | Simplified-BSD | OK | .
`dev/microcode/adw/advmcode.c` | C code | Simplified-BSD | OK | .
`dev/microcode/adw/advmcode.h` | C header | Simplified-BSD | OK | .
`dev/microcode/adw/adwmcode.c` | C code | Simplified-BSD | OK | .
`dev/microcode/adw/adwmcode.h` | C header | Simplified-BSD | OK | .
`dev/microcode/afb/afb-license` | License file | Expat | OK | .
`dev/microcode/afb/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/afb/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 30 total lines | .
`dev/microcode/afb/microcode.h` | C header | Expat | OK | .
`dev/microcode/aic7xxx/aic79xx.reg` | Assembler/C generator header | Modified-BSD | OK | .
**`dev/microcode/aic7xxx/aic79xx_reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3131 code lines + 10 commented lines; 3825 total lines [False-Positive: this file is generated by "aic79xx.seq" and "aic79xx.reg"] | .
`dev/microcode/aic7xxx/aic79xx.seq` | Assembler/C generator header | Modified-BSD | OK | .
**`dev/microcode/aic7xxx/aic79xx_seq.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1131 code lines + 8 commented lines; 1191 total lines [False-Positive: this file is generated by "aic79xx.seq" and "aic79xx.reg"] | .
`dev/microcode/aic7xxx/aic7xxx.reg` | Assembler/C generator header | Modified-BSD | OK | .
**`dev/microcode/aic7xxx/aic7xxx_reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1471 code lines + 10 commented lines; 1789 total lines [False-Positive: this file is generated by "aic7xxx.seq" and "aic7xxx.reg"] | .
`dev/microcode/aic7xxx/aic7xxx.seq` | Assembler/C generator header | Modified-BSD | OK | .
**`dev/microcode/aic7xxx/aic7xxx_seq.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1251 code lines + 8 commented lines; 1310 total lines [False-Positive: this file is generated by "aic7xxx.seq" and "aic7xxx.reg"] | .
`dev/microcode/aic7xxx/aicasm.c` | C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_gram.y` | Assembler/C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm.h` | C header | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_insformat.h` | C header | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_macro_gram.y` | Assembler/C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_macro_scan.l` | Assembler/C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_scan.l` | Assembler/C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_symbol.c` | C code | Modified-BSD | OK | .
`dev/microcode/aic7xxx/aicasm_symbol.h` | C header | Modified-BSD | OK | .
**`dev/microcode/aic7xxx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 48 code lines + 3 commented lines; 65 total lines | .
**`dev/microcode/atmel/atmel_at76c503_i3863_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c503_rfmd2_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c503_rfmd_acc_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c505_rfmd.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_intersil_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd2958_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd2958-smc_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
`dev/microcode/atmel/atu-license` | License file | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
`dev/microcode/atmel/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/atmel/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 31 non empty lines; 41 total lines | .
`dev/microcode/bnx/bnxfw.h` | C header | Modified-BSD | OK | .
`dev/microcode/bnx/bnx-license` | License file | Modified-BSD | OK | .
`dev/microcode/bnx/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/bnx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
`dev/microcode/bwi/build/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/bwi/build/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line ; 9 total lines | .
`dev/microcode/bwi/extract/extract.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/bwi/extract/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line ; 9 total lines | .
`dev/microcode/bwi/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line ; 6 total lines | .
`dev/microcode/cirruslogic/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/cirruslogic/cs4280_image.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3472 non empty lines; 3474 total lines | .
**`dev/microcode/cirruslogic/cs4280-license`** | License file | __Nonfree!!!__ | __Nonfree!!!__ [License contains information where it says this license needs to be solved by someone finding the right person at Crystal Semiconductor (reason: no copyright notice)] | .
**`dev/microcode/cirruslogic/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 30 total lines | .
`dev/microcode/cyclades/cyzfirm2h.c` | C code | Original-BSD | OK | .
`dev/microcode/cyclades/cyzfirm.h` | C header | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 2 commented lines ; 31 total lines [False-Positive???: this file is generated by "ZLOGIC.CYZ", however the source code is unknown] | .
`dev/microcode/esa/esadsp.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/microcode/fxp/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/fxp/fxp-license` | License file | Modified-BSD | OK | .
**`dev/microcode/fxp/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
`dev/microcode/fxp/rcvbundl.h` | C header | Modified-BSD | OK | .
`dev/microcode/isp/asm_1040.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_1080.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_12160.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_2100.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_2200.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_2300.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_2400.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_2500.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/isp/asm_sbus.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/kue/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/kue/kue_fw.h` | C header | Original-BSD | OK | .
`dev/microcode/kue/kue-license` | License file | Original-BSD | OK | .
**`dev/microcode/kue/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
`dev/microcode/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 1 commented line; 7 total lines | .
`dev/microcode/Makefile.inc` | Makefile | "Unlicensed!!!" | Trivial; 1 code lines + 1 commented line; 4 total lines | .
`dev/microcode/myx/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/myx/ethp_z8e.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/microcode/myx/eth_z8e.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
**`dev/microcode/myx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 24 non empty lines; 35 total lines | .
`dev/microcode/myx/myx-license` | License file | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/microcode/ncr53cxxx/ncr53cxxx.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/microcode/neomagic/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/neomagic/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 29 total lines | .
`dev/microcode/neomagic/neo-coeff.h` | C header | Simplified-BSD | OK | .
`dev/microcode/neomagic/neo-license` | License file | Simplified-BSD | OK | .
`dev/microcode/ral/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/ral/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 36 total lines | .
`dev/microcode/ral/microcode.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/ral/ral-license` | License file | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/rum/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/rum/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 non empty lines; 38 total lines | .
`dev/microcode/rum/microcode.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/microcode/rum/rum-license` | License file | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/siop/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 14 code lines + 2 commented lines; 24 total lines | .
`dev/microcode/siop/ncr53cxxx.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/microcode/siop/oosiop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 5 commented lines; 80 total lines [False-Positive: this file is generated by "oosiop.ss"] | .
`dev/microcode/siop/oosiop.ss` | Assembler/C generator header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
**`dev/microcode/siop/osiop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 175 code lines + 5 commented lines; 182 total lines [False-Positive: this file is generated by "osiop.ss"] | .
`dev/microcode/siop/osiop.ss` | Assembler/C generator header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/microcode/siop/siop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 397 code lines + 5 commented lines; 419 total lines [False-Positive: this file is generated by "siop.ss"] | .
`dev/microcode/siop/siop.ss` | Assembler/C generator header | Simplified-BSD | OK | .
**`dev/microcode/symbol/eprimsym`** | Binary file | __Unlicensed!!!__ | __Nonfree!!!__ [This file does not include source code] | .
**`dev/microcode/symbol/esecsym`** | Binary file | __Unlicensed!!!__ | __Nonfree!!!__ [This file does not include source code] | .
**`dev/microcode/symbol/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 21 total lines | .
`dev/microcode/symbol/spectrum24t_cf.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/symbol/symbol-license` | License file | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/tht/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/tht/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
`dev/microcode/tht/microcode.h` | C header | Original-BSD | OK | .
`dev/microcode/tht/tht-license` | License file | Original-BSD | OK | .
`dev/microcode/tigon/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/tigon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
**`dev/microcode/tigon/ti_fw2.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 4869 code lines + 12 commented lines; 4883 total lines | __Nonfree!!!__ [Under the file description, it was generated by genfw.c, but where is it and source code to generate ti_fw2.h???]
**`dev/microcode/tigon/ti_fw.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 4592 code lines + 7 commented lines; 4601 total lines | __Nonfree!!!__ [Under the file description, it was generated by genfw.c, but where is it and source code to generate ti_fw.h???]
**`dev/microcode/tigon/tigon-license`** | License file | __Nonfree-License!!!__ | __Nonfree!!!__ [License file is vague and only says "We believe the original license on the source did specifically allow re-distribution."] | .
`dev/microcode/tusb3410/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/tusb3410/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
**`dev/microcode/tusb3410/tusb3410.h`** | C header | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
**`dev/microcode/tusb3410/tusb3410-license`** | License file | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
`dev/microcode/typhoon/3c990img.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/typhoon/3c990-license` | License file | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/typhoon/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/typhoon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
`dev/microcode/udl/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/udl/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 21 code lines + 2 commented lines; 34 total lines | .
**`dev/microcode/udl/udl_huffman.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 65543 code lines + 11 commented lines; 65558 total lines [This file includes a note where says they don't know how to program generate the table. The algorythm could be hidden in the 4kB decompression table (udl_decomp_table) which is sent to the device at initialization time.] | .
`dev/microcode/yds/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/yds/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
**`dev/microcode/yds/yds_hwmcode.h`** | C header | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains information where it says this license has free distribution, no restriction for their distribution, however it lacks the essential freedom 0 and 1, see https://www.gnu.org/philosophy/free-sw.html#four-freedoms for futher info] | .
**`dev/microcode/yds/yds-license`** | License file | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains information where it says this license has free distribution, no restriction for their distribution, however it lacks the essential freedom 0 and 1, see https://www.gnu.org/philosophy/free-sw.html#four-freedoms for futher info] | .
`dev/microcode/zydas/build.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/microcode/zydas/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
`dev/microcode/zydas/microcode.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/microcode/zydas/zd1211-license` | License file | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/midi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/midi_if.h` | C header | Simplified-BSD | OK | .
`dev/midivar.h` | C header | Simplified-BSD | OK | .
`dev/mii/acphy.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/mii/acphyreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/mii/amphy.c` | C code | Original-BSD | OK | .
`dev/mii/amphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/atphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/bmtphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/bmtphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/brgphy.c` | C code | Original-BSD | OK | .
`dev/mii/brgphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/brswphy.c` | C code | Original-ISC + Original-ISC + Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/ciphy.c` | C code | Original-BSD | OK | .
`dev/mii/ciphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/dcphy.c` | C code | Original-BSD | OK | .
`dev/mii/devlist2h.awk` | AWK script | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/mii/eephy.c` | C code | Simplified-BSD | OK | .
`dev/mii/eephyreg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/mii/etphy.c` | C code | Modified-BSD | OK | .
`dev/mii/exphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/files.mii` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/mii/gentbi.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/icsphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/icsphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/inphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/inphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/iophy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/iophyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/ipgphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/ipgphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/jmphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/jmphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/luphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/lxtphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/lxtphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`dev/mii/mii_bitbang.c` | C code | Simplified-BSD | OK | .
`dev/mii/mii_bitbang.h` | C header | Simplified-BSD | OK | .
`dev/mii/mii.c` | C code | Simplified-BSD | OK | .
`dev/mii/miidevs` | AWK script | Simplified-BSD | OK | .
`dev/mii/miidevs.h` | C header | Simplified-BSD | OK | .
`dev/mii/mii.h` | C header | Simplified-BSD | OK | .
`dev/mii/mii_physubr.c` | C code | Simplified-BSD | OK | .
`dev/mii/miivar.h` | C header | Simplified-BSD | OK | .
`dev/mii/mlphy.c` | C code | Original-BSD + Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/mtdphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/nsgphy.c` | C code | Original-BSD | OK | .
`dev/mii/nsgphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/nsphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/nsphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/nsphyter.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/nsphyterreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/qsphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/qsphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/rdcphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/rgephy.c` | C code | Original-BSD | OK | .
`dev/mii/rgephyreg.h` | C header | Original-BSD | OK | .
`dev/mii/rlphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/sqphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/sqphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/tlphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/tlphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/tlphyvar.h` | C header | Simplified-BSD | OK | .
`dev/mii/tqphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/tqphyreg.h` | C header | Simplified-BSD | OK | .
`dev/mii/txphy.c` | C code | Simplified-BSD | OK | .
`dev/mii/ukphy.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/ukphy_subr.c` | C code | Simplified-BSD | OK | .
`dev/mii/urlphy.c` | C code | Modified-BSD | OK | .
`dev/mii/urlphyreg.h` | C header | Modified-BSD | OK | .
`dev/mii/xmphy.c` | C code | Original-BSD | OK | .
`dev/mii/xmphyreg.h` | C header | Original-BSD | OK | .
`dev/mulaw.c` | C code | Original-BSD | OK | .
`dev/mulaw.h` | C header | Simplified-BSD | OK | .
`dev/ofw/fdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/fdt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/files.ofw` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/ofw/ofw_clock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_clock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_gpio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_gpio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_misc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_misc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_pci.h` | C header | Simplified-BSD | OK | .
`dev/ofw/ofw_pinctrl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_pinctrl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_power.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_power.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_regulator.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_regulator.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_thermal.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/ofw_thermal.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/ofw/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/onewire/devlist2h.awk` | AWK script | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/files.onewire` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/onewire/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`dev/onewire/onewire_bitbang.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/onewire.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/onewiredevs` | AWK script | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/onewire/onewiredevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 39 code lines + 7 commented lines; 50 total lines [False-Positive: this file is generated by "dev/onewire/onewiredevs"] | .
**`dev/onewire/onewiredevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 32 code lines + 7 commented lines; 42 total lines [False-Positive: this file is generated by "dev/onewire/onewiredevs"] | .
`dev/onewire/onewirereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/onewire_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/onewirevar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/owctr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/owid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/owsbm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/onewire/owtemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/aac_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/adv_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/adw_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/agp_ali.c` | C code | Simplified-BSD | OK | .
`dev/pci/agp_amd.c` | C code | Simplified-BSD | OK | .
`dev/pci/agp_apple.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/agp.c` | C code | Simplified-BSD | OK | .
`dev/pci/agp_i810.c` | C code | Simplified-BSD | OK | .
`dev/pci/agp_intel.c` | C code | Simplified-BSD | OK | .
`dev/pci/agpreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/agp_sis.c` | C code | Simplified-BSD | OK | .
`dev/pci/agpvar.h` | C header | Simplified-BSD | OK | .
`dev/pci/agp_via.c` | C code | Simplified-BSD | OK | .
`dev/pci/ahci_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ahc_pci.c` | C code | Modified-BSD or GPL-2 | OK | .
`dev/pci/ahd_pci.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/pci/alipm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/amas.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/amas.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/amdiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/amdpcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/amdpm.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/pci/ami_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/arc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/auacer.c` | C code | Simplified-BSD | OK | .
`dev/pci/auacerreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/auglx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/auich.c` | C code | Simplified-BSD | OK | .
`dev/pci/auixp.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/auixpreg.h` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/auixpvar.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/autri.c` | C code | Simplified-BSD | OK | .
`dev/pci/autrireg.h` | C header | Simplified-BSD | OK | .
`dev/pci/autrivar.h` | C header | Simplified-BSD | OK | .
`dev/pci/auvia.c` | C code | Simplified-BSD | OK | .
`dev/pci/auviavar.h` | C header | Simplified-BSD | OK | .
`dev/pci/azalia.c` | C code | Simplified-BSD | OK | .
`dev/pci/azalia_codec.c` | C code | Simplified-BSD | OK | .
`dev/pci/azalia.h` | C code | Simplified-BSD | OK | .
`dev/pci/berkwdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/bktr/bktr_audio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_audio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_card.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_card.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_core.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_core.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_os.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_os.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_tuner.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_tuner.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cac_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/ccp_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ciss_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/cmpci.c` | C code | Simplified-BSD | OK | .
`dev/pci/cmpcireg.h` | C header | Simplified-BSD | OK | .
`dev/pci/cmpcivar.h` | C header | Simplified-BSD | OK | .
`dev/pci/com_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/cs4280.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4280reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4281.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4281reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cy82c693.c` | C code | Simplified-BSD | OK | .
`dev/pci/cy82c693reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/cy82c693var.h` | C header | Simplified-BSD | OK | .
`dev/pci/cy_pci.c` | C code | Modified-BSD | OK | .
`dev/pci/cz.c` | C code | Original-BSD | OK | .
`dev/pci/czreg.h` | C header | Original-BSD | OK | .
`dev/pci/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/drm/amd/acp/acp_hw.c` | C code | Expat | OK | .
`dev/pci/drm/amd/acp/include/acp_gfx_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_acp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_acp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_acpi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_afmt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_arcturus.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_fence.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v10_3.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v7.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v8.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v9.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gfx_v9.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd_gpuvm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_amdkfd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios_crtc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios_dp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios_encoders.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atombios_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atom.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atomfirmware.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_atomfirmware.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_benchmark.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_bios.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_bo_list.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_bo_list.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_cgs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_cik.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_cik_sdma.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_connectors.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_connectors.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_csa.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_csa.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_cs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ctx.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ctx.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_device.c` | C code | Expat | OK | .
**`dev/pci/drm/amd/amdgpu/amdgpu_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 116 code lines + 19 commented lines; 139 total lines | .
`dev/pci/drm/amd/amdgpu/amdgpu_df.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_discovery.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_discovery.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_display.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_display.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_dma_buf.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_dma_buf.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_doorbell.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_drv.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_drv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_encoders.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_fb.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_fence.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_fru_eeprom.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_fru_eeprom.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gart.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gart.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gds.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gem.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gem.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gfx.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gfx.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gfxhub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gmc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_gtt_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_i2c.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ib.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ids.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ids.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ioc32.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_irq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_irq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_job.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_job.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_jpeg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_jpeg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_kms.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mes.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mmhub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mmhub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mn.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_mode.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_nbio.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_nbio.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_object.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_object.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_pll.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_pll.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_pmu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_pmu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_psp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_psp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_rap.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_rap.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ras.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ras_eeprom.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ras_eeprom.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ras.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ring.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ring.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_rlc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_rlc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sa.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sched.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sched.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sdma.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sdma.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_si.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_si_dma.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_socbb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sync.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_sync.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_test.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_trace.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_trace_points.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ttm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ttm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ucode.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_ucode.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_umc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_umc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_uvd.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_uvd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_uvd_v3_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_uvd_v4_2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vce.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vce.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vce_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vcn.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vcn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vf_error.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vf_error.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_virt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_virt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vm_cpu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vm_sdma.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_vram_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_xgmi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgpu_xgmi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/amdgv_sriovmsg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/arct_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v2_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/athub_v2_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/atombios_crtc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/atombios_dp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/atombios_encoders.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/atombios_i2c.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/atom.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cikd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cik.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cik_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cik_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cik_sdma.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_ci.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_defs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_gfx10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_gfx9.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_si.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/clearstate_vi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cz_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/cz_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v10_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v10_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v11_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v11_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v6_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v6_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v8_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_v8_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_virtual.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/dce_virtual.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/df_v1_7.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/df_v1_7.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/df_v3_6.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/df_v3_6.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/emu_soc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v1_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v1_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v2_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfxhub_v2_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v10_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v10_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v6_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v6_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v7_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v7_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v8_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v8_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v9_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v9_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v9_4.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gfx_v9_4.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v10_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v10_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v6_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v6_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v7_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v7_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v8_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v8_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v9_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/gmc_v9_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/iceland_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/iceland_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/iceland_sdma_pkt_open.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v2_5.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v2_5.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v3_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/jpeg_v3_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mes_api_def.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mes_v10_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mes_v10_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v9_4.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmhub_v9_4.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmsch_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmsch_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mmsch_v3_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_ai.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_ai.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_nv.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_nv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_vi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/mxgpu_vi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi10_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi10_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi10_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi10_sdma_pkt_open.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi12_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/navi14_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v2_3.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v2_3.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v6_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v6_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v7_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v7_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v7_4.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nbio_v7_4.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nv.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nvd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/nv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/ObjectID.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_gfx_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v10_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v10_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v11_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v11_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v12_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v12_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v3_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/psp_v3_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v2_4.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v2_4.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v3_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v3_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v4_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v4_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v5_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v5_2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sdma_v5_2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/si_dma.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/sienna_cichlid_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/si_enums.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/si.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/si_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/si_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/smu_v11_0_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/smu_v11_0_i2c.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/soc15.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/soc15_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/soc15d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/soc15.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/ta_rap_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/ta_ras_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/ta_xgmi_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/tonga_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/tonga_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/tonga_sdma_pkt_open.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v6_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v6_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v6_1.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v6_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v8_7.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/umc_v8_7.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v3_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v4_2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v5_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v6_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v6_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v7_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/uvd_v7_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vce_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vce_v3_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vce_v3_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vce_v4_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vce_v4_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v2_5.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v2_5.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v3_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vcn_v3_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vega10_ih.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vega10_ih.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vega10_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vega10_sdma_pkt_open.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vega20_reg_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdgpu/vi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cik_event_interrupt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cik_int.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cik_regs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cwsr_trap_handler_gfx10.asm` | Assembler code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cwsr_trap_handler_gfx8.asm` | Assembler code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cwsr_trap_handler_gfx9.asm` | Assembler code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/cwsr_trap_handler.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_chardev.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_crat.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_crat.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_dbgdev.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_dbgdev.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_dbgmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_dbgmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager_cik.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager_v10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager_v9.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_device_queue_manager_vi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_doorbell.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_events.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_events.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_flat_memory.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_interrupt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_int_process_v9.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_iommu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_iommu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_kernel_queue.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_kernel_queue.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_module.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager_cik.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager_v10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager_v9.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_mqd_manager_vi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_packet_manager.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_packet_manager_v9.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_packet_manager_vi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pasid.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pm4_headers_ai.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pm4_headers_diq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pm4_headers.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pm4_headers_vi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_pm4_opcodes.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_priv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_process.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_process_queue_manager.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_queue.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_smi_events.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_smi_events.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_topology.c` | C code | Expat | OK | .
`dev/pci/drm/amd/amdkfd/kfd_topology.h` | C header | Expat | OK | .
`dev/pci/drm/amd/amdkfd/soc15_int.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_color.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_crc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_crc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_hdcp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_hdcp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_helpers.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_irq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_irq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_irq_params.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_mst_types.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_mst_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_pp_smu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_services.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/amdgpu_dm/amdgpu_dm_trace.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/amdgpu_vector.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/conversion.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/conversion.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/dc_common.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/dc_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/fixpt31_32.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/basics/logger.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_common.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_helper.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_helper.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_interface.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_types_internal2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/bios_parser_types_internal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table_helper2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table_helper2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table_helper.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table_helper.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/command_table_helper_struct.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce110/command_table_helper_dce110.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce110/command_table_helper_dce110.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce112/command_table_helper2_dce112.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce112/command_table_helper2_dce112.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce112/command_table_helper_dce112.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce112/command_table_helper_dce112.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce60/command_table_helper_dce60.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce60/command_table_helper_dce60.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce80/command_table_helper_dce80.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/bios/dce80/command_table_helper_dce80.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/bw_fixed.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/calcs_logger.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/custom_float.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/dce_calcs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/dcn_calc_auto.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/dcn_calc_auto.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/dcn_calc_math.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/calcs/dcn_calcs.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce100/dce_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce100/dce_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce110/dce110_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce110/dce110_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce112/dce112_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce112/dce112_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce120/dce120_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce120/dce120_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce60/dce60_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dce60/dce60_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr_clk.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr_clk.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr_vbios_smu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv1_clk_mgr_vbios_smu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv2_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn10/rv2_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn20/dcn20_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn20/dcn20_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn21/rn_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn21/rn_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn21/rn_clk_mgr_vbios_smu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn21/rn_clk_mgr_vbios_smu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn30/dalsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn30/dcn30_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn30/dcn30_clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn30/dcn30_clk_mgr_smu_msg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/clk_mgr/dcn30/dcn30_clk_mgr_smu_msg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/amdgpu_dc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_debug.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_link.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_link_ddc.c` | C code | Expat | OK | .
**`dev/pci/drm/amd/display/dc/core/dc_link_dp.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 3840 non empty lines; 4515 total lines | .
**`dev/pci/drm/amd/display/dc/core/dc_link_hwss.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 462 code lines + 35 commented lines; 596 total lines | .
`dev/pci/drm/amd/display/dc/core/dc_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_sink.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_stream.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_surface.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/core/dc_vm_helper.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_bios_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_ddc_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_dmub_srv.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_dmub_srv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_dp_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_dsc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce100/dce100_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce100/dce100_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce100/dce100_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce100/dce100_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_compressor.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_compressor.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_mem_input_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_mem_input_v.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_opp_csc_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_opp_regamma_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_opp_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_opp_v.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_resource.h` | C heaeder | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_timing_generator.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_timing_generator.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_timing_generator_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_timing_generator_v.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_transform_v.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce110/dce110_transform_v.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_compressor.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_compressor.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce112/dce112_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_timing_generator.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce120/dce120_timing_generator.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_timing_generator.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce60/dce60_timing_generator.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_timing_generator.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce80/dce80_timing_generator.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_abm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_abm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_audio.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_audio.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_aux.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_aux.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_clk_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_clock_source.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_clock_source.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_dmcu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_dmcu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_hwseq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_hwseq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c_hw.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c_hw.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c_sw.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_i2c_sw.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_ipp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_ipp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_mem_input.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_mem_input.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_opp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_opp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_panel_cntl.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_panel_cntl.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_scl_filters.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_scl_filters_old.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_stream_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_transform.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dce_transform.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_abm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_abm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_hw_lock_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_hw_lock_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_psr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dce/dmub_psr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc.h` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_helper.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_hw_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_link.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_cm_common.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_cm_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dpp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dpp_cm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dpp_dscl.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dpp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dwb.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_dwb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hubp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hubp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hw_sequencer.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hw_sequencer_debug.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hw_sequencer_debug.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_init.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_ipp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_ipp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_mpc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_mpc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_opp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_opp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_optc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_optc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_stream_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn10/dcn10_stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dccg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dccg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dpp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dpp_cm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dpp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dsc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dsc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dwb.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dwb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_dwb_scl.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hubp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hubp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hwseq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_hwseq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_init.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_mmhubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_mmhubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_mpc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_mpc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_opp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_opp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_optc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_optc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_stream_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_vmid.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn20/dcn20_vmid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hubp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hubp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hwseq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_hwseq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_init.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn21/dcn21_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_afmt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_afmt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_cm_common.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_cm_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dccg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dccg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dio_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dio_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dio_stream_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dio_stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dpp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dpp_cm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dpp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dwb.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dwb_cm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_dwb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hubp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hubp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hwseq.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_hwseq.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_init.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_init.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_mmhubbub.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_mmhubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_mpc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_mpc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_opp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_optc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_optc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_resource.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_vpg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dcn30/dcn30_vpg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_stream.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dc_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_cp_psp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_event_log.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_helpers.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dc_features.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_mode_vba_20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_mode_vba_20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_mode_vba_20v2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_mode_vba_20v2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_rq_dlg_calc_20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_rq_dlg_calc_20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_rq_dlg_calc_20v2.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn20/display_rq_dlg_calc_20v2.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn21/display_mode_vba_21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn21/display_mode_vba_21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn21/display_rq_dlg_calc_21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn21/display_rq_dlg_calc_21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn30/display_mode_vba_30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn30/display_mode_vba_30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn30/display_rq_dlg_calc_30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dcn30/display_rq_dlg_calc_30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_enums.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_lib.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_lib.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_structs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_vba.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_mode_vba.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_rq_dlg_helpers.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/display_rq_dlg_helpers.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dml1_display_rq_dlg_calc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dml1_display_rq_dlg_calc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dml_inline_defs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dml/dml_logger.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_pp_smu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_services.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dm_services_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/dc_dsc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/dscc_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/qp_tables.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/rc_calc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/rc_calc_dpi.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/dsc/rc_calc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce110/hw_factory_dce110.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce110/hw_factory_dce110.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce110/hw_translate_dce110.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce110/hw_translate_dce110.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce120/hw_factory_dce120.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce120/hw_factory_dce120.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce120/hw_translate_dce120.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce120/hw_translate_dce120.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce60/hw_factory_dce60.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce60/hw_factory_dce60.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce60/hw_translate_dce60.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce60/hw_translate_dce60.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce80/hw_factory_dce80.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce80/hw_factory_dce80.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce80/hw_translate_dce80.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dce80/hw_translate_dce80.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn10/hw_factory_dcn10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn10/hw_factory_dcn10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn10/hw_translate_dcn10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn10/hw_translate_dcn10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn20/hw_factory_dcn20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn20/hw_factory_dcn20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn20/hw_translate_dcn20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn20/hw_translate_dcn20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn21/hw_factory_dcn21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn21/hw_factory_dcn21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn21/hw_translate_dcn21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn21/hw_translate_dcn21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn30/hw_factory_dcn30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn30/hw_factory_dcn30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn30/hw_translate_dcn30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/dcn30/hw_translate_dcn30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/ddc_regs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/diagnostics/hw_factory_diag.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/diagnostics/hw_factory_diag.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/diagnostics/hw_translate_diag.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/diagnostics/hw_translate_diag.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/generic_regs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/gpio_base.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/gpio_regs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/gpio_service.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/gpio_service.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hpd_regs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_ddc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_ddc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_factory.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_factory.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_generic.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_generic.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_gpio.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_gpio.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_hpd.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_hpd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_translate.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/gpio/hw_translate.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/hdcp/hdcp_msg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/bw_fixed.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/clock_source.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/compressor.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/core_status.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/core_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/custom_float.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/dce_calcs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/dc_link_ddc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/dc_link_dp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/dcn_calc_math.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/dcn_calcs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/abm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/audio.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/aux_engine.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/clk_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/clk_mgr_internal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dccg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dchubbub.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dmcu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dpp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dsc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/dwb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/gpio.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/hubp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/hw_shared.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/ipp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/mcif_wb.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/mem_input.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/mpc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/opp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/panel_cntl.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw_sequencer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw_sequencer_private.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/timing_generator.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/transform.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/hw/vmid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/link_hwss.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/reg_helper.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/resource.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/inc/vm_helper.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce110/irq_service_dce110.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce110/irq_service_dce110.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce120/irq_service_dce120.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce120/irq_service_dce120.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce60/irq_service_dce60.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce60/irq_service_dce60.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce80/irq_service_dce80.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dce80/irq_service_dce80.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn10/irq_service_dcn10.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn10/irq_service_dcn10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn20/irq_service_dcn20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn20/irq_service_dcn20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn21/irq_service_dcn21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn21/irq_service_dcn21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn30/irq_service_dcn30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/dcn30/irq_service_dcn30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/irq_service.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq/irq_service.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/irq_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/os_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/virtual/virtual_link_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/virtual/virtual_link_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dc/virtual/virtual_stream_encoder.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dc/virtual/virtual_stream_encoder.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/dmub_srv.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/inc/dmub_cmd.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/inc/dmub_trace_buffer.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn20.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn20.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn21.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn21.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn30.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_dcn30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_reg.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_reg.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/dmub/src/dmub_srv.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/include/audio_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/bios_parser_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/bios_parser_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/dal_asic_id.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/dal_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/ddc_service_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/dpcd_defs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/fixed31_32.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/gpio_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/gpio_service_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/gpio_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/grph_object_ctrl_defs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/grph_object_defs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/grph_object_id.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/hdcp_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/i2caux_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/irq_service_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/link_service_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/logger_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/logger_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/set_mode_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/signal_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/include/vector.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/color/color_gamma.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/color/color_gamma.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/color/color_table.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/color/color_table.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/color/luts_1d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/freesync/freesync.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp1_execution.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp1_transition.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp2_execution.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp2_transition.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp_ddc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp_log.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp_log.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp_psp.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/hdcp/hdcp_psp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/inc/mod_freesync.h` | C header | Expat + Expat | OK (Combined Licenses) | .
`dev/pci/drm/amd/display/modules/inc/mod_hdcp.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/inc/mod_info_packet.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/inc/mod_shared.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/inc/mod_stats.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/inc/mod_vmid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/info_packet/info_packet.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/power/power_helpers.c` | C code | Expat | OK | .
`dev/pci/drm/amd/display/modules/power/power_helpers.h` | C header | Expat | OK | .
`dev/pci/drm/amd/display/modules/vmid/vmid.c` | C code | Expat | OK | .
**`dev/pci/drm/amd/display/TODO`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 73 data lines + 3 commented lines; 111 total lines | .
`dev/pci/drm/amd/include/amd_acpi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/amd_pcie.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/amd_pcie_helpers.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/amd_shared.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/arct_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_2_0_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_2_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_2_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_2_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/athub/athub_2_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_3_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_3_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_4_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_4_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_1_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/bif/bif_5_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/clk/clk_10_0_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/clk/clk_10_0_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/clk/clk_11_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/clk/clk_11_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_10_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_10_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_10_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_2_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_2_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_11_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_12_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_12_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_6_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_8_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_8_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dce/dce_8_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_2_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_2_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_2_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dcn/dcn_2_1_0_sh_mask.h` | C header | Expat | OK | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dcn_3_0_0_offset.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16593 code lines + 643 commented lines; 17881 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dcn_3_0_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 61291 code lines + 9117 commented lines; 70952 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dpcs_3_0_0_offset.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 503 code lines + 34 commented lines; 574 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dpcs_3_0_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3263 code lines + 267 commented lines; 3566 total lines | .
`dev/pci/drm/amd/include/asic_reg/df/df_1_7_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/df/df_1_7_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/df/df_1_7_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/df/df_3_6_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/df/df_3_6_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/df/df_3_6_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dpcs/dpcs_2_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dpcs/dpcs_2_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dpcs/dpcs_2_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/dpcs/dpcs_2_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_6_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_7_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_7_2_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_7_2_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_7_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_1_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gca/gfx_8_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_1_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_3_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_3_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_10_3_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_2_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_2_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_4_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gc/gc_9_4_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_6_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_7_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_7_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_7_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_7_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_1_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_2_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_2_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/gmc/gmc_8_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/hdp/hdp_4_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/hdp/hdp_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/hdp/hdp_5_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/hdp/hdp_5_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_1_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_2_0_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_2_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_2_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_3_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_3_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_4_1_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_4_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mmhub/mmhub_9_4_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_10_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_10_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_10_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_11_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_11_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_12_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_12_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_9_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/mp/mp_9_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbif/nbif_6_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbif/nbif_6_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_2_3_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_2_3_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_2_3_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_6_1_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_6_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_6_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_6_1_smn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_0_smn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_4_0_smn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_4_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/nbio/nbio_7_4_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_1_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_2_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_2_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_2_4_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_2_4_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_2_4_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_1_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/oss_3_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_4_0_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_4_0_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_4_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_5_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/oss/osssys_5_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/pwr/pwr_10_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/pwr/pwr_10_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/rsmu/rsmu_0_0_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/rsmu/rsmu_0_0_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_1_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma0/sdma0_4_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma1/sdma1_4_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma2/sdma2_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma2/sdma2_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma3/sdma3_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma3/sdma3_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma4/sdma4_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma4/sdma4_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma5/sdma5_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma5/sdma5_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma6/sdma6_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma6/sdma6_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma7/sdma7_4_2_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/sdma7/sdma7_4_2_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_11_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_11_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_12_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_12_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_9_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smuio/smuio_9_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_6_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_0_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_0_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_0_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_1_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_2_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_2_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_3_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_3_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_7_1_3_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_8_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_8_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/smu/smu_8_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_10_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_10_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_10_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_11_0_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_11_0_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_9_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_9_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/thm/thm_9_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_1_1_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_1_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_1_2_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_6_1_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/umc/umc_8_7_0_offset.h` | C header | Expat | OK | .
**`dev/pci/drm/amd/include/asic_reg/umc/umc_8_7_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 4 commented lines; 80 total lines | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_3_1_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_3_1_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_4_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_4_2_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_4_2_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_5_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_5_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_5_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_6_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_6_0_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_6_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_7_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/uvd/uvd_7_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_1_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_2_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_2_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_3_0_d.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_3_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_4_0_default.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_4_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vce/vce_4_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_1_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_1_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_2_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_2_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_2_5_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_2_5_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_3_0_0_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/vcn/vcn_3_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/wafl/wafl2_4_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/wafl/wafl2_4_0_0_smn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/xgmi/xgmi_4_0_0_sh_mask.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/asic_reg/xgmi/xgmi_4_0_0_smn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atombios.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atom-bits.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atomfirmware.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atomfirmwareid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atom-names.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/atom-types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/cgs_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/cik_structs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/discovery.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/displayobject.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/dm_pp_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/dcn/irqsrcs_dcn_1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/gfx/irqsrcs_gfx_10_1.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/gfx/irqsrcs_gfx_9_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/ivsrcid_vislands30.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/nbio/irqsrcs_nbif_7_4.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma0/irqsrcs_sdma0_4_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma0/irqsrcs_sdma0_5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma1/irqsrcs_sdma1_4_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma1/irqsrcs_sdma1_5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma2/irqsrcs_sdma2_5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/sdma3/irqsrcs_sdma3_5_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/smuio/irqsrcs_smuio_9_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/thm/irqsrcs_thm_9_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/uvd/irqsrcs_uvd_7_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/vce/irqsrcs_vce_4_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/vcn/irqsrcs_vcn_1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/vcn/irqsrcs_vcn_2_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/ivsrcid/vmc/irqsrcs_vmc_1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/kgd_kfd_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/kgd_pp_interface.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/navi10_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/navi10_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/navi12_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/navi14_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/renoir_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/sienna_cichlid_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/soc15_hw_ip.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/soc15_ih_clientid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/v10_structs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/v9_structs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/vega10_enum.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/vega10_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/vega20_ip_offset.h` | C header | Expat | OK | .
`dev/pci/drm/amd/include/vi_structs.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/amdgpu_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/amdgpu_pm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/inc/amdgpu_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/amdgpu_pm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/amdgpu_smu.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/amd_powerplay.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/arcturus_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/cz_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/fiji_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/hardwaremanager.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/polaris10_pwrvirus.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/power_state.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/pp_debug.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/pp_endian.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/ppinterrupt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/pp_thermal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/rv_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu10_driver_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_11_0_cdr_table.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu11_driver_if_arcturus.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu11_driver_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu11_driver_if_navi10.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu11_driver_if_sienna_cichlid.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu12_driver_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu71_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu71.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu72_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu72.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu73_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu73.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu74_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu74.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu75_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu75.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu7_common.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu7_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu7_fusion.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu7.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu7_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu8_fusion.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu8.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu9_driver_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu9.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_types.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_ucode_xfer_cz.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_ucode_xfer_vi.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v11_0_7_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v11_0_7_pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v11_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v11_0_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v11_0_pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v12_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/smu_v12_0_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/tonga_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/vega10_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/vega12_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/vega12/smu9_driver_if.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/inc/vega20_ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/amdgpu_kv_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/amdgpu_kv_smc.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/amd_powerplay.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/cik_dpm.h` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ci_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ci_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/common_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/common_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/fiji_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/fiji_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/hardwaremanager.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/hwmgr_ppt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/polaris_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/polaris_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ppatomctrl.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ppatomctrl.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ppatomfwctrl.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ppatomfwctrl.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/ppevvmath.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pp_overdriver.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pp_overdriver.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pppcielanes.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pppcielanes.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pp_psm.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pp_psm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/pptable_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/processpptables.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/processpptables.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/process_pptables_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/process_pptables_v1_0.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu10_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu10_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu10_inc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_clockpowergating.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_clockpowergating.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_dyn_defaults.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_powertune.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_powertune.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_thermal.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu7_thermal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu8_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu8_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu9_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu9_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu_helper.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/smu_helper.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/tonga_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/tonga_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_inc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_powertune.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_powertune.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_processpptables.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_processpptables.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_thermal.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega10_thermal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_inc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_processpptables.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_processpptables.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_thermal.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega12_thermal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_baco.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_baco.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_hwmgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_hwmgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_inc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_powertune.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_powertune.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_pptable.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_processpptables.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_processpptables.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_thermal.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/hwmgr/vega20_thermal.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/kv_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/r600_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/si_dpm.c` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/si_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/sislands_smc.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/si_smc.c` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/ci_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/ci_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/fiji_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/fiji_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/iceland_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/iceland_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/polaris10_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/polaris10_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu10_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu10_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu7_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu7_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu8_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu8_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu9_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smu9_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/tonga_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/tonga_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega10_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega10_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega12_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega12_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega20_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vega20_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vegam_smumgr.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/powerplay/smumgr/vegam_smumgr.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/amdgpu_smu.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/arcturus_ppt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/arcturus_ppt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/navi10_ppt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/navi10_ppt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/sienna_cichlid_ppt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/sienna_cichlid_ppt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu11/smu_v11_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu12/renoir_ppt.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu12/renoir_ppt.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu12/smu_v12_0.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu_cmn.c` | C code | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu_cmn.h` | C header | Expat | OK | .
`dev/pci/drm/amd/pm/swsmu/smu_internal.h` | C header | Expat | OK | .
`dev/pci/drm/dma-resv.c` | C code | Expat | OK | .
`dev/pci/drm/drm_agpsupport.c` | C code | Expat | OK | .
`dev/pci/drm/drm_atomic.c` | C code | Expat | OK | .
`dev/pci/drm/drm_atomic_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_atomic_state_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_atomic_uapi.c` | C code | Expat | OK | .
`dev/pci/drm/drm_auth.c` | C code | Expat | OK | .
`dev/pci/drm/drm_blend.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_bridge.c` | C code | Expat | OK | .
`dev/pci/drm/drm_bufs.c` | C code | Expat | OK | .
`dev/pci/drm/drm_cache.c` | C code | Expat | OK | .
`dev/pci/drm/drm_client.c` | C code | Expat or GPL-2 | OK | .
`dev/pci/drm/drm_client_modeset.c` | C code | Expat | OK | .
`dev/pci/drm/drm_color_mgmt.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_connector.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_context.c` | C code | Expat | OK | .
`dev/pci/drm/drm_crtc.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_crtc_helper.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_crtc_helper_internal.h` | C header | Expat | OK | .
`dev/pci/drm/drm_crtc_internal.h` | C header | Expat | OK | .
`dev/pci/drm/drm_damage_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/drm_debugfs_crc.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dma.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dp_aux_dev.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dp_dual_mode_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dp_helper.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_dp_mst_topology.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_dp_mst_topology_internal.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/drm_drv.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dsc.c` | C code | Expat | OK | .
`dev/pci/drm/drm_dumb_buffers.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_edid.c` | C code | Expat | OK | .
`dev/pci/drm/drm_encoder.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_encoder_slave.c` | C code | Expat | OK | .
`dev/pci/drm/drm_fb_helper.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_file.c` | C code | Expat | OK | .
`dev/pci/drm/drm_flip_work.c` | C code | Expat | OK | .
`dev/pci/drm/drm_format_helper.c` | C code | GPL-2 or Expat | OK | .
`dev/pci/drm/drm_fourcc.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_framebuffer.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_gem.c` | C code | Expat | OK | .
`dev/pci/drm/drm_gem_cma_helper.c` | C code | Simplified-BSD | OK | .
`dev/pci/drm/drm_gem_framebuffer_helper.c` | C code | Public-Domain (notified in file) | OK | .
`dev/pci/drm/drm_hashtab.c` | C code | Expat | OK | .
`dev/pci/drm/drm_hdcp.c` | C code | Public-Domain (notified in file) | OK | .
`dev/pci/drm/drm_internal.h` | C header | Expat | OK | .
`dev/pci/drm/drm_ioc32.c` | C code | Expat | OK | .
`dev/pci/drm/drm_ioctl.c` | C code | Expat | OK | .
`dev/pci/drm/drm_irq.c` | C code | Expat + Expat | OK (Combined Licenses) | .
`dev/pci/drm/drm_kms_helper_common.c` | C code | Expat | OK | .
`dev/pci/drm/drm_legacy.h` | C header | Expat | OK | .
`dev/pci/drm/drm_legacy_misc.c` | C code | Expat | OK | .
`dev/pci/drm/drm_linux.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/drm_lock.c` | C code | Expat | OK | .
`dev/pci/drm/drm_managed.c` | C code | Public-Domain (notified in file) | OK | .
`dev/pci/drm/drm_memory.c` | C code | Expat | OK | .
`dev/pci/drm/drm_mipi_dsi.c` | C code | Expat | OK | .
`dev/pci/drm/drm_mm.c` | C code | Expat | OK | .
`dev/pci/drm/drm_mode_config.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_mode_object.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_modes.c` | C code | Expat | OK | .
`dev/pci/drm/drm_modeset_helper.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_modeset_lock.c` | C code | Expat | OK | .
`dev/pci/drm/drm_mtrr.c` | C code | Expat | OK | .
`dev/pci/drm/drm_panel.c` | C code | Expat | OK | .
`dev/pci/drm/drm_panel_orientation_quirks.c` | C code | Expat | OK | .
`dev/pci/drm/drm_pci.c` | C code | Expat | OK | .
`dev/pci/drm/drm_plane.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_plane_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_prime.c` | C code | Expat | OK | .
`dev/pci/drm/drm_print.c` | C code | Expat | OK | .
`dev/pci/drm/drm_probe_helper.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_property.c` | C code | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/drm_rect.c` | C code | Expat | OK | .
`dev/pci/drm/drm_scatter.c` | C code | Expat | OK | .
`dev/pci/drm/drm_scdc_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_self_refresh_helper.c` | C code | Expat | OK | .
`dev/pci/drm/drm_syncobj.c` | C code | Expat | OK | .
`dev/pci/drm/drm_trace.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/drm_trace_points.c` | C code | "Unlicensed!!!" | Trivial; 3 data lines; 6 total lines | .
`dev/pci/drm/drm_vblank.c` | C code | Expat | OK | .
`dev/pci/drm/drm_vblank_work.c` | C code | Expat | OK | .
`dev/pci/drm/drm_vma_manager.c` | C code | Expat | OK | .
`dev/pci/drm/drm_vm.c` | C code | Expat | OK | .
`dev/pci/drm/files.drm` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pci/drm/hdmi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_ch7017.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_ch7xxx.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_ivch.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_ns2501.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_sil164.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/dvo_tfp410.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/icl_dsi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_acpi.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_atomic.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_atomic.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_atomic_plane.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_atomic_plane.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_audio.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_audio.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_bios.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_bios.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_bw.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_bw.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_cdclk.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_cdclk.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_color.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_color.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_combo_phy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_combo_phy.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_connector.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_connector.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_crt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_crt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_csr.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_csr.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_ddi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_ddi.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_de.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_display.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_display_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_display_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_display.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_display_power.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_display_power.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_display_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_aux_backlight.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_aux_backlight.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_hdcp.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dpio_phy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dpio_phy.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_link_training.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_link_training.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dpll_mgr.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dpll_mgr.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_mst.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dp_mst.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsb.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsb.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsi_dcs_backlight.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsi_dcs_backlight.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsi.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_dsi_vbt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dvo.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_dvo_dev.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/i915/display/intel_dvo.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_fbc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_fbc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_fbdev.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_fbdev.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_fifo_underrun.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_fifo_underrun.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_frontbuffer.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_frontbuffer.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_global_state.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_global_state.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_gmbus.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_gmbus.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_hdcp.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_hdcp.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_hdmi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_hdmi.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_hotplug.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_hotplug.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_lpe_audio.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_lpe_audio.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_lspcon.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_lspcon.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_lvds.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_lvds.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_opregion.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_opregion.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_overlay.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_overlay.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_panel.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_panel.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_pipe_crc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_pipe_crc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_psr.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_psr.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_quirks.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_quirks.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_sdvo.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_sdvo.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_sdvo_regs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_sprite.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_sprite.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_tc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_tc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_tv.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_tv.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_vbt_defs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_vdsc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_vdsc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/intel_vga.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/intel_vga.h` | C header | Expat | OK | .
`dev/pci/drm/i915/display/vlv_dsi.c` | C code | Expat | OK | .
`dev/pci/drm/i915/display/vlv_dsi_pll.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_busy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_clflush.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_clflush.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_client_blt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_client_blt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_context.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_context.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_context_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_dmabuf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_domain.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_execbuffer.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_fence.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gemfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gemfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_internal.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_ioctls.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_lmem.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_lmem.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_mman.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_mman.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_object_blt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_object_blt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_object.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_object.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_object_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_pages.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_phys.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_region.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_region.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_shmem.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_shrinker.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_shrinker.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_stolen.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_stolen.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_throttle.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_tiling.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_userptr.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/i915_gem_wait.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/huge_gem_object.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/huge_gem_object.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/huge_pages.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_client_blt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_coherency.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_context.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_dmabuf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_execbuffer.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_mman.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_object_blt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_object.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/i915_gem_phys.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/igt_gem_utils.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/igt_gem_utils.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/mock_context.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/mock_context.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/mock_dmabuf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/mock_dmabuf.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gem/selftests/mock_gem_object.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/agp_intel_gtt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/i915/gt/debugfs_engines.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/debugfs_engines.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/debugfs_gt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/debugfs_gt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/debugfs_gt_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/debugfs_gt_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen2_engine_cs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen2_engine_cs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen6_engine_cs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen6_engine_cs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen6_ppgtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen6_ppgtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen6_renderstate.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen7_renderclear.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen7_renderclear.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen7_renderstate.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen8_ppgtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen8_ppgtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/gen8_renderstate.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/gen9_renderstate.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/hsw_clear_kernel.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_breadcrumbs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_breadcrumbs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_breadcrumbs_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context_param.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context_param.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context_sseu.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_context_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_cs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_heartbeat.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_heartbeat.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_user.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_engine_user.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ggtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ggtt_fencing.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ggtt_fencing.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gpu_commands.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_buffer_pool.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_buffer_pool.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_buffer_pool_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_clock_utils.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_clock_utils.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_irq.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_irq.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_pm_irq.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_pm_irq.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_requests.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_requests.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_gt_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_llc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_llc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_llc_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_lrc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_lrc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_lrc_reg.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_mocs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_mocs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ppgtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rc6.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rc6.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rc6_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_renderstate.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_renderstate.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_reset.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_reset.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_reset_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ring.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ring.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ring_submission.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_ring_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rps.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rps.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_rps_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_sseu.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_sseu_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_sseu_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_sseu.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_timeline.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_timeline.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_timeline_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_workarounds.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/intel_workarounds.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/intel_workarounds_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/ivb_clear_kernel.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/mock_engine.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/mock_engine.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_engine_heartbeat.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_engine_heartbeat.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_gt_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_hangcheck.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_llc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_llc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_lrc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_mocs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_rc6.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_rc6.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_reset.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_ring_submission.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_rps.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_rps.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftests/mock_timeline.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftests/mock_timeline.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_timeline.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/selftest_workarounds.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/shaders/clear_kernel/hsw.asm` | Assembler code | Expat | OK | .
`dev/pci/drm/i915/gt/shaders/clear_kernel/ivb.asm` | Assembler code | Expat | OK | .
**`dev/pci/drm/i915/gt/shaders/README`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 73 data lines + 2 commented lines; 111 total lines | .
`dev/pci/drm/i915/gt/shmem_utils.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/shmem_utils.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/st_shmem_utils.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/sysfs_engines.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/sysfs_engines.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_ads.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_ads.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_ct.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_ct.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_fw.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_fw.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_fwif.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_log.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_log_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_log_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_log.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_reg.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_submission.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_guc_submission.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc_fw.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc_fw.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_huc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc_fw_abi.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc_fw.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc_fw.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gt/uc/intel_uc.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/aperture_gm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/cfg_space.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/cmd_parser.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/cmd_parser.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/debug.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/display.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/display.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/dmabuf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/dmabuf.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/edid.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/edid.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/execlist.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/execlist.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/fb_decoder.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/fb_decoder.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/firmware.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/gtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/gtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/gvt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/gvt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/handlers.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/hypercall.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/interrupt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/interrupt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/kvmgt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/mmio.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/mmio_context.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/mmio_context.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/mmio.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/mpt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/opregion.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/page_track.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/page_track.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/reg.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/sched_policy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/sched_policy.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/scheduler.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/scheduler.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/trace.h` | C header | Expat | OK | .
`dev/pci/drm/i915/gvt/trace_points.c` | C code | Expat | OK | .
`dev/pci/drm/i915/gvt/vgpu.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_active.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_active.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_active_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_buddy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_buddy.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_cmd_parser.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_config.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_debugfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_debugfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_debugfs_params.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_debugfs_params.h` | C header | Expat | OK | .
**`dev/pci/drm/i915/i915_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 268 code lines + 1 commented lines; 270 total lines | .
`dev/pci/drm/i915/i915_drv.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_drv.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_fixed.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_gem.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_gem_evict.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_gem_gtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_gem_gtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_gem.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_getparam.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_globals.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_globals.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_gpu_error.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_gpu_error.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_ioc32.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_ioc32.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_irq.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_irq.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_memcpy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_memcpy.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_mitigations.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_mitigations.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_mm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_params.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_params.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_pci.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_perf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_perf.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_perf_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_pmu.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_pmu.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_priolist_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_pvinfo.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_query.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_query.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_reg.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_request.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_request.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_scatterlist.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_scatterlist.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_scheduler.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_scheduler.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_scheduler_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_selftest.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_suspend.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_suspend.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_sw_fence.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_sw_fence.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_sw_fence_work.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_sw_fence_work.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_switcheroo.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_switcheroo.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_syncmap.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_syncmap.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_sysfs.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_sysfs.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_trace.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/i915/i915_user_extensions.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_user_extensions.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_utils.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_utils.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_vgpu.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_vgpu.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_vma.c` | C code | Expat | OK | .
`dev/pci/drm/i915/i915_vma.h` | C header | Expat | OK | .
`dev/pci/drm/i915/i915_vma_types.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_device_info.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_device_info.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_dram.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_dram.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_gvt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_gvt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_memory_region.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_memory_region.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_pch.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_pch.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_region_lmem.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_region_lmem.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_runtime_pm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_runtime_pm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_sideband.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_sideband.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_stolen.c` | C code | Public-Domain (notified in file) | OK | .
`dev/pci/drm/i915/intel_uncore.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_uncore.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_wakeref.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_wakeref.h` | C header | Expat | OK | .
`dev/pci/drm/i915/intel_wopcm.c` | C code | Expat | OK | .
`dev/pci/drm/i915/intel_wopcm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_active.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_buddy.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_gem.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_gem_evict.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_gem_gtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_perf.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_random.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_random.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_request.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_selftest.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_sw_fence.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_syncmap.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/i915_vma.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_atomic.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_atomic.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_flush_test.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_flush_test.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_live_test.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_live_test.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_mmap.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_mmap.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_reset.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_reset.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_spinner.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/igt_spinner.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/intel_memory_region.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/intel_uncore.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/librapl.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/librapl.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/lib_sw_fence.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_drm.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_gem_device.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_gtt.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_gtt.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_region.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_region.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_request.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_request.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_uncore.c` | C code | Expat | OK | .
`dev/pci/drm/i915/selftests/mock_uncore.h` | C header | Expat | OK | .
`dev/pci/drm/i915/selftests/scatterlist.c` | C code | Expat | OK | .
`dev/pci/drm/i915/vlv_suspend.c` | C code | Expat | OK | .
`dev/pci/drm/i915/vlv_suspend.h` | C header | Expat | OK | .
`dev/pci/drm/include/acpi/acpi_bus.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/acpi/button.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/acpi/video.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/agp.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/asm/bitsperlong.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/byteorder.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/cacheflush.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/cpufeature.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/current.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/div64.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/fpu/api.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/hypervisor.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/intel-mid.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/asm/ioctl.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/asm/iosf_mbi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/pgtable.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/preempt.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/set_memory.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/asm/smp.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/thread_info.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/asm/unaligned.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/drm/amd_asic_type.h` | C code | Expat | OK | .
`dev/pci/drm/include/drm/drm_agpsupport.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_atomic.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_atomic_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_atomic_state_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_atomic_uapi.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_audio_component.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_auth.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_blend.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_bridge.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_cache.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_client.h` | C header | GPL-2 or Expat | OK | .
`dev/pci/drm/include/drm/drm_color_mgmt.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_connector.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_crtc.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_crtc_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_damage_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_debugfs_crc.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_debugfs.h` | C header | Expat | OK | .
**`dev/pci/drm/include/drm/drm_device.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 119 code lines + 217 commented lines; 411 total lines | .
`dev/pci/drm/include/drm/drm_displayid.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_dp_dual_mode_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_dp_helper.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_dp_mst_helper.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_drv.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_dsc.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_edid.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_encoder.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_encoder_slave.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_fb_helper.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_file.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_fixed.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_flip_work.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_format_helper.h` | C header | GPL-2 or Expat | OK | .
`dev/pci/drm/include/drm/drm_fourcc.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_framebuffer.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_gem_cma_helper.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_gem_framebuffer_helper.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_gem.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_hashtab.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_hdcp.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_ioctl.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_irq.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_lease.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_legacy.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_managed.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_mipi_dsi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_mm.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_mode_config.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_mode_object.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_modeset_helper.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_modeset_helper_vtables.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_modeset_lock.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_modes.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_panel.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_pciids.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_plane.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_plane_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_prime.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_print.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_probe_helper.h` | C header | GPL-2 or Expat | OK | .
`dev/pci/drm/include/drm/drm_property.h` | C header | Old-Expat (with legal disclaimer) | OK | .
`dev/pci/drm/include/drm/drm_rect.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_scdc_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_self_refresh_helper.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_syncobj.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_sysfs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/drm_util.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_utils.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_vblank.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_vblank_work.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_vma_manager.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/drm_writeback.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/gpu_scheduler.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/i2c/ch7006.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/i2c/sil164.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/i915_component.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/i915_drm.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/i915_mei_hdcp_interface.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/i915_pciids.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/intel-gtt.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/drm/intel_lpe_audio.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/spsc_queue.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/task_barrier.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_bo_api.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_bo_driver.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_execbuf_util.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_memory.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_module.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_page_alloc.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_placement.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_resource.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_set_memory.h` | C header | Expat | OK | .
`dev/pci/drm/include/drm/ttm/ttm_tt.h` | C header | Expat | OK | .
`dev/pci/drm/include/generated/autoconf.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/generated/utsrelease.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/acpi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/aer.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/agp_backend.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/anon_inodes.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/ascii85.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/async.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/atomic.h` | C header | Expat | OK | .
`dev/pci/drm/include/linux/average.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/backlight.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/bitfield.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/bitmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/bitops.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/bits.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/bottom_half.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/bug.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/build_bug.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/byteorder/generic.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/cache.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/capability.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/cdev.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/circ_buf.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/compat.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/compiler.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/completion.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/component.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/console.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/cpufreq.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/cpumask.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ctype.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/debugfs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/debugobjects.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/delay.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/device.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-buf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/dma-direction.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-fence-array.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-fence-chain.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-fence.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-mapping.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/dma-resv.h` | C header | Expat | OK | .
`dev/pci/drm/include/linux/dmi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/efi.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/err.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/errno.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/export.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/fault-inject.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/fb.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/fdtable.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/file.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/firmware.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/fs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ftrace.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/gcd.h` | C header | Simplified-BSD | OK | .
`dev/pci/drm/include/linux/gfp.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/gpio/consumer.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/hardirq.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/hash.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/hashtable.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/hdmi.h` | C header | Expat | OK | .
`dev/pci/drm/include/linux/highmem.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/hmm.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/hrtimer.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/hwmon.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/hwmon-sysfs.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/i2c-algo-bit.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/i2c.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/idr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/init.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/input.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/intel-iommu.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/interrupt.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/interval_tree_generic.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/interval_tree.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/io-64-nonatomic-lo-hi.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/io.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/io-mapping.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/iommu.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/iopoll.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ioport.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ipc.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/irqdomain.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/irqflags.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/irq.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/irqreturn.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/irq_work.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/jiffies.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kallsyms.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kconfig.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kernel.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kernfs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kfifo.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/kgdb.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kmemleak.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kobject.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/kref.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/kthread.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ktime.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/linkage.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/list.h` | C header | Expat | OK | .
`dev/pci/drm/include/linux/list_sort.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/llist.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/lockdep.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/log2.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/math64.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/media-bus-format.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mem_encrypt.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mempolicy.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mfd/core.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/miscdevice.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mman.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mm.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mm_types.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mmu_context.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mmu_notifier.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mmzone.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mod_devicetable.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/module.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/moduleparam.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/mount.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/mutex.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/nmi.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/nospec.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/notifier.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/of_device.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/of.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/oom.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/overflow.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pagemap.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pagevec.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pci.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/pci-p2pdma.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/perf_event.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pfn.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/pfn_t.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/pid.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/platform_device.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/pm.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pm_qos.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pm_runtime.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pnp.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/poison.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/poll.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/power_supply.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/preempt.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/prefetch.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/prime_numbers.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/printk.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/processor.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/pseudo_fs.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/pwm.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/radix-tree.h` | C header | Simplified-BSD | OK | .
`dev/pci/drm/include/linux/random.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/ratelimit.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/rbtree.h` | C header | Simplified-BSD | OK | .
`dev/pci/drm/include/linux/rculist.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/rcupdate.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/reboot.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/refcount.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/relay.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/rwlock_types.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/rwsem.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/scatterlist.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/sched/clock.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sched.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/sched/mm.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/sched/signal.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sched/task.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/semaphore.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sem.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/seq_file.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/seqlock.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/shmem_fs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/shrinker.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sizes.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/slab.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/smp.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sort.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/spinlock.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/spinlock_types.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/srcu.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/stackdepot.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/stacktrace.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/stat.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/stop_machine.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/string.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/stringify.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/suspend.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/swap.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/swiotlb.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/sync_file.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/syscalls.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/sysfs.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/sysrq.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/thread_info.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/time.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/timekeeping.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/timer.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/tracepoint.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/tty.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/typecheck.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/types.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/uaccess.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/uio.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/utsname.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/uuid.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/version.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/vgaarb.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/vga_switcheroo.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/vmalloc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/vt.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/linux/wait_bit.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/wait.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/workqueue.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/drm/include/linux/ww_mutex.h` | C header | Simplified-BSD | OK | .
`dev/pci/drm/include/linux/xarray.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/linux/zlib.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/media/cec-notifier.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/trace/define_trace.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/trace/events/dma_fence.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/uapi/drm/amdgpu_drm.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/drm/drm_fourcc.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/drm/drm.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/drm/drm_mode.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/drm/i915_drm.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/drm/radeon_drm.h` | C header | Expat | OK | .
`dev/pci/drm/include/uapi/linux/ioctl.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/uapi/linux/kfd_ioctl.h` | C header | Expat | OK | .
`dev/pci/drm/include/video/mipi_display.h` | C header | Public-Domain (notified in file) | OK | .
`dev/pci/drm/include/video/of_videomode.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/include/video/videomode.h` | Empty file | Non-Copyrightable | OK | .
`dev/pci/drm/linux_list_sort.c` | C code | Simplified-BSD | OK | .
`dev/pci/drm/linux_radix.c` | C code | Simplified-BSD | OK | .
`dev/pci/drm/radeon/atombios_crtc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/atombios_dp.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/atombios_encoders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/atombios.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/atombios_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/atom-bits.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/atom.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/atom.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/atom-names.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/atom-types.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/avivod.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/btcd.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/btc_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/btc_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cayman_blit_shaders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/cayman_blit_shaders.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/cayman_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 514 code lines + 0 commented line; 515 total lines | .
`dev/pci/drm/radeon/ci_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/ci_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cik_blit_shaders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/cik_blit_shaders.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cik.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/cikd.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cik_reg.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cik_sdma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/ci_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/clearstate_cayman.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/clearstate_ci.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/clearstate_defs.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/clearstate_evergreen.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/clearstate_si.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/cypress_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/cypress_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/dce3_1_afmt.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/dce6_afmt.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/devlist.awk` | AWK script | "Unlicensed!!!" | Trivial; 11 code lines + 0 commented line; 12 total lines | .
`dev/pci/drm/radeon/evergreen_blit_shaders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/evergreen_blit_shaders.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/evergreen.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/evergreen_cs.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/evergreend.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/evergreen_dma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/evergreen_hdmi.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/evergreen_reg.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/evergreen_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 514 code lines + 0 commented line; 515 total lines | .
`dev/pci/drm/radeon/evergreen_smc.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/kv_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/kv_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/kv_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented lines; 8 total lines | .
`dev/pci/drm/radeon/mkregtable.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/ni.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/nid.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/ni_dma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/ni_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/ni_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/ni_reg.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/nislands_smc.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/ObjectID.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/ppsmc.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/pptable.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r100.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r100d.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/r100_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
`dev/pci/drm/radeon/r100_track.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r200.c` | C code | Expat | OK | .
**`dev/pci/drm/radeon/r200_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
`dev/pci/drm/radeon/r300.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r300d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r300_reg.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/r300_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 0 commented line; 43 total lines | .
`dev/pci/drm/radeon/r420.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r420d.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/r420_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 0 commented line; 43 total lines | .
`dev/pci/drm/radeon/r500_reg.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r520.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r520d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r600_blit_shaders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600_blit_shaders.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r600.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600_cs.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r600_dma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/r600_hdmi.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/r600_reg.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/r600_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 490 code lines + 0 commented line; 491 total lines | .
`dev/pci/drm/radeon/radeon_acpi.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_acpi.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_agp.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_asic.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_asic.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_atombios.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_audio.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_audio.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_benchmark.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_bios.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_clocks.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_combios.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_connectors.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_cs.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_cursor.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_device.c` | C code | Expat | OK | .
**`dev/pci/drm/radeon/radeon_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 701 code lines + 1 commented line; 703 total lines | .
`dev/pci/drm/radeon/radeon_display.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_dp_auxch.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_dp_mst.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_drv.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_drv.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_encoders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_family.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_fb.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_fence.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_gart.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_gem.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_i2c.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_ib.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_irq_kms.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_kms.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_legacy_crtc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_legacy_encoders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_legacy_tv.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_mn.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_mode.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_object.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_object.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_pm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_prime.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_reg.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_ring.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_sa.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_semaphore.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_sync.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_test.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_trace.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_trace_points.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_ttm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_ucode.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_ucode.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/radeon_uvd.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_vce.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/radeon_vm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/reg_srcs/cayman` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/evergreen` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 6 code lines + 0 commented line; 10 total lines | .
`dev/pci/drm/radeon/reg_srcs/r100` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/r200` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/r300` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/r420` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/r600` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/rn50` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/rs600` | regtable data | Non-Copyrightable | OK | .
`dev/pci/drm/radeon/reg_srcs/rv515` | regtable data | Non-Copyrightable | OK | .
**`dev/pci/drm/radeon/rn50_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
`dev/pci/drm/radeon/rs100d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rs400.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rs400d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rs600.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rs600d.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/rs600_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 0 commented line; 58 total lines | .
`dev/pci/drm/radeon/rs690.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rs690d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rs780d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rs780_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rs780_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv200d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv250d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv350d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv515.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv515d.h` | C header | Expat | OK | .
**`dev/pci/drm/radeon/rv515_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 0 commented line; 58 total lines | .
`dev/pci/drm/radeon/rv6xxd.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv6xx_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv6xx_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv730d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv730_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv740d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv740_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv770.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv770d.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv770_dma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv770_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv770_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/rv770_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/rv770_smc.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/si_blit_shaders.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/si_blit_shaders.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/si.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/sid.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/si_dma.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/si_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/si_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/si_reg.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/sislands_smc.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/si_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/smu7_discrete.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/smu7_fusion.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/smu7.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/sumod.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/sumo_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/sumo_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/sumo_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/trinityd.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/trinity_dpm.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/trinity_dpm.h` | C header | Expat | OK | .
`dev/pci/drm/radeon/trinity_smc.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/uvd_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/uvd_v2_2.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/uvd_v3_1.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/uvd_v4_2.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/vce_v1_0.c` | C code | Expat | OK | .
`dev/pci/drm/radeon/vce_v2_0.c` | C code | Expat | OK | .
`dev/pci/drm/scheduler/gpu_scheduler_trace.h` | C header | Expat | OK | .
`dev/pci/drm/scheduler/sched_entity.c` | C code | Expat | OK | .
`dev/pci/drm/scheduler/sched_fence.c` | C code | Expat | OK | .
`dev/pci/drm/scheduler/sched_main.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_agp_backend.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_bo.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_bo_util.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_bo_vm.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_execbuf_util.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_memory.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_module.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_page_alloc.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_page_alloc_dma.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_range_manager.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_resource.c` | C code | Expat | OK | .
`dev/pci/drm/ttm/ttm_tt.c` | C code | Expat | OK | .
`dev/pci/dwiic_pci.c` | C code | Original-ISC | OK | .
`dev/pci/eap.c` | C code | Simplified-BSD | OK | .
`dev/pci/eapreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/ehci_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/emuxki.c` | C code | Simplified-BSD | OK | .
`dev/pci/emuxkireg.h` | C header | Simplified-BSD | OK | .
`dev/pci/emuxkivar.h` | C header | Simplified-BSD | OK | .
`dev/pci/envy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/envyreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/envyvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/esa.c` | C code | Simplified-BSD | OK | .
`dev/pci/esareg.h` | C header | Simplified-BSD | OK | .
`dev/pci/esavar.h` | C header | Simplified-BSD | OK | .
`dev/pci/eso.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/esoreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/esovar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/files.agp` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pci/files.pci` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pci/fms.c` | C code | Simplified-BSD | OK | .
`dev/pci/fmsradio.c` | C code | Simplified-BSD | OK | .
`dev/pci/fmsreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/fmsvar.h` | C header | Simplified-BSD | OK | .
`dev/pci/gcu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/gcu_reg.h` | C header | Modified-BSD | OK | .
`dev/pci/gcu_var.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/gdt_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/glxpcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/glxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/glxvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/hifn7751.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/hifn7751reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/hifn7751var.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/i82365_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/i82365_pcivar.h` | C header | Simplified-BSD | OK | .
`dev/pci/ichiic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ichreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ichwdt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_acx_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_age.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_agereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_alc.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_alcreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_ale.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_alereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_an_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_aq_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_athn_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ath_pci.c` | C code | Modified-BSD | OK | .
`dev/pci/if_atw_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_bce.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/if_bcereg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/if_bge.c` | C code | Original-BSD | OK | .
`dev/pci/if_bgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_bnx.c` | C code | Modified-BSD | OK | .
`dev/pci/if_bnxreg.h` | C header | Modified-BSD | OK | .
`dev/pci/if_bnxt.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_bnxtreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_bwfm_pci.c` | C code | Original-ISC | OK | .
`dev/pci/if_bwfm_pci.h` | C header | Original-ISC | OK | .
`dev/pci/if_bwi_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_cas.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_casreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_casvar.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_dc_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_de.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/if_devar.h` | C header | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/if_em.c` | C code | Modified-BSD | OK | .
`dev/pci/if_em.h` | C header | Modified-BSD | OK | .
`dev/pci/if_em_hw.c` | C code | Modified-BSD | OK | .
`dev/pci/if_em_hw.h` | C header | Modified-BSD | OK | .
`dev/pci/if_em_osdep.h` | C header | Modified-BSD | OK | .
`dev/pci/if_em_soc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_em_soc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_epic_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_ep_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_et.c` | C code | Modified-BSD | OK | .
`dev/pci/if_etreg.h` | C header | Modified-BSD | OK | .
`dev/pci/if_fxp_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_gem_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_hme_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_iavf.c` | C code | Modified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_ipw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ipwreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ipwvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwm.c` | C code | Original-ISC ("and" is used instead of "and/or") + (GPL-2 or Modified-BSD) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_iwmreg.h` | C header | GPL-2 or Modified-BSD | OK | .
`dev/pci/if_iwmvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + (GPL-2 or Modified-BSD) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_iwn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwnreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwnvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_iwx.c` | C code | Original-ISC ("and" is used instead of "and/or") + (GPL-2 or Modified-BSD) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_iwxreg.h` | C header | GPL-2 or Modified-BSD | OK | .
`dev/pci/if_iwxvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + (GPL-2 or Modified-BSD) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_ix.c` | C code | Modified-BSD | OK | .
`dev/pci/if_ixgb.c` | C code | Modified-BSD | OK | .
`dev/pci/if_ixgb.h` | C header | Modified-BSD | OK | .
`dev/pci/if_ixgb_osdep.h` | C header | Modified-BSD | OK | .
`dev/pci/if_ix.h` | C header | Modified-BSD | OK | .
`dev/pci/if_ixl.c` | C code | Modified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_ixlreg.h` | C header | Modified-BSD | OK | .
`dev/pci/if_jme.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_jmereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_jmevar.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_lge.c` | C code | Original-BSD | OK | .
`dev/pci/if_lgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_lii.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_liireg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_malo_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_mcx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_msk.c` | C code | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_mskvar.h` | C header | Simplified-BSD + Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_mtd_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_myx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_myxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_nep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ne_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_nfe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_nfereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_nfevar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_nge.c` | C code | Original-BSD | OK | .
`dev/pci/if_ngereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_nxe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_oce.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/pci/if_ocereg.h` | C header | Modified-BSD | OK | .
`dev/pci/if_pcn.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_pgt_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ral_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_re_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_rge.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_rgereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_rl_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_rtwn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_rtw_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_se.c` | C code | Original-BSD | OK | .
`dev/pci/if_sereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_sf_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_sis.c` | C code | Original-BSD | OK | .
`dev/pci/if_sisreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_sk.c` | C code | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_skreg.h` | C header | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_skvar.h` | C header | Simplified-BSD + Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_ste.c` | C code | Original-BSD | OK | .
`dev/pci/if_stereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_stge.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_stgereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_tht.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_ti_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_tl.c` | C code | Original-BSD | OK | .
`dev/pci/if_tlreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_txp.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_txpreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_vge.c` | C code | Original-BSD | OK | .
`dev/pci/if_vgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_vgevar.h` | C header | Original-BSD | OK | .
`dev/pci/if_vic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_vmx.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_vmxreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_vr.c` | C code | Original-BSD | OK | .
`dev/pci/if_vrreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_vte.c` | C code | Simplified-BSD | OK | .
`dev/pci/if_vtereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/if_wb.c` | C code | Original-BSD | OK | .
`dev/pci/if_wbreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_wi_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_wpi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_wpireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_wpivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/if_xge.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_xgereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_xl_pci.c` | C code | Original-BSD | OK | .
`dev/pci/iha_pci.c` | C code | Custom-Simplified-BSD (second clause is a "non-endorsement clause") | OK | .
`dev/pci/ips.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/itherm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ixgbe_82598.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgbe_82599.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgbe.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgb_ee.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgb_ee.h` | C header | Modified-BSD | OK | .
`dev/pci/ixgbe.h` | C header | Modified-BSD | OK | .
`dev/pci/ixgbe_phy.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgbe_type.h` | C header | Modified-BSD | OK | .
`dev/pci/ixgbe_x540.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgbe_x550.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgb_hw.c` | C code | Modified-BSD | OK | .
`dev/pci/ixgb_hw.h` | C header | Modified-BSD | OK | .
`dev/pci/ixgb_ids.h` | C header | Modified-BSD | OK | .
`dev/pci/jmb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/kate.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/km.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ksmn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/maestro.c` | C code | Simplified-BSD | OK | .
`dev/pci/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`dev/pci/mbg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/mfii.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/mfi_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/mmuagp.c` | C code | Simplified-BSD | OK | .
`dev/pci/mpii.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/mpiireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/mpi_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/neo.c` | C code | Simplified-BSD | OK | .
`dev/pci/neoreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/nviic.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/nvme_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/ohci_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/pccbb.c` | C code | Simplified-BSD | OK | .
`dev/pci/pccbbreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pccbbvar.h` | C header | Simplified-BSD | OK | .
`dev/pci/pchtemp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs_data.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_acard_reg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_acer_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_amd_reg.h` | C header | Original-BSD | OK | .
`dev/pci/pciide_apollo_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_cmd_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_cy693_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_hpt_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_ite_reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/pciide_ixp_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_jmicron_reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/pciide_natsemi_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_nforce_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_opti_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_pdc202xx_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_piix_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_rdc_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciidereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_sii3112_reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_sis_reg.h` | C header | Simplified-BSD | OK | .
`dev/pci/pciide_svwsata_reg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/pciidevar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pci_map.c` | C code | Simplified-BSD | OK | .
`dev/pci/pci_quirks.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcireg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pci_subr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcivar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcscp.c` | C code | Simplified-BSD | OK | .
`dev/pci/pcscpreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/piixpm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/piixreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/plx9060reg.h` | C header | Original-BSD | OK | .
`dev/pci/plx9060var.h` | C header | Original-BSD | OK | .
`dev/pci/ppb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/ppbreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/puc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pucdata.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pucvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pwdog.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/qla_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/qle.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/qlereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/qlw_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/rtsx_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/safe.c` | C code | Simplified-BSD | OK | .
`dev/pci/safereg.h` | C header | Simplified-BSD | OK | .
`dev/pci/safevar.h` | C header | Simplified-BSD | OK | .
`dev/pci/sdhc_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/sili_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/siop_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/siop_pci_common.c` | C code | Simplified-BSD | OK | .
`dev/pci/siop_pci_common.h` | C header | Simplified-BSD | OK | .
`dev/pci/sti_pci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/sv.c` | C code | Modified-BSD | OK | .
`dev/pci/tcpcib.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/tga.c` | C code | CMU (similar to ISC) | OK | .
`dev/pci/tga_conf.c` | C code | CMU (similar to ISC) | OK | .
`dev/pci/tgareg.h` | C header | CMU (similar to ISC) | OK | .
`dev/pci/tgavar.h` | C header | CMU (similar to ISC) | OK | .
`dev/pci/trm_pci.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/pci/twe_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/ubsec.c` | C code | Simplified-BSD | OK | .
`dev/pci/ubsecreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/ubsecvar.h` | C header | Simplified-BSD | OK | .
`dev/pci/uhci_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/vga_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`dev/pci/vga_pci_common.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/vga_pcivar.h` | C header | CMU (similar to ISC) | OK | .
`dev/pci/viapm.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`dev/pci/virtio_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/virtio_pcireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/vmwpvs.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/wdt.c` | C code | Simplified-BSD | OK | .
`dev/pci/xhci_pci.c` | C code | Simplified-BSD | OK | .
`dev/pci/xspd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pci/yds.c` | C code | Simplified-BSD | OK | .
`dev/pci/ydsreg.h` | C header | Simplified-BSD | OK | .
`dev/pci/ydsvar.h` | C header | Simplified-BSD | OK | .
`dev/pckbc/files.pckbc` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pckbc/pckbd.c` | C code | Simplified-BSD + Modified-BSD | OK | .
**`dev/pckbc/pckbdreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 7 commented lines; 29 total lines | .
`dev/pckbc/pckbdvar.h` | C header | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 7 total lines | .
`dev/pckbc/pms.c` | C code | Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK | .
**`dev/pckbc/pmsreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 153 code lines + 30 commented lines; 222 total lines | .
`dev/pckbc/wskbdmap_mfii.c` | C code | Simplified-BSD | OK | .
`dev/pckbc/wskbdmap_mfii.h` | C header | Simplified-BSD | OK | .
`dev/pcmcia/aic_pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/cfxga.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pcmcia/cfxgareg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pcmcia/com_pcmcia.c` | C code | Modified-BSD + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/pcmcia/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/files.pcmcia` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pcmcia/if_an_pcmcia.c` | C code | Simplified-BSD | OK | .
`dev/pcmcia/if_ep_pcmcia.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/pcmcia/if_malo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pcmcia/if_maloreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pcmcia/if_malovar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pcmcia/if_ne_pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/if_sm_pcmcia.c` | C code | Simplified-BSD | OK | .
`dev/pcmcia/if_wi_pcmcia.c` | C code | Original-BSD | OK | .
`dev/pcmcia/if_xe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/if_xereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`dev/pcmcia/pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciachip.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmcia_cis.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmcia_cis_quirks.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciadevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciadevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/wdc_pcmcia.c` | C code | Simplified-BSD | OK | .
`dev/puc/com_puc.c` | C code | Modified-BSD | OK | .
`dev/puc/lpt_puc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pv/files.pv` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/pv/hvs.c` | C code | Simplified-BSD | OK | .
`dev/pv/hyperv.c` | C code | Simplified-BSD | OK | .
`dev/pv/hypervic.c` | C code | Simplified-BSD | OK | .
`dev/pv/hypervicreg.h` | C header | Simplified-BSD | OK | .
`dev/pv/hypervreg.h` | C header | Simplified-BSD | OK | .
`dev/pv/hypervvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/if_hvn.c` | C code | Simplified-BSD | OK | .
`dev/pv/if_hvnreg.h` | C header | Simplified-BSD | OK | .
`dev/pv/if_vio.c` | C code | Simplified-BSD | OK | .
`dev/pv/if_xnf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/ndis.h` | C header | Simplified-BSD | OK | .
`dev/pv/pvbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/pvclock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/pvreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/pvvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/vioblk.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pv/vioblkreg.h` | C header | Simplified-BSD | OK | .
`dev/pv/viocon.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/viomb.c` | C code | Simplified-BSD | OK | .
`dev/pv/viornd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/vioscsi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/vioscsireg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/virtio.c` | C code | Simplified-BSD | OK | .
`dev/pv/virtioreg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/pv/virtiovar.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/pv/vmmci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/vmt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/xbf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/xen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/xenreg.h` | C header | Expat | OK | .
`dev/pv/xenstore.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/pv/xenvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/radio.c` | C code | Simplified-BSD | OK | .
`dev/radio_if.h` | C header | Simplified-BSD | OK | .
`dev/radiovar.h` | C header | Simplified-BSD | OK | .
`dev/rasops/files.rasops` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/rasops/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 3 code lines + 2 commented lines; 9 total lines | .
`dev/rasops/rasops15.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops1.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops24.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops32.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops4.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops8.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops_bitops.h` | C header | Simplified-BSD | OK | .
`dev/rasops/rasops.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops.h` | C header | Simplified-BSD | OK | .
`dev/rasops/rasops_masks.c` | C code | Simplified-BSD | OK | .
`dev/rasops/rasops_masks.h` | C header | Simplified-BSD | OK | .
`dev/rd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/rnd.c` | C code | Modified-BSD | OK | .
`dev/rndis.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sbus/agten.c` | C code | Simplified-BSD | OK | .
`dev/sbus/apio.c` | C code | Simplified-BSD | OK | .
`dev/sbus/asio.c` | C code | Simplified-BSD | OK | .
`dev/sbus/asioreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/be.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/bereg.h` | C header | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/bpp.c` | C code | Simplified-BSD | OK | .
`dev/sbus/bppreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/bwtwo.c` | C code | Simplified-BSD | OK | .
`dev/sbus/cgsix.c` | C code | Simplified-BSD | OK | .
`dev/sbus/cgsixreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/cgthree.c` | C code | Simplified-BSD | OK | .
`dev/sbus/cgtwelve.c` | C code | Simplified-BSD | OK | .
`dev/sbus/cgtwelvereg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/cs4231.c` | C code | Simplified-BSD | OK | .
`dev/sbus/cs4231var.h` | C header | Simplified-BSD | OK | .
`dev/sbus/dma_sbus.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/esp_sbus.c` | C code | Simplified-BSD | OK | .
`dev/sbus/files.sbus` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/sbus/if_gem_sbus.c` | C code | Simplified-BSD | OK | .
`dev/sbus/if_hme_sbus.c` | C code | Simplified-BSD | OK | .
`dev/sbus/if_le.c` | C code | Simplified-BSD | OK | .
`dev/sbus/if_le_lebuffer.c` | C code | Simplified-BSD | OK | .
`dev/sbus/if_le_ledma.c` | C code | Simplified-BSD | OK | .
`dev/sbus/if_ti_sbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sbus/lebuffer.c` | C code | Simplified-BSD | OK | .
`dev/sbus/lebuffervar.h` | C header | Simplified-BSD | OK | .
`dev/sbus/magma.c` | C code | Simplified-BSD | OK | .
`dev/sbus/magmareg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/mgx.c` | C code | Simplified-BSD | OK | .
`dev/sbus/qec.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/qe.c` | C code | Simplified-BSD | OK | .
`dev/sbus/qecreg.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/sbus/qecvar.h` | C header | Simplified-BSD | OK | .
`dev/sbus/qereg.h` | C header | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/qla_sbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sbus/qlw_sbus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sbus/rfx.c` | C code | Simplified-BSD | OK | .
`dev/sbus/sbusvar.h` | C header | Simplified-BSD | OK | .
`dev/sbus/spif.c` | C code | Simplified-BSD | OK | .
`dev/sbus/spifreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/spifvar.h` | C header | Simplified-BSD | OK | .
`dev/sbus/stp4020.c` | C code | Simplified-BSD | OK | .
`dev/sbus/stp4020reg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/stp4020var.h` | C header | Simplified-BSD | OK | .
`dev/sbus/tvtwo.c` | C code | Simplified-BSD | OK | .
`dev/sbus/uperf_sbus.c` | C code | Simplified-BSD | OK | .
`dev/sbus/uperf_sbusreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/vigra.c` | C code | Simplified-BSD | OK | .
`dev/sbus/xbox.c` | C code | Simplified-BSD | OK | .
`dev/sbus/xboxreg.h` | C header | Simplified-BSD | OK | .
`dev/sbus/xboxvar.h` | C header | Simplified-BSD | OK | .
`dev/sbus/zx.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/sbus/zxreg.h` | C header | Simplified-BSD + Expat | OK (Combined Licenses) | .
`dev/sdmmc/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/sdmmc/files.sdmmc` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/sdmmc/if_bwfm_sdio.c` | C code | Original-ISC | OK | .
`dev/sdmmc/if_bwfm_sdio.h` | C header | Original-ISC | OK | .
`dev/sdmmc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 1 commented line; 8 total lines | .
`dev/sdmmc/sdhc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdhcreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdhcvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmcchip.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_cis.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmcdevs` | AWK script | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmcdevs.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_io.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_ioreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_mem.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmcreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_scsi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmc_scsi.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sdmmc/sdmmcvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_concat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_crypto.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_raid0.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_raid1c.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_raid1.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_raid5.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraid_raid6.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/softraidvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/spdmem.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/spdmemvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/spi/spivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/sun/disklabel.h` | C header | Modified-BSD | OK | .
`dev/sun/files.sun` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/sun/sunkbd.c` | C code | Simplified-BSD | OK | .
`dev/sun/sunkbdmap.c` | C code | Simplified-BSD | OK | .
`dev/sun/sunkbdreg.h` | C header | Simplified-BSD | OK | .
`dev/sun/sunkbdvar.h` | C header | Simplified-BSD | OK | .
`dev/sun/sunms.c` | C code | Simplified-BSD | OK | .
`dev/sun/sunmsvar.h` | C header | Simplified-BSD | OK | .
`dev/sun/uperfio.h` | C header | Simplified-BSD | OK | .
`dev/sun/z8530ms.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/tc/asc.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/tc/asc_tc.c` | C code | Simplified-BSD | OK | .
`dev/tc/asc_tcds.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
**`dev/tc/ascvar.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 25 total lines | .
`dev/tc/bba.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/tc/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/tc/files.tc` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/tc/if_le_ioasic.c` | C code | CMU (similar to ISC) | OK | .
`dev/tc/if_le_tc.c` | C code | CMU (similar to ISC) | OK | .
`dev/tc/if_levar.h` | C header | Modified-BSD | OK | .
`dev/tc/ioasicreg.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/ioasic_subr.c` | C code | CMU (similar to ISC) + Modified-BSD | OK (Combined Licenses) | .
`dev/tc/ioasicvar.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 4 code lines + 2 commented lines; 9 total lines | .
`dev/tc/tc.c` | C code | CMU (similar to ISC) | OK | .
`dev/tc/tcdevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/tc/tcdevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 179 code lines + 7 commented lines; 189 total lines | .
`dev/tc/tcds.c` | C code | Simplified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`dev/tc/tcdsreg.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/tcdsvar.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/tcreg.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/tcvar.h` | C header | CMU (similar to ISC) | OK | .
`dev/tc/zs_ioasic.c` | C code | Simplified-BSD | OK | .
`dev/tc/zs_ioasicvar.h` | C header | Simplified-BSD | OK | .
`dev/usb/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/usb/dwc2/dwc2.c` | C code | Simplified-BSD | OK | .
`dev/usb/dwc2/dwc2_core.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_core.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_coreintr.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_hcd.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_hcdddma.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_hcd.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_hcdintr.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2_hcdqueue.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2.h` | C header | Simplified-BSD | OK | .
`dev/usb/dwc2/dwc2_hw.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") or GPL-2 | OK | .
`dev/usb/dwc2/dwc2var.h` | C header | Simplified-BSD | OK | .
`dev/usb/dwc2/files.dwc2` | BSD kernel config. | Non-Copyrightable | OK. | .
`dev/usb/dwc2/list.h` | C header | Expat | OK | .
`dev/usb/ehci.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/usb/ehcireg.h` | C header | Simplified-BSD | OK | .
`dev/usb/ehcivar.h` | C header | Simplified-BSD | OK | .
`dev/usb/fido.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/files.usb` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/usb/if_athn_usb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_athn_usb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_atu.c` | C code | Original-BSD | OK | .
`dev/usb/if_atureg.h` | C header | Original-BSD | OK | .
`dev/usb/if_aue.c` | C code | Original-BSD | OK | .
`dev/usb/if_auereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_axe.c` | C code | Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
`dev/usb/if_axen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
**`dev/usb/if_axenreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 257 non empty lines; 298 total lines | .
`dev/usb/if_axereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_bwfm_usb.c` | C code | Original-ISC | OK | .
`dev/usb/if_cdce.c` | C code | Original-BSD | OK | .
`dev/usb/if_cdcereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_cue.c` | C code | Original-BSD | OK | .
`dev/usb/if_cuereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_kue.c` | C code | Original-BSD | OK | .
`dev/usb/if_kuereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_kuevar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_mos.c` | C code | Original-ISC ("and" is used instead of "and/or") + Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
`dev/usb/if_mosreg.h` | C header | Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
`dev/usb/if_mue.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_muereg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_otus.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_otusreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_ral.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_ralreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_ralvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_rsu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_rsureg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_rum.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_rumreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_rumvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_run.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_runvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_smsc.c` | C code | Simplified-BSD | OK | .
`dev/usb/if_smscreg.h` | C header | Simplified-BSD | OK | .
`dev/usb/if_uaq.c` | C code | Simplified-BSD | OK | .
`dev/usb/if_uath.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_uathreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_uathvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_udav.c` | C code | Modified-BSD | OK | .
`dev/usb/if_udavreg.h` | C header | Modified-BSD | OK | .
`dev/usb/if_ugl.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/usb/if_umb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_umb.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_upgt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_upgtvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_upl.c` | C code | Simplified-BSD | OK | .
`dev/usb/if_ure.c` | C code | Simplified-BSD | OK | .
`dev/usb/if_urereg.h` | C header | Simplified-BSD | OK | .
`dev/usb/if_url.c` | C code | Modified-BSD | OK | .
`dev/usb/if_urlreg.h` | C header | Modified-BSD | OK | .
`dev/usb/if_urndis.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_urndisreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_urtw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_urtwn.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_urtwreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_wi_usb.c` | C code | Simplified-BSD | OK | .
`dev/usb/if_wi_usb.h` | C header | Simplified-BSD | OK | .
`dev/usb/if_zyd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/if_zydreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/Makefile` | Makefile | "Unlicensed!!!" | Trival; 11 code lines + 2 commented lines; 18 total lines | .
`dev/usb/makemap.awk` | AWK script | Simplified-BSD | OK | .
`dev/usb/mbim.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/moscom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ohci.c` | C code | Simplified-BSD | OK | .
`dev/usb/ohcireg.h` | C header | Simplified-BSD | OK | .
`dev/usb/ohcivar.h` | C header | Simplified-BSD | OK | .
`dev/usb/uark.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uaudio.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ubcmtp.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/usb/uberry.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ubsa.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/usb/ucc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uchcom.c` | C code | Simplified-BSD | OK | .
`dev/usb/ucom.c` | C code | Simplified-BSD | OK | .
`dev/usb/ucomvar.h` | C header | Simplified-BSD | OK | .
`dev/usb/ucrcom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ucycom.c` | C code | Simplified-BSD | OK | .
`dev/usb/udcf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/udl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/udl.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/udlio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/udsbr.c` | C code | Simplified-BSD | OK | .
`dev/usb/uftdi.c` | C code | Simplified-BSD | OK | .
**`dev/usb/uftdireg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 66 code lines + 250 commented lines; 341 total lines | .
`dev/usb/ugen.c` | C code | Simplified-BSD | OK | .
`dev/usb/ugold.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uhci.c` | C code | Simplified-BSD | OK | .
`dev/usb/uhcireg.h` | C header | Simplified-BSD | OK | .
`dev/usb/uhcivar.h` | C header | Simplified-BSD | OK | .
`dev/usb/uhid.c` | C code | Simplified-BSD | OK | .
`dev/usb/uhidev.c` | C code | Simplified-BSD | OK | .
`dev/usb/uhidev.h` | C header | Simplified-BSD | OK | .
`dev/usb/uhid.h` | C header | Simplified-BSD | OK | .
`dev/usb/uhidpp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uhid_rdesc.h` | C header | Simplified-BSD | OK | .
`dev/usb/uhub.c` | C code | Simplified-BSD | OK | .
`dev/usb/uipaq.c` | C code | Simplified-BSD | OK | .
`dev/usb/ujoy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ukbd.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/usb/ukbdmap.c` | C code | Simplified-BSD | OK | .
`dev/usb/ukbdvar.h` | C header | Simplified-BSD | OK | .
`dev/usb/ukspan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/ulpt.c` | C code | Simplified-BSD | OK | .
`dev/usb/umass.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/usb/umass_quirks.c` | C code | Simplified-BSD | OK | .
`dev/usb/umass_quirks.h` | C header | Simplified-BSD | OK | .
`dev/usb/umass_scsi.c` | C code | Simplified-BSD | OK | .
`dev/usb/umass_scsi.h` | C header | Simplified-BSD | OK | .
`dev/usb/umassvar.h` | C header | Simplified-BSD | OK | .
`dev/usb/umbg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/umcs.c` | C code | Simplified-BSD | OK | .
`dev/usb/umcs.h` | C header | Simplified-BSD | OK | .
`dev/usb/umct.c` | C code | Simplified-BSD | OK | .
`dev/usb/umct.h` | C header | Simplified-BSD | OK | .
`dev/usb/umidi.c` | C code | Simplified-BSD | OK | .
`dev/usb/umidi_quirks.c` | C code | Simplified-BSD | OK | .
`dev/usb/umidi_quirks.h` | C header | Simplified-BSD | OK | .
`dev/usb/umidireg.h` | C header | Simplified-BSD | OK | .
`dev/usb/umidivar.h` | C header | Simplified-BSD | OK | .
`dev/usb/umodem.c` | C code | Simplified-BSD | OK | .
`dev/usb/ums.c` | C code | Simplified-BSD | OK | .
`dev/usb/umsm.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/umstc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/umt.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uoak.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uoaklux.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uoakrh.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uoak_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uoakv.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uonerng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uow.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uowreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/upd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uplcom.c` | C code | Simplified-BSD | OK | .
`dev/usb/urng.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/usb.c` | C code | Simplified-BSD | OK | .
`dev/usb/usbcdc.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbdevs` | AWK script | Simplified-BSD | OK | .
`dev/usb/usbdevs_data.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbdevs.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbdi.c` | C code | Simplified-BSD | OK | .
`dev/usb/usbdi.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbdi_util.c` | C code | Simplified-BSD | OK | .
`dev/usb/usbdi_util.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbdivar.h` | C header | Simplified-BSD | OK | .
`dev/usb/usb.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbhid.h` | C header | Simplified-BSD | OK | .
`dev/usb/usb_mem.c` | C code | Simplified-BSD | OK | .
`dev/usb/usb_mem.h` | C header | Simplified-BSD | OK | .
`dev/usb/usbpcap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/usb_quirks.c` | C code | Simplified-BSD | OK | .
`dev/usb/usb_quirks.h` | C header | Simplified-BSD | OK | .
`dev/usb/usb_subr.c` | C code | Simplified-BSD | OK | .
`dev/usb/uscom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uslcom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uslhcom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uslhcomreg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/usps.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uthum.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uticom.c` | C code | Simplified-BSD | OK | .
`dev/usb/utpms.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/usb/utrh.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uts.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/utvfu.c` | C code | Simplified-BSD or GPL | OK | .
`dev/usb/utvfu.h` | C header | Simplified-BSD or GPL | OK | .
`dev/usb/utwitch.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uvideo.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uvideo.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uvisor.c` | C code | Simplified-BSD | OK | .
`dev/usb/uvscom.c` | C code | Simplified-BSD | OK | .
`dev/usb/uwacom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/uxrcom.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/xhci.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/usb/xhcireg.h` | C header | Simplified-BSD | OK | .
`dev/usb/xhcivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/video.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/video_if.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/videomode/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/videomode/edid.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/videomode/ediddevs_data.h` | C header (generated) | Custom-Modified-BSD (third clause is a "non-endorsement clause") | [this file is generated by "dev/videomode/devlist2h.awk" with "dev/videomode/ediddevs"] | .
`dev/videomode/ediddevs` | EDID prod./vend. list | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/videomode/ediddevs.h` | C header (generated) | Custom-Modified-BSD (third clause is a "non-endorsement clause") | [this file is generated by "dev/videomode/devlist2h.awk" with "dev/videomode/ediddevs"] | .
`dev/videomode/edidreg.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/videomode/edidvar.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/videomode/Makefile.ediddevs` | Makefile | "Unlicensed!!!" | Trivial; 4 code line + 9 commented lines; 16 total lines | .
`dev/videomode/Makefile.videomode` | Makefile | "Unlicensed!!!" | Trivial; 4 code line + 8 commented lines; 15 total lines | .
`dev/videomode/modelines2c.awk` | AWK script | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
**`dev/videomode/modelines`** | Modeline config. | __Unlicensed!!!__ | Non-Trivial [False-Positive: this is a part of XFree86 "vesamodes" and "extramodes"; XFree86-1.0 or Non-Copyrightable] | .
**`dev/videomode/vesagtf.c`** | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Modified-BSD + __Nonfree-License!!! (VESA license)__ | __Non-Free!!!__ (Combined Licenses) | .
`dev/videomode/vesagtf.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
**`dev/videomode/videomode.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [False-Positive: this file is generated by "dev/videomode/modelines2c.awk" with "dev/videomode/modelines"; Custom-Modified-BSD (third clause is a "non-endorsement clause") + XFree86-1.0] | .
`dev/videomode/videomode.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`dev/vnd.c` | C code | Modified-BSD | OK | .
`dev/vndioctl.h` | C header | Modified-BSD | OK | .
`dev/vscsi.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/vscsivar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/wscons/ascii.h` | C header | "Unlicensed!!!" | Trivial; 13 code line + 2 commented lines; 17 total lines | .
`dev/wscons/files.wscons` | BSD kernel config. | Non-Copyrightable | OK | .
**`dev/wscons/unicode.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code line + 7 commented lines; 26 total lines | .
`dev/wscons/wscons_callbacks.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wscons_features.h` | C header | Public-Domain (notified in file) | OK | .
`dev/wscons/wsconsio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsdisplay.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsdisplay_compat_usl.c` | C code | Simplified-BSD | OK | .
**`dev/wscons/wsdisplay_usl_io.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 2 commented lines; 71 total lines | .
`dev/wscons/wsdisplayvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemulconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemul_dumb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemul_subr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/wscons/wsemul_sun.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/wscons/wsemulvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/wscons/wsemul_vt100.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`dev/wscons/wsemul_vt100_chars.c` | C code | Simplified-BSD | OK | .
`dev/wscons/wsemul_vt100_keys.c` | C code | Simplified-BSD | OK | .
`dev/wscons/wsemul_vt100_subr.c` | C code | Simplified-BSD | OK | .
`dev/wscons/wsemul_vt100var.h` | C header | Simplified-BSD | OK | .
`dev/wscons/wsevent.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wseventvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wskbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wskbdraw.h` | C header | Simplified-BSD | OK | .
`dev/wscons/wskbdutil.c` | C code | Simplified-BSD | OK | .
`dev/wscons/wskbdvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsksymdef.h` | C header | Simplified-BSD | OK | .
`dev/wscons/wsksymvar.h` | C header | Simplified-BSD | OK | .
`dev/wscons/wsmouse.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/wscons/wsmoused.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsmouseinput.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/wscons/wsmousevar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/wscons/wsmux.c` | C code | Simplified-BSD | OK | .
`dev/wscons/wsmuxvar.h` | C header | Simplified-BSD | OK | .
`dev/wscons/wstpad.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`dev/wsfont/bold8x16.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/bold8x16-iso1.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/files.wsfont` | BSD kernel config. | Non-Copyrightable | OK | .
`dev/wsfont/gallant12x22.h` | C header | Modified-BSD | OK | .
`dev/wsfont/spleen12x24.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/spleen16x32.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/spleen32x64.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/spleen5x8.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/spleen6x12.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/spleen8x16.h` | C header | Simplified-BSD | OK | .
`dev/wsfont/wsfont.c` | C code | Simplified-BSD | OK | .
`dev/wsfont/wsfont.h` | C header | Simplified-BSD | OK | .
`dev/x86emu/x86emu.c` | C code | Custom-Free-License (similar to X11; but without advertising) | OK | .
`dev/x86emu/x86emu.h` | C header | Custom-Free-License (similar to X11; but without advertising) | OK | .
`dev/x86emu/x86emu_regs.h` | C header | Custom-Free-License (similar to X11; but without advertising) | OK | .
`dev/x86emu/x86emu_util.c` | C code | Custom-Free-License (similar to X11; but without advertising) | OK | .
`isofs/cd9660/cd9660_bmap.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_extern.h` | C header | Modified-BSD | OK | .
`isofs/cd9660/cd9660_lookup.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_node.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_node.h` | C header | Modified-BSD | OK | .
`isofs/cd9660/cd9660_rrip.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_rrip.h` | C header | Modified-BSD | OK | .
`isofs/cd9660/cd9660_util.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_vfsops.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/cd9660_vnops.c` | C code | Modified-BSD | OK | .
`isofs/cd9660/iso.h` | C header | Modified-BSD | OK | .
`isofs/cd9660/iso_rrip.h` | C header | Modified-BSD | OK | .
`isofs/cd9660/TODO.hibler` | ASCII text | "Unlicensed!!!" | Trivial; 12 data lines + 2 commented lines; 18 total lines | .
`isofs/udf/ecma167-udf.h` | C header | Simplified-BSD | OK | .
`isofs/udf/udf_extern.h` | C header | Public-Domain (Pedro Martelletto) | OK | .
`isofs/udf/udf.h` | C header | Simplified-BSD | OK | .
`isofs/udf/udf_subr.c` | C code | Simplified-BSD | OK | .
`isofs/udf/udf_vfsops.c` | C code | Simplified-BSD | OK | .
`isofs/udf/udf_vnops.c` | C code | Simplified-BSD | OK | .
`kern/clock_subr.c` | C code | Modified-BSD | OK | .
`kern/dma_alloc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/exec_conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/exec_elf.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`kern/exec_script.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/exec_subr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/genassym.sh` | Shell script | Simplified-BSD | OK | .
`kern/init_main.c` | C code | Modified-BSD | OK | .
**`kern/init_sysent.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
`kern/kern_acct.c` | C code | Modified-BSD | OK | .
`kern/kern_bufq.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_clock.c` | C code | Modified-BSD | OK | .
`kern/kern_descrip.c` | C code | Modified-BSD | OK | .
`kern/kern_event.c` | C code | Simplified-BSD | OK | .
`kern/kern_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/kern_exit.c` | C code | Modified-BSD | OK | .
`kern/kern_fork.c` | C code | Modified-BSD | OK | .
`kern/kern_intrmap.c` | C code | Modified-BSD | OK | .
`kern/kern_kthread.c` | C code | Simplified-BSD | OK | .
`kern/kern_ktrace.c` | C code | Modified-BSD | OK | .
`kern/kern_lock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_malloc.c` | C code | Modified-BSD | OK | .
`kern/kern_physio.c` | C code | Modified-BSD | OK | .
`kern/kern_pledge.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_proc.c` | C code | Modified-BSD | OK | .
`kern/kern_prot.c` | C code | Modified-BSD | OK | .
`kern/kern_resource.c` | C code | Modified-BSD | OK | .
`kern/kern_rwlock.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_sched.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_sensors.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_sig.c` | C code | Modified-BSD | OK | .
`kern/kern_smr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_srp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_subr.c` | C code | Modified-BSD | OK | .
`kern/kern_synch.c` | C code | Modified-BSD | OK | .
`kern/kern_sysctl.c` | C code | Modified-BSD | OK | .
`kern/kern_task.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_tc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_time.c` | C code | Modified-BSD | OK | .
`kern/kern_timeout.c` | C code | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`kern/kern_unveil.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/kern_uuid.c` | C code | Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`kern/kern_watchdog.c` | C code | Simplified-BSD | OK | .
`kern/kern_xxx.c` | C code | Modified-BSD | OK | .
**`kern/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 10 commented lines; 59 total lines | .
`kern/makesyscalls.sh` | Shell script | Custom-Original-BSD (four clause is a "non-endorsement clause") | [this file is required to replace a completely new code file, because of "kern/syscalls.master", see "https://github.com/torvalds/linux/blob/master/arch/s390/kernel/syscalls/syscalltbl" as example] | .
`kern/sched_bsd.c` | C code | Modified-BSD | OK | .
`kern/spec_vnops.c` | C code | Modified-BSD | OK | .
`kern/subr_autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`kern/subr_disk.c` | C code | Modified-BSD | OK | .
`kern/subr_evcount.c` | C code | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`kern/subr_extent.c` | C code | Simplified-BSD | OK | .
`kern/subr_hibernate.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/subr_kubsan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/subr_log.c` | C code | Modified-BSD | OK | .
`kern/subr_percpu.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/subr_poison.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/subr_pool.c` | C code | Simplified-BSD | OK | .
`kern/subr_prf.c` | C code | Modified-BSD | OK | .
`kern/subr_prof.c` | C code | Modified-BSD | OK | .
`kern/subr_tree.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`kern/subr_userconf.c` | C code | Simplified-BSD | OK | .
`kern/subr_witness.c` | C code | Modified-BSD | OK | .
`kern/subr_xxx.c` | C code | Modified-BSD | OK | .
**`kern/syscalls.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
`kern/syscalls.conf` | makesyscalls.sh config. | Non-Copyrightable | OK | .
**`kern/syscalls.master`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 561 non empty lines; 568 total lines | .
`kern/sys_futex.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/sys_generic.c` | C code | Modified-BSD | OK | .
`kern/sys_pipe.c` | C code | Custom-Simplified-BSD (John S. Dyson; similar to Simplified-BSD with inclusion of unusual 2 clauses) | OK | .
`kern/sys_process.c` | C code | Modified-BSD | OK | .
`kern/sys_socket.c` | C code | Modified-BSD | OK | .
`kern/sysv_ipc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/sysv_msg.c` | C code | Original-ISC ("and" is used instead of "and/or") + RTMX (similar to Simplified-BSD) | OK (Combined Licenses) | .
`kern/sysv_sem.c` | C code | Original-ISC ("and" is used instead of "and/or") + RTMX (similar to Simplified-BSD) | OK (Combined Licenses) | .
`kern/sysv_shm.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`kern/tty.c` | C code | Modified-BSD | OK | .
`kern/tty_conf.c` | C code | Modified-BSD | OK | .
`kern/tty_endrun.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/tty_msts.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/tty_nmea.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/tty_pty.c` | C code | Modified-BSD | OK | .
`kern/tty_subr.c` | C code | Simplified-BSD | OK | .
`kern/tty_tty.c` | C code | Modified-BSD | OK | .
`kern/uipc_domain.c` | C code | Modified-BSD | OK | .
`kern/uipc_mbuf2.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`kern/uipc_mbuf.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`kern/uipc_proto.c` | C code | Modified-BSD | OK | .
`kern/uipc_socket2.c` | C code | Modified-BSD | OK | .
`kern/uipc_socket.c` | C code | Modified-BSD | OK | .
`kern/uipc_syscalls.c` | C code | Modified-BSD | OK | .
`kern/uipc_usrreq.c` | C code | Modified-BSD | OK | .
`kern/vfs_bio.c` | C code | Modified-BSD | OK | .
`kern/vfs_biomem.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`kern/vfs_cache.c` | C code | Modified-BSD | OK | .
`kern/vfs_default.c` | C code | Modified-BSD | OK | .
`kern/vfs_getcwd.c` | C code | Simplified-BSD | OK | .
`kern/vfs_init.c` | C code | Modified-BSD | OK | .
`kern/vfs_lockf.c` | C code | Modified-BSD | OK | .
`kern/vfs_lookup.c` | C code | Modified-BSD | OK | .
`kern/vfs_subr.c` | C code | Modified-BSD | OK | .
`kern/vfs_sync.c` | C code | Modified-BSD | OK | .
`kern/vfs_syscalls.c` | C code | Modified-BSD | OK | .
`kern/vfs_vnops.c` | C code | Modified-BSD | OK | .
`kern/vfs_vops.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD | OK (Combined Licenses) | .
`lib/libkern/adddi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/anddi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/arch/alpha/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines | [False-Positive: the code is moved to "memmove.S"; empty file]
`lib/libkern/arch/alpha/bzero.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__divl.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__divlu.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__divq.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__divqu.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/divrem.m4` | Macro processor script | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/ffs.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/alpha/htonl.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/alpha/htons.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
**`lib/libkern/arch/alpha/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 code lines + 2 commented lines; 48 total lines | .
`lib/libkern/arch/alpha/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/alpha/memmove.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__reml.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__remlu.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__remq.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/alpha/__remqu.S` | Assembler code | CMU (similar to ISC) | OK | .
**`lib/libkern/arch/amd64/bcmp.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 20 code lines; 22 total lines | .
`lib/libkern/arch/amd64/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/amd64/bzero.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/ffs.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/htonl.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/amd64/htons.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/amd64/index.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/amd64/memchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/memcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/amd64/memmove.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/amd64/memset.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/rindex.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/amd64/scanc.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/amd64/skpc.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/amd64/strchr.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/amd64/strcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/amd64/strlen.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/amd64/strrchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/arm64/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`lib/libkern/arch/arm/__aeabi_ldivmod.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/__aeabi_uldivmod.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/bcopy.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/bzero.S` | Assembler code | Simplified-BSD | OK | .
**`lib/libkern/arch/arm/divsi3.S`** | Assembler code | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
`lib/libkern/arch/arm/htonl.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/htons.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/memcpy.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/arm/memmove.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 5 commented lines; 7 total lines [False-Positive: the code is moved to "memcpy.S"; empty file] | .
`lib/libkern/arch/arm/memset.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/hppa/bcopy.m4` | Macro processor script | Simplified-BSD {generated by versionmacro} | OK | .
`lib/libkern/arch/hppa/bcopy.S` | Assembler code | Simplified-BSD | [generated from lib/libkern/arch/hppa/bcopy.m4] | .
`lib/libkern/arch/hppa/Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code line + 1 commented line; 15 total lines | .
`lib/libkern/arch/hppa/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/hppa/memmove.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/hppa/milli.S` | Assembler code | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`lib/libkern/arch/hppa/prefix.h` | Assembler header | Hewlett-Packard (similar to Original-ISC but without endorsement clause) | OK | .
`lib/libkern/arch/hppa/spcopy.S` | Assembler code | Simplified-BSD | [generated from lib/libkern/arch/hppa/bcopy.m4] | .
`lib/libkern/arch/i386/bcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/i386/bzero.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/DEFS.h` | Assembler header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`lib/libkern/arch/i386/ffs.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/htonl.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/i386/htons.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/i386/memchr.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/memcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/i386/memmove.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/i386/memset.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/scanc.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/i386/skpc.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/i386/strcmp.S` | Assembler code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/arch/i386/SYS.h` | Assembler header | Modified-BSD | OK | .
`lib/libkern/arch/m88k/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
`lib/libkern/arch/m88k/bzero.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/m88k/copy_subr.S` | Assembler code | CMU (similar to ISC) | OK | .
`lib/libkern/arch/m88k/divsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/arch/m88k/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
`lib/libkern/arch/m88k/memmove.S` | Assembler code | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 5 total lines | .
`lib/libkern/arch/m88k/modsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/arch/m88k/udivsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/arch/m88k/umodsi3.S` | Assembler code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/arch/mips64/bcmp.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/mips64/bzero.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/DEFS.h` | Assembler header | "Unlicensed!!!" | Trivial; 1 code line + 1 commented line; 4 total lines | .
`lib/libkern/arch/mips64/ffs.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/htonl.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/htons.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/mips64/memmove.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/strcmp.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/strlen.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/mips64/SYS.h` | Assembler header | Modified-BSD | OK | .
`lib/libkern/arch/powerpc64/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`lib/libkern/arch/powerpc/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/powerpc/ffs.S` | Assembler code | Public-Domain (Christian Weisgerber) | OK | .
`lib/libkern/arch/powerpc/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/powerpc/memmove.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sh/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/sh/ffs.S` | Assembler code | Simplified-BSD | OK | .
`lib/libkern/arch/sh/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/sh/memmove.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/sh/memset.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/sh/movstr_i4.S` | Assembler code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/sh/movstrSI12_i4.S` | Assembler code | Public-Domain (Theo de Raadt) | OK | .
`lib/libkern/arch/sh/sdivsi3.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sh/udivsi3.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/bcopy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/sparc64/bzero.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/sparc64/DEFS.h` | Assembler header | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/ffs.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/htonl.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/htons.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/memcpy.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: the code is moved to "memmove.S"; empty file] | .
`lib/libkern/arch/sparc64/memmove.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/sparc64/memset.S` | Assembler code | "Unlicensed!!!" | Trivial; 0 code line + 1 commented line; 2 total lines [False-Positive: no code found; empty file] | .
`lib/libkern/arch/sparc64/_setjmp.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/strlen.S` | Assembler code | Modified-BSD | OK | .
`lib/libkern/arch/sparc64/SYS.h` | Assembler header | Modified-BSD | OK | .
`lib/libkern/ashldi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/ashrdi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/bcmp.c` | C code | Modified-BSD | OK | .
`lib/libkern/bcopy.c` | C code | Modified-BSD | OK | .
`lib/libkern/bzero.c` | C code | Modified-BSD | OK | .
`lib/libkern/cmpdi2.c` | C code | Modified-BSD | OK | .
`lib/libkern/crt_glue.h` | C header | Expat or NCSA (LLVM dual license) | OK | .
`lib/libkern/divdi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/explicit_bzero.c` | C code | Public-Domain (Matthew Dempsky) | OK | .
`lib/libkern/ffs.c` | C code | Public-Domain (Dale Rahn) | OK | .
`lib/libkern/fls.c` | C code | Modified-BSD | OK | .
`lib/libkern/flsl.c` | C code | Modified-BSD | OK | .
`lib/libkern/funcs.h` | C header | "Unlicensed!!!" | Trivial; 4 code lines; 5 total lines | .
`lib/libkern/getsn.c` | C code | Simplified-BSD | OK | .
`lib/libkern/htonl.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/htons.c` | C code | Public-Domain (J.T. Conklin) | OK | .
`lib/libkern/imax.c` | C code | Modified-BSD | OK | .
`lib/libkern/imin.c` | C code | Modified-BSD | OK | .
`lib/libkern/iordi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/libkern.h` | C header | Modified-BSD | OK | .
`lib/libkern/lmax.c` | C code | Modified-BSD | OK | .
`lib/libkern/lmin.c` | C code | Modified-BSD | OK | .
`lib/libkern/lshldi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/lshrdi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/lshrti3.c` | C code | Expat or NCSA (LLVM dual license) | OK | .
`lib/libkern/max.c` | C code | Modified-BSD | OK | .
`lib/libkern/mcount.c` | C code | Modified-BSD | OK | .
`lib/libkern/memchr.c` | C code | Modified-BSD | OK | .
`lib/libkern/memcmp.c` | C code | Modified-BSD | OK | .
`lib/libkern/memcpy.c` | C code | Modified-BSD | OK | .
`lib/libkern/memmove.c` | C code | Modified-BSD | OK | .
`lib/libkern/memset.c` | C code | Modified-BSD | OK | .
**`lib/libkern/milieu.h`** | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/min.c` | C code | Modified-BSD | OK | .
`lib/libkern/moddi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/muldi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/multi3.c` | C code | Expat or NCSA (LLVM dual license) | OK | .
`lib/libkern/negdi2.c` | C code | Modified-BSD | OK | .
`lib/libkern/notdi2.c` | C code | Modified-BSD | OK | .
`lib/libkern/qdivrem.c` | C code | Modified-BSD | OK | .
`lib/libkern/quad.h` | C header | Modified-BSD | OK | .
`lib/libkern/random.c` | C code | Modified-BSD | OK | .
`lib/libkern/scanc.c` | C code | Modified-BSD | OK | .
`lib/libkern/skpc.c` | C code | Modified-BSD | OK | .
`lib/libkern/softfloat.c` | C code | __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat.h` | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat-macros.h` | C header | __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat-specialize.h` | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/strchr.c` | C code | Simplified-BSD | OK | .
`lib/libkern/strcmp.c` | C code | Modified-BSD | OK | .
`lib/libkern/strlcat.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/strlcpy.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/strlen.c` | C code | Modified-BSD | OK | .
`lib/libkern/strncasecmp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libkern/strncmp.c` | C code | Modified-BSD | OK | .
`lib/libkern/strncpy.c` | C code | Modified-BSD | OK | .
`lib/libkern/strnlen.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/strrchr.c` | C code | Simplified-BSD | OK | .
`lib/libkern/subdi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/timingsafe_bcmp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libkern/ucmpdi2.c` | C code | Modified-BSD | OK | .
`lib/libkern/udivdi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/ulmax.c` | C code | Modified-BSD | OK | .
`lib/libkern/ulmin.c` | C code | Modified-BSD | OK | .
`lib/libkern/umoddi3.c` | C code | Modified-BSD | OK | .
`lib/libkern/xordi3.c` | C code | Modified-BSD | OK | .
`lib/libsa/aes_xts.c` | C code | Custom-Free-License (Damien Miller) or GPL (any version) | OK | .
`lib/libsa/aes_xts.h` | C header | Custom-Free-License (Damien Miller) or GPL (any version) | OK | .
`lib/libsa/alloc.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/arc4.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/arc4.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/arp.c` | C code | Original-BSD | OK | .
`lib/libsa/bcrypt_pbkdf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/bcrypt_pbkdf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/blowfish.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/blowfish.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/bootparam.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the advertising one) | OK | .
`lib/libsa/bootparam.h` | C header | "Unlicensed!!!" | Trivial; 2 code lines + 1 commented line; 6 total lines [False-Positive: contains only 2 function declarations from "lib/libsa/bootparam.c"; Custom-Original-BSD] | .
`lib/libsa/bootp.c` | C code | Original-BSD | OK | .
`lib/libsa/bootp.h` | C header | Custom-CMU (Free license) | OK | .
`lib/libsa/cd9660.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/cd9660.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/closeall.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/close.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/cons.c` | C code | Modified-BSD | OK | .
`lib/libsa/cread.c` | C code | Simplified-BSD | OK | .
`lib/libsa/ctime.c` | C code | Simplified-BSD | OK | .
`lib/libsa/dev.c` | C code | Modified-BSD | OK | .
`lib/libsa/disklabel.c` | C code | Modified-BSD | OK | .
`lib/libsa/dkcksum.c` | C code | Modified-BSD | OK | .
`lib/libsa/ether.c` | C code | Original-BSD | OK | .
`lib/libsa/exit.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/explicit_bzero.c` | C code | Public-Domain (Ted Unangst) | OK | .
`lib/libsa/fchmod.c` | C code | Modified-BSD | OK | .
`lib/libsa/fstat.c` | C code | Modified-BSD | OK | .
`lib/libsa/getchar.c` | C code | Original-BSD | OK | .
`lib/libsa/getfile.c` | C code | Modified-BSD | OK | .
`lib/libsa/getln.c` | C code | Modified-BSD | OK | .
**`lib/libsa/globals.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 8 commented lines; 34 total lines [contains structure declarations, variables definitions, array definitions and included headers] | .
`lib/libsa/hexdump.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/hmac_sha1.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/hmac_sha1.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/in_cksum.c` | C code | Modified-BSD | OK | .
`lib/libsa/ioctl.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/iodesc.h` | C header | Original-BSD | OK | .
`lib/libsa/loadfile.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`lib/libsa/loadfile_elf.c` | C code | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`lib/libsa/loadfile.h` | C header | Simplified-BSD | OK | .
`lib/libsa/lseek.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
**`lib/libsa/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 33 code lines + 4 commented lines; 44 total lines | .
**`lib/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 code lines + 13 commented lines; 68 total lines | .
`lib/libsa/memcmp.c` | C code | Modified-BSD | OK | .
`lib/libsa/memcpy.c` | C code | Modified-BSD | OK | .
`lib/libsa/memmove.c` | C code | Modified-BSD | OK | .
`lib/libsa/memset.c` | C code | Modified-BSD | OK | .
`lib/libsa/net.c` | C code | Original-BSD | OK | .
`lib/libsa/net.h` | C header | Original-BSD | OK | .
`lib/libsa/netif.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`lib/libsa/netif.h`** | C header | __Unlicensed!!!__ | Non-Trivial [contains 10 function declarations, 1 structure declaration and 2 variable declarations from "lib/libsa/netif.c"; contains 1 included header and "4 structure definitions"] | .
`lib/libsa/netudp.c` | C code | Original-BSD | OK | .
`lib/libsa/nfs.c` | C code | Modified-BSD | OK | .
`lib/libsa/nfs.h` | C header | Modified-BSD | OK | .
`lib/libsa/nfsv2.h` | C header | Modified-BSD | OK | .
`lib/libsa/nullfs.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/open.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/pkcs5_pbkdf2.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/pkcs5_pbkdf2.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/printf.c` | C code | Modified-BSD | OK | .
`lib/libsa/putchar.c` | C code | Original-BSD | OK | .
`lib/libsa/rarp.c` | C code | Original-BSD | OK | .
`lib/libsa/read.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/readdir.c` | C code | Simplified-BSD | OK | .
`lib/libsa/rijndael.c` | C code | Public-Domain (Rijndael-cipher) | OK | .
`lib/libsa/rijndael.h` | C header | Public-Domain (Rijndael-cipher) | OK | .
`lib/libsa/rpc.c` | C code | Original-BSD | OK | .
`lib/libsa/rpc.h` | C header | Original-BSD | OK | .
`lib/libsa/rpcv2.h` | C header | Modified-BSD | OK | .
`lib/libsa/saerrno.h` | C header | Modified-BSD | OK | .
`lib/libsa/sha1.c` | C code | Public-Domain (SHA-1-in-C) | OK | .
`lib/libsa/sha1.h` | C header | Public-Domain (SHA-1-in-C) | OK | .
`lib/libsa/sha2.c` | C code | Modified-BSD | OK | .
`lib/libsa/sha2.h` | C header | Modified-BSD | OK | .
`lib/libsa/snprintf.c` | C code | Modified-BSD | OK | .
`lib/libsa/softraid.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/softraid.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libsa/stand.h` | C header | Modified-BSD | OK | .
`lib/libsa/stat.c` | C code | Modified-BSD | OK | .
`lib/libsa/strchr.c` | C code | Modified-BSD | OK | .
`lib/libsa/strcmp.c` | C code | Simplified-BSD | OK | .
`lib/libsa/strerror.c` | C code | Modified-BSD | OK | .
`lib/libsa/strlen.c` | C code | Modified-BSD | OK | .
`lib/libsa/strncmp.c` | C code | Simplified-BSD | OK | .
`lib/libsa/strncpy.c` | C code | Simplified-BSD | OK | .
`lib/libsa/strtol.c` | C code | Modified-BSD | OK | .
`lib/libsa/strtoll.c` | C code | Modified-BSD | OK | .
`lib/libsa/tftp.c` | C code | Simplified-BSD | OK | .
`lib/libsa/tftp.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`lib/libsa/ufs2.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/ufs2.h` | C header | Modified-BSD | OK | .
`lib/libsa/ufs.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libsa/ufs.h` | C header | Modified-BSD | OK | .
`lib/libsa/write.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`lib/libz/adler32.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/compress.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/crc32.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/crc32.h` | C header (generated) | zlib (copyright notice found in lib/libz/zlib.h and it does not contains license info!!) | [generated by lib/libz/crc32.c: make_crc_table() function] | .
`lib/libz/deflate.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/deflate.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/infback.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inffast.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inffast.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inffixed.h` | C header (generated) | zlib (copyright notice found in lib/libz/zlib.h and it does not contains license info!!) | [generated by lib/libz/inflate.c: makefixed() fuction] | .
`lib/libz/inflate.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inflate.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inftrees.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/inftrees.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
**`lib/libz/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 48 code lines + 16 commented lines; 74 total lines [use zlib Makefile as replacement! (https://github.com/madler/zlib)] | .
**`lib/libz/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 19 code lines + 6 commented lines; 35 total lines [use zlib Makefile as replacement! (https://github.com/madler/zlib)] | .
`lib/libz/trees.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/trees.h` | C header (generated) | zlib (copyright notice found in lib/libz/zlib.h and it does not contains license info!!) | [generated by lib/libz/trees.c: gen_trees_header() function] | .
`lib/libz/zconf.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/zlib.h` | C header | zlib | OK | .
`lib/libz/zopenbsd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`lib/libz/zutil.c` | C code | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`lib/libz/zutil.h` | C header | zlib (copyright notice found in lib/libz/zlib.h!) | OK | .
`Makefile` | Makefile | "Unlicensed!!!" | Trivial; 9 code lines + 2 commented lines; 15 total lines | .
`miscfs/deadfs/dead_vnops.c` | C code | Modified-BSD | OK | .
`miscfs/fifofs/fifo.h` | C header | Modified-BSD | OK | .
`miscfs/fifofs/fifo_vnops.c` | C code | Modified-BSD | OK | .
`miscfs/fuse/fusebuf.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fuse_device.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fuse_file.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fusefs.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fusefs_node.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fuse_lookup.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fuse_vfsops.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`miscfs/fuse/fuse_vnops.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`msdosfs/bootsect.h` | C header | Custom-Free-License (Paul-Popelka) | OK | .
`msdosfs/bpb.h` | C header | Custom-Free-License (Paul-Popelka) | OK | .
`msdosfs/denode.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/direntry.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/fat.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_conv.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_denode.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_fat.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_lookup.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfsmount.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_vfsops.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_vnops.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`net80211/ieee80211_amrr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_amrr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_crypto_bip.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_crypto.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_crypto_ccmp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_crypto.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_crypto_tkip.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_crypto_wep.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211.h` | C header | Simplified-BSD | OK | .
`net80211/ieee80211_input.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_ioctl.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_ioctl.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_mira.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_mira.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_node.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_node.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_output.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_pae_input.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_pae_output.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_priv.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_proto.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_proto.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net80211/ieee80211_ra.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_radiotap.h` | C header | Simplified-BSD | OK | .
`net80211/ieee80211_ra.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_regdomain.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_regdomain.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net80211/ieee80211_rssadapt.c` | C code | Simplified-BSD | OK | .
`net80211/ieee80211_rssadapt.h` | C header | Simplified-BSD | OK | .
`net80211/ieee80211_var.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`net/art.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/art.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/bfd.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/bfd.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/bpf.c` | C code | Modified-BSD | OK | .
`net/bpfdesc.h` | C header | Modified-BSD | OK | .
`net/bpf_filter.c` | C code | Modified-BSD | OK | .
`net/bpf.h` | C header | Modified-BSD | OK | .
`net/bridgectl.c` | C code | Simplified-BSD | OK | .
`net/bridgestp.c` | C code | Simplified-BSD | OK | .
`net/bsd-comp.c` | C code | Modified-BSD | OK | .
`net/ethertypes.h` | C header | Modified-BSD | OK | .
`net/fq_codel.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/fq_codel.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/hfsc.c` | C code | Custom-Free-License (similar to Expat) | OK | .
`net/hfsc.h` | C header | Custom-Free-License (similar to Expat) | OK | .
`net/if_aggr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_arp.h` | C header | Modified-BSD | OK | .
`net/if_bpe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_bpe.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_bridge.c` | C code | Simplified-BSD | OK | .
`net/if_bridge.h` | C header | Simplified-BSD | OK | .
`net/if.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/if_dl.h` | C header | Modified-BSD | OK | .
`net/if_enc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_enc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_etherbridge.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_etherbridge.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_etherip.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_etherip.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_ethersubr.c` | C code | Modified-BSD + Modified-BSD | OK | .
`net/if_gif.c` | C code | Modified-BSD | OK | .
`net/if_gif.h` | C header | Modified-BSD | OK | .
`net/if_gre.c` | C code | Simplified-BSD | OK | .
`net/if_gre.h` | C header | Simplified-BSD | OK | .
`net/if.h` | C header | Modified-BSD | OK | .
`net/if_llc.h` | C header | Modified-BSD | OK | .
`net/if_loop.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`net/if_media.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`net/if_media.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`net/if_mpe.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_mpip.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_mpw.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_pair.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_pflog.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`net/if_pflog.h` | C header | Simplified-BSD | OK | .
`net/if_pflow.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_pflow.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_pfsync.c` | C code | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`net/if_pfsync.h` | C header | Simplified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`net/if_ppp.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Modified-BSD | OK (Combined Licenses) | .
`net/if_ppp.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/if_pppoe.c` | C code | Simplified-BSD | OK | .
`net/if_pppoe.h` | C header | Simplified-BSD | OK | .
`net/if_pppvar.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK (Combined Licenses) | .
`net/if_pppx.c` | C code | Original-ISC ("and" is used instead of "and/or") + Simplified-BSD | OK (Combined Licenses) | .
`net/ifq.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/ifq.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_sppp.h` | C header | Simplified-BSD | OK | .
`net/if_spppsubr.c` | C code | Simplified-BSD | OK | .
`net/if_switch.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_switch.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_tpmr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_trunk.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_trunk.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_tun.c` | C code | Simplified-BSD | OK | .
`net/if_tun.h` | C header | Simplified-BSD | OK | .
`net/if_types.h` | C header | Modified-BSD | OK | .
`net/if_var.h` | C header | Modified-BSD | OK | .
`net/if_veb.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_vether.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_vlan.c` | C code | Custom-Free-License (similar to X11; but without advertising) | OK | .
`net/if_vlan_var.h` | C header | Custom-Free-License (similar to X11; but without advertising) | OK | .
`net/if_vxlan.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_vxlan.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_wg.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/if_wg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet6/dest6.c` | C code | Modified-BSD | OK | .
`netinet6/frag6.c` | C code | Modified-BSD | OK | .
`netinet6/icmp6.c` | C code | Modified-BSD | OK | .
`netinet6/in6.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/in6_cksum.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/in6.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/in6_ifattach.c` | C code | Modified-BSD | OK | .
`netinet6/in6_ifattach.h` | C header | Modified-BSD | OK | .
`netinet6/in6_pcb.c` | C code | Modified-BSD + Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/in6_proto.c` | C code | Modified-BSD | OK | .
`netinet6/in6_src.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/in6_var.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/ip6_divert.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet6/ip6_divert.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet6/ip6_forward.c` | C code | Modified-BSD | OK | .
`netinet6/ip6_id.c` | C code | Modified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`netinet6/ip6_input.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/ip6_mroute.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/ip6_mroute.h` | C header | Modified-BSD | OK | .
`netinet6/ip6_output.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/ip6protosw.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/ip6_var.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/mld6.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/mld6.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`netinet6/mld6_var.h` | C header | Modified-BSD | OK | .
`netinet6/nd6.c` | C code | Modified-BSD | OK | .
`netinet6/nd6.h` | C header | Modified-BSD | OK | .
`netinet6/nd6_nbr.c` | C code | Modified-BSD | OK | .
`netinet6/nd6_rtr.c` | C code | Modified-BSD | OK | .
`netinet6/raw_ip6.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet6/raw_ip6.h` | C header | Modified-BSD | OK | .
`netinet6/route6.c` | C code | Modified-BSD | OK | .
`netinet6/udp6_output.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/icmp6.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/icmp_var.h` | C header | Modified-BSD | OK | .
`netinet/if_ether.c` | C code | Modified-BSD | OK | .
`netinet/if_ether.h` | C header | Modified-BSD | OK | .
`netinet/igmp.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/igmp.h` | C header | Modified-BSD | OK | .
`netinet/igmp_var.h` | C header | Modified-BSD | OK | .
`netinet/in4_cksum.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/in.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/in_cksum.c` | C code | Modified-BSD | OK | .
`netinet/inet_nat64.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet/inet_ntop.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet/in.h` | C header | Modified-BSD | OK | .
`netinet/in_pcb.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/in_pcb.h` | C header | Modified-BSD + Modified-BSD | OK | .
`netinet/in_proto.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/in_systm.h` | C header | Modified-BSD | OK | .
`netinet/in_var.h` | C header | Modified-BSD | OK | .
`netinet/ip6.h` | C header | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/ip_ah.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_ah.h` | C header | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_carp.c` | C code | Simplified-BSD | OK | .
`netinet/ip_carp.h` | C header | Simplified-BSD | OK | .
`netinet/ip_divert.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet/ip_divert.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet/ip_ecn.c` | C code | Modified-BSD | OK | .
`netinet/ip_ecn.h` | C header | Modified-BSD | OK | .
`netinet/ip_esp.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_esp.h` | C header | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_ether.h` | C header | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_gre.c` | C code | Simplified-BSD | OK | .
`netinet/ip_gre.h` | C header | Simplified-BSD | OK | .
`netinet/ip.h` | C header | Modified-BSD | OK | .
`netinet/ip_icmp.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/ip_icmp.h` | C header | Modified-BSD | OK | .
`netinet/ip_id.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netinet/ip_input.c` | C code | Modified-BSD | OK | .
`netinet/ip_ipcomp.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`netinet/ip_ipcomp.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`netinet/ip_ipip.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_ipip.h` | C header | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_ipsp.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_ipsp.h` | C header | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_mroute.c` | C code | Modified-BSD | OK | .
**`netinet/ip_mroute.h`** | C header | __Nonfree-License!!!__ (David Waitzman; copyright notice only) | __Nonfree!!!__ | .
`netinet/ip_output.c` | C code | Modified-BSD | OK | .
`netinet/ipsec_input.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ipsec_output.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_spd.c` | C code | Custom-Free-License (Angelos D. Keromytis) or GPL (any version) | OK | .
`netinet/ip_var.h` | C header | Modified-BSD | OK | .
`netinet/raw_ip.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_debug.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_debug.h` | C header | Modified-BSD | OK | .
`netinet/tcp_fsm.h` | C header | Modified-BSD | OK | .
`netinet/tcp.h` | C header | Modified-BSD | OK | .
`netinet/tcp_input.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_output.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_seq.h` | C header | Modified-BSD | OK | .
`netinet/tcp_subr.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_timer.c` | C code | Modified-BSD | OK | .
`netinet/tcp_timer.h` | C header | Modified-BSD | OK | .
`netinet/tcp_usrreq.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_var.h` | C header | Modified-BSD | OK | .
`netinet/udp.h` | C header | Modified-BSD | OK | .
`netinet/udp_usrreq.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/udp_var.h` | C header | Modified-BSD | OK | .
`netmpls/mpls.h` | C header | Modified-BSD | OK | .
`netmpls/mpls_input.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netmpls/mpls_output.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`netmpls/mpls_proto.c` | C code | Modified-BSD | OK | .
`netmpls/mpls_raw.c` | C code | Modified-BSD | OK | .
`netmpls/mpls_shim.c` | C code | Modified-BSD | OK | .
`net/netisr.h` | C header | Modified-BSD | OK | .
`net/ofp.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/pf.c` | C code | Simplified-BSD | OK | .
`net/pf_if.c` | C code | Simplified-BSD | OK | .
`net/pf_ioctl.c` | C code | Simplified-BSD | OK | .
`net/pfkeyv2.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/pfkeyv2_convert.c` | C code | ( Custom-Free-License (Angelos D. Keromytis) or GPL (any version; impossible in this file, because Original-BSD) ) + Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/pfkeyv2.h` | C header | Original-BSD | OK | .
`net/pfkeyv2_parsemessage.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/pf_lb.c` | C code | Simplified-BSD | OK | .
`net/pf_norm.c` | C code | Simplified-BSD | OK | .
`net/pf_osfp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/pf_ruleset.c` | C code | Simplified-BSD | OK | .
`net/pf_syncookies.c` | C code | Simplified-BSD | OK | .
`net/pf_table.c` | C code | Simplified-BSD | OK | .
`net/pfvar.h` | C header | Simplified-BSD | OK | .
`net/pfvar_priv.h` | C header | Simplified-BSD | OK | .
`net/pipex.c` | C code | Simplified-BSD | OK | .
`net/pipex.h` | C header | Simplified-BSD | OK | .
`net/pipex_local.h` | C header | Simplified-BSD | OK | .
`net/ppp-comp.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp-deflate.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp_defs.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp_tty.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Prior-BSD (similar to Modified-BSD but with the source and binary clauses merged and the "non-endorsement clause" included) | OK | .
`net/radix.c` | C code | Modified-BSD | OK | .
`net/radix.h` | C header | Modified-BSD | OK | .
`net/route.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`net/route.h` | C header | Modified-BSD | OK | .
`net/rtable.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/rtable.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/rtsock.c` | C code | Modified-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/slcompress.c` | C code | Modified-BSD | OK | .
`net/slcompress.h` | C header | Modified-BSD | OK | .
`net/switchctl.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/switchofp.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/toeplitz.c` | C code | Modified-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`net/toeplitz.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/trunklacp.c` | C code | Simplified-BSD | OK | .
`net/trunklacp.h` | C header | Simplified-BSD | OK | .
`net/wg_cookie.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/wg_cookie.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/wg_noise.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`net/wg_noise.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
**`nfs/krpc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 4 commented lines; 30 total lines | .
`nfs/krpc_subr.c` | C code | Original-BSD | OK | .
`nfs/nfs_aiod.c` | C code | Modified-BSD | OK | .
`nfs/nfs_bio.c` | C code | Modified-BSD | OK | .
`nfs/nfs_boot.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`nfs/nfs_debug.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`nfs/nfsdiskless.h` | C header | Modified-BSD | OK | .
`nfs/nfs.h` | C header | Modified-BSD | OK | .
`nfs/nfs_kq.c` | C code | Simplified-BSD | OK | .
`nfs/nfsmount.h` | C header | Modified-BSD | OK | .
`nfs/nfsm_subs.h` | C header | Modified-BSD | OK | .
`nfs/nfs_node.c` | C code | Modified-BSD | OK | .
`nfs/nfsnode.h` | C header | Modified-BSD | OK | .
`nfs/nfsproto.h` | C header | Modified-BSD | OK | .
`nfs/nfsrvcache.h` | C header | Modified-BSD | OK | .
`nfs/nfs_serv.c` | C code | Modified-BSD | OK | .
`nfs/nfs_socket.c` | C code | Modified-BSD | OK | .
`nfs/nfs_srvcache.c` | C code | Modified-BSD | OK | .
`nfs/nfs_subs.c` | C code | Modified-BSD | OK | .
`nfs/nfs_syscalls.c` | C code | Modified-BSD | OK | .
`nfs/nfs_var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`nfs/nfs_vfsops.c` | C code | Modified-BSD | OK | .
`nfs/nfs_vnops.c` | C code | Modified-BSD | OK | .
`nfs/rpcv2.h` | C header | Modified-BSD | OK | .
`nfs/xdr_subs.h` | C header | Modified-BSD | OK | .
`ntfs/ntfs_compr.c` | C code | Simplified-BSD | OK | .
`ntfs/ntfs_compr.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_conv.c` | C code | Simplified-BSD | OK | .
`ntfs/ntfs.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_ihash.c` | C code | Modified-BSD | OK | .
`ntfs/ntfs_ihash.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_inode.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfsmount.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_subr.c` | C code | Simplified-BSD | OK | .
`ntfs/ntfs_subr.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_vfsops.c` | C code | Simplified-BSD | OK | .
`ntfs/ntfs_vfsops.h` | C header | Simplified-BSD | OK | .
`ntfs/ntfs_vnops.c` | C code | Modified-BSD | OK | .
`scsi/cd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/cd.h` | C header | TRW-Financial-Systems (similar to Expat) | OK | .
`scsi/ch.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`scsi/files.scsi` | BSD kernel config. | Non-Copyrightable | OK | .
`scsi/iscsi.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpath.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpath_emc.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpath_hds.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpath_rdac.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpath_sym.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/mpathvar.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/safte.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/safte.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/scsi_all.h` | C header | TRW-Financial-Systems (similar to Expat) | OK | .
`scsi/scsi_base.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`scsi/scsi_changer.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/scsiconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/scsiconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
**`scsi/scsi_debug.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 14 commented lines; 75 total lines | .
`scsi/scsi_disk.h` | C header | Open-Software-Foundation (similar to Expat) + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/scsi_ioctl.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`scsi/scsi_message.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 50 code lines + 5 commented lines; 68 total lines | .
`scsi/scsi_tape.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/sd.c` | C code | Simplified-BSD + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/sdvar.h` | C header | Simplified-BSD + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/ses.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`scsi/ses.h` | C header | Simplified-BSD | OK | .
`scsi/st.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/uk.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`stand/boot/bootarg.c` | C code | Simplified-BSD | OK | .
`stand/boot/bootarg.h` | C header | Simplified-BSD | OK | .
`stand/boot/boot.c` | C code | Simplified-BSD | OK | .
`stand/boot/boot.conf` | BSD bootoader config. | Non-Copyrightable | OK | .
`stand/boot/cmd.c` | C code | Simplified-BSD | OK | .
`stand/boot/cmd.h` | C header | Simplified-BSD | OK | .
`stand/boot/vars.c` | C code | Simplified-BSD | OK | .
`stand/efi/include/amd64/efibind.h` | C header | Public-Domain (notified in file) | OK | .
`stand/efi/include/arm64/efibind.h` | C header | Public-Domain (notified in file) | OK | .
`stand/efi/include/arm/efibind.h` | C header | Public-Domain (notified in file) | OK | .
`stand/efi/include/efiapi.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/eficon.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/eficonsctl.h` | C header | Simplified-BSD | OK | .
`stand/efi/include/efidebug.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efidef.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efidevp.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efierr.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efifpswa.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efifs.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efigop.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efi.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efinet.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efi_nii.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efipart.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efiprot.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efipxebc.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efiser.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/efistdarg.h` | C header | Intel-Simplified-BSD (as Simplified-BSD with some changes, see: stand/efi/include/README) | OK | .
`stand/efi/include/i386/efibind.h` | C header | Public-Domain (notified in file) | OK | .
`stand/efi/include/README` | License in this dir. | Non-Copyrightable | OK | .
`stand/efi/include/riscv64/efibind.h` | C header | Public-Domain (notified in file) | OK | .
`sys/acct.h` | C header | Modified-BSD | OK | .
**`sys/ataio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 34 code lines + 4 commented lines; 49 total lines | .
`sys/atomic.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/audioio.h` | C header | Original-BSD | OK | .
`sys/buf.h` | C header | Modified-BSD | OK | .
`sys/cdefs.h` | C header | Modified-BSD | OK | .
**`sys/cdio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 371 non empty lines; 435 total lines | .
`sys/chio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/conf.h` | C header | Modified-BSD | OK | .
`sys/core.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/ctf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/device.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/dirent.h` | C header | Modified-BSD | OK | .
`sys/dir.h` | C header | Modified-BSD | OK | .
`sys/disk.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/disklabel.h` | C header | Modified-BSD | OK | .
`sys/dkio.h` | C header | Modified-BSD | OK | .
`sys/domain.h` | C header | Modified-BSD | OK | .
`sys/_endian.h` | C header | Simplified-BSD | OK | .
`sys/endian.h` | C header | Simplified-BSD | OK | .
`sys/errno.h` | C header | Modified-BSD | OK | .
`sys/evcount.h` | C header | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`sys/event.h` | C header | Simplified-BSD | OK | .
`sys/eventvar.h` | C header | Simplified-BSD | OK | .
`sys/exec_elf.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`sys/exec.h` | C header | Modified-BSD | OK | .
`sys/exec_script.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/extent.h` | C header | Simplified-BSD | OK | .
`sys/fcntl.h` | C header | Modified-BSD | OK | .
`sys/filedesc.h` | C header | Modified-BSD | OK | .
`sys/file.h` | C header | Modified-BSD | OK | .
`sys/filio.h` | C header | Modified-BSD | OK | .
`sys/fusebuf.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/futex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/gmon.h` | C header | Modified-BSD | OK | .
`sys/gpio.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/hibernate.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/hotplug.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/intrmap.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/ioccom.h` | C header | Modified-BSD | OK | .
`sys/ioctl.h` | C header | Modified-BSD | OK | .
`sys/ipc.h` | C header | Modified-BSD | OK | .
`sys/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/kcov.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/kernel.h` | C header | Modified-BSD | OK | .
`sys/kstat.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/kthread.h` | C header | Simplified-BSD | OK | .
`sys/ktrace.h` | C header | Modified-BSD | OK | .
`sys/limits.h` | C header | Simplified-BSD | OK | .
`sys/lockf.h` | C header | Modified-BSD | OK | .
`sys/_lock.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`sys/lock.h` | C header | Modified-BSD | OK | .
`sys/malloc.h` | C header | Modified-BSD | OK | .
`sys/mbuf.h` | C header | Modified-BSD | OK | .
`sys/memrange.h` | C header | Simplified-BSD | OK | .
`sys/mman.h` | C header | Modified-BSD | OK | .
`sys/mount.h` | C header | Modified-BSD | OK | .
`sys/mplock.h` | C header | Simplified-BSD | OK | .
`sys/msgbuf.h` | C header | Modified-BSD | OK | .
`sys/msg.h` | C header | RTMX (similar to Simplified-BSD) | OK | .
`sys/mtio.h` | C header | Modified-BSD | OK | .
`sys/mutex.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/namei.h` | C header | Modified-BSD | OK | .
`sys/_null.h` | C header | Public-Domain (Todd C. Miller) | OK | .
`sys/param.h` | C header | Modified-BSD | OK | .
`sys/pciio.h` | C header | Simplified-BSD | OK | .
`sys/percpu.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/pipe.h` | C header | Custom-Simplified-BSD (John S. Dyson; similar to Simplified-BSD with inclusion of unusual 3 clauses) | OK | .
`sys/pledge.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/poll.h` | C header | Simplified-BSD | OK | .
`sys/pool.h` | C header | Simplified-BSD | OK | .
`sys/proc.h` | C header | Modified-BSD | OK | .
`sys/protosw.h` | C header | Modified-BSD | OK | .
`sys/ptrace.h` | C header | Modified-BSD | OK | .
`sys/queue.h` | C header | Modified-BSD | OK | .
`sys/radioio.h` | C header | Simplified-BSD | OK | .
`sys/reboot.h` | C header | Modified-BSD | OK | .
`sys/refcnt.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/resource.h` | C header | Modified-BSD | OK | .
`sys/resourcevar.h` | C header | Modified-BSD | OK | .
`sys/rwlock.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/sched.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
**`sys/scsiio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 55 code lines + 4 commented lines; 75 total lines | .
`sys/select.h` | C header | Modified-BSD | OK | .
`sys/selinfo.h` | C header | Modified-BSD | OK | .
**`sys/sem.h`** | C header | __Nonfree-License!!!__ (Daniel Boulet; copyright notice only) | __NonFree!!!__ | .
`sys/sensors.h` | C header | Simplified-BSD | OK | .
`sys/shm.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/siginfo.h` | C header | Simplified-BSD | OK | .
`sys/sigio.h` | C header | Modified-BSD | OK | .
`sys/signal.h` | C header | Modified-BSD | OK | .
`sys/signalvar.h` | C header | Modified-BSD | OK | .
`sys/smr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/socket.h` | C header | Modified-BSD | OK | .
`sys/socketvar.h` | C header | Modified-BSD | OK | .
`sys/sockio.h` | C header | Modified-BSD | OK | .
`sys/specdev.h` | C header | Modified-BSD | OK | .
`sys/srp.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/stacktrace.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/stat.h` | C header | Modified-BSD | OK | .
`sys/statvfs.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/stdarg.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/stdint.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/swap.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
**`sys/syscallargs.h`** | C header (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
**`sys/syscall.h`** | C header (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
`sys/syscall_mi.h` | C header | Modified-BSD | OK | .
`sys/sysctl.h` | C header | Modified-BSD | OK | .
`sys/syslimits.h` | C header | Modified-BSD | OK | .
`sys/syslog.h` | C header | Modified-BSD | OK | .
`sys/systm.h` | C header | Modified-BSD | OK | .
`sys/task.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/termios.h` | C header | Modified-BSD | OK | .
`sys/_time.h` | C header | Modified-BSD | OK | .
`sys/time.h` | C header | Modified-BSD | OK | .
`sys/timeout.h` | C header | Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK | .
`sys/times.h` | C header | Modified-BSD | OK | .
`sys/timetc.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/tprintf.h` | C header | Modified-BSD | OK | .
`sys/tracepoint.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/tree.h` | C header | Simplified-BSD | OK | .
`sys/ttycom.h` | C header | Modified-BSD | OK | .
`sys/ttydefaults.h` | C header | Modified-BSD | OK | .
`sys/tty.h` | C header | Modified-BSD | OK | .
`sys/_types.h` | C header | Modified-BSD | OK | .
`sys/types.h` | C header | Modified-BSD | OK | .
`sys/ucred.h` | C header | Modified-BSD | OK | .
`sys/uio.h` | C header | Modified-BSD | OK | .
`sys/un.h` | C header | Modified-BSD | OK | .
`sys/unistd.h` | C header | Modified-BSD | OK | .
`sys/unpcb.h` | C header | Modified-BSD | OK | .
`sys/user.h` | C header | Modified-BSD | OK | .
`sys/utsname.h` | C header | Modified-BSD | OK | .
`sys/uuid.h` | C header | Simplified-BSD | OK | .
`sys/varargs.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`sys/videoio.h` | C header | Modified-BSD or GPL-2 | OK | .
`sys/vmmeter.h` | C header | Modified-BSD | OK | .
`sys/vnode.h` | C header | Modified-BSD | OK | .
`sys/wait.h` | C header | Modified-BSD | OK | .
`sys/witness.h` | C header | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`tmpfs/tmpfs_fifoops.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs.h` | C header | Simplified-BSD | OK | .
`tmpfs/tmpfs_mem.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs_specops.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs_subr.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs_vfsops.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs_vnops.c` | C code | Simplified-BSD | OK | .
`tmpfs/tmpfs_vnops.h` | C header | Simplified-BSD | OK | .
`ufs/ext2fs/ext2fs_alloc.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_balloc.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_bmap.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_bswap.c` | C code | Original-BSD | OK | .
`ufs/ext2fs/ext2fs_dinode.h` | C header | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_dir.h` | C header | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_extents.c` | C code | Simplified-BSD | OK | .
`ufs/ext2fs/ext2fs_extents.h` | C header | Simplified-BSD | OK | .
`ufs/ext2fs/ext2fs_extern.h` | C header | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs.h` | C header | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_inode.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_lookup.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_readwrite.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_subr.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_vfsops.c` | C code | Modified-BSD | OK | .
`ufs/ext2fs/ext2fs_vnops.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_alloc.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_balloc.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_extern.h` | C header | Modified-BSD | OK | .
`ufs/ffs/ffs_inode.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_softdep.c` | C code | Simplified-BSD | OK | .
`ufs/ffs/ffs_softdep_stub.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK | .
`ufs/ffs/ffs_subr.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_tables.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_vfsops.c` | C code | Modified-BSD | OK | .
`ufs/ffs/ffs_vnops.c` | C code | Modified-BSD | OK | .
`ufs/ffs/fs.h` | C header | Modified-BSD | OK | .
`ufs/ffs/softdep.h` | C header | Simplified-BSD | OK | .
`ufs/mfs/mfs_extern.h` | C header | Modified-BSD | OK | .
`ufs/mfs/mfsnode.h` | C header | Modified-BSD | OK | .
`ufs/mfs/mfs_vfsops.c` | C code | Modified-BSD | OK | .
`ufs/mfs/mfs_vnops.c` | C code | Modified-BSD | OK | .
`ufs/ufs/dinode.h` | C header | Modified-BSD | OK | .
`ufs/ufs/dirhash.h` | C header | Simplified-BSD | OK | .
`ufs/ufs/dir.h` | C header | Modified-BSD | OK | .
`ufs/ufs/inode.h` | C header | Modified-BSD | OK | .
`ufs/ufs/quota.h` | C header | Modified-BSD | OK | .
`ufs/ufs/ufs_bmap.c` | C code | Modified-BSD | OK | .
`ufs/ufs/ufs_dirhash.c` | C code | Simplified-BSD | OK | .
`ufs/ufs/ufs_extern.h` | C header | Modified-BSD | OK | .
`ufs/ufs/ufs_ihash.c` | C code | Modified-BSD | OK | .
`ufs/ufs/ufs_inode.c` | C code | Modified-BSD | OK | .
`ufs/ufs/ufs_lookup.c` | C code | Modified-BSD | OK | .
`ufs/ufs/ufsmount.h` | C header | Modified-BSD | OK | .
`ufs/ufs/ufs_quota.c` | C code | Modified-BSD | OK | .
**`ufs/ufs/ufs_quota_stub.c`** | C code | __Unlicensed!!!__ | Non-Trivial: it contains 1 macro condiction, 10 function definitions and comment; 56 code lines + 1 commented line; 57 total lines | but it is easily replaceable, because it contains function definitions with only return zero, only return EOPNOTSUP or none in the function body;
`ufs/ufs/ufs_vfsops.c` | C code | Modified-BSD | OK | .
`ufs/ufs/ufs_vnops.c` | C code | Modified-BSD | OK | .
`uvm/uvm_addr.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`uvm/uvm_addr.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`uvm/uvm_amap.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_amap.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_anon.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_anon.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_aobj.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_aobj.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_ddb.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_device.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_device.h` | C header | Simplified-BSD | OK | .
**`uvm/uvmexp.h`** | C header | __Unlicensed!!!__ | Non-Trivial: it contains 2 macro condictions, 13 macro integer variables, 1 macro structure variable, 3 structure definitions, 1 structure declaration and comments; 149 code lines + 29 commented lines; 178 total lines | .
`uvm/uvm_extern.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`uvm/uvm_fault.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_fault.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_glue.c` | C code | Modified-BSD + CMU | OK | .
`uvm/uvm_glue.h` | C header | Simplified-BSD | OK | .
`uvm/uvm.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_init.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_io.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_km.c` | C code | Modified-BSD + CMU | OK | .
`uvm/uvm_km.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_map.c` | C code | Original-ISC ("and" is used instead of "and/or") + Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_map.h` | C header | Original-ISC ("and" is used instead of "and/or") + Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_meter.c` | C code | Modified-BSD | OK | .
`uvm/uvm_mmap.c` | C code | Original-BSD | OK | .
`uvm/uvm_object.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_object.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_page.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_page.h` | C header | Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_pager.c` | C code | Simplified-BSD | OK | .
`uvm/uvm_pager.h` | C header | Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`uvm/uvm_param.h` | C header | Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_pdaemon.c` | C code | Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_pmap.h` | C header | Modified-BSD + CMU | OK (Combined Licenses) | .
`uvm/uvm_pmemrange.c` | C code | Original-ISC ("and" is used instead of "and/or") | OK | .
`uvm/uvm_pmemrange.h` | C header | Original-ISC ("and" is used instead of "and/or") | OK | .
`uvm/uvm_swap.c` | C code | Simplified-BSD | OK | .
**`uvm/uvm_swap_encrypt.c`** | C code | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 variable declaration, 5 variable definitions, 7 function definitions and comments; 138 code lines + 60 commented lines; 198 total lines | .
**`uvm/uvm_swap_encrypt.h`** | C header | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 macro condiction, 6 macro integer variables, 1 macro structure variable, 2 macro functions, 1 structure definition, 7 function declarations, 4 variable declarations and comments; 42 code lines + 30 commented lines; 72 total lines | .
`uvm/uvm_swap.h` | C header | Simplified-BSD | OK | .
`uvm/uvm_unix.c` | C code | Modified-BSD | OK | .
`uvm/uvm_vnode.c` | C code | Modified-BSD | OK | .
**`uvm/uvm_vnode.h`** | C header | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 macro condiction, 1 structure definition, 1 structure declaration, 2 macro function calls, 12 macro integer variables and comments; 25 code lines + 59 commented lines; 84 total lines | .
---

## hyperbk-file-list.md - HyperBK file list for Todo list

Written in 2022 by **[M&aacute;rcio Silva][COADDE]** <coadde@hyperbola.info>
<br/>Written in 2022 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.info>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[COADDE]: https://www.hyperbola.info/members/founders/#coadde
[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
