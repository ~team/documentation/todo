File name | File type | License(s) | Description(s) / Status | Authors and Emails | Origin of The File
---|---|---|---|---|---
`arch/alpha/alpha/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | Copyright (c) 1992, 1993 The Regents of the University of California, Lawrence Berkeley Laboratory. | .
`arch/alpha/alpha/cpuconf.c` | C code | Modified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | Christopher G. Demetriou | .
`arch/alpha/alpha/db_instruction.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU | OK | Christopher G. Demetriou | .
`arch/alpha/alpha/fp_complete.c` | C code | Original-BSD | OK | Ross Harvey | .
`arch/alpha/alpha/process_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | Christopher G. Demetriou | .
`arch/alpha/alpha/trap.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | Christopher G. Demetriou | .
**`arch/alpha/conf/Makefile.alpha`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 106 code lines + 20 commented lines; 163 total lines | .
`arch/alpha/include/cpuconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/include/fpu.h` | C header | Original-BSD | OK | .
`arch/alpha/include/ieee.h` | C header | Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/alpha/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 32 code lines + 1 commented line; 38 total lines | .
`arch/alpha/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-Original-BSD (Original-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
**`arch/alpha/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 5 commented lines; 51 total lines | .
`arch/alpha/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsp_bus_io.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsp_bus_mem.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsp_dma.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/alpha/pci/tsp_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/pci/tsvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/boot/filesystem.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 4 commented lines; 63 total lines | .
**`arch/alpha/stand/boot/version`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 21 data lines + 2 commented lines; 30 total lines | .
**`arch/alpha/stand/bootxx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 2 commented lines; 43 total lines | .
`arch/alpha/stand/installboot.8` | troff (manual) | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/installboot.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/stand/libsa/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 code lines + 4 commented lines; 40 total lines | .
**`arch/alpha/stand/libz/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 46 code lines + 16 commented lines; 72 total lines | .
**`arch/alpha/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 11 code lines + 8 commented line; 27 total lines | .
**`arch/alpha/stand/netboot/conf.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 2 commented lines; 27 total lines | .
`arch/alpha/stand/netboot/dev_net.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause", four clause is the advertising) | OK | .
**`arch/alpha/stand/netboot/getsecs.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 35 total lines | .
`arch/alpha/stand/netboot/if_prom.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/alpha/stand/netboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 4 commented lines; 73 total lines | .
`arch/alpha/stand/setnetbootinfo/setnetbootinfo.8` | troff (manual) | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/alpha/stand/setnetbootinfo/setnetbootinfo.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/bus_dma.c` | C code | Simplified-BSD + Custom-Original-BSD (Original-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/amd64/amd64/copy.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/cpu.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/i8259.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/identcpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/intr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/ioapic.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/mainbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/amd64/mem.c` | C code | Original-BSD | OK | .
`arch/amd64/amd64/mpbios.c` | C code | Simplified-BSD + Original-BSD + Custom-BSD (similar to Simplified-BSD but with the "non-endorsement clause" and without the "binary clause") | OK (Combined Licenses) | .
`arch/amd64/amd64/mptramp.S` | Assembler code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/pmap.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/amd64/amd64/spl.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/amd64/amd64/vector.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK | .
**`arch/amd64/conf/Makefile.amd64`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 202 code lines + 20 commented lines; 266 total lines | .
`arch/amd64/include/atomic.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`arch/amd64/include/cacheinfo.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 43 commented lines; 112 total lines | .
`arch/amd64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/cpuvar.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/amd64/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/amd64/include/fpu.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 66 code lines + 15 commented lines ; 100 total lines | .
**`arch/amd64/include/frameasm.h`** | Assembler header | __Unlicensed!!!__ | Non-Trivial; 138 commented lines + 42 commented lines; 198 total lines | .
`arch/amd64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/amd64/include/intrdefs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 47 code lines + 32 commented lines ; 93 total lines | .
**`arch/amd64/include/mpconfig.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 35 code lines + 6 commented lines; 50 total lines | .
`arch/amd64/include/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/amd64/include/pctr.h`** | C header | __Nonfree-License!!!__ | (David Mazieres; it does not have the "use" permission in the license) | .
**`arch/amd64/include/pic.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 29 code lines + 8 commented lines; 45 total lines | .
`arch/amd64/include/pmap.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/amd64/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 12 code lines + 7 commented lines; 23 total lines | .
**`arch/amd64/include/sysarch.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 29 total lines | .
`arch/amd64/include/tss.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/amd64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines (1 long line) + 5 commented lines; 51 total lines | .
**`arch/amd64/pci/aapic.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 58 code lines +  5 commented lines; 82 total lines | .
`arch/amd64/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/pci/pci_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`arch/amd64/stand/biosboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 35 total lines | .
**`arch/amd64/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 71 code lines + 2 commented lines; 89 total lines | .
**`arch/amd64/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 59 code lines + 1 commented line; 74 total lines | .
**`arch/amd64/stand/cdbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 3 commented lines; 36 total lines | .
**`arch/amd64/stand/efi32/ldscript.i386`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 76 total lines | .
**`arch/amd64/stand/efi32/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
**`arch/amd64/stand/efi64/ldscript.amd64`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 66 total lines | .
**`arch/amd64/stand/efi64/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
**`arch/amd64/stand/efiboot/ldscript.amd64`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 66 total lines | .
**`arch/amd64/stand/efiboot/ldscript.i386`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 76 total lines | .
**`arch/amd64/stand/efiboot/Makefile.common`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 1 commented line; 80 total lines | .
**`arch/amd64/stand/etc/genassym.cf`** | C code generator | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 31 total lines [False-Positive: this file is used by "kern/genassym.sh" to generate C code files; "Simplified-BSD"] | .
`arch/amd64/stand/libsa/pxeboot.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/stand/libsa/pxe_call.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/amd64/stand/libsa/pxe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
**`arch/amd64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 11 commented lines; 46 total lines | .
**`arch/amd64/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 code lines + 5 commented lines; 36 total lines | .
**`arch/amd64/stand/pxeboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 79 total lines | .
`arch/amd64/stand/pxeboot/pxe_udp.c` | C code | Original-BSD | OK | .
`arch/arm64/arm64/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm64/arm64/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/arm64/arm64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm64/arm64/sig_machdep.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm64/arm64/sys_machdep.c` | C code | Original-BSD | OK | .
**`arch/arm64/conf/files.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 163 non empty lines; 212 total lines | .
**`arch/arm64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 472 non empty lines; 508 total lines | .
**`arch/arm64/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 81 non empty lines; 88 total lines | .
**`arch/arm64/conf/Makefile.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 157 non empty lines; 192 total lines | .
**`arch/arm64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 307 non empty lines; 340 total lines | .
`arch/arm64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm64/include/_float.h` | C header | Original-BSD (2X advertising clause, above and 3rd clause) | OK | .
`arch/arm64/include/float.h` | C header | Original-BSD (2X advertising clause, above and 3rd clause) | OK | .
`arch/arm64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/arm64/include/machdep.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 27 total lines | .
`arch/arm64/include/openpromio.h` | C header | Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/arm64/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 106 non empty lines; 123 total lines | .
**`arch/arm64/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 75 non empty lines; 83 total lines | .
**`arch/arm64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/arm64/stand/efiboot/acpi.dts`** | Device Tree Source File | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 56 total lines | .
**`arch/arm64/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
**`arch/arm64/stand/efiboot/dt_blob.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 380 non empty lines; 381 total lines | .
**`arch/arm64/stand/efiboot/ldscript.arm64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 79 non empty lines; 82 total lines | .
**`arch/arm64/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 71 non empty lines; 89 total lines | .
`arch/arm/arm/arm32_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/arm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/arm/arm/ast.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bcopyinout.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bcopy_page.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/bus_space_asm_generic.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/bus_space_notimpl.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/conf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/copystr.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpu.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpufunc_asm.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpufunc.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/cpuswitch7.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/db_disasm.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/disassem.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/disassem.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/exception.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/fault.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/fiq.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/fiq_subr.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/in_cksum_arm.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/irq_dispatch.S` | Assembler code | Modified-BSD (with differents names for copyrights) + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/arm/arm/pmap7.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non endorsement clause") + Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/arm/setstack.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/sigcode.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/sig_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/stubs.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/sys_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/undefined.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/armv7/armv7_space.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/arm/armv7/bus_space_asm_armv7.S` | Assembler code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/arm/vectors.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/arm/vm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/arm/conf/files.arm`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 88 non empty lines; 105 total lines | .
**`arch/arm/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 81 non empty lines; 88 total lines | .
`arch/arm/cortex/cortex.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/cortex/cortex.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/arm/cortex/files.cortex`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 24 total lines | .
`arch/arm/include/armreg.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/bus.h` | C header | Simplified-BSD with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/cpuconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/cpufunc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/fiq.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/_float.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
`arch/arm/include/fp.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/ieee.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
**`arch/arm/include/machdep.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 27 total lines | .
`arch/arm/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | ok | .
`arch/arm/include/param.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/pcb.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/arm/include/proc.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/profile.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/arm/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 47 non empty lines; 53 total lines | .
**`arch/arm/include/setjmp.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 59 non empty lines; 67 total lines | .
`arch/arm/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/softintr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/arm/include/sysarch.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/undefined.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/arm/include/vmparam.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/armv7/armv7/armv7_machdep.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non endorsement clause") | OK (Combined Licenses) | .
`arch/armv7/armv7/autoconf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/armv7/conf/files.armv7`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 58 non empty lines; 78 total lines | .
**`arch/armv7/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 380 non empty lines; 416 total lines | .
**`arch/armv7/conf/Makefile.armv7`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 134 non empty lines; 170 total lines | .
**`arch/armv7/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 262 non empty lines; 297 total lines | .
**`arch/armv7/exynos/files.exynos`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 46 total lines | .
`arch/armv7/include/bootconfig.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/armv7/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/armv7/include/param.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/armv7/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/armv7/marvell/files.marvell`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 28 non empty lines; 37 total lines | .
**`arch/armv7/omap/files.omap`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 76 non empty lines; 99 total lines | .
**`arch/armv7/omap/omehcivar.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 203 non empty lines; 232 total lines | .
**`arch/armv7/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
**`arch/armv7/stand/efiboot/ldscript.arm`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 62 non empty lines; 63 total lines | .
**`arch/armv7/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 67 non empty lines; 84 total lines | .
**`arch/hppa/conf/files.hppa`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 285 non empty lines; 350 total lines | .
**`arch/hppa/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 328 non empty lines; 361 total lines | .
**`arch/hppa/conf/Makefile.hppa`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 137 non empty lines; 174 total lines | .
**`arch/hppa/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 187 non empty lines; 216 total lines | .
**`arch/hppa/dev/cpudevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 353 non empty lines; 354 total lines | .
**`arch/hppa/dev/cpudevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 372 non empty lines; 373 total lines | .
`arch/hppa/gsc/gsckbc.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`arch/hppa/gsc/lpt_gsc.c`** | C code | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains restrictions for commercial use] | .
`arch/hppa/hppa/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/hppa/include/cpufunc.h`** | C header | Simplified-BSD + Hewlett-Packard (similar to Original-ISC but without endorsement clause) + __Nonfree-License!!!__ (CSL disclaimer only) | __Nonfree!!!__ (CSL disclaimer only) | .
`arch/hppa/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/hppa/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/hppa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/hppa/stand/boot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 49 total lines | .
**`arch/hppa/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 64 total lines | .
**`arch/hppa/stand/cdboot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 49 total lines | .
**`arch/hppa/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 66 total lines | .
**`arch/hppa/stand/libsa/dev_hppa.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 42 total lines | .
**`arch/hppa/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 44 total lines | .
**`arch/hppa/stand/libsa/pdc.c`** | C code | Simplified-BSD + OSF (similar to ISC and CMU) + __Nonfree-License!!!__ (Custom-BSD license) T2 | __Nonfree!!!__ [License contains distribution only] | .
**`arch/hppa/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 37 non empty lines; 41 total lines | .
**`arch/i386/conf/Makefile.i386`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 131 code lines + 20 commented lines; 191 total lines | .
`arch/i386/eisa/eisa_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/bus_space.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/i386/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/cpu.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/mainbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/i386/mptramp.s` | Assembler code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/i386/vector.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/cpufunc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/cpuvar.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/i386/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/i386/include/intrdefs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 62 commented line ; 131 total lines | .
`arch/i386/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/i386/include/joystick.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 2 commented lines ; 24 total lines | .
**`arch/i386/include/pic.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 8 commented lines; 43 total lines | .
`arch/i386/include/pio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/i386/include/sysarch.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 5 commented lines; 39 total lines | .
`arch/i386/isa/ahc_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/icu.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/isa/isapnp_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/isa/isapnp_machdep.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`arch/i386/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 5 commented lines; 51 total lines | .
`arch/i386/pci/pciide_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/pci_machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/i386/pci/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/i386/stand/biosboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 code lines + 2 commented lines; 34 total lines | .
**`arch/i386/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 2 commented lines; 94 total lines | .
**`arch/i386/stand/cdboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 62 code lines + 3 commented lines; 81 total lines | .
**`arch/i386/stand/cdbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 23 code lines + 3 commented lines; 33 total lines | .
**`arch/i386/stand/etc/genassym.cf`** | C code generator | __Unlicensed!!!__ | Non-Trivial; 26 code lines + 1 commented line; 31 total lines | .
`arch/i386/stand/libsa/pxeboot.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/i386/stand/libsa/pxe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/i386/stand/libsa/pxe_call.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/i386/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 code lines + 11 commented lines; 46 total lines | .
**`arch/i386/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 5 commented lines; 34 total lines | .
**`arch/i386/stand/pxeboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 61 code lines + 3 commented lines; 80 total lines | .
`arch/i386/stand/pxeboot/pxe_udp.c` | C code | Original-BSD | OK | .
**`arch/landisk/conf/files.landisk`** | ? | __Unlicensed!!!__ | Non-Trivial; 77 non empty lines; 98 total lines | .
**`arch/landisk/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 162 non empty lines; 186 total lines | .
**`arch/landisk/conf/Makefile.landisk`** | ? | __Unlicensed!!!__ | Non-Trivial; 128 non empty lines; 163 total lines | .
**`arch/landisk/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 78 non empty lines; 97 total lines | .
`arch/landisk/dev/obiovar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/landisk/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/landisk/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/landisk/include/cpu.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 26 total lines | .
`arch/landisk/include/pci_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/landisk/landisk/conf.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/landisk/landisk/consinit.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/landisk/landisk/shpcic_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/landisk/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/landisk/stand/boot/getsecs.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 168 non empty lines; 206 total lines | .
**`arch/landisk/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 42 non empty lines; 54 total lines | .
**`arch/landisk/stand/mbr/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 34 total lines | .
**`arch/landisk/stand/xxboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 54 total lines | .
**`arch/loongson/conf/files.loongson`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 114 non empty lines; 140 total lines | .
**`arch/loongson/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 226 non empty lines; 249 total lines | .
**`arch/loongson/conf/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 55 non empty lines; 61 total lines | .
**`arch/loongson/conf/Makefile.loongson`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 143 non empty lines; 180 total lines | .
**`arch/loongson/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 134 non empty lines; 155 total lines | .
**`arch/loongson/dev/bonitoreg.h`** | C header | __Nonfree-License!!!__ (Custom license) Bonito Register Map | __Nonfree!!!__ [License contains use and modification only] | .
`arch/loongson/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/loongson/loongson/conf.c` | C code | Original-BSD | OK | .
`arch/loongson/loongson/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/loongson/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/loongson/stand/boot/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 27 total lines | .
**`arch/loongson/stand/boot/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 44 total lines | .
**`arch/loongson/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 50 total lines | .
**`arch/loongson/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 29 non empty lines; 38 total lines | .
`arch/luna88k/cbus/i82365_cbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/luna88k/conf/files.luna88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 88 non empty lines; 111 total lines | .
**`arch/luna88k/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 78 non empty lines; 97 total lines | .
**`arch/luna88k/conf/Makefile.luna88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 131 non empty lines; 166 total lines | .
**`arch/luna88k/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 34 non empty lines; 45 total lines | .
`arch/luna88k/dev/mb89352.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") without disclaimer + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`arch/luna88k/dev/mb89352var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") without disclaimer + Custom-Modified-BSD (third clause is a "non-endorsement clause" T3) | OK (Combined Licenses) | .
`arch/luna88k/include/autoconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/luna88k/autoconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/luna88k/luna88k/clock.c` | C code | Original-BSD | OK | .
`arch/luna88k/luna88k/disksubr.c` | C code | Original-BSD | OK | .
`arch/luna88k/luna88k/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/m8820x.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/luna88k/machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
**`arch/luna88k/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/luna88k/stand/boot/bmc.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/bmd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/boot.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/devopen.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/font.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/getline.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/init_main.c` | C code | Original-BSD + Modified-BSD + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/kbd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/kbdreg.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
**`arch/luna88k/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 58 non empty lines; 75 total lines | .
`arch/luna88k/stand/boot/parse.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/rcvbuf.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sc.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/scsivar.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sd.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sio.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/sioreg.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/status.h` | C header | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`arch/luna88k/stand/boot/ufs_disksubr.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
**`arch/m88k/conf/files.m88k`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 51 non empty lines; 55 total lines | .
`arch/m88k/include/cpu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/ieeefp.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Modified-BSD | OK (Combined Licenses) | .
`arch/m88k/include/proc.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/ptrace.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/m88k/include/reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/in_cksum.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/m88110_mmu.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/m8820x_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") with Added CR for hyperbola + Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
`arch/m88k/m88k/m88k_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/pmap.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/process.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/sig_machdep.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
`arch/m88k/m88k/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/m88k/m88k/trap.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Old-Expat (without disclaimer 2) | OK (Combined Licenses) | .
**`arch/macppc/conf/files.macppc`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 228 non empty lines; 296 total lines | .
**`arch/macppc/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 376 non empty lines; 413 total lines | .
**`arch/macppc/conf/Makefile.macppc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 144 non empty lines; 181 total lines | .
**`arch/macppc/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 179 non empty lines; 202 total lines | .
`arch/macppc/dev/adb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
`arch/macppc/dev/akbd_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/if_wi_obio.c` | C code | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
`arch/macppc/dev/macgpio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/pm_direct.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/pm_direct.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/viareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/dev/zs.c` | C code | Custom-Original-BSD (third clause is a "non endorsement clause" and four clause is the "advertising clause") T1 | OK | .
`arch/macppc/include/bus.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/macppc/include/param.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/vmparam.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/macppc/macppc/clock.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/cpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/locore0.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/locore.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/ofw_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/ofwreal.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/opendev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/openfirm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/macppc/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/macppc/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/macppc/pci/macobio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/pci/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/alloc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/macppc/stand/cache.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 18 non empty lines; 20 total lines | .
`arch/macppc/stand/Locore.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/main.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/macppc/stand/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 38 total lines | .
**`arch/macppc/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 26 non empty lines; 34 total lines | .
`arch/macppc/stand/net.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/netif_of.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/ofdev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/macppc/stand/ofdev.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/macppc/stand/ofwboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 46 total lines | .
`arch/macppc/stand/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/mips64/conf/files.mips64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 32 non empty lines; 38 total lines | .
`arch/mips64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/mips64/include/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/mips64/mips64/mem.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/process_machdep.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/sendsig.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`arch/mips64/mips64/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/mips64/mips64/sys_machdep.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/trap.c` | C code | Original-BSD | OK | .
`arch/mips64/mips64/vm_machdep.c` | C code | Original-BSD | OK | .
**`arch/octeon/conf/BOOT`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 60 non empty lines; 79 total lines | .
**`arch/octeon/conf/files.octeon`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 149 non empty lines; 188 total lines | .
**`arch/octeon/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 192 non empty lines; 216 total lines | .
**`arch/octeon/conf/ld.script`** | Linker script | __Unlicensed!!!__ | Non-Trivial; 59 non empty lines; 66 total lines | .
**`arch/octeon/conf/Makefile.octeon`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 152 non empty lines; 189 total lines | .
**`arch/octeon/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 110 non empty lines; 133 total lines | .
`arch/octeon/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/octeon/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/octeon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
**`arch/octeon/octeon/cn3xxx_dts.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 330 non empty lines; 331 total lines | .
`arch/octeon/octeon/conf.c` | C code | Original-BSD | OK | .
`arch/octeon/octeon/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/octeon/octeon/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/octeon/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 39 total lines | .
**`arch/octeon/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 51 total lines | .
**`arch/octeon/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 non empty lines; 45 total lines | .
**`arch/powerpc64/conf/BOOT`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 61 non empty lines; 73 total lines | .
**`arch/powerpc64/conf/files.powerpc64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 92 non empty lines; 119 total lines | .
**`arch/powerpc64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 182 non empty lines; 192 total lines | .
**`arch/powerpc64/conf/Makefile.powerpc64`** | ? | __Unlicensed!!!__ | Non-Trivial; 153 non empty lines; 189 total lines | .
**`arch/powerpc64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 63 non empty lines; 75 total lines | .
**`arch/powerpc64/include/conf.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 24 total lines | .
`arch/powerpc64/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/powerpc64/include/vmparam.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 non empty lines; 50 total lines | .
**`arch/powerpc64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 44 non empty lines; 51 total lines | .
`arch/powerpc64/powerpc64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc64/powerpc64/syncicache.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc64/powerpc64/sys_machdep.c` | C code | Original-BSD | OK | .
`arch/powerpc64/powerpc64/trap_subr.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/powerpc64/stand/boot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 30 non empty lines; 39 total lines | .
**`arch/powerpc64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 non empty lines; 44 total lines | .
**`arch/powerpc/conf/files.powerpc`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 41 non empty lines; 44 total lines | .
`arch/powerpc/include/asm.h` | Assembler header | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/powerpc/include/bat.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/cpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/fpu.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/frame.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/powerpc/include/intr.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/ipkdb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/limits.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/param.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pcb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/powerpc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/proc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/psl.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/pte.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/reloc.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/signal.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/include/trap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/isa/isa_machdep.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/bus_space.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/copystr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/fpu.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/in_cksum.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/intr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/process_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/softintr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/sys_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/trap.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/powerpc/powerpc/vm_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/riscv64/conf/files.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 116 non empty lines; 143 total lines | .
**`arch/riscv64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 155 non empty lines; 185 total lines | .
**`arch/riscv64/conf/kern.ldscript`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 76 non empty lines; 83 total lines | .
**`arch/riscv64/conf/Makefile.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 138 non empty lines; 173 total lines | .
**`arch/riscv64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 129 non empty lines; 156 total lines | .
`arch/riscv64/include/conf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/riscv64/include/_float.h` | C header | Original-BSD (Double advertising clause: above and 3rd clause) | OK | .
`arch/riscv64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/riscv64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/riscv64/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 19 total lines | .
**`arch/riscv64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/riscv64/riscv64/conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/riscv64/riscv64/db_instruction.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3407 non empty lines; 3409 total lines | .
`arch/riscv64/riscv64/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/riscv64/riscv64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/riscv64/riscv64/sig_machdep.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
**`arch/riscv64/stand/efiboot/disk.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
**`arch/riscv64/stand/efiboot/ldscript.riscv64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 83 non empty lines; 85 total lines | .
**`arch/riscv64/stand/efiboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 85 total lines | .
**`arch/sh/conf/files.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 52 non empty lines; 57 total lines | .
`arch/sh/dev/pciide_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sh/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") | OK | .
**`arch/sh/include/exec.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 30 total lines | .
`arch/sh/include/frame.h` | C header | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sh/include/limits.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sh/include/ptrace.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/sh/include/reloc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 106 non empty lines; 110 total lines | .
`arch/sh/sh/locore_c.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/mem.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/process_machdep.c` | C code | Old-Expat (with legal disclaimer 2) + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/sys_machdep.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/trap.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sh/sh/vm_machdep.c` | C code | Old-Expat (with legal disclaimer 2) + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
**`arch/sparc64/conf/files.sparc64`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 369 non empty lines; 476 total lines | .
**`arch/sparc64/conf/GENERIC`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 503 non empty lines; 562 total lines | .
**`arch/sparc64/conf/Makefile.sparc64`** | ? | __Unlicensed!!!__ | Non-Trivial; 135 non empty lines; 170 total lines | .
**`arch/sparc64/conf/RAMDISK`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 261 non empty lines; 293 total lines | .
**`arch/sparc64/conf/RAMDISKU1`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 72 non empty lines; 89 total lines | .
**`arch/sparc64/conf/RAMDISKU5`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 68 non empty lines; 90 total lines | .
`arch/sparc64/dev/fb.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/dev/fd.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`arch/sparc64/dev/iommureg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/dev/rtc.c` | C code | Custom-Original-BSD (Original-BSD with a 2nd advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/dev/sbus.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK (Combined Licenses) | .
`arch/sparc64/dev/sbusvar.h` | C header | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/dev/z8530kbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/fpu/fpu_add.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_arith.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/fpu/fpu_compare.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_div.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_emu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_explode.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_implode.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_mul.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_sqrt.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/fpu/fpu_subr.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/autoconf.h` | C header | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/include/bus.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`arch/sparc64/include/cpu.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/disklabel.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/sparc64/include/endian.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 62 non empty lines; 84 total lines | .
**`arch/sparc64/include/exec.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 90 non empty lines; 99 total lines | .
`arch/sparc64/include/fbvar.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/_float.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/frame.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/fsr.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/idprom.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/ieee.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/instr.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/openpromio.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/param.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-BSD (similar to Simplified-BSD but without the "binary clause") | OK (Combined Licenses) | .
`arch/sparc64/include/pcb.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/pmap.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/proc.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/profile.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/psl.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/ptrace.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/reg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/signal.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/sparc64.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/include/vmparam.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/include/z8530var.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`arch/sparc64/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 43 non empty lines; 50 total lines | .
`arch/sparc64/sparc64/autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) with added CR for Hyperbola | OK | 
`arch/sparc64/sparc64/cache.c` | C code | Custom-Original-BSD (Original-BSD with a 2nd advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/cache.h` | C header | Custom-Original-BSD (four clause is a "non endorsement clause") T with double authors | OK | .
`arch/sparc64/sparc64/clock.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for double authors | OK | .
`arch/sparc64/sparc64/conf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/cpu.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/db_disasm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/genassym.cf` | C code generator | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/in4_cksum.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/in_cksum.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/intr.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/intreg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/locore.s` | Assembler code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/machdep.c` | C code | Simplified-BSD + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`arch/sparc64/sparc64/ofw_machdep.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/openfirm.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/sparc64/openprom.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/sys_machdep.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/timerreg.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`arch/sparc64/sparc64/trap.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/sparc64/vm_machdep.c` | C code | Custom-Original-BSD (Original-BSD with double advertising clause: above and 3rd clause) T for multiples contribs | OK | .
`arch/sparc64/stand/bootblk/genfth.cf` | C code generator | Simplified-BSD + Original-BSD (Double advertising clause: above and 3rd clause) | OK (Combined Licenses) | .
**`arch/sparc64/stand/bootblk/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 40 non empty lines; 54 total lines | .
**`arch/sparc64/stand/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 33 non empty lines; 48 total lines | .
**`arch/sparc64/stand/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 32 non empty lines; 45 total lines | .
`arch/sparc64/stand/ofwboot/alloc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/boot.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/sparc64/stand/ofwboot/elf64_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") with added CR for Hyperbola | OK | .
`arch/sparc64/stand/ofwboot/Locore.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`arch/sparc64/stand/ofwboot/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 65 non empty lines; 85 total lines | .
`arch/sparc64/stand/ofwboot/net.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/netif_of.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/ofdev.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/ofdev.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`arch/sparc64/stand/ofwboot/srt0.s` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`conf/makegap.sh`** | Shell script | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 1 commented line; 72 total lines [this generates the "gap.link" (linker script) file; and with the "ld" command and the "gap.link" file, generate the "gap.o" (object code) file; most part of the "gap.link" code are included in this script] | .
`crypto/blf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`crypto/blf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`ddb/db_extern.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`ddb/db_interface.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/adb.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/akbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/akbdvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/ams.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/amsvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/adb/keyboard.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/audio_if.h` | C header | Original-BSD | OK | .
`dev/cardbus/if_ath_cardbus.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/cardbus/if_dc_cardbus.c` | C code | Original-BSD | OK | .
`dev/eisa/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs_data.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisadevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/eisavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/if_ep_eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/eisa/uha_eisa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/fdt/sxitwi.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/i2c/i2c_bitbang.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_bitbang.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2c_io.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/i2c/i2cvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/ad1848reg.h` | C header | Original-BSD | OK | .
`dev/ic/aic6250.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6250var.h` | C header | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/aic6360var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/am7930.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/am79900.c` | C code | Simplified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/an.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`dev/ic/anreg.h` | C header | Original-BSD | OK | .
`dev/ic/anvar.h` | C header | Original-BSD | OK | .
`dev/ic/bt8xx.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/comvar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/dc.c` | C code | Original-BSD | OK | .
`dev/ic/dcreg.h` | C header | Original-BSD | OK | .
`dev/ic/elink3.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/ic/i8042reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 43 code lines + 2 commented lines; 52 total lines | .
`dev/ic/i82365.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i82365reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i82365var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/i82586reg.h` | C header | Original-BSD | OK | .
`dev/ic/i82596.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/i82596var.h` | C header | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/if_wi.c` | C code | Original-BSD | OK | .
`dev/ic/if_wi_hostap.c` | C code | Original-BSD | OK | .
`dev/ic/if_wi_hostap.h` | C header | Original-BSD | OK | .
`dev/ic/if_wi_ieee.h` | C header | Original-BSD | OK | .
`dev/ic/if_wireg.h` | C header | Original-BSD | OK | .
`dev/ic/if_wivar.h` | C header | Original-BSD | OK | .
`dev/ic/lpt.c` | C code | Original-BSD | OK | .
`dev/ic/lptvar.h` | C header | Original-BSD | OK | .
`dev/ic/mc6845.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/ncr53c9x.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Simplified-BSD | OK (Combined Licenses) | .
**`dev/ic/pcdisplay.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 30 code lines + 9 commented line; 49 total lines | .
`dev/ic/re.c` | C code | Original-BSD | OK | .
`dev/ic/rtl81x9.c` | C code | Original-BSD | OK | .
`dev/ic/rtl81x9reg.h` | C header | Original-BSD | OK | .
`dev/ic/smc91cxx.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/ic/smc91cxxreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/smc93cx6.c` | C code | Custom-Original-BSD (third and four clauses are custom clauses) | OK | .
`dev/ic/tcic2.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/tcic2var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/ti.c` | C code | Original-BSD | OK | .
`dev/ic/tireg.h` | C header | Original-BSD | OK | .
`dev/ic/uha.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/uhareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/uhavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/ic/wd33c93.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/wd33c93reg.h` | C header | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`dev/ic/xl.c` | C code | Original-BSD | OK | .
`dev/ic/xlreg.h` | C header | Original-BSD | OK | .
`dev/ic/z8530tty.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/isa/ad1848.c` | C code | Original-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/isa/ad1848var.h` | C header | Original-BSD | OK | .
`dev/isa/addcom_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/aic_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Modified-BSD (third clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/isa/ast.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/boca.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/elink.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/elink.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isasubr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/i82365_isavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_eg.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_egreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_ep_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/if_ep_isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/isa/if_ie507.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 15 code lines + 5 commented lines; 24 total lines | .
**`dev/isa/if_ieatt.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 27 total lines | .
`dev/isa/if_ie.c` | C code | Original-BSD | OK | .
`dev/isa/if_iee16.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isa.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`dev/isa/isadmareg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 13 code lines + 8 commented lines; 27 total lines | .
`dev/isa/isapnp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpdebug.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isapnpres.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/isavar.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
**`dev/isa/mpu_isapnp.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 1 commented line; 73 total lines | .
`dev/isa/pas.c` | C code | Original-BSD | OK | .
**`dev/isa/pasreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 193 code lines + 34 commented lines; 256 total lines | .
**`dev/isa/pnpdevs`** | AWK script | __Unlicensed!!!__ | Non-Trivial; 96 code lines + 364 commented lines; 487 total lines | .
**`dev/isa/pnpdevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 98 code lines + 6 commented lines; 106 total lines [False-Positive: this file is generated by "dev/isa/pnpdevs.h"] | .
`dev/isa/sb.c` | C code | Original-BSD | OK | .
`dev/isa/sbdsp.c` | C code | Original-BSD | OK | .
`dev/isa/sbdspvar.h` | C code | Original-BSD | OK | .
`dev/isa/sb_isa.c` | C code | Original-BSD | OK | .
`dev/isa/sb_isapnp.c` | C code | Original-BSD | OK | .
`dev/isa/sbreg.h` | C header | Original-BSD | OK | .
`dev/isa/sbvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/spkr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/tcic2_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/uha_isa.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/isa/wds.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/isa/wdsreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 119 code lines + 13 commented lines; 154 total lines | .
**`dev/microcode/afb/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 30 total lines | .
**`dev/microcode/aic7xxx/aic79xx_reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3131 code lines + 10 commented lines; 3825 total lines [False-Positive: this file is generated by "aic79xx.seq" and "aic79xx.reg"] | .
**`dev/microcode/aic7xxx/aic79xx_seq.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1131 code lines + 8 commented lines; 1191 total lines [False-Positive: this file is generated by "aic79xx.seq" and "aic79xx.reg"] | .
**`dev/microcode/aic7xxx/aic7xxx_reg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1471 code lines + 10 commented lines; 1789 total lines [False-Positive: this file is generated by "aic7xxx.seq" and "aic7xxx.reg"] | .
**`dev/microcode/aic7xxx/aic7xxx_seq.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 1251 code lines + 8 commented lines; 1310 total lines [False-Positive: this file is generated by "aic7xxx.seq" and "aic7xxx.reg"] | .
**`dev/microcode/aic7xxx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 48 code lines + 3 commented lines; 65 total lines | .
**`dev/microcode/atmel/atmel_at76c503_i3863_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c503_rfmd2_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c503_rfmd_acc_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_at76c505_rfmd.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_intersil_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd2958_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd2958-smc_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/atmel_rfmd_fw.h`** | C header | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
`dev/microcode/atmel/atu-license` | License file | __Nonfree-License!!!__ (Custom-BSD license) | __Nonfree!!!__ [License contains clause which allows restribution of firmware in object code only] | .
**`dev/microcode/atmel/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 31 non empty lines; 41 total lines | .
**`dev/microcode/bnx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
**`dev/microcode/cirruslogic/cs4280_image.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3472 non empty lines; 3474 total lines | .
**`dev/microcode/cirruslogic/cs4280-license`** | License file | __Nonfree!!!__ | __Nonfree!!!__ [License contains information where it says this license needs to be solved by someone finding the right person at Crystal Semiconductor (reason: no copyright notice)] | .
**`dev/microcode/cirruslogic/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 20 non empty lines; 30 total lines | .
`dev/microcode/cyclades/cyzfirm2h.c` | C code | Original-BSD | OK | .
`dev/microcode/cyclades/cyzfirm.h` | C header | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 2 commented lines ; 31 total lines [False-Positive???: this file is generated by "ZLOGIC.CYZ", however the source code is unknown] | .
**`dev/microcode/fxp/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
`dev/microcode/kue/kue_fw.h` | C header | Original-BSD | OK | .
`dev/microcode/kue/kue-license` | License file | Original-BSD | OK | .
**`dev/microcode/kue/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
**`dev/microcode/myx/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 24 non empty lines; 35 total lines | .
`dev/microcode/ncr53cxxx/ncr53cxxx.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/microcode/neomagic/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 19 non empty lines; 29 total lines | .
**`dev/microcode/ral/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 36 total lines | .
**`dev/microcode/rum/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 27 non empty lines; 38 total lines | .
**`dev/microcode/siop/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 14 code lines + 2 commented lines; 24 total lines | .
`dev/microcode/siop/ncr53cxxx.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/microcode/siop/oosiop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 5 commented lines; 80 total lines [False-Positive: this file is generated by "oosiop.ss"] | .
**`dev/microcode/siop/osiop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 175 code lines + 5 commented lines; 182 total lines [False-Positive: this file is generated by "osiop.ss"] | .
`dev/microcode/siop/osiop.ss` | Assembler/C generator header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/microcode/siop/siop.out`** | C header | __Unlicensed!!!__ | Non-Trivial; 397 code lines + 5 commented lines; 419 total lines [False-Positive: this file is generated by "siop.ss"] | .
**`dev/microcode/symbol/eprimsym`** | Binary file | __Unlicensed!!!__ | __Nonfree!!!__ [This file does not include source code] | .
**`dev/microcode/symbol/esecsym`** | Binary file | __Unlicensed!!!__ | __Nonfree!!!__ [This file does not include source code] | .
**`dev/microcode/symbol/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 17 non empty lines; 21 total lines | .
**`dev/microcode/tht/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 33 total lines | .
`dev/microcode/tht/microcode.h` | C header | Original-BSD | OK | .
`dev/microcode/tht/tht-license` | License file | Original-BSD | OK | .
**`dev/microcode/tigon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 23 non empty lines; 33 total lines | .
**`dev/microcode/tigon/ti_fw2.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 4869 code lines + 12 commented lines; 4883 total lines | .
**`dev/microcode/tigon/ti_fw.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 4592 code lines + 7 commented lines; 4601 total lines | .
**`dev/microcode/tigon/tigon-license`** | License file | __Nonfree-License!!!__ | __Nonfree!!!__ [License file is vague and only says "We believe the original license on the source did specifically allow re-distribution."] | .
**`dev/microcode/tusb3410/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
**`dev/microcode/tusb3410/tusb3410.h`** | C header | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
**`dev/microcode/tusb3410/tusb3410-license`** | License file | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
**`dev/microcode/typhoon/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
**`dev/microcode/udl/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 21 code lines + 2 commented lines; 34 total lines | .
**`dev/microcode/udl/udl_huffman.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 65543 code lines + 11 commented lines; 65558 total lines [This file includes a note where says they don't know how to program generate the table. The algorythm could be hidden in the 4kB decompression table (udl_decomp_table) which is sent to the device at initialization time.] | .
**`dev/microcode/yds/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 22 non empty lines; 32 total lines | .
**`dev/microcode/yds/yds_hwmcode.h`** | C header | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains information where it says this license has free distribution, no restriction for their distribution, however it lacks the essential freedom 0 and 1, see https://www.gnu.org/philosophy/free-sw.html#four-freedoms for futher info] | .
**`dev/microcode/yds/yds-license`** | License file | __Nonfree-License!!!__ | __Nonfree!!!__ [License contains information where it says this license has free distribution, no restriction for their distribution, however it lacks the essential freedom 0 and 1, see https://www.gnu.org/philosophy/free-sw.html#four-freedoms for futher info] | .
**`dev/microcode/zydas/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 25 non empty lines; 35 total lines | .
`dev/mii/acphy.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/mii/acphyreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/mii/amphy.c` | C code | Original-BSD | OK | .
`dev/mii/amphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/brgphy.c` | C code | Original-BSD | OK | .
`dev/mii/brgphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/ciphy.c` | C code | Original-BSD | OK | .
`dev/mii/ciphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/dcphy.c` | C code | Original-BSD | OK | .
`dev/mii/devlist2h.awk` | AWK script | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/mii/mlphy.c` | C code | Original-BSD + Simplified-BSD + Simplified-BSD | OK (Combined Licenses) | .
`dev/mii/nsgphy.c` | C code | Original-BSD | OK | .
`dev/mii/nsgphyreg.h` | C header | Original-BSD | OK | .
`dev/mii/rgephy.c` | C code | Original-BSD | OK | .
`dev/mii/rgephyreg.h` | C header | Original-BSD | OK | .
`dev/mii/xmphy.c` | C code | Original-BSD | OK | .
`dev/mii/xmphyreg.h` | C header | Original-BSD | OK | .
`dev/mulaw.c` | C code | Original-BSD | OK | .
`dev/ofw/openfirm.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/onewire/onewiredevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 39 code lines + 7 commented lines; 50 total lines [False-Positive: this file is generated by "dev/onewire/onewiredevs"] | .
**`dev/onewire/onewiredevs.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 32 code lines + 7 commented lines; 42 total lines [False-Positive: this file is generated by "dev/onewire/onewiredevs"] | .
`dev/pci/bktr/bktr_audio.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_audio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_card.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_card.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_core.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_core.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_os.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_os.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_tuner.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/bktr/bktr_tuner.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4280.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4280reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4281.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cs4281reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/cz.c` | C code | Original-BSD | OK | .
`dev/pci/czreg.h` | C header | Original-BSD | OK | .
`dev/pci/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/pci/drm/amd/amdgpu/amdgpu_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 116 code lines + 19 commented lines; 139 total lines | .
**`dev/pci/drm/amd/display/dc/core/dc_link_dp.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 3840 non empty lines; 4515 total lines | .
**`dev/pci/drm/amd/display/dc/core/dc_link_hwss.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 462 code lines + 35 commented lines; 596 total lines | .
**`dev/pci/drm/amd/display/TODO`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 73 data lines + 3 commented lines; 111 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dcn_3_0_0_offset.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16593 code lines + 643 commented lines; 17881 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dcn_3_0_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 61291 code lines + 9117 commented lines; 70952 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dpcs_3_0_0_offset.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 503 code lines + 34 commented lines; 574 total lines | .
**`dev/pci/drm/amd/include/asic_reg/dcn/dpcs_3_0_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 3263 code lines + 267 commented lines; 3566 total lines | .
**`dev/pci/drm/amd/include/asic_reg/umc/umc_8_7_0_sh_mask.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 73 code lines + 4 commented lines; 80 total lines | .
**`dev/pci/drm/i915/gt/shaders/README`** | ASCII text | __Unlicensed!!!__ | Non-Trivial; 73 data lines + 2 commented lines; 111 total lines | .
**`dev/pci/drm/i915/i915_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 268 code lines + 1 commented lines; 270 total lines | .
**`dev/pci/drm/include/drm/drm_device.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 119 code lines + 217 commented lines; 411 total lines | .
**`dev/pci/drm/radeon/cayman_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 514 code lines + 0 commented line; 515 total lines | .
**`dev/pci/drm/radeon/evergreen_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 514 code lines + 0 commented line; 515 total lines | .
**`dev/pci/drm/radeon/r100_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
**`dev/pci/drm/radeon/r200_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
**`dev/pci/drm/radeon/r300_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 0 commented line; 43 total lines | .
**`dev/pci/drm/radeon/r420_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 42 code lines + 0 commented line; 43 total lines | .
**`dev/pci/drm/radeon/r600_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 490 code lines + 0 commented line; 491 total lines | .
**`dev/pci/drm/radeon/radeon_devlist.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 701 code lines + 1 commented line; 703 total lines | .
**`dev/pci/drm/radeon/rn50_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 28 code lines + 0 commented line; 29 total lines | .
**`dev/pci/drm/radeon/rs600_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 0 commented line; 58 total lines | .
**`dev/pci/drm/radeon/rv515_reg_safe.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 57 code lines + 0 commented line; 58 total lines | .
`dev/pci/i82365_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_an_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_bge.c` | C code | Original-BSD | OK | .
`dev/pci/if_bgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_dc_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_ep_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_lge.c` | C code | Original-BSD | OK | .
`dev/pci/if_lgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_msk.c` | C code | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_mskvar.h` | C header | Simplified-BSD + Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_nge.c` | C code | Original-BSD | OK | .
`dev/pci/if_ngereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_pcn.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_rl_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_se.c` | C code | Original-BSD | OK | .
`dev/pci/if_sereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_sis.c` | C code | Original-BSD | OK | .
`dev/pci/if_sisreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_sk.c` | C code | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_skreg.h` | C header | Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_skvar.h` | C header | Simplified-BSD + Original-BSD + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/pci/if_ste.c` | C code | Original-BSD | OK | .
`dev/pci/if_stereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_ti_pci.c` | C code | Original-BSD | OK | .
`dev/pci/if_tl.c` | C code | Original-BSD | OK | .
`dev/pci/if_tlreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_vge.c` | C code | Original-BSD | OK | .
`dev/pci/if_vgereg.h` | C header | Original-BSD | OK | .
`dev/pci/if_vgevar.h` | C header | Original-BSD | OK | .
`dev/pci/if_vr.c` | C code | Original-BSD | OK | .
`dev/pci/if_vrreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_wb.c` | C code | Original-BSD | OK | .
`dev/pci/if_wbreg.h` | C header | Original-BSD | OK | .
`dev/pci/if_xge.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_xgereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/if_xl_pci.c` | C code | Original-BSD | OK | .
`dev/pci/pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs_data.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcidevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_amd_reg.h` | C header | Original-BSD | OK | .
`dev/pci/pciide.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciidereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciide_sii3112_reg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pciidevar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pci_quirks.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcireg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pci_subr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pcivar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/plx9060reg.h` | C header | Original-BSD | OK | .
`dev/pci/plx9060var.h` | C header | Original-BSD | OK | .
`dev/pci/ppb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/ppbreg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/puc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pucdata.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/pucvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pci/vga_pci.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + CMU (similar to ISC) | OK (Combined Licenses) | .
**`dev/pckbc/pckbdreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 18 code lines + 7 commented lines; 29 total lines | .
**`dev/pckbc/pmsreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 153 code lines + 30 commented lines; 222 total lines | .
`dev/pcmcia/aic_pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/if_ep_pcmcia.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/pcmcia/if_ne_pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/if_wi_pcmcia.c` | C code | Original-BSD | OK | .
`dev/pcmcia/if_xe.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/if_xereg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmcia.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciachip.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmcia_cis.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmcia_cis_quirks.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciadevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciadevs.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciareg.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/pcmcia/pcmciavar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/puc/lpt_puc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/sdmmc/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/sun/z8530ms.c` | C code | Simplified-BSD + Original-BSD | OK (Combined Licenses) | .
**`dev/tc/ascvar.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code lines + 5 commented lines; 25 total lines | .
`dev/tc/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/tc/tcdevs` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/tc/tcdevs_data.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 179 code lines + 7 commented lines; 189 total lines | .
`dev/usb/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/usb/if_atu.c` | C code | Original-BSD | OK | .
`dev/usb/if_atureg.h` | C header | Original-BSD | OK | .
`dev/usb/if_aue.c` | C code | Original-BSD | OK | .
`dev/usb/if_auereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_axe.c` | C code | Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
**`dev/usb/if_axenreg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 257 non empty lines; 298 total lines | .
`dev/usb/if_axereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_cdce.c` | C code | Original-BSD | OK | .
`dev/usb/if_cdcereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_cue.c` | C code | Original-BSD | OK | .
`dev/usb/if_cuereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_kue.c` | C code | Original-BSD | OK | .
`dev/usb/if_kuereg.h` | C header | Original-BSD | OK | .
`dev/usb/if_mos.c` | C code | Original-ISC ("and" is used instead of "and/or") + Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
`dev/usb/if_mosreg.h` | C header | Original-ISC ("and" is used instead of "and/or") + Original-BSD | OK (Combined Licenses) | .
**`dev/usb/uftdireg.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 66 code lines + 250 commented lines; 341 total lines | .
`dev/videomode/devlist2h.awk` | AWK script | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/videomode/modelines`** | Modeline config. | __Unlicensed!!!__ | Non-Trivial [False-Positive: this is a part of XFree86 "vesamodes" and "extramodes"; XFree86-1.0 or Non-Copyrightable] | .
**`dev/videomode/vesagtf.c`** | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Modified-BSD + __Nonfree-License!!! (VESA license)__ | __Non-Free!!!__ (Combined Licenses) | .
**`dev/videomode/videomode.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [False-Positive: this file is generated by "dev/videomode/modelines2c.awk" with "dev/videomode/modelines"; Custom-Modified-BSD (third clause is a "non-endorsement clause") + XFree86-1.0] | .
**`dev/wscons/unicode.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 16 code line + 7 commented lines; 26 total lines | .
`dev/wscons/wscons_callbacks.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsconsio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsdisplay.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`dev/wscons/wsdisplay_usl_io.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 52 code lines + 2 commented lines; 71 total lines | .
`dev/wscons/wsdisplayvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemulconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemul_dumb.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsemul_sun.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/wscons/wsemulvar.h` | C header | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`dev/wscons/wsevent.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wseventvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wskbd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK (Combined Licenses) | .
`dev/wscons/wskbdvar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsmouse.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`dev/wscons/wsmoused.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`dev/wscons/wsmousevar.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Original-ISC ("and" is used instead of "and/or") | OK (Combined Licenses) | .
`kern/exec_conf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/exec_elf.c` | C code | Custom-Modified-BSD (third clause is a "non-endorsement clause") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`kern/exec_script.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/exec_subr.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`kern/init_sysent.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
`kern/kern_exec.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`kern/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 38 code lines + 10 commented lines; 59 total lines | .
`kern/makesyscalls.sh` | Shell script | Custom-Original-BSD (four clause is a "non-endorsement clause") | [this file is required to replace a completely new code file, because of "kern/syscalls.master", see "https://github.com/torvalds/linux/blob/master/arch/s390/kernel/syscalls/syscalltbl" as example] | .
`kern/subr_autoconf.c` | C code | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`kern/syscalls.c`** | C code (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
**`kern/syscalls.master`** | BSD kernel config. | __Unlicensed!!!__ | Non-Trivial; 561 non empty lines; 568 total lines | .
`kern/sysv_ipc.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`kern/sysv_shm.c` | C code | Original-ISC ("and" is used instead of "and/or") + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`kern/uipc_mbuf.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`lib/libkern/arch/alpha/ffs.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`lib/libkern/arch/alpha/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 35 code lines + 2 commented lines; 48 total lines | .
**`lib/libkern/arch/amd64/bcmp.S`** | Assembler code | __Unlicensed!!!__ | Non-Trivial; 20 code lines; 22 total lines | .
`lib/libkern/arch/amd64/htonl.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/amd64/htons.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/amd64/memmove.S` | Assembler code | Original-BSD | OK | .
**`lib/libkern/arch/arm/divsi3.S`** | Assembler code | __Nonfree-License!!!__ (BSD disclaimer only) | __Nonfree!!!__ | .
`lib/libkern/arch/arm/memset.S` | Assembler code | Original-BSD | OK | .
`lib/libkern/arch/i386/scanc.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libkern/arch/i386/skpc.S` | Assembler code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`lib/libkern/milieu.h`** | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat.c` | C code | __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat.h` | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat-macros.h` | C header | __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/softfloat-specialize.h` | C header | Simplified-BSD + __SoftFloat-2!!!__ | __NonFree!!!__; SoftFloat-2 license contains restrictions on legal use [replace this file to SoftFloat-3 code] | .
`lib/libkern/strncasecmp.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/arp.c` | C code | Original-BSD | OK | .
`lib/libsa/blowfish.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/blowfish.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/bootparam.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the advertising one) | OK | .
`lib/libsa/bootp.c` | C code | Original-BSD | OK | .
`lib/libsa/cd9660.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/cd9660.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/ether.c` | C code | Original-BSD | OK | .
`lib/libsa/exit.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`lib/libsa/getchar.c` | C code | Original-BSD | OK | .
**`lib/libsa/globals.c`** | C code | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 8 commented lines; 34 total lines [contains structure declarations, variables definitions, array definitions and included headers] | .
`lib/libsa/iodesc.h` | C header | Original-BSD | OK | .
**`lib/libsa/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 33 code lines + 4 commented lines; 44 total lines | .
**`lib/libsa/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 36 code lines + 13 commented lines; 68 total lines | .
`lib/libsa/net.c` | C code | Original-BSD | OK | .
`lib/libsa/net.h` | C header | Original-BSD | OK | .
`lib/libsa/netif.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`lib/libsa/netif.h`** | C header | __Unlicensed!!!__ | Non-Trivial [contains 10 function declarations, 1 structure declaration and 2 variable declarations from "lib/libsa/netif.c"; contains 1 included header and "4 structure definitions"] | .
`lib/libsa/netudp.c` | C code | Original-BSD | OK | .
`lib/libsa/putchar.c` | C code | Original-BSD | OK | .
`lib/libsa/rarp.c` | C code | Original-BSD | OK | .
`lib/libsa/rpc.c` | C code | Original-BSD | OK | .
`lib/libsa/rpc.h` | C header | Original-BSD | OK | .
**`lib/libz/Makefile.inc`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 48 code lines + 16 commented lines; 74 total lines [use zlib Makefile as replacement! (https://github.com/madler/zlib)] | .
**`lib/libz/Makefile`** | Makefile | __Unlicensed!!!__ | Non-Trivial; 19 code lines + 6 commented lines; 35 total lines [use zlib Makefile as replacement! (https://github.com/madler/zlib)] | .
`msdosfs/denode.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/direntry.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/fat.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_conv.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_denode.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_fat.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_lookup.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfsmount.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_vfsops.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`msdosfs/msdosfs_vnops.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + Custom-Free-License (Paul-Popelka) | OK (Combined Licenses) | .
`net/if_loop.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`net/if_media.c` | C code | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`net/if_media.h` | C header | Simplified-BSD + Custom-Original-BSD (four clause is a "non-endorsement clause") | OK (Combined Licenses) | .
`net/if_ppp.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Modified-BSD | OK (Combined Licenses) | .
`net/if_ppp.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/if_pppvar.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK (Combined Licenses) | .
`netinet6/in6_pcb.c` | C code | Modified-BSD + Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`netinet/in_pcb.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/in_proto.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/ip_icmp.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
**`netinet/ip_mroute.h`** | C header | __Nonfree-License!!!__ (David Waitzman; copyright notice only) | __Nonfree!!!__ | .
`netinet/raw_ip.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_debug.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_input.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_output.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_subr.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/tcp_usrreq.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`netinet/udp_usrreq.c` | C code | Modified-BSD + Original-BSD | OK (Combined Licenses) | .
`net/pfkeyv2.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/pfkeyv2_convert.c` | C code | ( Custom-Free-License (Angelos D. Keromytis) or GPL (any version; impossible in this file, because Original-BSD) ) + Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/pfkeyv2.h` | C header | Original-BSD | OK | .
`net/pfkeyv2_parsemessage.c` | C code | Original-BSD + Modified-BSD | OK (Combined Licenses) | .
`net/ppp-comp.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp-deflate.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp_defs.h` | C header | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") | OK | .
`net/ppp_tty.c` | C code | Custom-Original-BSD (third clause is a "non-endorsement clause" and four clause is the "advertising clause") + Prior-BSD (similar to Modified-BSD but with the source and binary clauses merged and the "non-endorsement clause" included) | OK | .
`net/route.c` | C code | Modified-BSD + Modified-BSD + Original-BSD | OK (Combined Licenses) | .
**`nfs/krpc.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 20 code lines + 4 commented lines; 30 total lines | .
`nfs/krpc_subr.c` | C code | Original-BSD | OK | .
`nfs/nfs_var.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`scsi/cd.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/ch.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`scsi/scsi_base.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`scsi/scsi_changer.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/scsiconf.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/scsiconf.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
**`scsi/scsi_debug.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 51 code lines + 14 commented lines; 75 total lines | .
`scsi/scsi_ioctl.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`scsi/scsi_message.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 50 code lines + 5 commented lines; 68 total lines | .
`scsi/scsi_tape.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/st.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") + TRW-Financial-Systems (similar to Expat) | OK (Combined Licenses) | .
`scsi/uk.c` | C code | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`sys/ataio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 34 code lines + 4 commented lines; 49 total lines | .
`sys/audioio.h` | C header | Original-BSD | OK | .
**`sys/cdio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 371 non empty lines; 435 total lines | .
`sys/chio.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/core.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/device.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/disk.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
`sys/exec_script.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
`sys/kcore.h` | C header | Custom-Original-BSD (four clause is a "non-endorsement clause") | OK | .
**`sys/scsiio.h`** | C header | __Unlicensed!!!__ | Non-Trivial; 55 code lines + 4 commented lines; 75 total lines | .
**`sys/sem.h`** | C header | __Nonfree-License!!!__ (Daniel Boulet; copyright notice only) | __NonFree!!!__ | .
`sys/shm.h` | C header | Custom-Original-BSD (Modified-BSD with the advertising clause above and separated from other clauses) | OK | .
**`sys/syscallargs.h`** | C header (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
**`sys/syscall.h`** | C header (generated) | __Unlicensed!!!__ | Non-Trivial [this file is generated by "kern/makesyscalls.sh" with "kern/syscalls.master" and "kern/syscalls.conf" to generate syscall files, but the "kern/syscalls.master" is "Unlicensed!!!" too] | .
`ufs/ext2fs/ext2fs_bswap.c` | C code | Original-BSD | OK | .
**`ufs/ufs/ufs_quota_stub.c`** | C code | __Unlicensed!!!__ | Non-Trivial: it contains 1 macro condiction, 10 function definitions and comment; 56 code lines + 1 commented line; 57 total lines | .
**`uvm/uvmexp.h`** | C header | __Unlicensed!!!__ | Non-Trivial: it contains 2 macro condictions, 13 macro integer variables, 1 macro structure variable, 3 structure definitions, 1 structure declaration and comments; 149 code lines + 29 commented lines; 178 total lines | .
`uvm/uvm_mmap.c` | C code | Original-BSD | OK | .
**`uvm/uvm_swap_encrypt.c`** | C code | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 variable declaration, 5 variable definitions, 7 function definitions and comments; 138 code lines + 60 commented lines; 198 total lines | .
**`uvm/uvm_swap_encrypt.h`** | C header | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 macro condiction, 6 macro integer variables, 1 macro structure variable, 2 macro functions, 1 structure definition, 7 function declarations, 4 variable declarations and comments; 42 code lines + 30 commented lines; 72 total lines | .
**`uvm/uvm_vnode.h`** | C header | __Custom-Original-BSD__ (four clause is the author not endorsement only) | Non-Trivial: it contains 1 macro condiction, 1 structure definition, 1 structure declaration, 2 macro function calls, 12 macro integer variables and comments; 25 code lines + 59 commented lines; 84 total lines | .
---

## openbsd_kernel-file-list-with-license-issues.md - License issues file list for Todo list

Written in 2022 by **[M&aacute;rcio Silva][COADDE]** <coadde@hyperbola.info>
<br/>Written in 2022 by **[Andr&eacute; Silva][EMULATORMAN]** <emulatorman@hyperbola.info>
<br/>Written in 2023 by **[rachad][RACHAD]** <rachad@hyperbola.info>

To the extent possible under law, the author(s) have dedicated all copyright
<br/>and related and neighboring rights to this software to the public domain
<br/>worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along
<br/>with this software. If not, see
<br/><https://creativecommons.org/publicdomain/zero/1.0/>.


[COADDE]: https://www.hyperbola.info/members/founders/#coadde
[EMULATORMAN]: https://www.hyperbola.info/members/founders/#Emulatorman
[RACHAD]: https://www.hyperbola.info/members/developers/#rachad
