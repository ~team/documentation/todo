# List of packages not to be included in Hyperbola 0.4

Further explanations: All the following packages have **unwanted dependencies** or are not longer updated.
Definition of **unwanted dependencies** to follow:

- ConsoleKit
- PolicyKit
- PAM
- D-Bus
- systemd / elogind
- PulseAudio
- Avahi

<br/><br/>

| Name | Description | No unwanted requirements? | Can be included? | Upstream Bug Reports? | Annotations | Responsible | Tracking Issue |
--- | --- | :---: | :---: | :---: | --- | :---: | :---:
| ~~wmii~~ | dynamic wm | N | N | NA | Abandonware |   |   |
| ~~matchbox wm~~ | stacking wm | N | N | NA | Requires Git to use it; no tarball (http://git.yoctoproject.org/cgit/cgit.cgi/matchbox-window-manager-2/) |   |   |
| ~~afterstep~~ | stacking wm | N | N | NA | Abandonware; the fork requires Git to use lastest changes (https://github.com/sashavasko/afterstep-devel) |   |   |
| ~~wmfs~~ | tiling wm | N | N | NA | Requires Git to use it; old tarball |   |   |
| ~~gtkgreet~~ | login manager | N | N | NA | Requires PAM, needs Rust to compile |   |   |
| ~~lightdm~~ | login manager | N | N | NA | Requires D-Bus |   |   |
| ~~sddm~~ | login manager | N | N | NA | Requires D-Bus |   |   |
| ~~slim~~ | login manager | N | N | NA | Requires D-Bus |   |   |
| ~~wdm~~ | login manager | N | N | NA | Abandonware |   |   |
| ~~qingy~~ | login manager | N | N | NA | Requires DirectFB, it is Abandonware |   |   |
| ~~sugar~~ | desktop environment | N | N | NA | Requires D-Bus-Python |   |
| ~~enlightenment (e2x)~~ | desktop environment | N | N | NA | Requires D-Bus |   |   |
| ~~deepin~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~xfce~~ | desktop environment | N | N | NA | Requires ConsoleKit/logind and D-Bus |   |
| ~~lxde~~ | desktop environment | N | N | NA | Requires D-Bus-Glib |   |
| ~~lxqt (fork of lxde and razor)~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~tde (fork of kde3)~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~kde-plasma~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~mate-desktop)~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~cinnamon~~ | desktop environment | N | N | NA | Requires D-Bus |   |
| ~~budgie~~ | desktop environment | N | N | NA | Requires Gnome |   |
| ~~gnome~~ | desktop environment | N | N | NA | Requires everything that is wrong with the world |   |
| ~~draco~~ | desktop environment | N | N | https://github.com/rodlie/draco/issues/50 | Requires ConsoleKit / logind and D-Bus; the recommendation stays therefore at Lumina from the author of Draco; see upstream bug report |   |
| ~~mate-system-monitor~~ | system monitor (graphical) | N | N | NA | Way too much dependencies and bloat |   |   |
| ~~pcmanfm (legacy)~~ | file-manager | N | N | NA | Optional HAL with D-Bus-Glib dependency can be filipped off, but elementary libraries for lxde also needed |   |   |
| ~~pcmanfm (lxde)~~ | file-manager | N | N | NA | Elementary libraries for lxde also needed |   |   |
| ~~pcmanfm (lxqt)~~ | file-manager | N | N | NA | Requires D-Bus |   |   |
| ~~thunar~~ | file-manager | N | N | NA | Optional GIO dependency can be filipped off, but why integrating half-way Xfce-environment? |   |   |
| ~~doublecmd-qt / doublecmd-gtk~~ | file-manager | ? | ? | NA | Requires desktop-file-utils & shared-mime-info and also unclear state of Lazarus |   |   |
| ~~engrampa~~ | archive manager | N | N | NA | Needs Caja, which itself is part for mate-desktop and therefore integration of D-Bus. |   |   |
| ~~eom~~ | image viewer | N | N | NA | Requires mate-desktop and therefore D-Bus |   |   |
| ~~mate-screenshot~~ | utilities (screenshot) | N | N | NA | Package is mate-utils, which itself has dependencies for mate-panel and therefore requires D-Bus  |   |   |
| ~~cairo-dock~~ | taskbar | N | N | NA | Requires D-Bus (look at: https://github.com/Cairo-Dock/cairo-dock-core/blob/master/CMakeLists.txt) |   |   |
| ~~plank~~ | taskbar | N | N | NA | Requires D-Bus |   |   |
| ~~pasystray~~ | system tray | N | N | NA | Requires PulseAudio |   |   |
| ~~mate-power-manager~~ | power manager | N | N | NA | Requires D-Bus |   |   |
| ~~xfce-power-manager~~ | power manager | N | N | NA | Requires D-Bus |   |   |
| ~~darktable~~ | post-production tool for photography | N | N | NA | Requires D-Bus-Glib |   |   |
| ~~rawstudio~~ | torrent client | N | N | NA | Requires D-Bus |   |   |
| ~~ufraw~~ | post-production tool for photography | N | N | NA | Abandonware |   |   |
| ~~glimpse (fork of gimp 2.10)~~ | raster graphics editor | N | N | NA | Fork of GIMP 2.10.x, it requires D-Bus/GDBus-codegen |   |   |
| ~~cinepaint (fork of gimp 1.0.4)~~ | raster graphics editor | N | N | NA | Abandonware; Cinepaint Oyranos v0.25.2 is the best version; and better than LazPaint |   |   |
| ~~gimphoto (fork of gimp 2.4.3)~~ | raster graphics editor | N | N | NA | Abandonware |   |   |
| ~~gimpshop (fork of gimp 2.2.11)~~ | raster graphics editor | N | N | NA | Abandonware |   |   |
| ~~tupitube~~ | raster animation editor (for kids) | N | N | NA | Requires Git to use the source code; no version tarball or script? |   |   |
| ~~kdenlive~~ | post-production tools for video | N | N | NA | Requires D-Bus |   |   |
| ~~openshot~~ | post-production tools for video | N | N | NA | QT version; Requires D-Bus |   |   |
| ~~flowblade~~ | post-production tools for video | N | N | NA | Requires D-Bus-Glib and D-Bus-Python |   |   |
| ~~pluma~~ | text editor | N | N | NA | Is not an independent package and integral parts from Mate have to be used |   |   |
| ~~imagemagick~~ | graphics library | N | N | NA | Licensing |   |   |
| ~~sweethome3d~~ | 3d environment | N | N | NA | Requires Java |   |   |
| ~~mps-youtube~~ | terminal based youtube jukebox | N | N | NA | Only compatible with YouTube |   |   |
| ~~mate-calc~~ | accessories, calculator | N | N | NA | Part of Mate and alternatives will be used |   |   |
| ~~mate-icon-theme~~ | themes | N | N | NA | Part of Mate and alternatives will be used |   |   |
| ~~mate-themes~~ | themes | N | N | NA | Part of Mate and alternatives will be used |   |   |
| ~~kompare~~ | system tools | N | N | NA | Needs complete integration of KDE-framework, including D-Bus |   |   |
| ~~brasero~~ | CD/DVD mastering tool | N | N | NA | Too many dependencies, including libcanberra as mandatory |   |   |
| ~~xfburn~~ | CD/DVD mastering tool | N | N | NA | Too many dependencies, including libxfce4 as mandatory |   |   |
| ~~jami~~ | distributed communication platform | N | N | NA | libcanberra is mandatory and we want to keep exactly those components bloated excluded, look therefore at cmake-options for the client |   |   |
| ~~pitivi~~ | post-production tools for video | N | N | NA | Issues with licensing of needed components |   |   |
| ~~lumiera~~ | post-production tools for video | N | N | NA | No stable version for download yet, colliding with packaging-guidelines |   |   |
| ~~evince~~ | ebook viewer | N | N | NA | Gnome won't be included, so therefore also applications being directly depending onto that |   |   |
| ~~atril~~ | ebook viewer | N | N | NA | Mate won't be included, so therefore also applications being directly depending onto that |   |   |
| ~~calligra~~ | office suite | N | N | NA | Requires D-Bus |   |   |
| ~~digikam~~ | digital photo management | N | N | NA | KDE-libraries, optional dependency towards darktable |   |   |
| ~~kolourpaint~~ | raster graphics editor | N | N | NA | KDE-libraries |   |   |
| ~~zathura, zathura-cb, zathura, zathura-pdf-mupdf, zathura-pdf-poppler, zathura-ps~~ | ebook viewer | N | N | NA | Keep it small, simple and not too many dependencies |   |   |
| ~~lazpaint~~ | raster graphics editor | N | N | NA | Free-Pascal based and this won't be part of Hyperbola |   |   |
| ~~epiphany~~ | webkit web browser | N | N | NA | Depends on libsecret as mandatory, therefore also dbus |   |   |
| ~~selektor~~ | Tor launcher and exit node chooser | N | N | NA | Requires Java |   |   |
| ~~wicd~~ | network managing | N | N | NA | Requires D-Bus |   |   |
| ~~syncthing~~ | continuous replication | N | N | NA | Requires newest go-version, being more incompatible with i386, requires git for compilation |   |   |
| ~~syncthing-gtk~~ | GTK-client for syncthing | N | N | NA | Only to use with syncthing |   |   |

<br/>

---

**[List of packages for Hyperbola 0.4][SOURCE]**
 © 2021 by **[Tobias Dausend][THROUGH]** is licensed under
 **[Creative Commons&reg; Attribution-ShareAlike 4.0 International][LICENSE]**


[SOURCE]: https://git.hyperbola.info:50100/~team/documentation/todo.git/tree/todo-list_0.4.md
[LICENSE]: https://creativecommons.org/licenses/by-sa/4.0/

[THROUGH]: https://www.hyperbola.info/members/developers/#throgh
